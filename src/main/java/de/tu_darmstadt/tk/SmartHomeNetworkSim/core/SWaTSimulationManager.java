package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Calendar;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;

/**
 * Manages the physical and network traffic simulation of the SWat scenario.
 * @author Fabian Kaiser
 */
public class SWaTSimulationManager {	
	/**
	 * The controller of the scenario.
	 */
	private Controller controller;
	
	/**
	 * Variable needed as Observable.
	 */
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);	
	
	/**
	 * The constructor.
	 * @param controller The controller of the scenario.
	 */
	public SWaTSimulationManager(Controller controller) {
		this.controller = controller;
	}
	
	/**
	 * Starts the physical simulation and after its done uses its results to start the network simulation.
	 * @param duration  The duration of the simulation in seconds.
	 * @param startTime The start time of the simulation.
	 * @param outputDirectoryPath The path of the directory for the output files.
	 * @param startStatus The status at which the simulation is to be started. Has the format of a line of the network traffic data.
	 * @param passByReferenceValue_KeepSimulationRunning The PropertyChangeListener to observe this class.
	 * @param propertyChangeListener The PropertyChangeListener to observe this class.
	 * @param timeOffset The time offset between the occurrence of a sensor reading in the physical data and the time it is used in network packets.
	 */
	public void startSimulations(String duration, Calendar startTime, String outputDirectoryPath, boolean[] passByReferenceValue_KeepSimulationRunning, PropertyChangeListener propertyChangeListener, String physicalStartStatus, int timeOffset) {
		this.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
		
		SWaTSimulationManagerPhysical managerPhysical = new SWaTSimulationManagerPhysical(controller);
		managerPhysical.startSimulation(duration, startTime, outputDirectoryPath, physicalStartStatus, passByReferenceValue_KeepSimulationRunning, propertyChangeListener, SWaTUtilities.loadAttacksFromUserDataFile(controller.getNetworkController()));
				
		SWaTSimulationManagerNetwork managerNetwork = new SWaTSimulationManagerNetwork(controller);
		managerNetwork.startSimulation(duration, startTime, outputDirectoryPath, passByReferenceValue_KeepSimulationRunning, propertyChangeListener, managerPhysical.getSimulationResults(), timeOffset);
		
		propertyChangeSupport.firePropertyChange("simulationFinished", false, true);
	}
	
	/**
	 * Starts only the physical simulation.
	 * @param duration  The duration of the simulation in seconds.
	 * @param startTime The start time of the simulation.
	 * @param outputDirectoryPath The path of the directory for the output files.
	 * @param startStatus The status at which the simulation is to be started. Has the format of a line of the network traffic data.
	 * @param passByReferenceValue_KeepSimulationRunning The PropertyChangeListener to observe this class.
	 * @param propertyChangeListener The PropertyChangeListener to observe this class.
	 */
	public void startPhysicalSimulation(String duration, Calendar startTime, String outputDirectoryPath, boolean[] passByReferenceValue_KeepSimulationRunning, PropertyChangeListener propertyChangeListener, String physicalStartStatus) {
		this.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
		
		SWaTSimulationManagerPhysical managerPhysical = new SWaTSimulationManagerPhysical(controller);
		managerPhysical.startSimulation(duration, startTime, outputDirectoryPath, physicalStartStatus, passByReferenceValue_KeepSimulationRunning, propertyChangeListener, SWaTUtilities.loadAttacksFromUserDataFile(controller.getNetworkController()));
		
		propertyChangeSupport.firePropertyChange("simulationFinished", false, true);
	}

}
