package de.tu_darmstadt.tk.SmartHomeNetworkSim;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.MainFrame;

/**
 * Main class which initializes and connects the different parts and starts the program
 *
 * @author Andreas T. Meyer-Berg, Fabian Kaiser (adjustments for SWaT scenario)
 */
public class Main {
	/**
	 * Model of the smart home scenario.
	 */
	static Model modelSmartHome;
	/**
	 * Model of the SWaT scenario.
	 */
	static Model modelSWaT;	
	
	/**
	 * Visualization of the smart home network
	 */
	static MainFrame view;
	
	/**
	 * Controller of the smart home scenario.
	 */
	static Controller controllerSmartHome;
	
	/**
	 * Controller of the SWaT senario.
	 */
	static Controller controllerSWaT;	
	
	/**
	 * Starts the program
	 * @param args will be ignored
	 */
	public static void main(String[] args) {
		modelSmartHome = new Model();
		modelSWaT = new Model();
		controllerSmartHome = new Controller(modelSmartHome);
		controllerSWaT = new Controller(modelSWaT);
	    view = new MainFrame(controllerSmartHome, controllerSWaT);
	}
}
