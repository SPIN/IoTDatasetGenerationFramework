/**
 * Core classes of the Model and Simulation.
 *
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;