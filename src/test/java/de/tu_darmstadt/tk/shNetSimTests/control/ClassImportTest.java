package de.tu_darmstadt.tk.shNetSimTests.control;
import java.io.File;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.ClassImportException;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.ImportController;

/**
 * Tests Import Functionalities of the 
 * 
 *
 * @author Andreas T. Meyer-Berg
 */
public class ClassImportTest {

	String pathToRessources = "src/test/resources/control/testCompilation/";
	File compileTest = new File(pathToRessources + "project1/MQTT_protocolProject1.javaTest");
	File compileTestJava = new File(pathToRessources + "project1/MQTT_protocolProject1.java");
	File compileInvalid = new File(pathToRessources + "project1/MQTT_invalidPackage.javaTest");
	File compileInvalidJava = new File(pathToRessources + "project1/MQTT_invalidPackage.java");
	File compilePackage = new File(pathToRessources + "packageTest/MQTT_protocolPackageTest.javaTest");
	File compilePackageJava = new File(pathToRessources + "packageTest/MQTT_protocolPackageTest.java");
	File compileDeepPackage = new File(pathToRessources + "packageTest/deepPackage/reallyDeepPackage/MQTT_protocolDeepPackageTest.javaTest");
	File compileDeepPackageJava = new File(pathToRessources + "packageTest/deepPackage/reallyDeepPackage/MQTT_protocolDeepPackageTest.java");
	
	
	@Before
	public void renameJavaFilesBefore(){
		compileTest.renameTo(compileTestJava);
		compileInvalid.renameTo(compileInvalidJava);
		compilePackage.renameTo(compilePackageJava);
		compileDeepPackage.renameTo(compileDeepPackageJava);
	}
	
	
    @Test
    public void javaPackageName() {
        Assert.assertEquals("packageTest.deepPackage.reallyDeepPackage", ImportController.getJavaPackageName(compileDeepPackageJava));
    }
    
    @Test
    public void javaDefaultPackage(){
    	Assert.assertEquals("", ImportController.getJavaPackageName(compileTestJava));
    }
    
    @Test
    public void javaPackageNameError(){
    	//File does not exist
    	Assert.assertNull(ImportController.getJavaPackageName(new File(pathToRessources + "control/testCompilation/invalid.java")));
    }
    
    @Test
    public void compileFile(){
    	Class<?> test = null;
    	try {
			test = ImportController.importJavaClass(compileTestJava);
			throw new ClassImportException("EclEmmaCoverage");
		} catch (ClassImportException e) {
		}
    	Assert.assertNotNull(test);
    }
    
    @Test
    public void compileFilePackage(){
    	Class<?> test = null;
    	try {
			test = ImportController.importJavaClass(compilePackageJava);
			throw new ClassImportException("EclEmmaCoverage");
		} catch (ClassImportException e) {
		}
    	Assert.assertNotNull(test);
    }
    
    @Test
    public void compileFileDeepPackage(){
    	Class<?> test = null;
    	try {
			test = ImportController.importJavaClass(compileDeepPackageJava);
			throw new ClassImportException("EclEmmaCoverage");
		} catch (ClassImportException e) {
		}
    	Assert.assertNotNull(test);
    }
    
    @Test(expected = ClassImportException.class) 
    public void compileInvalidPackage() throws ClassImportException{
    	ImportController.importJavaClass(compileInvalidJava);			
    }
    
    @Test
    public void accessFile(){
    	Assert.assertTrue(compileTestJava.exists());
    }

    @After
    public void renameJavaFilesAfter(){
    	compileTestJava.renameTo(compileTest);
    	compileInvalidJava.renameTo(compileInvalid);
    	compilePackageJava.renameTo(compilePackage);
    	compileDeepPackageJava.renameTo(compileDeepPackage);
    }
}
