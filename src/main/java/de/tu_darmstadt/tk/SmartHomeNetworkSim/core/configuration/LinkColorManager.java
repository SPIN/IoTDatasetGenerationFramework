package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;

import java.awt.Color;
import java.util.HashMap;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.Pair;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Container for the different Link colors
 *
 * @author Andreas T. Meyer-Berg
 */
public class LinkColorManager {

	/**
	 * Map which stores the Colors and circle positions for all links
	 */
	private HashMap<Link, Pair<Integer,Color>> linkColors = null;
	/**
	 * Integer which contains the next color index, which should be used
	 */
	private int i = 0;
	/**
	 * Initialize the color manager
	 */
	public LinkColorManager() {
		linkColors = new HashMap<Link, Pair<Integer,Color>>();
	}
	
	/**
	 * Returns the color and index of the given Link
	 * @param link link which should be looked up
	 * @return Index and Color
	 */
	public Pair<Integer,Color> getColorOfLink(Link link){
		return linkColors.get(link);
	}
	
	/**
	 * Removes the given Link and its color
	 * @param link link to be removed
	 */
	public void removeLink(Link link){
		linkColors.remove(link);
	}

	/**
	 * Adds a link with the given Color
	 * @param link link to be added
	 * @param pair Index and Color of the link
	 */
	public void addLinkColor(Link link, Pair<Integer, Color> pair) {
		linkColors.put(link, pair);
	}
	
	/**
	 * Returns the color of the next Link
	 * @return next color
	 */
	public Color getNextLinkColor(){
		/**
		 * Distinct Color as String containing the hex values (128 static ones should be enough)
		 */
		String colorHex = Utility.indexcolors[i++%Utility.indexcolors.length];
		/**
		 * Color of the given Hex String (Base16)
		 */
		Color color =  new Color(
				Integer.valueOf(colorHex.substring(1,3),16),
	            Integer.valueOf(colorHex.substring(3,5),16),
	            Integer.valueOf(colorHex.substring(5,7),16));
		return color;
	}
}
