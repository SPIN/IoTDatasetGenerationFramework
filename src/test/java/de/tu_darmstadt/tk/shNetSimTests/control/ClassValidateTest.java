package de.tu_darmstadt.tk.shNetSimTests.control;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.ImportController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Protocol;

public class ClassValidateTest {

	Controller controller;
	ImportController importController;
	
	@Before
	public void init() {
		controller = new Controller(new Model());
		importController = controller.getImportController();
	}
	
	@Test
	public void validProtocol(){
		Class<? extends Protocol> protocol = ValidProtocol.class;
		Assert.assertFalse(importController.getProtocols().contains(protocol));
		Assert.assertTrue(importController.isValidProtocol(protocol));
		importController.addProtocol(protocol);
		Assert.assertTrue(importController.getProtocols().contains(protocol));
		importController.removeProtocol(protocol);
		Assert.assertFalse(importController.getProtocols().contains(protocol));
	}
}
