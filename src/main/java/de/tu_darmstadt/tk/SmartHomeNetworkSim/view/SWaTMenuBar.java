package de.tu_darmstadt.tk.SmartHomeNetworkSim.view;

import java.awt.FlowLayout;
import java.awt.Font;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.menuBar.SWaTMenuBarStages;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups.SWaTAttacksWindow;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups.SWaTElementConfigurationWindow;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups.SWaTImportPhysicalDataWindow;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups.SWaTSimulationWindow;

/**
 * Menu bar for the SWaT scenario. Based on {@link MenuBar}.
 * @author Fabian Kaiser
 *
 */
public class SWaTMenuBar extends JMenuBar {

	/**
	 * The controller.
	 */
	private Controller controller;

	/**
	 * The network controler.
	 */
	private NetworkController networkController;
	
	/**
	 * JMenu for scenarios.
	 */
	private JMenu mnScenarios;
	
	/**
	 * JMenu for help with the SWaT scenario.
	 */ 
	private JMenu mnHelpSWaT;
	
	/**
	 * JMenu for selection the SWaT stages.
	 */ 
	private JMenu mnStagesSWaT;
	
	/**
	 * JMenu for attacks.
	 */ 
	private JMenu mnAttacksSWaT;	
	
	/**
	 * JMenu for file.
	 */
	private JMenu mnfileSWaT;
	
	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 4293499386792032117L;

	/**
	 * Array with the selected stages.
	 */
	private boolean customization[] = {false, false, false, false, false, false, false, false, false};
	
	/**
	 * Menu item for the simulation configuration.
	 */
	private JMenuItem controlSimulation;
	
	/**
	 * Menu item for the attack configuration.
	 */
	private JMenuItem controlAttacks;
	
	/**
	 * Menu item for the connection configuration.
	 */
	private JMenuItem controlConfiguration;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizeSCADA;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizeEngineeringWorkstation;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizeHistorian;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC1;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC2;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC3;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC4;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC5;
	
	/**
	 * Checkbox for stage select.
	 */
	private JCheckBox customizePLC6;
	
	/**
	 * A list of the checkboxes.
	 */
	private LinkedList<JCheckBox> customizeCheckboxes = new LinkedList<JCheckBox>();

	/**
	 * Initialize the Menu Bar, add all the items and add the different actions
	 * 
	 * @param controller
	 *            Controller
	 */
	public SWaTMenuBar(Controller controller) {
		this.controller = controller;
		this.networkController = controller.getNetworkController();
		
		this.setLayout(new FlowLayout(FlowLayout.LEADING));
		
		// Fill list
		initializeStagesMenuSWaT();
		customizeCheckboxes.add(customizeSCADA);
		customizeCheckboxes.add(customizeEngineeringWorkstation);
		customizeCheckboxes.add(customizeHistorian);
		customizeCheckboxes.add(customizePLC1);
		customizeCheckboxes.add(customizePLC2);
		customizeCheckboxes.add(customizePLC3);
		customizeCheckboxes.add(customizePLC4);
		customizeCheckboxes.add(customizePLC5);
		customizeCheckboxes.add(customizePLC6);

		// Initialize all components
		initializeHelpMenuSWaT();
		initializeScenarioMenu();		
		initializeControlMenu();
		initializeFileMenu();

		// Add all menu items for the SWaT scenario
		this.add(mnScenarios);
		this.add(mnStagesSWaT);
		this.add(mnAttacksSWaT);
		this.add(mnfileSWaT);
		this.add(mnHelpSWaT);
		
		// Delete user data from the last session.
		SWaTUtilities.deleteUserData();
	}

	/**
	 * Initializes the Help Menu of the SWaT scenario
	 */
	private void initializeHelpMenuSWaT() {
		mnHelpSWaT = new JMenu("Help");
		JMenu infoTextMenu = new JMenu("Tips");
		JTextArea infoText = new JTextArea();
		infoText.setFont(new Font("Roboto", Font.PLAIN, 16));
		infoText.append("A resolution of at least 1920*1080 pixels is recommended.\n");
		infoText.append("Nodes can be added and removed via the \"Stages\" menu.\n");
		infoText.append("Connections and switches are organized automatically.\n");
		infoText.append("A left click on a node opens the configuration window.");
		infoText.setEditable(false);
		infoTextMenu.add(infoText);
		mnHelpSWaT.add(infoTextMenu);
		
		JMenu infoAboutMenu = new JMenu("About");
		JTextArea infoAboutHeadline = new JTextArea();
		infoAboutHeadline.setFont(new Font("Roboto", Font.PLAIN, 20));
		infoAboutHeadline.append("IoT Dataset Generation Framework \u2013 Secure Water Treatment (SWaT)");
		JTextArea infoAbout = new JTextArea();
		infoAbout.setFont(new Font("Roboto", Font.PLAIN, 16));
		infoAbout.append("\nThis is a simulation of the Secure Water Treatment (SWaT) testbed operated by iTrust, the\nCentre for Research in Cyber Security at Singapore University of Technology and Design.");
		infoAbout.append("\n\nDeveloper: Fabian Kaiser (Master's Thesis at TU Darmstadt)");
		infoAbout.setEditable(false);
		infoAboutMenu.add(infoAboutHeadline);
		infoAboutMenu.add(infoAbout);
		mnHelpSWaT.add(infoAboutMenu);		
	}
	
	/**
	 * Initializes the scenario menu.
	 */
	private void initializeScenarioMenu() {
		mnScenarios = new JMenu("Scenarios");
		
		ButtonGroup scenarioGroup = new ButtonGroup();
		
		JMenuItem scenarioSmartHome = new JRadioButtonMenuItem ("Smart Home");
		JMenuItem scenarioSWaT = new JRadioButtonMenuItem ("Secure Water Treatment (SWaT)", true);
		
		scenarioSmartHome.addActionListener(a -> {
			((MainFrame) this.getParent().getParent().getParent()).switchScenarioGUI("SmartHome");
			scenarioSmartHome.setSelected(false);
			scenarioSWaT.setSelected(true);
		});
		mnScenarios.add(scenarioSmartHome);
		scenarioGroup.add(scenarioSmartHome);
		mnScenarios.add(scenarioSWaT);
		scenarioGroup.add(scenarioSWaT);	
	}			

	/**
	 * Initializes the stages menu of the SWaT scenario.
	 */
	private void initializeStagesMenuSWaT() {
		mnStagesSWaT = new JMenu("Stages");			
		
		JMenu stage2M = new JMenu("Selection");	
		customizeSCADA = new JCheckBox ("SCADA");
		customizeSCADA.addActionListener(a -> {
			customization[0] = !customization[0]; 
			selectionActionListener(customization);
			activateDeactivateControls();});		
		stage2M.add(customizeSCADA);
		customizeEngineeringWorkstation = new JCheckBox ("Engineering Workstation");
		customizeEngineeringWorkstation.addActionListener(a -> {
			customization[1] = !customization[1]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizeEngineeringWorkstation);	
		customizeHistorian = new JCheckBox ("Historian");
		customizeHistorian.addActionListener(a -> {
			customization[2] = !customization[2]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizeHistorian);
		customizePLC1 = new JCheckBox ("PLC 1");
		customizePLC1.addActionListener(a -> {
			customization[3] = !customization[3]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizePLC1);		
		customizePLC2 = new JCheckBox ("PLC 2");
		customizePLC2.addActionListener(a -> {
			customization[4] = !customization[4]; 
			selectionActionListener(customization);
			activateDeactivateControls();});			
		stage2M.add(customizePLC2);
		customizePLC3 = new JCheckBox ("PLC 3");
		customizePLC3.addActionListener(a -> {
			customization[5] = !customization[5]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizePLC3);			
		customizePLC4 = new JCheckBox ("PLC 4");
		customizePLC4.addActionListener(a -> {
			customization[6] = !customization[6]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizePLC4);
		customizePLC5 = new JCheckBox ("PLC 5");
		customizePLC5.addActionListener(a -> {
			customization[7] = !customization[7]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizePLC5);		
		customizePLC6 = new JCheckBox ("PLC 6");
		customizePLC6.addActionListener(a -> {
			customization[8] = !customization[8]; 
			selectionActionListener(customization);
			activateDeactivateControls();});	
		stage2M.add(customizePLC6);

				JMenuItem stage1M = new JMenuItem("All");	
		stage1M.addActionListener(a -> {
			this.controller.getNetworkController().deleteNetworkModel();
			SWaTMenuBarStages stages = new SWaTMenuBarStages(this.controller); stages.createAll();
			for(int i = 0; i <= 8; i++) {
				customization[i] = true;
				customizeCheckboxes.get(i).setSelected(true);
			}
			
			activateDeactivateControls();
		});	
		
		JMenuItem stage3M = new JMenuItem("None");
		stage3M.addActionListener(a -> {
			resetVisualization();
		});
		
		mnStagesSWaT.add(stage1M);		
		mnStagesSWaT.add(stage2M);		
		mnStagesSWaT.add(stage3M);		
	}
	
	/**
	 * Action listener behavior for stage selection.
	 * @param selection Boolean array with selection for [SCADA, EngineeringWorkstation, Historian,
	 * ServerSwitch, PLCSwitch, Stage 1, Stage 2, Stage 3,Stage 4, Stage 5, Stage 6]. Must have length 9. 
	 */
	private void selectionActionListener(boolean[] selection) {
		this.controller.getNetworkController().deleteNetworkModel();
		SWaTMenuBarStages stages = new SWaTMenuBarStages(this.controller); 
		stages.createSelection(selection);
	}
	
	/**
	 * Initializes the control menu of the SWaT scenario.
	 */	
	private void initializeControlMenu() {
		mnAttacksSWaT = new JMenu("Control");
		controlSimulation = new JMenuItem("Simulation");
		controlSimulation.addActionListener(a -> {
			SWaTSimulationWindow simulationWindow = new SWaTSimulationWindow(controller);
			simulationWindow.setLocationRelativeTo(this.getParent());
			simulationWindow.setVisible(true);
		});
		controlSimulation.setEnabled(false);
		mnAttacksSWaT.add(controlSimulation);
		
		controlAttacks = new JMenuItem("Attacks");
		controlAttacks.addActionListener(a -> {
			SWaTAttacksWindow attacksWindows = new SWaTAttacksWindow(controller);
			attacksWindows.setLocationRelativeTo(this.getParent());
			attacksWindows.setVisible(true);
		});
		controlAttacks.setEnabled(false);
		mnAttacksSWaT.add(controlAttacks);		
		
		controlConfiguration = new JMenuItem("Configuration");
		controlConfiguration.addActionListener(a -> {
			String deviceName = SWaTUtilities.getSortedDeviceNames(networkController).get(0);
			SmartDevice device = SWaTUtilities.getDeviceByName(deviceName, networkController);
			SWaTElementConfigurationWindow configurationWindow = new SWaTElementConfigurationWindow(controller, device, null);
			configurationWindow.setLocationRelativeTo(this.getParent().getParent().getParent()); // Relative to Main frame
			configurationWindow.setVisible(true);
		});
		controlConfiguration.setEnabled(false);
		mnAttacksSWaT.add(controlConfiguration);		
	}
	
	/**
	 * Initializes the file menu of the SWaT scenario.
	 */	
	private void initializeFileMenu() {
		mnfileSWaT = new JMenu("File");
		JMenuItem load = new JMenuItem("Load user data");
		load.addActionListener(a -> {
			JFileChooser loadFileChooser = new JFileChooser();
			loadFileChooser.setDialogTitle("User data file to load");
			loadFileChooser.setCurrentDirectory(new java.io.File("."));
			loadFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);	
			loadFileChooser.setAcceptAllFileFilterUsed(false);
			FileNameExtensionFilter filter = new FileNameExtensionFilter("SWaT user data file (*.SWaT)", "SWaT");
			loadFileChooser.setFileFilter(filter);
			if(loadFileChooser.showOpenDialog(((MainFrame) this.getParent().getParent().getParent()).getContentPane()) == JFileChooser.APPROVE_OPTION) {
				SWaTUtilities.loadUserData(loadFileChooser.getSelectedFile().getPath());
				loadVisualization();				
			}
		});
		
		mnfileSWaT.add(load);
		JMenuItem save = new JMenuItem("Save user data");
		save.addActionListener(a -> {
			JFileChooser saveFileChooser = new JFileChooser();
			saveFileChooser.setDialogTitle("File to save to");
			saveFileChooser.setCurrentDirectory(new java.io.File("."));
			saveFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);	
			saveFileChooser.setAcceptAllFileFilterUsed(false);
			FileNameExtensionFilter filter = new FileNameExtensionFilter("SWaT user data file (*.SWaT)", ".SWaT");
			saveFileChooser.setFileFilter(filter);
			String fileName = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss").format(new Date())+".SWaT";
			saveFileChooser.setSelectedFile(new File(fileName));			
			if(saveFileChooser.showSaveDialog(((MainFrame) this.getParent().getParent().getParent()).getContentPane()) == JFileChooser.APPROVE_OPTION) {
				SWaTUtilities.saveVisualizationToUserDataFile(customization);
				SWaTUtilities.saveUserData(saveFileChooser.getSelectedFile().getPath());
			}
		});
		mnfileSWaT.add(save);
		
		JMenuItem importPhysical = new JMenuItem("Import physical data");
		importPhysical.addActionListener(a -> {
			SWaTImportPhysicalDataWindow importPhysicalWindow = new SWaTImportPhysicalDataWindow(controller);
			importPhysicalWindow.setLocationRelativeTo(this.getParent());
			importPhysicalWindow.setVisible(true);
		});
		mnfileSWaT.add(importPhysical);
		
		JMenuItem reset = new JMenuItem("Reset");
		reset.addActionListener(a -> {
			SWaTUtilities.deleteUserData();
			resetVisualization();
		});
		mnfileSWaT.add(reset);	
	}
	
	/**
	 * Checks if the currently selected stages can be simulated.
	 * <br/>
	 * Can be simulated if:<br/>
	 * - At least 1 PLC<br/>
	 * - At least 2 of {SCADA, Engineering Workstation, Historian}<br/>
	 * - At least 1 of {SCADA, Engineering Workstation, Historian} and at least 1 PLC<br/>
	 * @return The result of the check.
	 */
	private boolean correctStages() {
		int counterSCADASystem = 0;
		int counterPLCs = 0;
		
		for(int i = 0; i < 9; i++) {
			if(customization[i] == true)
				if(i <= 2)
					counterSCADASystem++;
				else if(2 < i && i <= 8)
					counterPLCs++;
		}
		
		if(counterPLCs >= 1 || counterSCADASystem >= 2 || (counterPLCs >= 1 && counterSCADASystem >= 1))
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if there currently is at least one PLC selected.
	 * @return The result of the check.
	 */
	private boolean atLeastOnePLC() {
		int counterPLCs = 0;
		
		for(int i = 0; i < 9; i++) {
			if(customization[i] == true)
				if(2 < i && i <= 8)
					counterPLCs++;
		}
		
		if(counterPLCs >= 1)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if there currently is at least one stage selected.
	 * @return The result of the check.
	 */
	private boolean atLeastOneStage() {
		for(int i = 0; i < 9; i++) {
			if(customization[i] == true)
				return true;
		}
		return false;
	}
	
	/**
	 * Activates or deactivates the buttons of the control menu. 
	 */
	private void activateDeactivateControls() {
			controlSimulation.setEnabled(correctStages());
			controlAttacks.setEnabled(atLeastOnePLC());
			controlConfiguration.setEnabled(atLeastOneStage());
	}
	
	/**
	 * Resets the visualization.
	 */
	private void resetVisualization() {
		this.controller.getNetworkController().deleteNetworkModel();
		for(int i = 0; i <= 8; i++) {
			customization[i] = false;
			customizeCheckboxes.get(i).setSelected(false);
		}
		
		activateDeactivateControls();
	}

	/**
	 * Loads a visualization according to the customization array.
	 */
	private void loadVisualization() {
		boolean customizationToLoad[] = SWaTUtilities.loadVisualizationFromUserDataFile();
		if(customizationToLoad.length != 9) return;
		this.customization = customizationToLoad;
		
		int trueCounter = 0;
		for(int i = 0; i <= 8; i++) {
			if(customizationToLoad[i] != false)
				trueCounter++;
		}
		if(trueCounter == 0)
			resetVisualization();
		else {
			for(int i = 0; i <= 8; i++) {
				if(customizationToLoad[i])
					customizeCheckboxes.get(i).setSelected(true);
				else
					customizeCheckboxes.get(i).setSelected(false);
			}
			selectionActionListener(customizationToLoad);
			activateDeactivateControls();
		}
	}
}

