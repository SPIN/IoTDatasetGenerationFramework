package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.VisualizationPanel;

/**
 * Class for various helper methods.
 *
 * @author Andreas T. Meyer-Berg
 */
public class Utility {

	/**
	 * Multiple distinct colors for various purposes, like {@link VisualizationPanel}'s link coloring.
	 * This array is taken from <a href="https://stackoverflow.com/questions/2328339/how-to-generate-n-different-colors-for-any-natural-number-n">https://stackoverflow.com/questions/2328339/how-to-generate-n-different-colors-for-any-natural-number-n</a>
	 */
	public static final String[] indexcolors = new String[]{
        "#000000", "#7ED379", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
        "#5A0007", "#809693", "#012C58", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
        "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
        "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
        "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
        "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
        "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

        "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
        "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
        "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
        "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#FFFF00", "#FEFFE6"
	};
	
	/**
	 * Loads resource image from the given path (resources/path). Returns default
	 * image, if loading failed
	 * 
	 * @param path
	 *            path where the image is located
	 * @return Image from path, or default image if it failed
	 */
	public static BufferedImage loadFile(String path) {
		try {
			// Try loading Image from jar/resources
			InputStream i = path.getClass().getClassLoader()
					.getResourceAsStream(path);
			return ImageIO.read(i);
		} catch (Exception e) {
			// Try loading from absolute path, if it failed (e.g. starting the
			// program from eclipse or jvm)
			// extend path with path to resources
			path = "src/main/resources/" + path;
			try {
				return ImageIO.read(new File(path));
			} catch (Exception e2) {
				/**
				 * return default Image (red cross on white background)
				 */
				BufferedImage def = new BufferedImage(64, 64,
						BufferedImage.TYPE_INT_RGB);
				for (int i = 0; i < 64; i++)
					for (int j = 0; j < 64; j++) {
						if (i * (64 - i) - j * (64 - j) == 0)
							def.setRGB(i, j, Color.RED.getRGB());
						else
							def.setRGB(i, j, Color.white.getRGB());
					}
				return def;
			}
		}
	}
	/**
	 * Returns the anti clockwise angle between a vector pointing right and a vector pointing to the point. 0° starts at 3o'clock.
	 *
	 * @param x start position x
	 * @param y start position y
	 * @param x_target target x position
	 * @param y_target target y position
	 * @return angle in degrees
	 */
	public static float getAngle(int x, int y, int x_target, int y_target) {
	    float angle = (float) Math.toDegrees(Math.atan2(y_target - y, x_target - x));

	    if(angle <= 0){
	        angle += 360;
	    }
	    
	    // Anti clock wise
	    return 360-angle;
	}
}
