/**
 * Packet for the different classes and interfaces required for event scheduling 
 * during the simulation of the network and world model. 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.scheduler;