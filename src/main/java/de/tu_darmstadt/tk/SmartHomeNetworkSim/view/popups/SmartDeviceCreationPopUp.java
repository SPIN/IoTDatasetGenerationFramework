package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.BoolCollector;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.BoolSensor;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.FloatCollector;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.FloatSensor;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.IntegerInputValidator;

import javax.swing.JComboBox;

@SuppressWarnings("serial")
/**
 * PopUp for Creation of SmartDevices, which allows configuration of a new
 * SmartDevice
 *
 * @author Andreas T. Meyer-Berg
 */
public class SmartDeviceCreationPopUp extends JDialog {
	/**
	 * Textfield for the x-position of the new SmartDevice
	 */
	private JTextField tfXposition;
	/**
	 * Textfield for the y-position of the new SmartDevice
	 */
	private JTextField tfYposition;
	/**
	 * Textfield for the name of the new SmartDevice
	 */
	private JTextField tfName;
	/**
	 * The SmartDevice which will be created or configured
	 */
	private SmartDevice newDevice;
	/**
	 * Controller which adds the new Device to the model
	 */
	private Controller controller;
	/**
	 * Width of the model, which is the maximum x-Position
	 */
	private int maxX;
	/**
	 * Height of the model, which is the maximum y-Position
	 */
	private int maxY;
	/**
	 * Visualization radius of SmartDevices which
	 */
	private int visualisationRadius;
	/**
	 * True if the SmartDevice is being edited and not a new one. If true, the
	 * device should not be added to the controller.
	 */
	private boolean edit;
	/**
	 * Mutex to disable listeners during refresh
	 */
	private boolean mutex = false;
	/**
	 * Last selected SmartDevice index
	 */
	private int lastSmartDeviceIndex = -1;
	/**
	 * ComboBox containing the different Device classes
	 */
	private JComboBox<String> cmbDevice;
	/**
	 * Bottom TabbedPane for different Sensor Collector settings.
	 */
	JTabbedPane tbConfigureDevice;

	/**
	 * Allows editing/creation of a smartDevice
	 * 
	 * @param deviceToEdit
	 *            SmartDevice to edit
	 * @param edit
	 *            true if it is editing of a device
	 * @param control
	 *            controller, which should add the device
	 */
	public SmartDeviceCreationPopUp(SmartDevice deviceToEdit, boolean edit, Controller control) {

		//setModal(true);
		// setType(Type.POPUP); -> Crashes on Linux
		this.controller = control;
		this.maxX = controller.getSettingsController().getWidth();
		this.maxY = controller.getSettingsController().getHeight();
		this.visualisationRadius = controller.getSettingsController().getDeviceVisualizationRadius();
		this.edit = edit;
		newDevice = deviceToEdit;

		setSize(640, 480);
		setResizable(false);
		// setIconImage(Toolkit.getDefaultToolkit().getImage(SmartDeviceCreationPopUp.class.getResource("/javax/swing/plaf/metal/icons/ocean/computer.gif")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("SmartDevice Configuration Panel");

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel panelGeneral = new JPanel();
		tabbedPane.addTab("General", null, panelGeneral, "Edit general Information of the SmartDevice");
		panelGeneral.setLayout(null);

		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 3, 230, 16);
		lblName.setToolTipText("Change the name of the SmartDevice");
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		panelGeneral.add(lblName);

		tfName = new JTextField();
		tfName.setBounds(250, 0, 226, 22);
		tfName.setToolTipText("Enter the SmartDevice name");
		panelGeneral.add(tfName);
		tfName.setColumns(20);
		tfName.setText(newDevice.getName());

		JLabel lblXposition = new JLabel(
				"x-position in [" + visualisationRadius + ", " + (maxX - visualisationRadius) + "]:");
		lblXposition.setBounds(10, 30, 230, 16);
		lblXposition.setToolTipText("Set the x position, which should be an integer in the given interval.");
		lblXposition.setHorizontalAlignment(SwingConstants.RIGHT);
		panelGeneral.add(lblXposition);

		tfXposition = new JTextField();
		tfXposition.setBounds(250, 27, 116, 22);
		lblXposition.setToolTipText("Enter the x position, which should be an integer in the given interval.");
		panelGeneral.add(tfXposition);
		tfXposition.setColumns(10);
		tfXposition.setText("" + newDevice.getX());
		tfXposition.setInputVerifier(new IntegerInputValidator(visualisationRadius, maxX - visualisationRadius));

		JLabel lblYposition = new JLabel(
				"y-position in [" + visualisationRadius + ", " + (maxY - visualisationRadius) + "]:");
		lblYposition.setBounds(10, 57, 230, 16);
		lblYposition.setToolTipText("Set the y position, which should be an integer in the given interval.");
		lblYposition.setHorizontalAlignment(SwingConstants.RIGHT);
		panelGeneral.add(lblYposition);

		tfYposition = new JTextField();
		tfYposition.setBounds(250, 54, 116, 22);
		tfYposition.setToolTipText("Set the y position, which should be an integer in the given interval.");
		panelGeneral.add(tfYposition);
		tfYposition.setColumns(10);
		tfYposition.setText("" + newDevice.getY());
		tfYposition.setInputVerifier(new IntegerInputValidator(visualisationRadius, maxY - visualisationRadius));

		cmbDevice = new JComboBox<String>();
		cmbDevice.setBounds(0, 112, 365, 22);
		panelGeneral.add(cmbDevice);
		cmbDevice.addActionListener(a -> {
			if (mutex) {
				return;
			}
			cmbDevice.getSelectedIndex();
			int selectedIndex = cmbDevice.getSelectedIndex();
			if (lastSmartDeviceIndex != selectedIndex && selectedIndex != -1) {
				SmartDevice importedDevice = controller.getNetworkController().changeDeviceType(newDevice,
						controller.getImportController().getSmartDevices().get(selectedIndex));
				if (importedDevice == null) {
					System.out.println("Warning invalid SmartDevice changed");
					cmbDevice.setSelectedIndex(lastSmartDeviceIndex);
				} else {
					newDevice = importedDevice;
					lastSmartDeviceIndex = selectedIndex;
					refreshGUI();
				}
			} else {
				cmbDevice.setSelectedIndex(lastSmartDeviceIndex);
			}

		});

		JButton btnImportSmartDevice = new JButton("Import Smart Device");
		btnImportSmartDevice.setBounds(418, 111, 199, 25);
		panelGeneral.add(btnImportSmartDevice);
		btnImportSmartDevice.addActionListener(a -> {
			ImportPopUp<SmartDevice> popUp = new ImportPopUp<SmartDevice>(this, SmartDevice.class);
			try {
				Class<? extends SmartDevice> imported = popUp.showPopUp();
				if (imported == null)
					return;
				if (controller.getImportController().addSmartDevice(imported)) {
					refreshGUI();
				} else {
					JOptionPane.showMessageDialog(this, "Import failed: Invalid Connection");
				}
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(this, "Import failed: " + e1.getMessage());
			}
		});

		/**
		 * Bottom TabbedPane for different Sensor Collector settings.
		 */
		tbConfigureDevice = new JTabbedPane(JTabbedPane.TOP);
		tbConfigureDevice.setBounds(10, 147, 607, 230);
		panelGeneral.add(tbConfigureDevice);

		/*
		 * Other Tabs
		 */
		LinkEditorPanel panelLinks = new LinkEditorPanel(newDevice, control);
		tabbedPane.addTab("Links", null, panelLinks, "Edit Links of the SmartDevice");

		PortEditorPanel panelConnections = new PortEditorPanel(newDevice, control);
		tabbedPane.addTab("Connections", null, panelConnections.getSplitPane(),
				"Edit Connections/Ports/Timings of the SmartDevice");
		panelConnections.setEnabled(true);

		/**
		 * for(Port p:newDevice.getPorts()){ panelConnectionsScroll.add(new
		 * JLabel("Port "+p.getPortNumber()+": "
		 * +p.getConnection().getProtocol().getName()+" state:"
		 * +Port.statusToString(p.getStatus())+" triggerIntervall:"
		 * +p.getTriggerInterval())); panelConnectionsScroll.add(new JLabel(
		 * "Port "+p.getPortNumber()+": "
		 * +p.getConnection().getProtocol().getName()+" state:"
		 * +Port.statusToString(p.getStatus())+" triggerIntervall:"
		 * +p.getTriggerInterval())); panelConnectionsScroll.add(new JLabel(
		 * "Port "+p.getPortNumber()+": "
		 * +p.getConnection().getProtocol().getName()+" state:"
		 * +Port.statusToString(p.getStatus())+" triggerIntervall:"
		 * +p.getTriggerInterval())); }
		 */

		JButton btnCreateDevice = new JButton("Verify & Create SmartDevice");
		btnCreateDevice.addActionListener(a -> validateAndAddDevice());
		getContentPane().add(btnCreateDevice, BorderLayout.SOUTH);

		tabbedPane.setSelectedIndex(0);
		refreshGUI();
	}

	/**
	 * Sets focus to the name text field
	 */
	public void setNameFocus() {
		tfName.setFocusable(true);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				tfName.requestFocusInWindow();
			}
		});
	}

	/**
	 * Validate the input Fields, and add the new SmartDevice to the model and
	 * panel
	 */
	private void validateAndAddDevice() {
		/**
		 * True if the user input is valid an the device can be created
		 */
		boolean valid = true;
		try {
			newDevice.setName(tfName.getText());
			// WhiteBackground if valid
			tfName.setBackground(Color.WHITE);
		} catch (Exception e) {
			// Red Background and false if not
			valid = false;
			tfName.setBackground(Color.RED);
		}
		try {
			/**
			 * x-position entered by the user
			 */
			int x = Integer.parseInt(tfXposition.getText());
			// filter invalid positions
			if (x < visualisationRadius || maxX - visualisationRadius < x)
				throw new NumberFormatException("Number not in bounds");
			newDevice.setX(x);
			tfXposition.setBackground(Color.WHITE);
		} catch (Exception e) {
			valid = false;
			tfXposition.setBackground(Color.RED);
		}
		try {
			/**
			 * y-position entered by the user
			 */
			int y = Integer.parseInt(tfYposition.getText());
			// filter invalid positions
			if (y < visualisationRadius || maxY - visualisationRadius < y)
				throw new NumberFormatException("Number not in bounds");
			newDevice.setY(y);
			tfYposition.setBackground(Color.WHITE);
		} catch (Exception e) {
			valid = false;
			tfYposition.setBackground(Color.RED);
		}
		if (valid) {
			if (edit == false) {
				// Add new Device, update Visualization and dispose the PopUp
				controller.getNetworkController().addSmartDevice(newDevice);

			}
			controller.notifyObservers();
			this.setVisible(false);
			this.dispose();
		}
	}

	public void refreshGUI() {
		mutex = true;
		// Update selected Device Type
		cmbDevice.removeAllItems();
		LinkedList<Class<? extends SmartDevice>> availableDevices = controller.getImportController().getSmartDevices();
		for (Class<? extends SmartDevice> c : availableDevices)
			cmbDevice.addItem(c.getSimpleName());
		// Set Index to selected SmartDevice
		for (int i = 0; i < availableDevices.size(); i++)
			if (newDevice.getClass().equals(availableDevices.get(i))) {
				// Select the right Device Type and save last index
				lastSmartDeviceIndex = i;
			}
		cmbDevice.setSelectedIndex(lastSmartDeviceIndex);
		
		/**
		 * Update Sensor/Collector Configuration
		 * TODO: Maybe allow custom UIs in SmartDevice, and maybe add new Class for the panel to clean up this large class
		 */
		tbConfigureDevice.removeAll();
		tbConfigureDevice.setVisible(false);
		initializeFloatSensorConfigurationPanel();
		initializeBoolSensorConfigurationPanel();
		initializeFloatCollectorConfigurationPanel();
		initializeBoolCollectorConfigurationPanel();
		if (tbConfigureDevice.getTabCount() > 0)
			tbConfigureDevice.setVisible(true);

		mutex = false;
	}

	/**
	 * Initializes a panel for Float Sensors
	 */
	private void initializeFloatSensorConfigurationPanel() {
		if (!(newDevice instanceof FloatSensor))
			return;
		FloatSensor sensor = (FloatSensor) newDevice;

		/**
		 * Configure FloatSensor Tab
		 */
		JPanel pnlFloatSensor = new JPanel();
		tbConfigureDevice.addTab("FloatSensor", null, pnlFloatSensor, null);
		pnlFloatSensor.setLayout(null);

		JLabel lblInformationname = new JLabel("InformationName:");
		lblInformationname.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInformationname.setBounds(10, 10, 200, 20);
		pnlFloatSensor.add(lblInformationname);

		JTextField tfInfoName = new JTextField();
		tfInfoName.setBounds(220, 10, 220, 20);
		tfInfoName.setColumns(10);
		tfInfoName.setText(sensor.getFSinfoName());
		tfInfoName.addActionListener(a -> {
			sensor.setFSinfoName(tfInfoName.getText());
		});
		tfInfoName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				sensor.setFSinfoName(tfInfoName.getText());
			}
		});
		pnlFloatSensor.add(tfInfoName);

		JLabel lblValue = new JLabel("Value:");
		lblValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblValue.setBounds(10, 40, 200, 20);
		pnlFloatSensor.add(lblValue);

		JTextField tfValue = new JTextField();
		tfValue.setColumns(10);
		tfValue.setBounds(220, 40, 220, 20);
		tfValue.setText("" + sensor.getFSval());
		tfValue.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					float val = Float.parseFloat(tfValue.getText());
					tfValue.setBackground(Color.WHITE);
					sensor.setFSval(val);
				} catch (Exception ef) {
					tfValue.setBackground(Color.RED);
				}
			}
		});
		tfValue.addActionListener(a -> {
			try {
				float val = Float.parseFloat(tfValue.getText());
				tfValue.setBackground(Color.WHITE);
				sensor.setFSval(val);
			} catch (Exception e) {
				tfValue.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfValue);

		JLabel lblMinimum = new JLabel("Minimum:");
		lblMinimum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMinimum.setBounds(10, 70, 200, 20);
		pnlFloatSensor.add(lblMinimum);

		JTextField tfMinimum = new JTextField();
		tfMinimum.setColumns(10);
		tfMinimum.setBounds(220, 70, 220, 20);
		tfMinimum.setText("" + sensor.getFSmin());
		tfMinimum.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					float val = Float.parseFloat(tfMinimum.getText());
					tfMinimum.setBackground(Color.WHITE);
					sensor.setFSmin(val);
				} catch (Exception ef) {
					tfMinimum.setBackground(Color.RED);
				}
			}
		});
		tfMinimum.addActionListener(a -> {
			try {
				float val = Float.parseFloat(tfMinimum.getText());
				tfMinimum.setBackground(Color.WHITE);
				sensor.setFSmin(val);
			} catch (Exception e) {
				tfMinimum.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfMinimum);

		JLabel lblMaximum = new JLabel("Maximum:");
		lblMaximum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMaximum.setBounds(10, 100, 200, 20);
		pnlFloatSensor.add(lblMaximum);

		JTextField tfMaxmimum = new JTextField();
		tfMaxmimum.setColumns(10);
		tfMaxmimum.setBounds(220, 100, 220, 20);
		tfMaxmimum.setText("" + sensor.getFSmax());
		tfMaxmimum.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					float val = Float.parseFloat(tfMaxmimum.getText());
					tfMaxmimum.setBackground(Color.WHITE);
					sensor.setFSmax(val);
				} catch (Exception er) {
					tfMaxmimum.setBackground(Color.RED);
				}
			}
		});
		tfMaxmimum.addActionListener(a -> {
			try {
				float val = Float.parseFloat(tfMaxmimum.getText());
				tfMaxmimum.setBackground(Color.WHITE);
				sensor.setFSmax(val);
			} catch (Exception e) {
				tfMaxmimum.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfMaxmimum);

	}

	/**
	 * Initializes a panel for Bool Sensors
	 */
	private void initializeBoolSensorConfigurationPanel() {
		if (!(newDevice instanceof BoolSensor))
			return;
		BoolSensor sensor = (BoolSensor) newDevice;

		/**
		 * Configure FloatSensor Tab
		 */
		JPanel pnlFloatSensor = new JPanel();
		tbConfigureDevice.addTab("BoolSensor", null, pnlFloatSensor, null);
		pnlFloatSensor.setLayout(null);

		JLabel lblInformationname = new JLabel("InformationName:");
		lblInformationname.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInformationname.setBounds(10, 10, 200, 20);
		pnlFloatSensor.add(lblInformationname);

		JTextField tfInfoName = new JTextField();
		tfInfoName.setBounds(220, 10, 220, 20);
		tfInfoName.setColumns(10);
		tfInfoName.setText(sensor.getBSinfoName());
		tfInfoName.addActionListener(a -> {
			sensor.setBSinfoName(tfInfoName.getText());
		});
		tfInfoName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				sensor.setBSinfoName(tfInfoName.getText());
			}
		});
		pnlFloatSensor.add(tfInfoName);

		JLabel lblValue = new JLabel("Value:");
		lblValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblValue.setBounds(10, 40, 200, 20);
		pnlFloatSensor.add(lblValue);

		JTextField tfValue = new JTextField();
		tfValue.setColumns(10);
		tfValue.setBounds(220, 40, 220, 20);
		tfValue.setText("" + sensor.getBSval());
		tfValue.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					boolean val = Boolean.parseBoolean(tfValue.getText());
					tfValue.setBackground(Color.WHITE);
					sensor.setBSval(val);
				} catch (Exception ef) {
					tfValue.setBackground(Color.RED);
				}
			}
		});
		tfValue.addActionListener(a -> {
			try {
				boolean val = Boolean.parseBoolean(tfValue.getText());
				tfValue.setBackground(Color.WHITE);
				sensor.setBSval(val);
			} catch (Exception ef) {
				tfValue.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfValue);
	}
	
	/**
	 * Initializes a panel for Float Collectors
	 */
	private void initializeFloatCollectorConfigurationPanel() {
		if (!(newDevice instanceof FloatCollector))
			return;
		FloatCollector collector = (FloatCollector) newDevice;

		/**
		 * Configure FloatSensor Tab
		 */
		JPanel pnlFloatSensor = new JPanel();
		tbConfigureDevice.addTab("FloatCollector", null, pnlFloatSensor, null);
		pnlFloatSensor.setLayout(null);

		JLabel lblInformationname = new JLabel("InformationName:");
		lblInformationname.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInformationname.setBounds(10, 10, 200, 20);
		pnlFloatSensor.add(lblInformationname);

		JTextField tfInfoName = new JTextField();
		tfInfoName.setBounds(220, 10, 220, 20);
		tfInfoName.setColumns(10);
		tfInfoName.setText(collector.getFCinfoName());
		tfInfoName.addActionListener(a -> {
			collector.setFCinfoName(tfInfoName.getText());
		});
		tfInfoName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				collector.setFCinfoName(tfInfoName.getText());
			}
		});
		pnlFloatSensor.add(tfInfoName);

		JLabel lblValue = new JLabel("Value:");
		lblValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblValue.setBounds(10, 40, 200, 20);
		pnlFloatSensor.add(lblValue);

		JTextField tfValue = new JTextField();
		tfValue.setColumns(10);
		tfValue.setBounds(220, 40, 220, 20);
		tfValue.setText("" + collector.getFCval());
		tfValue.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					float val = Float.parseFloat(tfValue.getText());
					tfValue.setBackground(Color.WHITE);
					collector.setFCval(val);
				} catch (Exception ef) {
					tfValue.setBackground(Color.RED);
				}
			}
		});
		tfValue.addActionListener(a -> {
			try {
				float val = Float.parseFloat(tfValue.getText());
				tfValue.setBackground(Color.WHITE);
				collector.setFCval(val);
			} catch (Exception ef) {
				tfValue.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfValue);
	}
	
	/**
	 * Initializes a panel for Bool Collectors
	 */
	private void initializeBoolCollectorConfigurationPanel() {
		if (!(newDevice instanceof BoolCollector))
			return;
		BoolCollector collector = (BoolCollector) newDevice;

		/**
		 * Configure FloatSensor Tab
		 */
		JPanel pnlFloatSensor = new JPanel();
		tbConfigureDevice.addTab("BoolCollector", null, pnlFloatSensor, null);
		pnlFloatSensor.setLayout(null);

		JLabel lblInformationname = new JLabel("InformationName:");
		lblInformationname.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInformationname.setBounds(10, 10, 200, 20);
		pnlFloatSensor.add(lblInformationname);

		JTextField tfInfoName = new JTextField();
		tfInfoName.setBounds(220, 10, 220, 20);
		tfInfoName.setColumns(10);
		tfInfoName.setText(collector.getBCinfoName());
		tfInfoName.addActionListener(a -> {
			collector.setBCinfoName(tfInfoName.getText());
		});
		tfInfoName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				collector.setBCinfoName(tfInfoName.getText());
			}
		});
		pnlFloatSensor.add(tfInfoName);

		JLabel lblValue = new JLabel("Value:");
		lblValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblValue.setBounds(10, 40, 200, 20);
		pnlFloatSensor.add(lblValue);

		JTextField tfValue = new JTextField();
		tfValue.setColumns(10);
		tfValue.setBounds(220, 40, 220, 20);
		tfValue.setText("" + collector.getBCval());
		tfValue.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					boolean val = Boolean.parseBoolean(tfValue.getText());
					tfValue.setBackground(Color.WHITE);
					collector.setBCval(val);
				} catch (Exception ef) {
					tfValue.setBackground(Color.RED);
				}
			}
		});
		tfValue.addActionListener(a -> {
			try {
				boolean val = Boolean.parseBoolean(tfValue.getText());
				tfValue.setBackground(Color.WHITE);
				collector.setBCval(val);
			} catch (Exception ef) {
				tfValue.setBackground(Color.RED);
			}
		});
		pnlFloatSensor.add(tfValue);
	}
}
