/**
 * Package containing simple implementations of the given interfaces of the core package 
 *
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.simpleImplementation;