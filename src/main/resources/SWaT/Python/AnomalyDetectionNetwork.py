# Hand-implemented anomaly detection for the SWaT network data.
# Looks for network traffic data in the same directory if executed without arguments. Make sure there are no other CSV files in the directory.
# Alternatively, the directory of the network traffic data can be set as first argument.
# @author Fabian Kaiser

import sys
import os
import csv
from datetime import datetime
import re

print('Anomaly detection for SWaT network traffic data.')
path = os.getcwd() # Current directory
if len(sys.argv) > 2:
    print('Ignoring the arguments and using the current directory.')
elif len(sys.argv) == 1:
    print('Using the current directory.')
else:
    path = sys.argv[1]
    print('Using '+path+' as the directory.')

# Define normal behavior
normal_orig = ['192.168.1.48', '192.168.1.88', '192.168.1.87'] # orig, column 3
normal_type = ['log', 'control', 'alert', 'loe'] # type, column 4
normal_ifname = ['eth1', '', 'geod daemon'] # i/f_name, column 5
normal_ifdir = ['outbound', 'inbound'] # i/f_dir, column 6
normal_src = ['192.168.1.60', '192.168.1.10', '192.168.1.20', '192.168.1.30', '192.168.1.88', '', '192.168.1.40', '192.168.1.201', '192.168.1.49', '192.168.1.200', '192.168.1.199', '192.168.1.134', '192.168.1.202', '192.168.1.136', '192.168.1.100', '192.168.1.157', '192.168.1.249', '169.254.245.229', '192.168.1.107', '10.19.1.211', '192.168.1.19', '10.15.51.65'] # src, column 7
normal_dst = ['192.168.1.10', '192.168.1.20', '192.168.1.30', '192.168.1.40', '192.168.1.255', '', '192.168.1.50', '224.0.0.252', '192.168.1.201', '192.168.1.100', '192.168.1.200', '192.168.1.60', '192.168.1.88', '239.192.2.63', '239.255.255.250', '224.0.0.251', '192.168.1.49', '192.168.1.87', '239.192.4.191', '192.168.1.157', '192.168.1.11', '169.254.255.255', '255.255.255.255', '10.19.255.255', '192.148.1.10', '192.168.1.103', '192.168.1.19', '192.168.1.70'] # dst, column 8
normal_proto = ['tcp', 'udp', ''] # proto, column 9
normal_appiname = ['CIP_read_tag_service', 'NetBIOS Datagram Service', '', 'LLMNR Protocol', 'NetBIOS Name Service', 'DCE-RPC Protocol', 'Unknown Traffic', 'VNC', 'OSIsoft PI', 'EtherNet/IP', 'SSDP', 'Multicast DNS Protocol (mDNS)', 'Common Industrial Protocol - get attribute single', 'Common Industrial Protocol - get attribute all', 'Common Industrial Protocol - multiple service packet', 'Remote Desktop Protocol', 'Web Browsing', 'NetBIOS Session Service', 'Server Message Block (SMB)', 'SIC Protocol', 'Common Industrial Protocol', 'CIP_func75', 'CIP_gen', 'Common Industrial Protocol - RMW (Read/Modify/Write)', 'Common Industrial Protocol - success', 'Kaspersky Lab-update', 'Common Industrial Protocol - unconnected send-get attribute all', 'Common Industrial Protocol - read data fragmented', 'CIP_func92', 'CIP_set_attributes_list', 'CIP_func79', 'Simple Object Access Protocol', 'Common Industrial Protocol - execute PCCC', 'DHCP Protocol', 'DNS Protocol', 'Google Chrome', 'CIP_write_tag_service', 'DNP3 Protocol - initialize data to defaults'] # appi_name, column 10
normal_ModbusFunctionCode = ['76', '', '75', '92', '4', '79', '83', '78', '77', '7', '5', '80', '85', '3', '6', '1'] # Modbus_Function_Code, column 12
normal_ModbusFunctionDescription = ['Read Tag Service', 'Read Tag Service - Response', '', 'Unknown Function Code', 'Unknown Function Code - Response', 'Set Attribute List', 'Set_Attribute_List - Response', 'Write Tag Fragmented Service', 'Write Tag Fragmented Service - Response', 'Forward Close - Response', 'Write Tag Service', 'Stop', 'Stop - Response', 'Reset', 'Reset - Response', 'Get Attributes List', 'Get Attributes List - Response', 'Start', 'Start - Response', 'Get Attributes All'] # Modbus_Function_Description, column 13
normal_SCADATag = ['HMI_LIT101', 'HMI_FIT201', 'HMI_AIT202', 'HMI_LIT301', 'HMI_LIT401', ''] # SCADA_Tag, column 15
normal_service = ['44818', '138', '', '5355', '137', '135', '1332', '5900', '5450', '2222', '1900', '5353', '3389', '80', '139', '18190', '1947', '5357', '67', '53', '63791'] # service, column 17
normal_sport = ['53260', '54592', '53250', '53312', '53508', '138', '', '52736', '51908', '137', '38138', '38149', '60020', '61146', '49162', '49643', '53320', '49904', '57104', '54594', '53512', '57653', '51040', '62200', '57674', '2222', '57854', '63575', '56165', '54600', '5353', '53314', '7031', '51564', '51284', '61559', '62311', '16543', '59458', '51522', '54310', '61796', '50566', '61962', '1900', '60819', '56154', '60825', '60834', '51759', '58850', '61526', '62521', '55429', '52544', '29837', '10464', '49189', '53326', '52546', '52548', '54608', '49191', '49194', '49195', '49193', '49192', '49196', '51935', '51521', '7033', '54229', '51994', '60065', '53255', '57508', '16546', '61708', '52216', '50502', '52501', '63410', '50116', '64021', '52437', '53029', '61177', '55159', '49158', '13276', '40403', '64830', '60342', '61301', '56261', '52660', '57852', '59348', '62780', '58834', '58845', '58877', '62965', '62968', '49585', '54542', '50037', '53958', '64696', '55874', '53863', '57683', '55886', '52881', '51573', '55845', '53917', '62239', '61441', '49993', '50466', '49180', '53104', '52971', '59028', '54314', '16548', '51278', '53328', '64199', '49861', '62908', '53558', '63578', '53961', '64979', '51150', '53779', '53437', '53036', '57109', '64302', '52350', '51283', '55968', '50469', '60352', '55032', '20676', '9973', '55043', '9975', '49515', '49641', '49686', '62833', '50967', '51178', '51724', '51743', '54706', '49614', '55161', '55993', '53708', '59728', '68', '53809', '13486', '13487', '60137', '55578', '55580', '57140', '60851', '58085', '55384', '63660', '49601', '59636', '59079', '50891', '59764', '55606', '54101', '57410', '49181', '49880', '60772', '55827', '55255', '65149', '55158', '63966', '54620', '51424', '56058', '49964', '61088', '49231', '61571', '56099', '56279', '61131', '51104', '52536', '63371', '56504', '61443', '57032', '57179', '56726', '56559', '62956', '64354', '64192', '51679', '51890', '56834', '61504', '63900', '55173', '53629', '56980', '53041', '42221', '57003', '56056', '63246', '57216', '59137', '57918', '54304', '57438', '65064', '64845', '51523', '60538', '57660', '54658', '52382', '45612', '57789', '55538', '57892', '60596', '64407', '46162', '49434', '52315', '45278', '58055', '59917', '58118', '50210', '58048', '58029', '58338', '57711', '51639', '58346', '59577', '56219', '65395', '62398', '53614', '58567', '62207', '54068', '49599', '57449', '49974', '61115', '58797', '51818', '53688', '54534', '58470', '48518', '58923', '55395', '59024', '51045', '58216', '16544', '59119', '46315', '59225', '59251', '62179', '64145', '51205', '5355', '65436', '59472', '53740', '56823', '7032', '56753', '51085', '46341', '49984', '61599', '56893', '55948', '51829', '48567', '59789', '61828', '50949', '59946', '57384', '65079', '61812', '63798', '57050', '62560', '60168', '63319', '49348', '49840', '60389', '52914', '58019', '46402', '60487', '51305', '59609', '60616', '59263', '52139', '48627', '53641', '48653', '59237', '60837', '49564', '62707', '48658', '60926', '64649', '61730', '7037', '62020', '56036', '61064', '56001', '64376', '51201', '61294', '61921', '64664', '64950', '63507', '61412', '45558', '51393', '61550', '51982', '53732', '7036', '16545', '7030', '43737', '49212', '49214', '49215', '49213', '49211', '49210', '46653', '49217', '49216', '49221', '49220', '49218', '49219', '65141', '43846', '61771', '58497', '59336', '64449', '61993', '65158'] # s_port, column 18
normal_relation_ModbusFunction_Code_to_Description = {'Read Tag Service':'76', 'Read Tag Service - Response':'76', 'Get Attributes All':'1', 'Start':'6', '	Start - Response':'6', 'Get Attributes List':'3', 'Get Attributes List - Response':'3', 'Reset':'5', 'Reset - Response':'5', 'Stop':'7', 'Stop - Response':'7', 'Write Tag Service':'77', 'Forward Close - Response':'78', 'Write Tag Fragmented Service':'83', 'Write Tag Fragmented Service - Response':'83', 'Set Attribute List':'4', 'Set_Attribute_List - Response':'4', 'Unknown Function Code':'75', 'Unknown Function Code - Response':'75'}
normal_sport_PLC1 = ['54592', '54608', '54600', '2222', '54594']
normal_sport_PLC2 = ['53312', '53326', '53314', '53320']
normal_sport_PLC3 = ['53508', '53512', '52544', '2222', '52546', '52548', ]
normal_sport_PLC4 = ['52736']
normal_sport_PLC6 = ['53260', '53250']

# For the transactionID check
transactionIDs_in_use = {} 

# RegEx strings for the payloads
match_string_hex_line = '(((0x[0123456789abcdef]{2} ){3})(0x[0123456789abcdef]{2}; ))'
match_string_hex_line_no_semicolon = '(((0x[0123456789abcdef]{2} ){3})(0x[0123456789abcdef]{2}))'

# Frequency counters
frequency_counter_total = 0
frequency_counter_PLC1 = 0
frequency_counter_PLC2 = 0
frequency_counter_PLC3 = 0
frequency_counter_PLC4 = 0
frequency_counter_PLC5 = 0
frequency_counter_PLC6 = 0

# The array for the outputs
outputs_array_anomalies = []
print_outputs_to_terminal = 1

# Get CSV files
csv_files = []
all_files = sorted(os.listdir(path)) # First sort alphabetically
all_files = sorted(all_files, key=len) # Then sort according to length, so _2 is between _1 and _3 rather than between _19 and _20
for file in all_files:
    if file.endswith(".csv"):
        csv_files.append(file)

# Iterate over CSV files
number_of_files = len(csv_files)
counter = 0
first_time_stamp = 1
last_time_stamp = datetime.now()
for csv_file_name in csv_files:
    counter += 1
    print('Checking file '+str(counter)+' of '+str(number_of_files)+': '+csv_file_name)
    
    file = open(path+'/'+csv_file_name, "r")
    csv_reader = csv.reader(file, delimiter=',')

    for row in csv_reader:
        row_number = csv_reader.line_num
        row_anomaly_string = csv_file_name+' row '+str(row_number)+': '
        if row_number > 1:
            # 1) Check for normal behavior without relations between columns
            orig = row[3]
            if orig not in normal_orig:
                anomaly_string = row_anomaly_string+'Unknown value in column 4 \"orig\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)
            
            type = row[4]
            if type not in normal_type:
                anomaly_string = row_anomaly_string+'Unknown value in column 5 \"type\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            ifname = row[5]
            if ifname not in normal_ifname:
                anomaly_string = row_anomaly_string+'Unknown value in column 6 \"i/f_name\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            ifdir = row[6]
            if ifdir not in normal_ifdir:
                anomaly_string = row_anomaly_string+'Unknown value in column 7 \"i/f_dir\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            src = row[7]
            if src not in normal_src:
                anomaly_string = row_anomaly_string+'Unknown value in column 8 \"src\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            dst = row[8]
            if dst not in normal_dst:
                anomaly_string = row_anomaly_string+'Unknown value in column 9 \"dst\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)
            proto = row[9]
            if proto not in normal_proto:
                anomaly_string = row_anomaly_string+'Unknown value in column 10 \"proto\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            appiname = row[10]
            if appiname not in normal_appiname:
                anomaly_string = row_anomaly_string+'Unknown value in column 11 \"appi_name\"'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            ModbusFunctionCode = row[12]
            if ModbusFunctionCode not in normal_ModbusFunctionCode:
                anomaly_string = row_anomaly_string+'Unknown value in column 13 \"Modbus_Function_Code\"'
                if print_outputs_to_terminal == 1:     
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            ModbusFunctionDescription = row[13]
            if ModbusFunctionDescription not in normal_ModbusFunctionDescription:
                anomaly_string = row_anomaly_string+'Unknown value in column 14 \"Modbus_Function_Description\"'  
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            SCADATag = row[15]
            if SCADATag not in normal_SCADATag:
                anomaly_string = row_anomaly_string+'Unknown value in column 16 \"SCADA_Tag\"' 
                if print_outputs_to_terminal == 1:   
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            service = row[17]
            if service not in normal_service:
                anomaly_string = row_anomaly_string+'Unknown value in column 18 \"service\"'
                if print_outputs_to_terminal == 1:  
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)    

            sport = row[18]
            if sport not in normal_sport:
                anomaly_string = row_anomaly_string+'Unknown value in column 19 \"s_port\"' 
                if print_outputs_to_terminal == 1:   
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            # 2) Check whether src and proxy_src_ip are equal                                                      
            proxysrcip = row[11]
            if appiname != '' and src != proxysrcip:
                anomaly_string = row_anomaly_string+'src and proxy_src_ip are not equal'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            # 3) tcp and udp always have the interface direction outbound
            if proto in ['tcp', 'udp'] and ifdir != 'outbound':
                anomaly_string = row_anomaly_string+'Unknown combination of proto and i/f_dir (tcp and udp always have the interface direction outbound)'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)                

            # 4) Check whether Modbus function code and description match
            if ModbusFunctionDescription in normal_relation_ModbusFunction_Code_to_Description:
                if normal_relation_ModbusFunction_Code_to_Description[ModbusFunctionDescription] != ModbusFunctionCode and not ModbusFunctionDescription.startswith('Unknown Function Code'):
                    anomaly_string = row_anomaly_string+'Modbus_Function_Code and Modbus_Function_Description do not match'
                    if print_outputs_to_terminal == 1:
                        print('\t'+anomaly_string)
                    outputs_array_anomalies.append(anomaly_string)                    
   
            # 5) Check whether SCADA tag and source IP address match
            SCADATag_mismatch = 0
            if SCADATag == 'HMI_LIT101' and not (dst == '192.168.1.10' or dst == '192.148.1.10'):
                SCADATag_mismatch = 1
            elif (SCADATag == 'HMI_FIT201' or SCADATag == 'HMI_AIT202') and not dst == '192.168.1.20':
                SCADATag_mismatch = 1
            elif SCADATag == 'HMI_LIT301' and not dst == '192.168.1.30':
                SCADATag_mismatch = 1
            elif SCADATag == 'HMI_LIT401' and not dst == '192.168.1.40':
                SCADATag_mismatch = 1            
            if SCADATag_mismatch == 1:
                anomaly_string = row_anomaly_string+'SCADA tag and source IP address do not match'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            # 6) Check whether read tag service packets have the correct service port
            if ModbusFunctionCode == '76' and not service == '44818':
                anomaly_string = row_anomaly_string+'Read Tag Service packets should have the value 44818 in the column service'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            # 7) Check whether Start, Stop, and Reset packets are only sent from the engineering workstation
            if (ModbusFunctionDescription.startswith('Stop') or ModbusFunctionDescription.startswith('Start') or ModbusFunctionDescription.startswith('Reset')) and not src == '192.168.1.201':
                anomaly_string = row_anomaly_string+'Start, Stop, and Reset packets can only be sent from the engineering workstation'
                if print_outputs_to_terminal == 1:
                    print('\t'+anomaly_string)
                outputs_array_anomalies.append(anomaly_string)

            # 8) Check whether timestamp is equal or higher than before
            #if ifname != '': # Ignore the special cases
            try:
                time_stamp_temp = datetime.strptime(row[1]+' '+row[2], '%d%b%Y %H:%M:%S')
            except ValueError:
                pass
            try:
                time_stamp_temp = datetime.strptime(row[1]+' '+row[2], '%d-%b-%y %H:%M:%S')
            except ValueError:
                pass
            if first_time_stamp == 1:
                first_time_stamp = 0
                last_time_stamp = time_stamp_temp
            else:
                if time_stamp_temp < last_time_stamp:
                    anomaly_string = row_anomaly_string+'Timestamp is before last timestamp'
                    if print_outputs_to_terminal == 1:
                        print('\t'+anomaly_string)
                    outputs_array_anomalies.append(anomaly_string)

                # 12) Check whether the packet frequencies are normal
                if time_stamp_temp != last_time_stamp:
                    if frequency_counter_total > 525: # Training set maximum: 495
                        anomaly_string = row_anomaly_string+'Total packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string) 

                    if frequency_counter_PLC1 > 150: # Training set maximum: 125
                        anomaly_string = row_anomaly_string+'PLC 1 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)       

                    if frequency_counter_PLC2 > 100: # Training set maximum: 85
                        anomaly_string = row_anomaly_string+'PLC 2 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)   

                    if frequency_counter_PLC3 > 150: # Training set maximum: 133
                        anomaly_string = row_anomaly_string+'PLC 3 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)   

                    if frequency_counter_PLC4 > 10: # Training set maximum: 3
                        anomaly_string = row_anomaly_string+'PLC 4 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)   

                    if frequency_counter_PLC5 > 0: # Training set maximum: 0
                        anomaly_string = row_anomaly_string+'PLC 5 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)   

                    if frequency_counter_PLC6 > 250: # Training set maximum: 221
                        anomaly_string = row_anomaly_string+'PLC 6 packet frequency too high at '+str(last_time_stamp)
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string) 

                    frequency_counter_total = 0                                                                                                                                                                   
                    frequency_counter_PLC1 = 0
                    frequency_counter_PLC2 = 0
                    frequency_counter_PLC3 = 0
                    frequency_counter_PLC4 = 0
                    frequency_counter_PLC5 = 0
                    frequency_counter_PLC6 = 0
                else:
                        frequency_counter_total += 1
                        if src == '192.168.1.10':                                                                                                                                                                   
                            frequency_counter_PLC1 += 1
                        if src == '192.168.1.20':                                                                                                                                                                   
                            frequency_counter_PLC2 += 1
                        if src == '192.168.1.30':                                                                                                                                                                   
                            frequency_counter_PLC3 += 1
                        if src == '192.168.1.40':                                                                                                                                                                   
                            frequency_counter_PLC4 += 1
                        if src == '192.168.1.50':                                                                                                                                                                   
                            frequency_counter_PLC5 += 1
                        if src == '192.168.1.60':                                                                                                                                                                   
                            frequency_counter_PLC6 += 1  

                last_time_stamp = time_stamp_temp

            # 9) Check that a transaction ID is not used twice at the same time
            ModbusTransactionID = row[14]
            if ModbusTransactionID != '9217': # Ignore the special case for responses without requests
                if ModbusFunctionCode == '76': # Read tag packet
                    if ModbusTransactionID in transactionIDs_in_use:                        
                        if ModbusFunctionDescription.endswith('Response'): # Response
                            del transactionIDs_in_use[ModbusTransactionID]
                        else: # Request
                            anomaly_string = row_anomaly_string+'Transaction ID already in use'
                            if print_outputs_to_terminal == 1:
                                print('\t'+anomaly_string)
                            outputs_array_anomalies.append(anomaly_string)
                            transactionIDs_in_use[ModbusTransactionID] = 1

            # 10) Check whether the modbus payload format is correct
            if ModbusFunctionCode == '76' and ModbusTransactionID != '9217': # Read tag packet and ignore the special case for responses without requests ('40962' could be another common special case)
                ModbusValue = row[16]
                if ModbusFunctionDescription.endswith('Response'): # Response
                    if SCADATag == 'HMI_FIT201':
                        regEx_search_result = re.search(match_string_hex_line+'{8}[0-9]+; '+match_string_hex_line+'[0-9]+', ModbusValue)      
                    else:
                        regEx_search_result = re.search(match_string_hex_line+'{7}[0-9]+; '+match_string_hex_line_no_semicolon, ModbusValue)
                    if regEx_search_result == None:
                        anomaly_string = row_anomaly_string+'Incorrect Modbus value format for the SCADA tag'
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)
                else:
                    if ModbusValue != 'Number of Elements: 1':
                        anomaly_string = row_anomaly_string+'Incorrect Modbus value for a Read Tag request packet'
                        if print_outputs_to_terminal == 1:
                            print('\t'+anomaly_string)
                        outputs_array_anomalies.append(anomaly_string)

            # 11) Checks whether the source ports of packets sent by the PLCs are known
            if ModbusFunctionCode == '76' and ModbusTransactionID != '9217': # Read tag packet and ignore the special case for responses without requests
                if (src == '192.168.1.10' and sport not in normal_sport_PLC1) or (src == '192.168.1.20' and sport not in normal_sport_PLC2) or (src == '192.168.1.30' and sport not in normal_sport_PLC3) or (src == '192.168.1.60' and sport not in normal_sport_PLC6):
                    anomaly_string = row_anomaly_string+'Unknown sport value for the source IP address'
                    if print_outputs_to_terminal == 1:
                        print('\t'+anomaly_string)
                    outputs_array_anomalies.append(anomaly_string)



# Write result to a file
if len(outputs_array_anomalies) > 0:
    output_file_name = 'AnomalyDetectionNetwork_Results_'+str(datetime.now())+'.txt'
    output_file_name = output_file_name.replace(':','.')
    output_file = open(output_file_name, "a")
    for anomaly in outputs_array_anomalies:
        output_file.write(anomaly+'\n')
    output_file.close()
    print('Output written to '+output_file_name)
else:
    print('No output to write to a file.')