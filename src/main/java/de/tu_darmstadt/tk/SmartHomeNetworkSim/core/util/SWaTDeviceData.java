package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * The data of SWaT devices from the Connections.csv file.
 * @author Fabian Kaiser
 */
public class SWaTDeviceData {
	
	/**
	 * The list of connections of the device.
	 */
	private LinkedList<String[]> connectionsList = new LinkedList<String[]>();
	
	/**
	 * The name of the device.
	 */
	private String deviceName;
	
	
	/**
	 * Constructor. Load the data from the file into the variables.
	 * @param deviceName The specified SWaT device.
	 */
	public SWaTDeviceData(String deviceName) {
		this.deviceName = deviceName;
		
		File conFile = new File("src/main/resources/SWaT/Connections.csv");
		Scanner conFileReader;
		try {
			conFileReader = new Scanner(conFile);
			while (conFileReader.hasNextLine() == true) {
				String connection = conFileReader.nextLine();
				String[] entries = connection.split(",");
				
				if(SWaTUtilities.getSourceDeviceName(entries[0]).equals(deviceName))
					connectionsList.add(entries);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Returns the device data of the specified SWaT device.
	 * @return The device data of the specified SWaT device.
	 */
	@SuppressWarnings("unused")
	public LinkedHashMap<String, String> getDeviceData(){
		LinkedHashMap<String, String> deviceData = new LinkedHashMap<String, String>();
		
		deviceData.put("name", deviceName);
		String connection;
		String sourceIPAddress;
		String outboundPort;
		String inboundPort;
		String datasetA1SendingMean;
		String datasetA1SendingStandardDeviation;
		String datasetA1SendingMinimum;
		String datasetA1SendingMaximum;
		String datasetA1ResponseMean;
		String datasetA1ResponseStandardDeviation;
		String datasetA1ResponseMinimum;
		String datasetA1ResponseMaximum;
		String datasetA6SendingMean;
		String datasetA6SendingStandardDeviation;
		String datasetA6SendingMinimum;
		String datasetA6SendingMaximum;
		String datasetA6ResponseMean;
		String datasetA6ResponseStandardDeviation;
		String datasetA6ResponseMinimum;
		String datasetA6ResponseMaximum;
		
		for(String[] connectionEntry:connectionsList) {
			connection = connectionEntry[0];
			sourceIPAddress = connectionEntry[1];
			outboundPort = connectionEntry[2];
			inboundPort = connectionEntry[3];
			datasetA1SendingMean = connectionEntry[4];
			datasetA1SendingStandardDeviation = connectionEntry[5];
			datasetA1SendingMinimum = connectionEntry[6];
			datasetA1SendingMaximum = connectionEntry[7];
			datasetA1ResponseMean = connectionEntry[8];
			datasetA1ResponseStandardDeviation = connectionEntry[9];
			datasetA1ResponseMinimum = connectionEntry[10];
			datasetA1ResponseMaximum = connectionEntry[11];
			datasetA6SendingMean = connectionEntry[12];
			datasetA6SendingStandardDeviation = connectionEntry[13];
			datasetA6SendingMinimum = connectionEntry[14];
			datasetA6SendingMaximum = connectionEntry[15];
			datasetA6ResponseMean = connectionEntry[16];
			datasetA6ResponseStandardDeviation = connectionEntry[17];
			datasetA6ResponseMinimum = connectionEntry[18];
			datasetA6ResponseMaximum = connectionEntry[19];
			
			deviceData.put(connection+"_sourceIPAddress", sourceIPAddress);
			deviceData.put(connection+"_outboundPort", outboundPort);
			deviceData.put(connection+"_inboundPort", inboundPort);
			deviceData.put(connection+"_sendingCurrent", "Dataset A1 Distribution");
			deviceData.put(connection+"_sendingNormalDistributionMean", datasetA1SendingMean);
			deviceData.put(connection+"_sendingNormalDistributionSD", datasetA1SendingStandardDeviation);
			deviceData.put(connection+"_sendingConstantValue", datasetA1SendingMean);
			deviceData.put(connection+"_responseCurrent", "Dataset A1 Distribution");				
			deviceData.put(connection+"_responseNormalDistributionMean", datasetA1ResponseMean);
			deviceData.put(connection+"_responseNormalDistributionSD", datasetA1ResponseStandardDeviation);
			deviceData.put(connection+"_responseConstantValue", datasetA1ResponseMean);
			deviceData.put(connection+"_DatasetA1DistributionSendingSD", datasetA1SendingStandardDeviation);
			deviceData.put(connection+"_DatasetA1DistributionResponseSD", datasetA1ResponseStandardDeviation);
			deviceData.put(connection+"_DatasetA6DistributionSendingSD", datasetA6SendingStandardDeviation);
			deviceData.put(connection+"_DatasetA6DistributionResponseSD", datasetA6ResponseStandardDeviation);
		}

		return deviceData;
	}
}