package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

/**
 * Constants for the types of devices in SWaT.
 * 
 * @author Fabian Kaiser
 */
public final class SWaTDeviceTypes {
	/**
	 * Actuator.
	 */
	public static final int Actuator = 0;
	
	/**
	 * Sensor.
	 */
	public static final int Sensor = 1;
	
	/**
	 * Programmable Logic Operator (PLC).
	 */
	public static final int PLC = 2;
	
	/**
	 * SCADA server.
	 */
	public static final int SCADA = 3;	

	/**
	 * Historian server.
	 */
	public static final int Historian = 4;	
	
	/**
	 * Engineering workstation.
	 */
	public static final int EngineeringWorkstation = 5;	

	/**
	 * Switch.
	 */
	public static final int Switch = 6;	
}
