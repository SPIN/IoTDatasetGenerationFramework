package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * InputVerifier which validates if the value of an JTextfield is within the
 * given min and max bounds.
 *
 * @author Andreas T. Meyer-Berg
 */
public class IntegerInputValidator extends InputVerifier {

	/**
	 * Minimum valid value
	 */
	private int min;

	/**
	 * Maximum valid value
	 */
	private int max;

	/**
	 * Creates a new IntegerinputValidator which verifies that an integer of the
	 * JTextfieldis in this specified range.<br>
	 * {@code true if x element of [min,max]}
	 * 
	 * @param minValue
	 *            Smallest allowed value
	 * @param maxValue
	 *            Largest allowed value
	 */
	public IntegerInputValidator(int minValue, int maxValue) {
		min = minValue;
		max = maxValue;
	}

	@Override
	public boolean verify(JComponent input) {
		try {
			int val = Integer.parseInt(((JTextField) input).getText());
			return min <= val && val <= max;
		} catch (Exception e) {
			return false;
		}
	}

}
