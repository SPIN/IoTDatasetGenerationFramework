# This script plots the values of the IoT devices for each stage of SWaT separately.
# Usage: DatasetA1Plots.py PATH
# Reading the files in CSV format is significantly faster than reading them in XLSX format.
# Requires ~5 GiB RAM.
# @author Fabian Kaiser
#
# Conversion from XLSX to CSV: Open Office Calc -> File -> Save as -> Text CSV -> Save -> OK -> Remove first line or two first lines by hand so the column description is in row 1 -> Remove last (empty) line by hand
#
# The following code is based on a Tensorflow tutorial:
# https://www.tensorflow.org/tutorials/structured_data/time_series

# cd .\git\thesis-fabian-iiot-ad\Code\src\main\resources\SWaT\Python
# DatasetA1Plots.py C:/Users/user/master/SWaT_Dataset_Normal_v1.csv
import sys
import datetime
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

if len(sys.argv) != 2:
    print('Use exactly one argument for the path.')
elif (not sys.argv[1].endswith('.xlsx')) and not(sys.argv[1].endswith('.csv')):
    print('File must be in XLSX or CSV format.')
else:    
    path = sys.argv[1]
    mpl.rcParams['figure.figsize'] = (8, 9)
    mpl.rcParams['axes.grid'] = False

    print('Reading the file. Please wait.')
    if sys.argv[1].endswith('.csv'):
        df = pd.read_csv(path, skipinitialspace = True)
    else: 
        print('This may take several minutes.')
        df = pd.read_excel(path, skiprows = 1)
        df = df.rename(columns = str.strip)
        df['Timestamp'] = df['Timestamp'].str.strip()

    date_time = pd.to_datetime(df.pop('Timestamp'), format = '%d/%m/%Y %I:%M:%S %p')
    
    plot_cols = ['FIT101','LIT101','MV101','P101','P102']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 1')

    plot_cols = ['AIT201','AIT202','AIT203','FIT201','MV201','P201','P202','P203','P204','P205','P206']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 2')

    plot_cols = ['DPIT301','FIT301','LIT301','MV301','MV302','MV303','MV304','P301','P302']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 3')

    plot_cols = ['AIT401','AIT402','FIT401','LIT401','P401','P402','P403','P404','UV401']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 4')

    plot_cols = ['AIT501','AIT502','AIT503','AIT504','FIT501','FIT502','FIT503','FIT504','P501','P502','PIT501','PIT502','PIT503']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 5')

    plot_cols = ['FIT601','P601','P602','P603']
    plot_features = df[plot_cols]
    plot_features.index = date_time
    _ = plot_features.plot(subplots = True, title = 'Stage 6')

    plt.show()