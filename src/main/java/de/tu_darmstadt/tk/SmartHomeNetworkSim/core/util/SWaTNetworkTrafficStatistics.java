package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Analyzes the statistics of the packets of the network traffic data of SWaT dataset A1.</br>
 * These are very memory intensive operations. Add heap memory to the Java VM with the argument -Xmx20000m</br>
 * Execution of the main method takes several hours (~07:27:10).</br>
 * The variables pathToCSVDirectory and pathToA6Directory have to be set correctly.</br>
 * Throws exceptions if unrelated CSV files are in the same directories.</br>
 * @author Fabian Kaiser
 */
public class SWaTNetworkTrafficStatistics {
	/**
	 * The number of thread to be used in parallel.
	 */
	private static int threadNumber = Runtime.getRuntime().availableProcessors()-2;
	
	/**
	 * The path of the directory that contains the CSV files of the SWaT A1 dataset.
	 * The files for the results are written in this directory, too.
	 */
	public static String pathToCSVDirectory = "C:/Users/user/master";	
	
	/**
	 * Contains HashMaps where the key is the concatenation of date and time, value is the number of lines with this date and time.</br>
	 * Different threads append to the queue.
	 */
	public static ConcurrentLinkedQueue<LinkedHashMap<String, Integer>> timestampLinesQueue = new ConcurrentLinkedQueue<LinkedHashMap<String, Integer>>();

	/**
	 * The path to the file that contains Wireshark filter results of the SWaT A6 dataset.
	 * The files for the results are written in this directory, too.
	 */
	public static String pathToA6Directory = "C:/Users/user/master";
	
	
	/**
	 * Executes the statistics analysis.
	 */
	public static void main(String[] args){
		try {
			if(args.length >= 1) {
				switch(args[0]) {
				case "parallel":
					executeParallelizedVersion();
					break;
				case "old":
					executeOldStatisticAnalysis();
					break;
				case "A6":
					executeDatasetA6WiresharkFilterStatistics();
					break;
				}
			}
			else
				executeNonParallelizedVersion();
		} catch (IOException e) {
			System.out.println("CSV directory path not correctly set.");
		}
	}

	/**
	 * Analyzes the statistics of filter results from Wireshark of dataset A6.</br>
	 * </br>
	 * Shell commands to generate the needed input files (~45 minutes run-time per command): 
	 * </br></br>
	 * tshark -r Dec2019_00000_20191206100500.pcap -2 -R "(cip.sc == 0x4c && cip.genstat == 0x00 && cip.symbol[0-2]==\"HMI\") || (cip && (ip.dst in {192.168.1.100 192.168.1.200 192.168.1.201} || (ip.src in {192.168.1.100 192.168.1.200 192.168.1.201} && enip.time > 0)))" -T fields -E separator=, -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e enip.time -e cip.symbol > A6_response_times.csv
	 * </br></br>
	 * tshark -r Dec2019_00000_20191206100500.pcap -2 -R "cip.rr == 0x0 && ip.src in {192.168.1.10 192.168.1.20 192.168.1.30 192.168.1.40 192.168.1.50 192.168.1.60 192.168.1.100 192.168.1.200 192.168.1.201}" -T fields -E separator=, -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e frame.time_relative > A6_sending_times.csv
	 * </br></br>
	 * tshark -r Dec2019_00000_20191206100500.pcap -2 -R "cip && ip.dst in {192.168.1.100 192.168.1.200 192.168.1.201} && ip.src in {192.168.1.100 192.168.1.200 192.168.1.201}" -T fields -E separator=, -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e enip.time -e cip.symbol > A6_SCADA_system_information.csv
	 * </br></br></br>
	 * If the PCAP files are too large, they can be reduced with tcpdump -r input.pcap -w output.pcap -C 100
	 * @throws IOException 
	 * 
	 */
	private static void executeDatasetA6WiresharkFilterStatistics() throws IOException{
		try {
			LinkedHashMap<String, List<Double>> collectedResponseTimes = new LinkedHashMap<String, List<Double>>();
			
			Scanner csvFileReader = new Scanner(new File(pathToA6Directory+"/A6_response_times.csv"));
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");	
				String sourceIPAddress = entries[0];
				String destinationIPAddress = entries[1];
				if(entries.length >= 5) {
					Double responseTime = Double.parseDouble(entries[4]);
					
					List<Double> responseTimeList = new LinkedList<Double>();
					String connection = sourceIPAddress+"->"+destinationIPAddress;
					if(collectedResponseTimes.containsKey(connection)) {
						responseTimeList = collectedResponseTimes.get(connection);
						responseTimeList.add(responseTime);
					}
					collectedResponseTimes.put(connection, responseTimeList);
				}
			}			
			writeStatisticsData(pathToA6Directory+"/csvStatistics_"+System.currentTimeMillis()+"_A6 Response Statistics", "Statistics of response connections in seconds.\nConnection \t\t\t\t\t\tMean \t\t\t\t\tStandard Deviation \t\tMinimum \t\t\t\tMaximum\n",collectedResponseTimes);
			writeDistributionData(pathToA6Directory+"/csvStatistics_"+System.currentTimeMillis()+"_A6 Response Distributions", "Distributions statistics of response connections in seconds.\nCan be used to draw diagrams.\nFrequency\t Value\n",collectedResponseTimes);
			
			
			LinkedHashMap<String, List<Double>> collectedSendingTimes = new LinkedHashMap<String, List<Double>>();
			LinkedHashMap<String, Double> sendingFrequencyCounterKeyLastSeen = new LinkedHashMap<String, Double>();
			csvFileReader = new Scanner(new File(pathToA6Directory+"/A6_sending_times.csv"));
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");	
				String sourceIPAddress = entries[0];
				String destinationIPAddress = entries[1];
				Double timestamp = Double.parseDouble(entries[4]);				

				String sendingFrequencyCounterKey = sourceIPAddress+"->"+destinationIPAddress;
				if(sendingFrequencyCounterKeyLastSeen.containsKey(sendingFrequencyCounterKey)) {
					List<Double> sendingTimes = new LinkedList<Double>();					
					if(collectedSendingTimes.containsKey(sendingFrequencyCounterKey))
						sendingTimes = collectedSendingTimes.get(sendingFrequencyCounterKey);
						sendingTimes.add(timestamp-sendingFrequencyCounterKeyLastSeen.get(sendingFrequencyCounterKey));
					collectedSendingTimes.put(sendingFrequencyCounterKey,sendingTimes);
				}
				sendingFrequencyCounterKeyLastSeen.put(sendingFrequencyCounterKey, timestamp);				
			}
			writeStatisticsData(pathToA6Directory+"/csvStatistics_"+System.currentTimeMillis()+"_A6 Sending Statistics", "Statistics of sending connections in seconds.\nConnection \t\t\t\t\t\tMean \t\t\t\t\tStandard Deviation \t\tMinimum \t\t\t\tMaximum\n",collectedSendingTimes);
			writeDistributionData(pathToA6Directory+"/csvStatistics_"+System.currentTimeMillis()+"_A6 Sending Distributions", "Distributions statistics of sending connections in seconds.\nCan be used to draw diagrams.\nFrequency\t Value\n",collectedSendingTimes);

		} catch (FileNotFoundException e) {
			System.out.println("Variable pathToA6Directory set incorrectly.");
		}
	}
	
	
	/**
	 * Executes a non-parallelized version of this.
	 * @throws IOException 
	 */
	private static void executeNonParallelizedVersion() throws IOException {
		LinkedHashMap<String, Integer> timestampData = new LinkedHashMap<String, Integer>(); // <date+time, timestamp count>
		LinkedHashMap<String, List<Double>> collectedResponseTimes = new LinkedHashMap<String, List<Double>>(); // <sourceIP+destIP, list of response times for the connection>
		LinkedHashMap<String, List<Integer>> collectedResponseTimesPerDateTime = new LinkedHashMap<String, List<Integer>>(); // <sourceIP+destIP, list of response times during the current date+time>
		LinkedHashMap<String, Integer> responseTimeCounterKeyLastSeen = new LinkedHashMap<String, Integer>(); // <date+time+ModbusTransactionId+sourceIP+destIP, response time counter for the key>
		LinkedHashMap<String, Integer> responseTimeCounters = new LinkedHashMap<String, Integer>(); // <date+time+ModbusTransactionId+sourceIP+destIP, response time value for the key>
		
		LinkedHashMap<String, List<Double>> collectedSendingFrequencies = new LinkedHashMap<String, List<Double>>(); // <sourceIP+destIP, list of frequencies for the connection>
		LinkedList<Pair<String, Integer>> collectedSendingFrequenciesPerDateTime = new LinkedList<Pair<String, Integer>>(); // <sourceIP+destIP, list of frequencies during the current date+time>
		LinkedHashMap<String, Integer> sendingFrequencyCounterKeyLastSeen = new LinkedHashMap<String, Integer>(); // <sourceIP+destIP, frequency counter for the key>
		
		// First and last date+time of the training set are not complete and would distort the data
		LinkedList<String> excludedDateTimes = new LinkedList<String>(); 
		excludedDateTimes.add("21Dec201522:17:56");
		excludedDateTimes.add("28Dec201510:16:36");
		

		
		// Read all csv files from folder
		List<String> csvFiles = new ArrayList<String>();		
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}		
		
		// Process csv files
		for(String filename:csvFiles){
			// Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			boolean secondLine = false;
			String currentDateTime = "";
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");						
				String keyDateTime = entries[1]+entries[2];				
				if(firstLine == false){ // First lines only contain column descriptions
					if(secondLine) {
						currentDateTime = keyDateTime;
						secondLine = false;
					}
					
					// Collect timestamp data
					if(timestampData.containsKey(keyDateTime) == true) {
						timestampData.put(keyDateTime, timestampData.get(keyDateTime)+1);
					}
					else {
						timestampData.put(keyDateTime, 1);
					}
					
					// New date+time					
					if(currentDateTime.equals(keyDateTime) == false) {
						// Add data from last date+time to collectedResponseTimesPerDateTime
						for(String responseTimeCounterKey:responseTimeCounters.keySet()) {
							if(excludedDateTimes.contains(responseTimeCounterKey) == false) {
								String connectionID = responseTimeCounterKey.split("_")[1];
								List<Integer> responseTimesTemp = new LinkedList<Integer>();
								if(collectedResponseTimesPerDateTime.containsKey(connectionID)) {
									responseTimesTemp = collectedResponseTimesPerDateTime.get(connectionID);								
								}
								responseTimesTemp.add(responseTimeCounters.get(responseTimeCounterKey));
								collectedResponseTimesPerDateTime.put(connectionID, responseTimesTemp);
							}
						}
						
						Double currentTimeStampData = timestampData.get(currentDateTime)+0.0;
						for(String connection:collectedResponseTimesPerDateTime.keySet()) {
							// Iterate over all collected response times for each connection and put the values in collectedResponseTimes
							for(int oneResponseTimes:collectedResponseTimesPerDateTime.get(connection)) {
								Double responseTime = oneResponseTimes/currentTimeStampData;
								
								if(collectedResponseTimes.containsKey(connection)) { // Append to existing entry
									List<Double> responseTimes = collectedResponseTimes.get(connection);
									responseTimes.add(responseTime);
									collectedResponseTimes.put(connection, responseTimes);
								}
								else { // New entry
									List<Double> responseTimes = new LinkedList<Double>();
									responseTimes.add(responseTime);
									collectedResponseTimes.put(connection, responseTimes);
								}								
							}							
						}
						

						for(Pair<String, Integer> pair:collectedSendingFrequenciesPerDateTime) {
							// Iterate over all collected frequencies for each connection and put the values in collectedSendingFrequencies
							String connection = pair.getLeft();
							int oneFrequency = pair.getRight();
							Double frequency = oneFrequency/currentTimeStampData;							
							if(collectedSendingFrequencies.containsKey(connection)) { // Append to existing entry
								List<Double> frequencies = collectedSendingFrequencies.get(connection);
								frequencies.add(frequency);
								collectedSendingFrequencies.put(connection, frequencies);
							}
							else { // New entry
								List<Double> frequencies = new LinkedList<Double>();
								frequencies.add(frequency);
								collectedSendingFrequencies.put(connection, frequencies);
							}	
						}						
						
						currentDateTime = keyDateTime;
						collectedResponseTimesPerDateTime = new LinkedHashMap<String, List<Integer>>();
						responseTimeCounterKeyLastSeen = new LinkedHashMap<String, Integer>();
						responseTimeCounters = new LinkedHashMap<String, Integer>();
						collectedSendingFrequenciesPerDateTime = new LinkedList<Pair<String, Integer>>();
//						sendingFrequencyCounterKeyLastSeen = new LinkedHashMap<String, Integer>();
					}
					if(excludedDateTimes.contains(keyDateTime) == false && entries[7].equals("") == false && entries[8].equals("") == false) {
						// Count numbers of lines from request to response					
						for(String id:responseTimeCounterKeyLastSeen.keySet()) {// Count up existing elements by 1
							responseTimeCounterKeyLastSeen.put(id, responseTimeCounterKeyLastSeen.get(id)+1);
						}				
						String responseTimeCounterKey = keyDateTime+entries[14]+"_"+entries[7]+"<-"+entries[8]; // + ModbusTransactionID + sourceIPAddress + destIPAddress	
						if(responseTimeCounterKeyLastSeen.containsKey(responseTimeCounterKey)) {
							// Add final count to responseTimeCounters and reset last seen value
							responseTimeCounters.put(responseTimeCounterKey, responseTimeCounterKeyLastSeen.get(responseTimeCounterKey));
							responseTimeCounterKeyLastSeen.remove(responseTimeCounterKey);
						}
						else { // Add new element
							responseTimeCounterKeyLastSeen.put(responseTimeCounterKey, 0);
						}					
						
						// Count lines until request from same connection appears again
						for(String id:sendingFrequencyCounterKeyLastSeen.keySet()) {// Count up existing elements by 1
							sendingFrequencyCounterKeyLastSeen.put(id, sendingFrequencyCounterKeyLastSeen.get(id)+1);
						}
						if(entries[13].equals("") || entries[13].endsWith("Response") == false){
							String sendingFrequencyCounterKey = entries[7]+"->"+entries[8]; // sourceIPAddress + destIPAddress
							if(sendingFrequencyCounterKeyLastSeen.containsKey(sendingFrequencyCounterKey)) {
								// Add final count to collectedSendingFrequenciesPerDateTime
								collectedSendingFrequenciesPerDateTime.add(new Pair<String, Integer>(sendingFrequencyCounterKey, sendingFrequencyCounterKeyLastSeen.get(sendingFrequencyCounterKey)));
							}
							sendingFrequencyCounterKeyLastSeen.put(sendingFrequencyCounterKey, 0);
						}
					}
				}
				else {					
					firstLine = false;
					secondLine = true;
				}
			}
			csvFileReader.close();
		}
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Data collection finished.");
		String fileNameStart = "csvStatistics_"+System.currentTimeMillis();
		FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_Packets per Timestamp");
		rFWriter.write("The packets that are sent per unique timestamp (date+time).\n");
		for(String keyDateTime:timestampData.keySet())
			rFWriter.write(keyDateTime+"\t"+timestampData.get(keyDateTime)+"\n");
		rFWriter.close();
		
		// Can be saved, but the resulting file is several GiB large
//		rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_Response Distribution per Connection");
//		rFWriter.write("Response connections and the lists of their response times.\n");
//		for(String connectionID:collectedResponseTimes.keySet())
//			rFWriter.write(connectionID+"\t"+collectedResponseTimes.get(connectionID).toString()+"\n");
//		rFWriter.close();
		
		// Process response data
		writeStatisticsData(pathToCSVDirectory+"/"+fileNameStart+"_Response Statistics", "Statistics of response connections in seconds.\nConnection \t\t\t\t\t\tMean \t\t\t\t\tStandard Deviation \t\tMinimum \t\t\t\tMaximum\n", collectedResponseTimes);
		writeDistributionData(pathToCSVDirectory+"/"+fileNameStart+"_Response Distribution Statistics","Distributions statistics of response connections in seconds.\nCan be used to draw diagrams.\nFrequency\t Value\n", collectedResponseTimes);
		
		// Process frequency data
		writeStatisticsData(pathToCSVDirectory+"/"+fileNameStart+"_Request Statistics", "Statistics of request connections in seconds.\nConnection \t\t\t\t\t\tMean \t\t\t\t\tStandard Deviation \t\tMinimum \t\t\t\tMaximum\n", collectedSendingFrequencies);
		writeDistributionData(pathToCSVDirectory+"/"+fileNameStart+"_Request Distribution Statistics","Distributions statistics of request connections in seconds.\nCan be used to draw diagrams.\nFrequency\t Value\n", collectedSendingFrequencies);
		
	}
	
	/**
	 * Processes and writes the frequency or response time data.
	 * @param path The path of the file to save to.
	 * @param firstLines The first lines of the file.
	 * @param collectedData The collected Data
	 * @throws IOException
	 */
	private static void writeStatisticsData(String path, String firstLines, LinkedHashMap<String, List<Double>> collectedData) throws IOException {
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Starting statistics data writing for: "+path);
		FileWriter rFWriter = new FileWriter(path+".txt");
		rFWriter.write(firstLines);
		int counter = 1;
		int dataSize = collectedData.size();
		for(String connectionID:collectedData.keySet()) {
			System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Statistics data "+counter+" of "+dataSize);
			counter++;
			
			List<Double> responseTimes = collectedData.get(connectionID);
			Collections.sort(responseTimes);
			int numberOfResponseTimes = responseTimes.size();
			Double minimum = responseTimes.get(0);
			Double maximum = responseTimes.get(numberOfResponseTimes-1);
			Double mean = 0.0;
			Double standardDeviaton = 0.0;
			for(Double responseTime:responseTimes) {
				mean += responseTime;
			}
			mean = mean/numberOfResponseTimes;
			
			for(Double responseTime:responseTimes) {
				standardDeviaton += (responseTime-mean)*(responseTime-mean);
			}			
			standardDeviaton = standardDeviaton/numberOfResponseTimes;
			
			String formattedNumbers = "\t"+String.format("%.20f",mean)+"\t"+String.format("%.20f",standardDeviaton)+"\t"+String.format("%.20f",minimum)+"\t"+String.format("%.20f",maximum);
			rFWriter.write(connectionID+"\t"+formattedNumbers+"\n");
		}
		rFWriter.close();
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Finished statistics data writing for: "+path);
	}
	
	/**
	 * Processes and writes the frequency or response time distribution data.
	 * @param path The path of the file to save to.
	 * @param firstLines The first lines of the file.
	 * @param collectedData The collected Data
	 * @throws IOException
	 */
	private static void writeDistributionData(String path, String firstLines, LinkedHashMap<String, List<Double>> collectedData) throws IOException {
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Starting distribution data writing for: "+path);
		FileWriter rFWriter = new FileWriter(path+".txt");
		rFWriter.write(firstLines);
		int counter = 1;
		int dataSize = collectedData.size();
		for(String connectionID:collectedData.keySet()) {
			System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Distribution data "+counter+" of "+dataSize);
			counter++;
			
			rFWriter.write("\n\n\n"+connectionID+"\n\n");
			List<Double> responseTimes = collectedData.get(connectionID);
			Collections.sort(responseTimes);
			
			LinkedHashMap<Double, Integer> frequencies = new LinkedHashMap<Double, Integer>();
			for(Double responseTime:responseTimes) {
				if(frequencies.containsKey(responseTime) == false) {
					frequencies.put(responseTime, Collections.frequency(responseTimes, responseTime));
				}
			}
			for(Double responseTime:frequencies.keySet()) {
				int frequency = frequencies.get(responseTime);
				rFWriter.write(frequency+"\t"+String.format("%.6f",responseTime)+"\n");
			}
		}
		rFWriter.close();
		System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+" Finished distribution data writing for: "+path);
	}
	
	
	/**
	 * Analyzes the statistics of the training set with respect to the timestamps.
	 * @throws IOException
	 */
	private static void executeOldStatisticAnalysis() throws IOException {
		// The response times for a connection
		LinkedHashMap<String, List<Integer>> collectedResponseTimes = new LinkedHashMap<String, List<Integer>>();
		LinkedHashMap<String, Integer> lastResponseTimeValue = new LinkedHashMap<String, Integer>();
		
		// The sending intervals for a connection
		LinkedHashMap<String, List<Integer>> collectedSendingTimes = new LinkedHashMap<String, List<Integer>>();
		LinkedHashMap<String, Integer> lastSendingTimeValue = new LinkedHashMap<String, Integer>();
		
		// Read all csv files from folder
		List<String> csvFiles = new ArrayList<String>();		
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}		
		
		// Process csv files
		for(String filename:csvFiles){
			// Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			while (csvFileReader.hasNextLine() == true) {				
				if(firstLine == false){ // First lines only contain column descriptions
					String csvLine = csvFileReader.nextLine();
					String[] entries = csvLine.split(",");
					String modbusFunctionDescription = entries[13];
					// Only interested in Read Tag Service requests and responses
					if(modbusFunctionDescription.startsWith("Read Tag Service") == true) {
						// Source IP address + destination IP address + Modbus function description
						String connectionIdentifier = "Source: "+entries[7]+"\tDestination: "+entries[8];
						String modbusTransactionID = entries[14];
						String lastResponseTimeValueKey = connectionIdentifier+"\tModbus Transaction ID: "+modbusTransactionID;
						try {
						    int timeInSeconds = parseSVCTime(entries[2]);
						    
						    // Requests
							if(modbusFunctionDescription.equals("Read Tag Service")) {
								if(lastSendingTimeValue.containsKey(connectionIdentifier)) {
									int timeDifference = timeInSeconds-lastSendingTimeValue.get(connectionIdentifier);
									List<Integer> sendingTimes;
									lastSendingTimeValue.remove(connectionIdentifier);
									
									if(collectedSendingTimes.containsKey(connectionIdentifier)) {
										sendingTimes = collectedSendingTimes.get(connectionIdentifier);
										
									}
									else {
										sendingTimes = new LinkedList<Integer>();					
									}
									sendingTimes.add(timeDifference);
									collectedSendingTimes.put(connectionIdentifier, sendingTimes);
								}
								lastResponseTimeValue.put(lastResponseTimeValueKey, timeInSeconds);
								lastSendingTimeValue.put(connectionIdentifier, timeInSeconds);
							}
							else if(modbusFunctionDescription.equals("Read Tag Service - Response")) {
								// Add response time to collectedResponseTimes
								if(lastResponseTimeValue.containsKey(lastResponseTimeValueKey)) {
									int responseTime = timeInSeconds-lastResponseTimeValue.get(lastResponseTimeValueKey);
									List<Integer> responseTimes;
									lastResponseTimeValue.remove(lastResponseTimeValueKey);
									
									if(collectedResponseTimes.containsKey(connectionIdentifier)) {
										responseTimes = collectedResponseTimes.get(connectionIdentifier);
									}
									else {
										responseTimes = new LinkedList<Integer>();					
									}
									responseTimes.add(responseTime);
									collectedResponseTimes.put(connectionIdentifier, responseTimes);								
								}
							}						    
						} catch (NumberFormatException e) {	
							System.out.println("NumberFormatException in "+filename+" at number "+entries[0]);
						}
					}
				}
				firstLine = false;		
			}
			csvFileReader.close();
		}
		
		writeOutputOld(collectedResponseTimes, "Response Times", "Response times for Read Tag packets with respect to connections:");
		writeOutputOld(collectedSendingTimes, "Sending Times", "Sending times for Read Tag packets with respect to connections");
	}
	
	/**
	 * Parses the time in the string to seconds.
	 * @param SVCTime The string to parse.
	 * @return The parsed value in seconds
	 */
	private static int parseSVCTime(String String) {
		String[] timeParts = String.split(":");		
		return Integer.parseInt(timeParts[0])*60*60+Integer.parseInt(timeParts[1])*60+Integer.parseInt(timeParts[2]);
	}
	
	/**
	 * Write the output to a file.
	 * @param outputData The data to be written.
	 * @param fileNameEnd The ending of the file name.
	 * @param title The first line of the file.
	 * @throws IOException
	 */
	private static void writeOutputOld(LinkedHashMap<String, List<Integer>> outputData, String fileNameEnd, String title) throws IOException {
		String fileNameStart = "csvStatistics_"+System.currentTimeMillis();	
		String outputString;
		List<Integer> responseTimes;
		int minimum;
		int maximum;
		double average;
		HashMap<Integer, Integer> occurrences;
		String distributions;
		double standardDeviation;
		FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+fileNameEnd);
		rFWriter.write(title+"\n");
		for(String str:outputData.keySet()) {
			outputString = str;
			responseTimes = outputData.get(str);
			Collections.sort(responseTimes);
			minimum = responseTimes.get(0);
			maximum = responseTimes.get(responseTimes.size()-1);
			outputString += "\tMinimum: "+minimum+"\tMaximum: "+maximum;
			
			occurrences = new HashMap<Integer, Integer>();
			average = 0;
			for(int i:responseTimes) {
				average += i;
				if(occurrences.containsKey(i) == false)
					occurrences.put(i, 1);
				else {
					occurrences.put(i, occurrences.get(i)+1);
				}
			}
			average = average/responseTimes.size();
			outputString += "\tMean: "+average;

			standardDeviation = 0.0;
			distributions = "\tDistributions:";
			for(int j:occurrences.keySet()) {
				standardDeviation += (j-average)*(j-average);
				distributions += " "+j+"->"+occurrences.get(j)+"/"+responseTimes.size();
			}
			standardDeviation = standardDeviation/responseTimes.size();
			outputString += "\tStandard Deviation: "+standardDeviation;
			outputString += distributions;
			rFWriter.write(outputString+"\n");
		}
	    rFWriter.close();
	}
	
	/**
	 * Experimental parallelized version.
	 * @return
	 * @throws IOException
	 */
	private static void executeParallelizedVersion() throws IOException {
		//Read all csv files from folder
		List<String> csvFiles = new ArrayList<String>();		
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(pathToCSVDirectory+"/"+file.getName());
		    }
		}
		double fileNumber = csvFiles.size();
		int filesPerThread = (int) Math.ceil(fileNumber/threadNumber);
		int filesForLastThread = (int) fileNumber-filesPerThread*(threadNumber-1);
		
		ExecutorService parallelExecution = Executors.newFixedThreadPool(threadNumber);
		List<String> filePathsList;
		if(fileNumber >= threadNumber) {
			for(int i = 0; i < threadNumber; i++) {
				if(i < threadNumber-1)
					filePathsList = csvFiles.subList(i*filesPerThread, (i+1)*filesPerThread);
				else 
					filePathsList = csvFiles.subList(i*filesPerThread, i*filesPerThread+filesForLastThread);
	
				// Execute each list in a different thread
				parallelExecution.execute(new dataProcessor(filePathsList));
			}
		}
		else
			parallelExecution.execute(new dataProcessor(csvFiles));
		parallelExecution.shutdown();
		while(parallelExecution.isTerminated() == false) {}
		
		// Combine HashMaps
		LinkedHashMap<String, Integer> processedLines = new LinkedHashMap<String, Integer>();
		LinkedHashMap<String, Integer> timestampLinesFinal = new LinkedHashMap<String, Integer>();
		for(LinkedHashMap<String, Integer> timestampLines:timestampLinesQueue) {			
			for(String keyDateTime:timestampLines.keySet()) {
				int timestampLineCount = timestampLines.get(keyDateTime);
				if(processedLines.containsKey(keyDateTime) && processedLines.get(keyDateTime) == timestampLineCount) {					
				} // Prevent duplicates				
				else {
					processedLines.put(keyDateTime, timestampLineCount);
					if(timestampLinesFinal.containsKey(keyDateTime))
						timestampLinesFinal.put(keyDateTime, timestampLinesFinal.get(keyDateTime)+timestampLineCount);
					else
						timestampLinesFinal.put(keyDateTime, timestampLineCount);
				}
			}
		}
		
		String fileNameStart = "csvStatistics_"+System.currentTimeMillis();
		FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_Packets per Timestamp");
		rFWriter.write("The packets that are sent per unique timestamp (date+time).\n");
		for(String keyDateTime:timestampLinesFinal.keySet())
			rFWriter.write(keyDateTime+"\t"+timestampLinesFinal.get(keyDateTime)+"\n");
		rFWriter.close();
	}
}


/**
 * Class to parallelize data processing.
 */
class dataProcessor implements Runnable{
	/**
	 * The list of file paths.
	 */
	private List<String> filePathsList;
	
	/**
	 * The key is the concatenation of date and time, value is the number of lines with this date and time.
	 */
	private LinkedHashMap<String, Integer> timestampLines = new LinkedHashMap<String, Integer>();
	
	/**
	 * Constructor
	 * @param filePathsList The list of file paths.
	 */	
	dataProcessor(List<String> filePathsList){
		this.filePathsList = filePathsList;
	}
	@Override
	public void run() {
		for(String filePath:filePathsList) {
			File csvFile = new File(filePath);
			Scanner csvFileReader;
			try {
				csvFileReader = new Scanner(csvFile);
				while (csvFileReader.hasNextLine() == true) {
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");	
						String keyDateTime = entries[1]+entries[2];
						if(keyDateTime.equals("datetime") == false) {
							if(timestampLines.containsKey(keyDateTime))
									timestampLines.put(keyDateTime, timestampLines.get(keyDateTime)+1);
							else
								timestampLines.put(keyDateTime, 1);	
						}
				}
				csvFileReader.close();
				SWaTNetworkTrafficStatistics.timestampLinesQueue.add(timestampLines);
			} catch (FileNotFoundException e) {
				System.out.println("File not found: "+filePath);
			}
		}		
	}	
}