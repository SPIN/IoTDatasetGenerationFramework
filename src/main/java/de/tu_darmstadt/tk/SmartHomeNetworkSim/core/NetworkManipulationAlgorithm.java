package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;

/**
 * Interface for programs/algorithms/manipulations/optimizations which could run during each
 * simulation step. The Main Controller which is an argument is quite powerful and
 * allows full access to the whole model, simulation, parts of the GUI and
 * should be used wisely.<br>
 * The runAlgorithm method will be called at the end of each simulation cycle,
 * if imported and added to the simulation.<br>
 * Packets which were sent during the last simulation step can be accessed by using {@link Controller#getNetworkController()},{@link NetworkController#getLinks()},
 * {@link Link#getPackets()} for each link.<br>
 * For further accesses to the different components, check the javaDoc, or traverse the controller.
 *
 * @author Andreas T. Meyer-Berg
 */
public interface NetworkManipulationAlgorithm {

	/**
	 * 
	 * @param controller
	 *            Main controller of the framework, access to the network model,
	 *            settings, simulation etc.
	 * @param currentTimeStep
	 *            current time in the simulation
	 */
	public void runAlgorithm(Controller controller, long currentTimeStep);
}
