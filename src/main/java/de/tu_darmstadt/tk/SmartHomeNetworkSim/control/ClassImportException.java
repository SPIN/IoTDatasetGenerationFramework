package de.tu_darmstadt.tk.SmartHomeNetworkSim.control;

/**
 * Import Exception to be thrown if Exceptions occur during class import.
 * 
 * @author Andreas T. Meyer-Berg
 */
public class ClassImportException extends Exception {

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = -6450062037053760733L;
	
	/**
	 * Creates a new exception which contains a message for the
	 * problem which occurred during class import
	 * 
	 * @param msg Exception message which is source of the error
	 */
	public ClassImportException(String msg) {
		super(msg);
	}

}
