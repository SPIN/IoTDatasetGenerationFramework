package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Window for the import of physical data from SWaT dataset A1.
 * @author Fabian Kaiser
 */
public class SWaTImportPhysicalDataWindow extends JDialog {
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 1452366212344321L;
	
	/**
	 * The network controller.
	 */
	private NetworkController networkController;
	
	/**
	 * The import button.
	 */
	private JButton importButton = new JButton("Import");
	
	/**
	 * List of positions of floating point values in a physical data row.
	 */
	private LinkedList<Integer> floatingPointPositions = new LinkedList<Integer>();
	
	/**
	 * List of positions of pumps and UV values in a physical data row.
	 */
	private LinkedList<Integer> onOffPositions = new LinkedList<Integer>();
	
	/**
	 * List of positions of motorized valves values in a physical data row.
	 */
	private LinkedList<Integer> onTransitionOffPositions = new LinkedList<Integer>();
	
	/**
	 * A Hashmap with the position of a device as key and its name as value.
	 */
	private LinkedHashMap<Integer, String> deviceNamePositionMap = new  LinkedHashMap<Integer, String>();
	
	/**
	 * The label for the import status.
	 */
	private JLabel importStatus = new JLabel("");
	
	/**
	 * Constructor.
	 * @param controller The controller of the model.
	 */
	public SWaTImportPhysicalDataWindow(Controller controller) {
		networkController = controller.getNetworkController();
		createWindow();
		this.setModal(true);
		
		floatingPointPositions.add(1);
		floatingPointPositions.add(2);
		floatingPointPositions.add(6);
		floatingPointPositions.add(7);
		floatingPointPositions.add(8);
		floatingPointPositions.add(9);
		floatingPointPositions.add(17);
		floatingPointPositions.add(18);
		floatingPointPositions.add(19);
		floatingPointPositions.add(26);
		floatingPointPositions.add(27);
		floatingPointPositions.add(28);
		floatingPointPositions.add(29);
		floatingPointPositions.add(35);
		floatingPointPositions.add(36);
		floatingPointPositions.add(37);
		floatingPointPositions.add(38);
		floatingPointPositions.add(39);
		floatingPointPositions.add(40);
		floatingPointPositions.add(41);
		floatingPointPositions.add(42);	
		floatingPointPositions.add(45);
		floatingPointPositions.add(46);
		floatingPointPositions.add(47);
		floatingPointPositions.add(48);
		
		onOffPositions.add(4);
		onOffPositions.add(5);
		onOffPositions.add(11);
		onOffPositions.add(12);
		onOffPositions.add(13);
		onOffPositions.add(14);
		onOffPositions.add(15);
		onOffPositions.add(16);
		onOffPositions.add(24);
		onOffPositions.add(25);
		onOffPositions.add(30);
		onOffPositions.add(31);
		onOffPositions.add(32);
		onOffPositions.add(33);
		onOffPositions.add(34);
		onOffPositions.add(43);
		onOffPositions.add(44);
		onOffPositions.add(49);
		onOffPositions.add(50);
		onOffPositions.add(51);
		
		onTransitionOffPositions.add(3);
		onTransitionOffPositions.add(10);
		onTransitionOffPositions.add(20);
		onTransitionOffPositions.add(21);
		onTransitionOffPositions.add(22);
		onTransitionOffPositions.add(23);
		
		deviceNamePositionMap.put(1,"FIT-101");
		deviceNamePositionMap.put(2,"LIT-101");
		deviceNamePositionMap.put(3,"MV-101");
		deviceNamePositionMap.put(4,"P-101");
		deviceNamePositionMap.put(5,"P-102");
		deviceNamePositionMap.put(6,"AIT-201");
		deviceNamePositionMap.put(7,"AIT-202");
		deviceNamePositionMap.put(8,"AIT-203");
		deviceNamePositionMap.put(9,"FIT-201");
		deviceNamePositionMap.put(10,"MV-201");
		deviceNamePositionMap.put(11,"P-201");
		deviceNamePositionMap.put(12,"P-202");
		deviceNamePositionMap.put(13,"P-203");
		deviceNamePositionMap.put(14,"P-204");
		deviceNamePositionMap.put(15,"P-205");
		deviceNamePositionMap.put(16,"P-206");
		deviceNamePositionMap.put(17,"DPIT-301");
		deviceNamePositionMap.put(18,"FIT-301");
		deviceNamePositionMap.put(19,"LIT-301");
		deviceNamePositionMap.put(20,"MV-301");
		deviceNamePositionMap.put(21,"MV-302");
		deviceNamePositionMap.put(22,"MV-303");
		deviceNamePositionMap.put(23,"MV-304");
		deviceNamePositionMap.put(24,"P-301");
		deviceNamePositionMap.put(25,"P-302");
		deviceNamePositionMap.put(26,"AIT-401");
		deviceNamePositionMap.put(27,"AIT-402");
		deviceNamePositionMap.put(28,"FIT-401");
		deviceNamePositionMap.put(29,"LIT-401");
		deviceNamePositionMap.put(30,"P-401");
		deviceNamePositionMap.put(31,"P-402");
		deviceNamePositionMap.put(32,"P-403");
		deviceNamePositionMap.put(33,"P-404");
		deviceNamePositionMap.put(34,"UV-401");
		deviceNamePositionMap.put(35,"AIT-501");
		deviceNamePositionMap.put(36,"AIT-502");
		deviceNamePositionMap.put(37,"AIT-503");
		deviceNamePositionMap.put(38,"AIT-504");
		deviceNamePositionMap.put(39,"FIT-501");
		deviceNamePositionMap.put(40,"FIT-502");
		deviceNamePositionMap.put(41,"FIT-503");
		deviceNamePositionMap.put(42,"FIT-504");
		deviceNamePositionMap.put(43,"P-501");
		deviceNamePositionMap.put(44,"P-502");
		deviceNamePositionMap.put(45,"PIT-501");
		deviceNamePositionMap.put(46,"PIT-502");
		deviceNamePositionMap.put(47,"PIT-503");
		deviceNamePositionMap.put(48,"FIT-601");
		deviceNamePositionMap.put(49,"P-601");
		deviceNamePositionMap.put(50,"P-602");
		deviceNamePositionMap.put(51,"P-603");
	}

	/**
	 * Creates the window.
	 */
	private void createWindow() {
		getContentPane().setMinimumSize(new Dimension(640, 320));
		setBounds(new Rectangle(0, 0, 640, 320));
		setResizable(false);
		setTitle("Import Physical Data");
		setIconImage(Utility.loadFile("images/IoTDGF_square.png"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		
		String usageDescriptionText = 
				"<html>"
				+"Physical data from dataset A1 may be imported.<br/>"
				+"Select a row containing data in one of the three files SWaT_Dataset_Normal_v0.xlsx, SWaT_Dataset_Normal_v1.xlsx, "
				+"or SWaT_Dataset_Attack_v0.xlsx. Copy the row and paste it in the field below without changing anything. "
				+"Click on the import button.<br/>"
				+"Points or commas are detected as radix character to separate decimals.<br/>"
				+"Timestamp and label (normal or attack) are ignored."
				+"</html>";
		JLabel usageDescription = new JLabel(usageDescriptionText);
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.PAGE_START;
		gridBagConstraints.insets = new Insets(0,5,5,5);
		getContentPane().add(usageDescription, gridBagConstraints); 
		
		JTextArea importText = new JTextArea();
		importText.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		importText.setLineWrap(true);
		importText.setWrapStyleWord(true);
		importText.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				importStatus.setText("");
				if(checkImportText(importText.getText()) == false) {
					importText.setBackground(Color.RED);
					importText.setBorder(BorderFactory.createLineBorder(Color.GRAY));
					importButton.setEnabled(false);
				}
				else {
					importText.setBackground(null);
					importText.setBorder(BorderFactory.createLineBorder(Color.GREEN));
					importButton.setEnabled(true);
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.weightx = 0.9;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.PAGE_START;
		getContentPane().add(importText, gridBagConstraints); 
		
		
		importButton.setEnabled(false);
		importButton.addActionListener(a->{
			importData(importText.getText());
			importStatus.setText("Import successful");
		});
		getContentPane().add(importButton);
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.weightx = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		getContentPane().add(importButton, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		getContentPane().add(importStatus, gridBagConstraints); 
	}
	
	/**
	 * Checks general parameters of the import text area.
	 * @param importText The import text.
	 * @return True if the the text is generally correct.
	 */
	private boolean checkImportText(String importText) {
		importText = importText.trim();
		importText = importText.replace(",", ".");
		String[] values = importText.split("\t");
		
		if(values.length != 53) return false;
		if(Pattern.matches("\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}\\s[AP]M", values[0]) == false) return false;
		String value;
		for(int i = 1; i < 52; i++) {
			value = values[i];
			if(floatingPointPositions.contains(i)) {
				try {
					Double.parseDouble(value);
				}
				catch (Exception e){
					return false;
				}
				// && Pattern.matches("[\\d]+[\\.,]{0,1}[\\d]*", values[i]) == false) return false;
			}
			else if(onOffPositions.contains(i) && Pattern.matches("[12]", value) == false) return false;
			else if(onTransitionOffPositions.contains(i) && Pattern.matches("[012]", value) == false) return false;
		}
		String lastValue = values[52];
		if(lastValue.equals("Normal") == false && lastValue.equals("Attack") == false) return false;			
		return true;
	}
	
	/**
	 * Saves the data as standard status of the IoT devices.
	 * @param importText The import text.
	 */
	private void importData(String importText) {
		importText = importText.trim();
		importText = importText.replace(",", ".");
		String[] values = importText.split("\t");
		
		for(int i = 1; i < 52; i++) {
			SWaTDevice currentDevice = ((SWaTDevice) SWaTUtilities.getDeviceByName(deviceNamePositionMap.get(i), networkController));
			if(currentDevice == null) continue;
			LinkedHashMap<String, String> currentDeviceData = currentDevice.getDeviceData();			
			
			if(floatingPointPositions.contains(i)) { // Sensors
				currentDeviceData.put("value", SWaTUtilities.correctExponentialFormat(values[i]));
			}
			else if(onOffPositions.contains(i) || onTransitionOffPositions.contains(i)){ // Actuators
				currentDeviceData.put("status", values[i]);
			}
			
			currentDevice.setDeviceData(currentDeviceData);			
		}		
	}
}
