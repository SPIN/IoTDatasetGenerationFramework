package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Scanner;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTDeviceData;

/**
 * Additional functionalities for the devices of SWaT.
 * @author Fabian Kaiser
 *
 */
public class SWaTDevice extends SmartDevice{

	/**
	 * The type of device {@link SWaTDeviceTypes}. 
	 */	
	private int deviceType;	

	/**
	 * The height to width ratio for the graphical representation of a rectangular element.
	 * Height = Width*drawRatio.
	 */	
	private double drawRatio;
	
	/**
	 * The data of the device.
	 */
	private LinkedHashMap<String, String> deviceData;
	
	/**
	 * The standard values for the data of the device.
	 */
	private LinkedHashMap<String, String> deviceDataStandard;	
	
	/**
	 * The name of this device.
	 */
	private String deviceName;
		
	/**
	 * The path of the user data file.
	 */
	private static final String userDataFilePath = "src/main/resources/SWaT/userData.SWaT";
	
	/**
	 * Constructor.
	 * @param name The name of the device. 
	 * @param deviceType The type of device {@link SWaTDeviceTypes}.
	 * @param drawRatio The height to width ratio for the graphical representation of this element. Height = Width*drawRatio.
	 */
	public SWaTDevice(String name, int deviceType, double drawRatio) {
		super(name);
		this.deviceName = name;
		this.deviceType = deviceType;
		this.drawRatio = drawRatio;
		SWaTDeviceData deviceDataLocal = new SWaTDeviceData(name);
		this.deviceData = deviceDataLocal.getDeviceData();
		this.deviceDataStandard = deviceDataLocal.getDeviceData();
		
		loadUserData();
	}
	
	/**
	 * Gets the device type.
	 * @return The type of device {@link #SWaTDeviceTypes}.
	 */
	public int getDeviceType() {
		return deviceType;
	}

	/**
	 * Sets the device type.
	 * @param deviceType The type of device {@link #SWaTDeviceTypes}.
	 */
	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * Gets the draw ratio.
	 * @return The height to width ratio for the graphical representation of this element. Height = Width*drawRatio.
	 */
	public double getDrawRatio() {
		return drawRatio;
	}

	/**
	 * Sets the draw ratio.
	 * @param drawRatio The height to width ratio for the graphical representation of this element. Height = Width*drawRatio.
	 */
	public void setDrawRatio(long drawRatio) {
		this.drawRatio = drawRatio;
	}

	/**
	 * Returns the data of this device.
	 * @return The data of this device.
	 */
	public LinkedHashMap<String, String> getDeviceData() {
		return deviceData;
	}
	
	/**
	 * Sets the data of this device.
	 * @param deviceData The data to be set.
	 */
	public void setDeviceData(LinkedHashMap<String, String> deviceData) {
		this.deviceData = deviceData;
		saveUserData();
	}
	
	/**
	 * Appends the key and value as entry to the device data.
	 * @param key The key for the device data Hashmap.
	 * @param value The value for the device data Hashmap.
	 */
	protected void appendToDeviceData(String key, String value) {
		deviceData.put(key, value);
	}
	
	/**
	 * Appends the key and value as entry to the device data.
	 * @param key The key for the device data Hashmap.
	 * @param value The value for the device data Hashmap.
	 */
	protected void appendToDeviceDataStandard(String key, String value) {
		deviceDataStandard.put(key, value);
	}
	
	/**
	 * Returns the standard values for the data of this device.
	 * @return The standard values for the data of this device.
	 */
	public LinkedHashMap<String, String> getDeviceDataStandard() {
		return deviceDataStandard;
	}
	
	/**
	 * Saves the user data of this device.
	 */
	private void saveUserData(){	
		File userDataFile = new File(userDataFilePath);

		if(userDataFile.isFile()) {
			try {
				String userDataString = "";
				boolean firstLine = true;
				for(String key:deviceData.keySet()) {
					if(firstLine == false)
						userDataString+=";";
					firstLine = false;
					
					userDataString+=key+"#"+deviceData.get(key);
				}
				
				LinkedHashMap<String, String> userData = new LinkedHashMap<String, String>();
								
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] entries = csvLine.split(",");
					userData.put(entries[0], entries[1]);						
				}
				userDataFileReader.close();	
				userData.put(deviceName,userDataString);
				
			    FileWriter userDataWriter;
				try {
					userDataWriter = new FileWriter(userDataFile);
					firstLine = true;
				    for(String key:userData.keySet()) {
						if(firstLine == false)
							userDataWriter.write("\n");
						firstLine = false;
						userDataWriter.write(key+","+userData.get(key));	    	
				    } 
				    userDataWriter.close();
				} catch (IOException e) {
					System.out.println("Cannot write to user data file.");
				}
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
	}
	
	/**
	 * Loads user data of this device from the file.
	 */
	private void loadUserData(){	
		File userDataFile = new File(userDataFilePath);

		if(userDataFile.isFile()) {
			try {			
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] deviceEntries = csvLine.split(",");
					if(deviceEntries[0].equals(deviceName)){							
						String[] userDataListEntries = deviceEntries[1].split(";");
						for(String str:userDataListEntries) {
							String[] userDataEntries = str.split("#");
							if(userDataEntries.length == 2)
								deviceData.put(userDataEntries[0], userDataEntries[1]);
						}
					}						
				}
				userDataFileReader.close();	
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
	}
}
