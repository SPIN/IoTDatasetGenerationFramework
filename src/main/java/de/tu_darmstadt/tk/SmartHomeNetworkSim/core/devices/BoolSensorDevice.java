package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;


/**
 * A simple boolean sensor, which will generate float reading within the given bounds.
 *
 * @author Andreas T. Meyer-Berg
 */
public class BoolSensorDevice extends SmartDevice implements BoolSensor {

	/**
	 * Current value
	 */
	private boolean val;

	
	/**
	 * Name of the sensor reading
	 */
	private String infoName;
	
	/**
	 * Initialize the Sensor
	 */
	public BoolSensorDevice() {
		super();
		init();
	}
	/**
	 * Create a new Sensor with the given name
	 * @param name
	 */
	public BoolSensorDevice(String name) {
		super(name);
		init();
	}
	
	private void init(){
		val = false;
		infoName = "doorOpen";
	}
	
	@Override
	public void simulateTimeStep(long startTime, long duration) {
		boolean newVal = !val;
		val = newVal;
	}
	@Override
	public void setBSinfoName(String name) {
		this.infoName = name;
		
	}
	@Override
	public String getBSinfoName() {
		return infoName;
	}
	@Override
	public boolean getBSval() {
		return val;
	}
	@Override
	public void setBSval(boolean val) {
		this.val = val;
	}

}
