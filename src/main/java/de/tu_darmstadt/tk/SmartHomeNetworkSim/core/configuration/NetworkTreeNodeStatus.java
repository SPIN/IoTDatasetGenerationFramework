package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;

/**
 * Status of node in the NetworkTree, stores information like if it is visible, expanded and which object is represented
 *
 * @author Andreas T. Meyer-Berg
 */
public class NetworkTreeNodeStatus {
	
	/**
	 * Is the TreeNode expanded
	 */
	private boolean expanded;
	
	/**
	 * Is the object visible or hidden
	 */
	private boolean visible;
	
	/**
	 * Object which is represented
	 */
	private Object nodeObject;
	
	/**
	 * Creates a new NetorkTreeNodeStatus for the object o, which is visible and not expanded 
	 * @param o Object for the new status
	 */
	public NetworkTreeNodeStatus(Object o) {
		nodeObject = o;
		visible = true;
		expanded = false;
	}

	/**
	 * @return the expanded
	 */
	public boolean isExpanded() {
		return expanded;
	}

	/**
	 * @param expanded the expanded to set
	 */
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the nodeObject
	 */
	public Object getNodeObject() {
		return nodeObject;
	}

	/**
	 * @param nodeObject the nodeObject to set
	 */
	public void setNodeObject(Object nodeObject) {
		this.nodeObject = nodeObject;
	}
}
