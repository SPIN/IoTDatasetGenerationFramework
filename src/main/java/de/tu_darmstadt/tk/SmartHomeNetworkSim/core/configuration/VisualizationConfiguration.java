package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;

/**
 * Stores the configuration details of the visualization
 *
 * @author Andreas T. Meyer-Berg
 */
public class VisualizationConfiguration {

	/**
	 * Whether connections should be shown
	 */
	private boolean showConnections;

	/**
	 * Whether links should be shown
	 */
	private boolean showLinks;
	/**
	 * Whether link toolTips should be shown
	 */
	private boolean showLinkToolTips;

	/**
	 * Whether the link names should be presented on the left hand side of the
	 * visualization panel
	 */
	private boolean showLinkNameList;

	/**
	 * Whether SmartDevices should be shown
	 */
	private boolean showSmartDevices;

	/**
	 * Whether SmartDevice names should be shown
	 */
	private boolean showSmartDeviceNames;

	/**
	 * Whether terminated connections should be shown
	 */
	private boolean showTerminatedConnections;

	/**
	 * Radius (including the middlePoinnt) of a smartDevice
	 */
	private int deviceVisualizationRadius;

	/**
	 * Radius of the link. (deviceRadius+linkRadius)
	 */
	private int linkRadius;
	
	/**
	 * True if debug information should be shown
	 */
	private boolean debugModus = false;

	/**
	 * Initialize the configuration
	 */
	public VisualizationConfiguration() {
		setShowConnections(true);
		setShowLinks(true);
		setShowSmartDevices(true);
		setShowSmartDeviceNames(true);
		setShowTerminatedConnections(false);
		setDeviceVisualizationRadius(25);
		setShowLinkToolTips(true);
		setShowLinkNameList(true);
		setLinkRadius(10);
	}

	/**
	 * @return the showConnections
	 */
	public boolean isShowConnections() {
		return showConnections;
	}

	/**
	 * @param showConnections
	 *            the showConnections to set
	 */
	public void setShowConnections(boolean showConnections) {
		this.showConnections = showConnections;
	}

	/**
	 * @return the showLinks
	 */
	public boolean isShowLinks() {
		return showLinks;
	}

	/**
	 * @param showLinks
	 *            the showLinks to set
	 */
	public void setShowLinks(boolean showLinks) {
		this.showLinks = showLinks;
	}

	/**
	 * @return the showSmartDevices
	 */
	public boolean isShowSmartDevices() {
		return showSmartDevices;
	}

	/**
	 * @param showSmartDevices
	 *            the showSmartDevices to set
	 */
	public void setShowSmartDevices(boolean showSmartDevices) {
		this.showSmartDevices = showSmartDevices;
	}

	/**
	 * @return the showSmartDeviceNames
	 */
	public boolean isShowSmartDeviceNames() {
		return showSmartDeviceNames;
	}

	/**
	 * @param showSmartDeviceNames
	 *            the showSmartDeviceNames to set
	 */
	public void setShowSmartDeviceNames(boolean showSmartDeviceNames) {
		this.showSmartDeviceNames = showSmartDeviceNames;
	}

	/**
	 * @return the showTerminatedConnections
	 */
	public boolean isShowTerminatedConnections() {
		return showTerminatedConnections;
	}

	/**
	 * @param showTerminatedConnections
	 *            the showTerminatedConnections to set
	 */
	public void setShowTerminatedConnections(boolean showTerminatedConnections) {
		this.showTerminatedConnections = showTerminatedConnections;
	}

	/**
	 * @return the deviceVisualizationRadius
	 */
	public int getDeviceVisualizationRadius() {
		return deviceVisualizationRadius;
	}

	/**
	 * @param deviceVisualizationRadius
	 *            the deviceVisualizationRadius to set
	 */
	public void setDeviceVisualizationRadius(int deviceVisualizationRadius) {
		this.deviceVisualizationRadius = deviceVisualizationRadius;
	}

	/**
	 * @return the linkRadius
	 */
	public int getLinkRadius() {
		return linkRadius;
	}

	/**
	 * @param linkRadius
	 *            the linkRadius to set
	 */
	public void setLinkRadius(int linkRadius) {
		this.linkRadius = linkRadius;
	}

	/**
	 * @return the showLinkToolTips
	 */
	public boolean isShowLinkToolTips() {
		return showLinkToolTips;
	}

	/**
	 * @param showLinkToolTips
	 *            the showLinkToolTips to set
	 */
	public void setShowLinkToolTips(boolean showLinkToolTips) {
		this.showLinkToolTips = showLinkToolTips;
	}

	/**
	 * @return the showLinkNameList
	 */
	public boolean isShowLinkNameList() {
		return showLinkNameList;
	}

	/**
	 * @param showLinkNameList the showLinkNameList to set
	 */
	public void setShowLinkNameList(boolean showLinkNameList) {
		this.showLinkNameList = showLinkNameList;
	}

	/**
	 * @return the debugModus
	 */
	public boolean isDebugModus() {
		return debugModus;
	}

	/**
	 * @param debugModus the debugModus to set
	 */
	public void setDebugModus(boolean debugModus) {
		this.debugModus = debugModus;
	}

}
