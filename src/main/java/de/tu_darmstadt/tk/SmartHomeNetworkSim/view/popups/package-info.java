/**
 * Popups, panels and configuration frames for the SmartHomeNetworkSim
 * 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;