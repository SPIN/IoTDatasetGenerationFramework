package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import java.util.LinkedList;
import java.util.List;

/**
 * Model of an SmartDevice, which keeps track of its main attributes, like name,
 * position, links and connections
 * 
 * @author Andreas T. Meyer-Berg
 */
public class SmartDevice {
	
	/**
	 * Label
	 */
	protected short label = 0;
	
	/**
	 * Name of the Device
	 */
	private String name;
	
	/**
	 * Physical connections to other SmartDevices
	 */
	private List<Link> links;

	/**
	 * Ports of this device
	 */
	private List<Port> ports;
	
	/**
	 * Creates a new SmartDevice without links
	 * 
	 * @param name
	 *            name of the device
	 */
	public SmartDevice(String name) {
		this.name = name;
		links = new LinkedList<Link>();
		ports = new LinkedList<Port>();
	}
	
	/**
	 * Creates a new SmartDevice without links
	 */
	public SmartDevice() {
		this.name = "Unnamed";
		links = new LinkedList<Link>();
		ports = new LinkedList<Port>();
	}

	/**
	 * Position on the x-Axis
	 */
	private int x;

	/**
	 * Position on the y-Axis
	 */
	private int y;

	/**
	 * Position on the z-Axis
	 */
	private int z;

	/**
	 * Returns the {@link SmartDevice#name}
	 * 
	 * @return {@link SmartDevice#name}
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the {@link SmartDevice#name}
	 * 
	 * @param name
	 *            the {@link SmartDevice#name} to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns links of this device
	 * 
	 * @return the links of this device
	 */
	public List<Link> getLinks() {
		return links;
	}

	/**
	 * Adds a new link to this device
	 * 
	 * @param link
	 *            the link to add
	 */
	public void addLink(Link link) {
		if(!links.contains(link) && link != null)
			links.add(link);
	}

	/**
	 * Removes the link from this SmartDevice
	 * 
	 * @param link
	 *            Link which should be removed
	 */
	public void removeLink(Link link) {
		if (link == null)
			return;
		links.remove(link);
	}

	/**
	 * Returns the x-position
	 * 
	 * @return the x-position
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x-position
	 * 
	 * @param x
	 *            the x-position to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Returns the y-position
	 * 
	 * @return the y-position
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y-position
	 * 
	 * @param y
	 *            the y-position to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the z-position
	 * 
	 * @return the z-position
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Sets the z-position
	 * 
	 * @param z
	 *            the z-position to set
	 */
	public void setZ(int z) {
		this.z = z;
	}

	/**
	 * Simulates the next Timestep
	 * 
	 * @param startTime
	 *            Time the simulation interval starts in
	 *            System.currentTimeMillis() time
	 * @param duration
	 *            Duration of the simulation interval in milliseconds
	 */
	public void simulateTimeStep(long startTime, long duration) {

	}

	/**
	 * @return the ports
	 */
	public List<Port> getPorts() {
		return ports;
	}

	/**
	 * @param port the port to add
	 */
	public void addPort(Port port) {
		if(!ports.contains(port)&&port!=null)
		this.ports.add(port);
	}
	
	/**
	 * @param port the ports to remove
	 */
	public void removePort(Port port) {
		this.ports.remove(port);
	}
	
	/**
	 * @return label
	 */
	public short getLabel() {
		return label;
	}
	
	/**
	 * Sets the label
	 * @param label label to set
	 */
	public void setLabel(Short label) {
		this.label = label;
	}

}
