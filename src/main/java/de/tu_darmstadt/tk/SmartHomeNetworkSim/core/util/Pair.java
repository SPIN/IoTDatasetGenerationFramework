package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

public class Pair<L, R>{
	private L left;
	private R right;
	
	public Pair(L left, R right){
		this.left = left;
		this.right = right;
	}

	/**
	 * @return the left
	 */
	public L getLeft() {
		return left;
	}

	/**
	 * @param left the left to set
	 */
	public void setLeft(L left) {
		this.left = left;
	}

	/**
	 * @return the right
	 */
	public R getRight() {
		return right;
	}

	/**
	 * @param right the right to set
	 */
	public void setRight(R right) {
		this.right = right;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || ! (obj instanceof Pair<?, ?>))return false;
		Pair<?, ?> other = (Pair<?, ?>)obj;
		return left.equals(other.left)&&right.equals(other.right) 
			|| left.equals(other.right)&&right.equals(other.left) ;
	}
}
