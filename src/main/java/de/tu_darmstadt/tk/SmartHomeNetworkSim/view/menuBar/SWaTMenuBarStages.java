package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.menuBar;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.SettingsController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.ConnectionPrecision;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.PrecisionLink;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTActuator;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTSensor;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols.SWaTSimplifiedModbusProtocol;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTActuatorTypes;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTDeviceTypes;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTSensorTypes;

/**
 * Creates the stages of the SWaT scenario.
 * Based on {@link MenuBarNetworkExamples}.
 * @author Fabian Kaiser
 *
 */
public class SWaTMenuBarStages{

	/**
	 * Model controller.
	 */
	private Controller controller;
	
	/**
	 * Network controller.
	 */
	private NetworkController networkController;
	
	/**
	 * Settings controller.
	 */
	private SettingsController settingsController;	
	
	/**
	 * The vertical dimension of the panel in pixels.
	 */
	private int verticalPanelDimension;
	
	/**
	 * The vertical dimension of the panel in pixels.
	 */
	private int horizontalPanelDimension;
	
	/**
	 * Whether the SCADA is currently on the plane.
	 */
	private boolean SCADACreated = false;
	
	/**
	 * Whether the EngineeringWorkstation is currently on the plane.
	 */
	private boolean EngineeringWorkstationCreated = false;

	/**
	 * Whether the Historian is currently on the plane.
	 */
	private boolean HistorianCreated = false;
	
	/**
	 * Whether the Server Switch is currently on the plane.
	 */
	private boolean ServerSwitchCreated = false;

	/**
	 * Whether the PLC Switch is currently on the plane.
	 */
	private boolean PLCSwitchCreated = false;

	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS1Created = false;	
	
	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS2Created = false;	
	
	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS3Created = false;	
	
	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS4Created = false;	
	
	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS5Created = false;	
	
	/**
	 * Whether PLC 1is currently on the plane.
	 */
	private boolean PLCS6Created = false;	
	
	/**
	 * The ethernet link between the devices.
	 */
	private Link ethernetLink = new PrecisionLink("Ethernet");
	
	/**
	 * Connection between the devices.
	 */
	private Connection SWaTConnection = new ConnectionPrecision();
	
	/**
	 * Protocol used for SWaT.
	 */
	private SWaTSimplifiedModbusProtocol SWaTProtocol = new SWaTSimplifiedModbusProtocol();
	
	/**
	 * Constructor.
	 * @param controller
	 */
	public SWaTMenuBarStages(Controller controller) {
		
		this.controller = controller;
		this.networkController = controller.getNetworkController();
		this.settingsController = controller.getSettingsController();
		this.verticalPanelDimension = settingsController.getHeight();
		this.horizontalPanelDimension = settingsController.getWidth();
		
		networkController.addLink(ethernetLink);
		settingsController.setShowLinks(false);
		SWaTConnection.setName("SWaT");
		networkController.addConnectionToLink(SWaTConnection, ethernetLink);
		SWaTConnection.setProtocol(SWaTProtocol);
		networkController.addConnection(SWaTConnection);
	}
	
	/**
	 * Creates all SWaT stages.
	 */
	public void createAll() {
		createSCADA();
		createEngineeringWorkstation();
		createHistorian();
		createServerSwitch();
		createPLCSwitch();
		createStage1();
		createStage2();
		createStage3();
		createStage4();
		createStage5();
		createStage6();
		controller.notifyObservers();
	}
	
	/**
	 * Creates a selection of SWaT stages.
	 * @param selection Boolean array with selection for [SCADA, EngineeringWorkstation, Historian,
	 * ServerSwitch, PLCSwitch, Stage 1, Stage 2, Stage 3,Stage 4, Stage 5, Stage 6]. Must have length 9. 
	 */
	public void createSelection(boolean[] selection) {
		if(selection.length != 9) return;
		
		int trueCounter = 0;
		for(int i = 0; i <= 8; i++) {
			if(selection[i] != false)
				trueCounter++;
		}
		if(trueCounter == 0)
			this.controller.getNetworkController().deleteNetworkModel();
		
		if(selection[0]) createSCADA();
		if(selection[1]) createEngineeringWorkstation();
		if(selection[2]) createHistorian();
		if(selection[3]) createStage1();
		if(selection[4]) createStage2();
		if(selection[5]) createStage3();
		if(selection[6]) createStage4();
		if(selection[7]) createStage5();
		if(selection[8]) createStage6();
	}
	
	/**
	 * Creates the SCADA.
	 */
	public void createSCADA() {
		if(SCADACreated == false) {			
			SmartDevice SCADA = new SWaTDevice("SCADA", SWaTDeviceTypes.SCADA, 1.3);
			SCADA.setX(convertPxToRelativeHorizontal(75));
			SCADA.setY(verticalPanelDimension/2-convertPxToRelativeVertical(100));
			networkController.addSmartDevice(SCADA);
			
			SCADACreated = true;
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, SCADA);
			Port portSCADA = new Port(SCADA, (short)1);
			portSCADA.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portSCADA, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);
		}
	}
	
	/**
	 * Creates the Engineering Workstation.
	 */
	public void createEngineeringWorkstation() {
		if(EngineeringWorkstationCreated == false) {			
			SmartDevice EngineeringWorkstation = new SWaTDevice("Engineering Workstation", SWaTDeviceTypes.EngineeringWorkstation, 1.3);
			EngineeringWorkstation.setX(convertPxToRelativeHorizontal(75));
			EngineeringWorkstation.setY(verticalPanelDimension/2);
			networkController.addSmartDevice(EngineeringWorkstation);
			
			EngineeringWorkstationCreated = true;
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, EngineeringWorkstation);
			Port portEngineeringWorkstation = new Port(EngineeringWorkstation, (short)1);
			portEngineeringWorkstation.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portEngineeringWorkstation, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);
		}
	}
	
	/**
	 * Creates the Historian server.
	 */
	public void createHistorian() {
		if(HistorianCreated == false) {			
			SmartDevice Historian = new SWaTDevice("Historian", SWaTDeviceTypes.SCADA, 1.3);
			Historian.setX(convertPxToRelativeHorizontal(75));
			Historian.setY(verticalPanelDimension/2+convertPxToRelativeVertical(100));
			networkController.addSmartDevice(Historian);	
			
			HistorianCreated = true;
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, Historian);
			Port portHistorian = new Port(Historian, (short)1);
			portHistorian.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portHistorian, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}
	
	/**
	 * Creates the switch as the center of the PLC star network. 
	 */
	public void createPLCSwitch() {
		if(PLCSwitchCreated == false) {
			SmartDevice Switch1 = new SWaTDevice("Switch 1", SWaTDeviceTypes.Switch, 1);
			Switch1.setX(convertPxToRelativeHorizontal(760));
			Switch1.setY(verticalPanelDimension/2);
			
			PLCSwitchCreated = true;
			networkController.addSmartDevice(Switch1);
			
			networkController.addLinkToDevice(ethernetLink, Switch1);
			Port portSwitch1 = new Port(Switch1, (short)1);
			portSwitch1.setStatus(Port.OPEN);
			networkController.addDeviceToConnectionAndProtocol(portSwitch1, SWaTConnection, SWaTProtocol.roleSwitch);			
		}
	}
	
	/**
	 * Creates the switch for SCADA, Historian, and Engineering Workstation and the PLC switch.
	 */
	public void createServerSwitch() {
		if(ServerSwitchCreated == false) {
			SmartDevice Switch2 = new SWaTDevice("Switch 2", SWaTDeviceTypes.Switch, 1);
			Switch2.setX(convertPxToRelativeHorizontal(230));
			Switch2.setY(verticalPanelDimension/2);
			
			ServerSwitchCreated = true;
			networkController.addSmartDevice(Switch2);
			
			networkController.addLinkToDevice(ethernetLink, Switch2);
			Port portSwitch2 = new Port(Switch2, (short)1);
			portSwitch2.setStatus(Port.OPEN);
			networkController.addDeviceToConnectionAndProtocol(portSwitch2, SWaTConnection, SWaTProtocol.roleSwitch);					
		}	
	}
	
	/**
	 * Creates Stage 1 (P1) of SWaT.
	 */
	public void createStage1(){
		if(PLCS1Created == false) {
			PLCS1Created = true;
			
			SmartDevice MV101 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-101"); 
			MV101.setX(convertPxToRelativeHorizontal(270));
			MV101.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(MV101);
	
			SmartDevice FIT101 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-101"); 
			FIT101.setX(convertPxToRelativeHorizontal(330));
			FIT101.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(FIT101);
			
			SmartDevice LIT101 = new SWaTSensor(SWaTSensorTypes.LIT, "LIT-101"); 
			LIT101.setX(convertPxToRelativeHorizontal(390));
			LIT101.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(LIT101);
			
			SmartDevice P101 = new SWaTActuator(SWaTActuatorTypes.P, "P-101");
			P101.setX(convertPxToRelativeHorizontal(450));
			P101.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(P101);
			
			SmartDevice P102 = new SWaTActuator(SWaTActuatorTypes.P, "P-102");
			P102.setX(convertPxToRelativeHorizontal(450));
			P102.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(P102);		
	
			SmartDevice PLC1 = new SWaTDevice("PLC 1", SWaTDeviceTypes.PLC, 0.6);
			PLC1.setX(convertPxToRelativeHorizontal(360));
			PLC1.setY(convertPxToRelativeVertical(230));
			networkController.addSmartDevice(PLC1);
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC1);
			Port portPLC1 = new Port(PLC1, (short)1);
			portPLC1.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC1, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}	

	/**
	 * Creates Stage 2 (P2) of SWaT.
	 */
	public void createStage2(){ 
		if(PLCS2Created == false) {
			PLCS2Created = true;
			
			SmartDevice FIT201 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-201"); 
			FIT201.setX(convertPxToRelativeHorizontal(550));
			FIT201.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(FIT201);
			
			SmartDevice AIT201 = new SWaTSensor(SWaTSensorTypes.AITConductivity, "AIT-201"); 
			AIT201.setX(convertPxToRelativeHorizontal(610));
			AIT201.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(AIT201);
			
			SmartDevice P201 = new SWaTActuator(SWaTActuatorTypes.P, "P-201"); 
			P201.setX(convertPxToRelativeHorizontal(670));
			P201.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(P201);
			
			SmartDevice P202 = new SWaTActuator(SWaTActuatorTypes.P, "P-202"); 
			P202.setX(convertPxToRelativeHorizontal(670));
			P202.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(P202);
			
			SmartDevice P203 = new SWaTActuator(SWaTActuatorTypes.P, "P-203"); 
			P203.setX(convertPxToRelativeHorizontal(730));
			P203.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(P203);
			
			SmartDevice P204 = new SWaTActuator(SWaTActuatorTypes.P, "P-204"); 
			P204.setX(convertPxToRelativeHorizontal(730));
			P204.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(P204);
			
			SmartDevice P205 = new SWaTActuator(SWaTActuatorTypes.P, "P-205"); 
			P205.setX(convertPxToRelativeHorizontal(790));
			P205.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(P205);
			
			SmartDevice P206 = new SWaTActuator(SWaTActuatorTypes.P, "P-206"); 
			P206.setX(convertPxToRelativeHorizontal(790));
			P206.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(P206);
			
			SmartDevice MV201 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-201"); 
			MV201.setX(convertPxToRelativeHorizontal(850));
			MV201.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(MV201);		
			
			SmartDevice AIT202 = new SWaTSensor(SWaTSensorTypes.AITPH, "AIT-202"); 
			AIT202.setX(convertPxToRelativeHorizontal(910));
			AIT202.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(AIT202);
			
			SmartDevice AIT203 = new SWaTSensor(SWaTSensorTypes.AITORP, "AIT-203"); 
			AIT203.setX(convertPxToRelativeHorizontal(970));
			AIT203.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(AIT203);		
	
			SmartDevice PLC2 = new SWaTDevice("PLC 2", SWaTDeviceTypes.PLC, 0.6);
			PLC2.setX(convertPxToRelativeHorizontal(760));
			PLC2.setY(convertPxToRelativeVertical(230));
			networkController.addSmartDevice(PLC2);	
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC2);
			Port portPLC2 = new Port(PLC2, (short)1);
			portPLC2.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC2, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}
	
	/**
	 * Creates Stage 3 (P3) of SWaT.
	 */	
	public void createStage3() {
		if(PLCS3Created == false) {
			PLCS3Created = true;
			
			SmartDevice LIT301 = new SWaTSensor(SWaTSensorTypes.LIT, "LIT-301"); 
			LIT301.setX(convertPxToRelativeHorizontal(1070));
			LIT301.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(LIT301);
			
			SmartDevice FIT301 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-301"); 
			FIT301.setX(convertPxToRelativeHorizontal(1130));
			FIT301.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(FIT301);	
			
			SmartDevice P301 = new SWaTActuator(SWaTActuatorTypes.P, "P-301");
			P301.setX(convertPxToRelativeHorizontal(1190));
			P301.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(P301);
			
			SmartDevice P302 = new SWaTActuator(SWaTActuatorTypes.P, "P-302");
			P302.setX(convertPxToRelativeHorizontal(1190));
			P302.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(P302);			
					
			SmartDevice DPIT301 = new SWaTSensor(SWaTSensorTypes.DPIT, "DPIT-301"); 
			DPIT301.setX(convertPxToRelativeHorizontal(1250));
			DPIT301.setY(convertPxToRelativeVertical(100));
			networkController.addSmartDevice(DPIT301);	
			
			SmartDevice MV301 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-301");
			MV301.setX(convertPxToRelativeHorizontal(1310));
			MV301.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(MV301);		
			
			SmartDevice MV302 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-302");
			MV302.setX(convertPxToRelativeHorizontal(1310));
			MV302.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(MV302);
			
			SmartDevice MV303 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-303");
			MV303.setX(convertPxToRelativeHorizontal(1370));
			MV303.setY(convertPxToRelativeVertical(50));
			networkController.addSmartDevice(MV303);			
			
			SmartDevice MV304 = new SWaTActuator(SWaTActuatorTypes.MV, "MV-304");
			MV304.setX(convertPxToRelativeHorizontal(1370));
			MV304.setY(convertPxToRelativeVertical(150));
			networkController.addSmartDevice(MV304);
			
			SmartDevice PLC3 = new SWaTDevice("PLC 3", SWaTDeviceTypes.PLC, 0.6);
			PLC3.setX(convertPxToRelativeHorizontal(1220));
			PLC3.setY(convertPxToRelativeVertical(230));
			networkController.addSmartDevice(PLC3);
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC3);
			Port portPLC3 = new Port(PLC3, (short)1);
			portPLC3.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC3, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}
	
	/**
	 * Creates Stage 4 (P4) of SWaT.
	 */	
	public void createStage4() {
		if(PLCS4Created == false) {
			PLCS4Created = true;
			
			
			SmartDevice LIT401 = new SWaTSensor(SWaTSensorTypes.LIT, "LIT-401"); 
			LIT401.setX(convertPxToRelativeHorizontal(1370));
			LIT401.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(LIT401);	
	
			SmartDevice P401 = new SWaTActuator(SWaTActuatorTypes.P, "P-401");
			P401.setX(convertPxToRelativeHorizontal(1310));
			P401.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(P401);		
			
			SmartDevice P402 = new SWaTActuator(SWaTActuatorTypes.P, "P-402");
			P402.setX(convertPxToRelativeHorizontal(1310));
			P402.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(P402);
			
			SmartDevice FIT401 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-401"); 
			FIT401.setX(convertPxToRelativeHorizontal(1250));
			FIT401.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(FIT401);			
			
			SmartDevice UV401 = new SWaTActuator(SWaTActuatorTypes.UV, "UV-401");
			UV401.setX(convertPxToRelativeHorizontal(1190));
			UV401.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(UV401);	
			
			SmartDevice AIT401 = new SWaTSensor(SWaTSensorTypes.AITWaterHardness, "AIT-401"); 
			AIT401.setX(convertPxToRelativeHorizontal(1130));
			AIT401.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(AIT401);
			
			SmartDevice AIT402 = new SWaTSensor(SWaTSensorTypes.AITORP, "AIT-402"); 
			AIT402.setX(convertPxToRelativeHorizontal(1130));
			AIT402.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(AIT402);
			
			SmartDevice P403 = new SWaTActuator(SWaTActuatorTypes.P, "P-403");
			P403.setX(convertPxToRelativeHorizontal(1070));
			P403.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(P403);
			
			SmartDevice P404 = new SWaTActuator(SWaTActuatorTypes.P, "P-404");
			P404.setX(convertPxToRelativeHorizontal(1070));
			P404.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(P404);		
			
			SmartDevice PLC4 = new SWaTDevice("PLC 4", SWaTDeviceTypes.PLC, 0.6);
			PLC4.setX(convertPxToRelativeHorizontal(1220));
			PLC4.setY(convertPxToRelativeVertical(-230));
			networkController.addSmartDevice(PLC4);
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC4);
			Port portPLC4 = new Port(PLC4, (short)1);
			portPLC4.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC4, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}

	/**
	 * Creates Stage 5 (P5) of SWaT.
	 */
	public void createStage5() {
		if(PLCS5Created == false) {
			PLCS5Created = true;			
			
			SmartDevice AIT501 = new SWaTSensor(SWaTSensorTypes.AITPH, "AIT-501"); 
			AIT501.setX(convertPxToRelativeHorizontal(970));
			AIT501.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(AIT501);
			
			SmartDevice AIT502 = new SWaTSensor(SWaTSensorTypes.AITORP, "AIT-502"); 
			AIT502.setX(convertPxToRelativeHorizontal(910));
			AIT502.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(AIT502);
			
			SmartDevice AIT503 = new SWaTSensor(SWaTSensorTypes.AITConductivity, "AIT-503"); 
			AIT503.setX(convertPxToRelativeHorizontal(910));
			AIT503.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(AIT503);
			
			SmartDevice P501 = new SWaTActuator(SWaTActuatorTypes.P, "P-501");
			P501.setX(convertPxToRelativeHorizontal(850));
			P501.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(P501);	
			
			SmartDevice P502 = new SWaTActuator(SWaTActuatorTypes.P, "P-502");
			P502.setX(convertPxToRelativeHorizontal(850));
			P502.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(P502);			
			
			SmartDevice FIT501 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-501"); 
			FIT501.setX(convertPxToRelativeHorizontal(790));
			FIT501.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(FIT501);		
			
			SmartDevice PIT501 = new SWaTSensor(SWaTSensorTypes.FIT, "PIT-501"); 
			PIT501.setX(convertPxToRelativeHorizontal(790));
			PIT501.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(PIT501);
			
			SmartDevice AIT504 = new SWaTSensor(SWaTSensorTypes.AITConductivity, "AIT-504"); 
			AIT504.setX(convertPxToRelativeHorizontal(730));
			AIT504.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(AIT504);
			
			SmartDevice PIT502 = new SWaTSensor(SWaTSensorTypes.FIT, "PIT-502"); 
			PIT502.setX(convertPxToRelativeHorizontal(730));
			PIT502.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(PIT502);			
			
			SmartDevice FIT502 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-502"); 
			FIT502.setX(convertPxToRelativeHorizontal(670));
			FIT502.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(FIT502);	
			
			SmartDevice FIT503 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-503"); 
			FIT503.setX(convertPxToRelativeHorizontal(610));
			FIT503.setY(convertPxToRelativeVertical(-230+80));
			networkController.addSmartDevice(FIT503);	
			
			SmartDevice PIT503 = new SWaTSensor(SWaTSensorTypes.FIT, "PIT-503"); 
			PIT503.setX(convertPxToRelativeHorizontal(610));
			PIT503.setY(convertPxToRelativeVertical(-230+180));
			networkController.addSmartDevice(PIT503);			
			
			SmartDevice FIT504 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-504"); 
			FIT504.setX(convertPxToRelativeHorizontal(550));
			FIT504.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(FIT504);
			
			SmartDevice PLC5 = new SWaTDevice("PLC 5", SWaTDeviceTypes.PLC, 0.6);
			PLC5.setX(convertPxToRelativeHorizontal(760));
			PLC5.setY(convertPxToRelativeVertical(-230));
			networkController.addSmartDevice(PLC5);
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC5);
			Port portPLC5 = new Port(PLC5, (short)1);
			portPLC5.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC5, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}
	
	/**
	 * Creates Stage 6 (P6) of SWaT.
	 */	
	public void createStage6() {
		if(PLCS6Created == false) {
			PLCS6Created = true;
			
			SmartDevice FIT601 = new SWaTSensor(SWaTSensorTypes.FIT, "FIT-601"); 
			FIT601.setX(convertPxToRelativeHorizontal(450));
			FIT601.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(FIT601);		
			
			SmartDevice P601 = new SWaTActuator(SWaTActuatorTypes.P, "P-601"); 
			P601.setX(convertPxToRelativeHorizontal(390));
			P601.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(P601);	
			
			SmartDevice P602 = new SWaTActuator(SWaTActuatorTypes.P, "P-602"); 
			P602.setX(convertPxToRelativeHorizontal(330));
			P602.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(P602);	
			
			SmartDevice P603 = new SWaTActuator(SWaTActuatorTypes.P, "P-603"); 
			P603.setX(convertPxToRelativeHorizontal(270));
			P603.setY(convertPxToRelativeVertical(-230+130));
			networkController.addSmartDevice(P603);			
			
			SmartDevice PLC6 = new SWaTDevice("PLC 6", SWaTDeviceTypes.PLC, 0.6);
			PLC6.setX(convertPxToRelativeHorizontal(360));
			PLC6.setY(convertPxToRelativeVertical(-230));
			networkController.addSmartDevice(PLC6);
			
			validateSwitches();
			
			networkController.addLinkToDevice(ethernetLink, PLC6);
			Port portPLC6 = new Port(PLC6, (short)1);
			portPLC6.setStatus(Port.SENDING);
			networkController.addDeviceToConnectionAndProtocol(portPLC6, SWaTConnection, SWaTProtocol.roleSenderAndReceiver);			
		}
	}	
	
	/**
	 * Converts a pixel value on the x axis to the relative value in the pane.
	 * @param pixel Pixel value.
	 * @return The relative value in the pane
	 */
	private int convertPxToRelativeHorizontal(int pixel) {
		// Standard pane size on 1920*1080 is 1432*738 pixels.
		return (int) Math.round(horizontalPanelDimension*pixel/1432);
	}
	
	/**
	 * Converts a pixel value on the y axis to the relative value in the pane.
	 * @param pixel Pixel value.
	 * @return The relative value in the pane
	 */
	private int convertPxToRelativeVertical(int pixel) {
		// Standard pane size on 1920*1080 is 1432*738 pixels.
		if(pixel >= 0)
			return (int) Math.round(verticalPanelDimension*pixel/738);
		else
			return (int) Math.round(verticalPanelDimension+verticalPanelDimension*pixel/738);
	}
	
	/**
	 * Checks whether the switches are needed in the current configuration.
	 * Creates or removes the switches accordingly.
	 */
	private void validateSwitches() {
		int ServerTrueSum = (SCADACreated ? 1 : 0) + (EngineeringWorkstationCreated ? 1 : 0) + (HistorianCreated ? 1 : 0);
		int PLCTrueSum =  (PLCS1Created ? 1 : 0) + (PLCS2Created ? 1 : 0) + (PLCS3Created ? 1 : 0) + (PLCS4Created ? 1 : 0) + (PLCS5Created ? 1 : 0) + (PLCS6Created ? 1 : 0); 
		if(PLCTrueSum >= 2 || (PLCTrueSum == 1 && ServerTrueSum >= 1)) {
			createPLCSwitch();
		}
		if(ServerTrueSum >= 2 || (PLCTrueSum == 1 && ServerTrueSum >= 1)) {
			createServerSwitch();
		}
	}
}
