package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import javax.swing.JPanel;

import org.apache.commons.math3.random.RandomGenerator;

/**
 * Allows selection and configuration of such a class in view. Also Import & visualization of the example panel.
 *
 * @author Andreas T. Meyer-Berg
 */
public interface ProbabilityDistributionHandler {

	/**
	 * Set the random generator which should be used
	 * @param rng random generator
	 */
	public void setRandomGenerator(RandomGenerator rng);

	/**
	 * Next value from the distribution
	 * @return next sample value
	 */
	public long sampleNextValue();
	
	/**
	 * Returns a configuration panel for the distribution.
	 * Should be set enabled, visible and embedded by the calling methods.
	 * @return panel for configuration
	 */
	public JPanel getConfigurationPanel();
	
	/**
	 * Returns a simple description/name/identifier of the Distribution.
	 * For example: "Gaussian Distribution"
	 * @return simple description
	 */
	public String getSimpleDescription();
}
