package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.simpleImplementation;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;

/**
 * Simple dummy packet for testing purposes
 *
 * @author Andreas T. Meyer-Berg
 */
public class SimplePacket extends Packet {
	/**
	 * Payload which was sent
	 */
	private String payload;

	/**
	 * Creates a new dummy packet without payload
	 * 
	 * @param time
	 *            time the package was created in System.currentTimeMillis
	 * @param source
	 *            SmartDevice which sent this packet
	 * @param destination
	 *            SmartDevice which should receive this packet
	 */
	public SimplePacket(long time, Port source, Port destination) {
		super(time, source, destination);
		this.payload = "";
	}

	/**
	 * Creates a new dummy packet with a String as payload
	 * 
	 * @param time
	 *            time the package was created in System.currentTimeMillis
	 * @param source
	 *            SmartDevice which sent this packet
	 * @param destination
	 *            SmartDevice which should receive this packet
	 * @param payload
	 *            String which represents the payload which is encapsulated by
	 *            this packet
	 * @param label
	 * 			  Label of this packet, represented as a short
	 */
	public SimplePacket(long time, Port source, Port destination, String payload) {
		super(time, source, destination);
		this.source = source;
		this.destination = destination;
		this.payload = payload;
	}
	
	/**
	 * Creates a new dummy packet with a String as payload
	 * 
	 * @param time
	 *            time the package was created in System.currentTimeMillis
	 * @param source
	 *            SmartDevice which sent this packet
	 * @param destination
	 *            SmartDevice which should receive this packet
	 * @param payload
	 *            String which represents the payload which is encapsulated by
	 *            this packet
	 * @param label
	 * 			  Label of this packet, represented as a short
	 */
	public SimplePacket(long time, Port source, Port destination, String payload, short label) {
		super(time, source, destination);
		this.source = source;
		this.destination = destination;
		this.payload = payload;
		this.label = label;
	}

	@Override
	public byte[] dumpBytes() {
		return this.dumpBytes();
	}

	@Override
	public String getTextualRepresentation() {
		return this.toString();
	}
	
	@Override
	public String toString() {
		String destName = destination == null ? "null" : (destination.getOwner().getName()+":"+destination.getPortNumber());
		String srcName = source == null ? "null" : (source.getOwner().getName()+":"+source.getPortNumber());

		return "[SimplePacket:" + payload + " time:" + timestamp + ";source:" + srcName + ";dest:"
				+ destName + "]";
	}

	@Override
	public String getPayload() {
		return payload == null ? "" : payload;
	}

	@Override
	public String getProtocolName() {
		return "Simple";
	}
}
