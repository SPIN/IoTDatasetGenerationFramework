﻿   
# IoT Dataset Generation Framework – IoTDGF
A framework for the visualization and generation of network traffic for the Internet of Things. <br>
The following scenarios are pre-implemented:
* Smart Home
* Secure Water Treatment (SWaT) Testbed: based on data from the real-life testbed created and operated by [iTrust, the Centre for Research in Cyber Security at Singapore University of Technology and Design](https://itrust.sutd.edu.sg/testbeds/secure-water-treatment-swat/)

## Publications
The short paper "IoT dataset generation framework for evaluating anomaly detection mechanisms" by Andreas Meyer-Berg, Rolf Egert, Leon Böck, and Max Mühlhäuser for the general framework with its smart home scenario is available at [https://doi.org/10.1145/3407023.3407036](https://doi.org/10.1145/3407023.3407036).

## Installation
Clone the repository:

	$ git clone https://git.tk.informatik.tu-darmstadt.de/SPIN/IoTDatasetGenerationFramework.git

Change to the directory:

	$ cd IoTDatasetGenerationFramework

Build and run the project:

	$ ./gradlew run

## Technical Details
### Prerequisites
* [Java SE JDK 8.0](https://www.oracle.com/technetwork/java/javase/downloads/index.html) or greater
	
### Required Libraries
Gradle downloads these during the build process

* [JUnit4](https://junit.org/junit4/) - Java unit test framework
* [Math3](https://commons.apache.org/proper/commons-math/) - Apache Common Maths (for Distribution function)
* [Weka 3](https://www.cs.waikato.ac.nz/ml/weka/) - Weka machine learning framework (for example anomaly detection algorithms)
<!--* [Pcap4j](https://www.pcap4j.org/) - Java Library used for Pcap File creation (not yet implemented)-->
<!-- PCAP file writing example (as part of the export manager - but probably required packet transformation): https://www.devdungeon.com/content/packet-capturing-java-pcap4j#writing_pcap_file -->

### Gradle Tasks
Gradle will download required libraries and handle the build process

	$ gradlew build		# Assembles and tests this project
	
	$ gradlew jar 		# Creates an executable Jar-file
	
	$ gradlew javadoc	# Generates Java Doc for the framework
	
	$ gradlew test		# Runs the included unit tests
	
	$ gradlew run		# Runs the IoTDatasetGenerationFramework as a JVM Application
	
	$ gradlew runIt		# Runs the IoTDatasetGenerationFramework as a Jar-file
	
	$ gradlew tasks		# Displays further available tasks

## Adaptations and Custom Implementations
To change code of the framework, it is possible to import it into an IDE of choice (e.g., into Eclipse with File > Import > Gradle > Existing Gradle Project). Custom implementations of devices, protocols etc. can also be written in a separate project and later be imported. <br>
For access to libraries during development the Jar files in "\build\install\SmartHomeNetworkSim\lib" can be imported. While running the framework, externally implemented programs can be imported.

## Credits for the General Framework and the Smart Home Scenario
Andreas T. Meyer-Berg (development as part of his Bachelor's Thesis)

## Credits for the SWaT Scenario
Fabian Kaiser (data analysis, development, and evaluation as part of his Master's Thesis) <br>
Leon Böck (initial idea and thesis supervision)
