# Anomaly detection for the physical data of SWaT by testing on a trained one-class support vector machine.
# The algorithm parameters are taken from Inoue et al.: https://ieeexplore.ieee.org/document/8215783
# @author Fabian Kaiser

################ Algorithm Parameters ####################
sliding_window_size = 4
standardization_trick = True
##########################################################
################## File Path Parameters ##################
# Conversion from XLSX to CSV: Open Office Calc -> File -> Save as -> Text CSV -> Save -> OK -> Remove first line or two first lines by hand so the column description is in row 1 -> Remove last (empty) line by hand
file_path_SWaT_Dataset_Normal = "C:/Users/user/master/Evaluation/physical/SWaT_Dataset_Normal_v1.csv"
file_path_SWaT_Dataset_TestValidation = "C:/Users/user/master/Evaluation/physical/SWaT_Dataset_Attack_v0 - Kopie.csv"
file_path_Trained_Classifier = "C:/Users/user/master/Evaluation/physical/AnomalyDetectionPhysical_TrainedClassifier_OCSVM_2021-06-29 07.52.43.174848 (nu=0.004584962079820046, kernel=rbf, gamma=0.0008181483058667633, w=4)_trained on dataset A1.joblib"
##########################################################

import pandas as pd
import numpy as np
from joblib import load
from datetime import datetime

print("Start time:\t"+str(datetime.now()))




# Read the files and create the sets
dataset_A1_normal_v1 = pd.read_csv(file_path_SWaT_Dataset_Normal, skipinitialspace = True)
dataset_length = len(dataset_A1_normal_v1)
training_set = dataset_A1_normal_v1.loc[0:int(dataset_length*1)].copy()
dataset_A1_attack_v0 = pd.read_csv(file_path_SWaT_Dataset_TestValidation, skipinitialspace = True)
dataset_length = len(dataset_A1_attack_v0)
test_set = dataset_A1_attack_v0.loc[0:int(dataset_length*1)].copy()


# No periodicy which is related to minute of hours, hour of day, and so on.
training_set.pop('Timestamp')
test_set.pop('Timestamp')


# Replace attack labels with numeric values
training_set = training_set.replace(to_replace =["Normal", "Attack"], value =[1, -1])
test_set = test_set.replace(to_replace =["Normal", "Attack", "A ttack"], value =[1, -1, -1])


# Standardization
# Only continous values need to be changed since all discrete values are already numeric and in {0, 1, 2} or {1, 2}
training_set_continous = training_set[["FIT101", "LIT101", "AIT201", "AIT202", "AIT203", "FIT201", "DPIT301", "FIT301", "LIT301", "AIT401", "AIT402", "FIT401", "LIT401", "AIT501", "AIT502", "AIT503", "AIT504", "FIT501", "FIT502", "FIT503", "FIT504", "PIT501", "PIT502", "PIT503", "FIT601"]]
test_set_continous = test_set[["FIT101", "LIT101", "AIT201", "AIT202", "AIT203", "FIT201", "DPIT301", "FIT301", "LIT301", "AIT401", "AIT402", "FIT401", "LIT401", "AIT501", "AIT502", "AIT503", "AIT504", "FIT501", "FIT502", "FIT503", "FIT504", "PIT501", "PIT502", "PIT503", "FIT601"]]

if standardization_trick == False:
    training_set_continous_mean = training_set_continous.mean()
    training_set_continous_std = training_set_continous.std()
    training_set_continous_std = training_set_continous_std.replace(to_replace=[0.0], value=[1.0]) # Prevent division by zero errors for constant values
    test_set_continous = (test_set_continous - training_set_continous_mean) / training_set_continous_std
else:
    test_set_continous_mean = test_set_continous.mean()
    test_set_continous_std = test_set_continous.std()
    test_set_continous_std = test_set_continous_std.replace(to_replace=[0.0], value=[1.0]) # Prevent division by zero errors for constant values
    test_set_continous = (test_set_continous - test_set_continous_mean) / test_set_continous_std  

test_set = pd.DataFrame({
    "FIT101": test_set_continous["FIT101"],
    "LIT101": test_set_continous["LIT101"],
    "MV101": test_set["MV101"],
    "P101": test_set["P101"],
    "P102": test_set["P102"],
    "AIT201": test_set_continous["AIT201"],
    "AIT202": test_set_continous["AIT202"],
    "AIT203": test_set_continous["AIT203"],
    "FIT201": test_set_continous["FIT201"],
    "MV201": test_set["MV201"],
    "P201": test_set["P201"],
    "P202": test_set["P202"],
    "P203": test_set["P203"],
    "P204": test_set["P204"],
    "P205": test_set["P205"],
    "P206": test_set["P206"],
    "DPIT301": test_set_continous["DPIT301"],
    "FIT301": test_set_continous["FIT301"],
    "LIT301": test_set_continous["LIT301"],
    "MV301": test_set["MV301"],
    "MV302": test_set["MV302"],
    "MV303": test_set["MV303"],
    "MV304": test_set["MV304"],
    "P301": test_set["P301"],
    "P302": test_set["P302"],
    "AIT401": test_set_continous["AIT401"],
    "AIT402": test_set_continous["AIT402"],
    "FIT401": test_set_continous["FIT401"],
    "LIT401": test_set_continous["LIT401"],
    "P401": test_set["P401"],
    "P402": test_set["P402"],
    "P403": test_set["P403"],
    "P404": test_set["P404"],
    "UV401": test_set["UV401"],
    "AIT501": test_set_continous["AIT501"],
    "AIT502": test_set_continous["AIT502"],
    "AIT503": test_set_continous["AIT503"],
    "AIT504": test_set_continous["AIT504"],
    "FIT501": test_set_continous["FIT501"],
    "FIT502": test_set_continous["FIT502"],
    "FIT503": test_set_continous["FIT503"],
    "FIT504": test_set_continous["FIT504"],
    "P501": test_set["P501"],
    "P502": test_set["P502"],    
    "PIT501": test_set_continous["PIT501"],
    "PIT502": test_set_continous["PIT502"],
    "PIT503": test_set_continous["PIT503"],
    "FIT601": test_set_continous["FIT601"],
    "P601": test_set["P601"],
    "P602": test_set["P602"],
    "P603": test_set["P603"],
    "label": test_set["Normal/Attack"]   
})

# Creates the sliding windows with the specified parameters.
# The default values are an example.
def create_sliding_windows(data = np.array([[1,2],[3,4],[5,6],[7,8],[9,10],[11,12]]), sliding_window_size = 3, elements_per_row = 2):
    number_of_sliding_windows = len(data)-sliding_window_size+1
    dimensions_elements_per_row = elements_per_row
    if elements_per_row == 52:
        dimensions_elements_per_row = 51
    array_sliding_windows = np.zeros(shape=(number_of_sliding_windows, sliding_window_size*dimensions_elements_per_row))
    array_sliding_windows_labels = np.zeros(shape=(number_of_sliding_windows))
    array_sliding_windows_index = 0
    for i in range(sliding_window_size, len(data)+1):
        array_current_sliding_window_index = 0
        array_current_sliding_window = np.zeros(shape=(sliding_window_size*dimensions_elements_per_row))
        current_sliding_window_label = 1
        for j in range(i-sliding_window_size, i):  
            for k in range(0, elements_per_row):
                if k == 51:
                    if data[j][k] == -1:
                        current_sliding_window_label = -1
                else:
                    array_current_sliding_window[array_current_sliding_window_index] = data[j][k]
                    array_current_sliding_window_index+=1

        array_sliding_windows[array_sliding_windows_index] = array_current_sliding_window
        array_sliding_windows_labels[array_sliding_windows_index] = current_sliding_window_label
        array_sliding_windows_index+=1 
    return array_sliding_windows, array_sliding_windows_labels

array_sliding_windows_test, array_sliding_windows_test_labels = create_sliding_windows(test_set.to_numpy(),sliding_window_size,52)


# Load model from disk
loaded_classifier = load(file_path_Trained_Classifier)
prediction_results = loaded_classifier.predict(array_sliding_windows_test)

# Save prediction results
output_file_name = "AnomalyDetectionPhysical_OCSVM_Prediction results_"+str(datetime.now())+".csv"
output_file_name = output_file_name.replace(':','.')
np.savetxt(output_file_name, prediction_results, delimiter=",")

print("End time:\t"+str(datetime.now()))
print(prediction_results)


# Compute metrics
false_positive_counter = 0
false_negative_counter = 0
true_positive_counter = 0
true_negative_counter = 0

for n in range(0, len(prediction_results)):
    label = array_sliding_windows_test_labels[n]
    prediction = prediction_results[n]
    if label == 1 and prediction == 1:
        true_negative_counter+=1
    elif label == 1 and prediction == -1:
        false_positive_counter+=1
    elif label == -1 and prediction == -1:
        true_positive_counter+=1
    elif label == -1 and prediction == 1:
        false_negative_counter+=1
 
print("False positives: "+str(false_positive_counter))
print("False negatives: "+str(false_negative_counter))
print("True positives: "+str(true_positive_counter))
print("True negatives: "+str(true_negative_counter))
if true_positive_counter+false_positive_counter != 0:
    print("Precision: "+str(true_positive_counter/(true_positive_counter+false_positive_counter)))
else:
    print("Precision: denominator (TP+FP) is 0")
if true_positive_counter+false_negative_counter != 0:
    print("Recall: "+str(true_positive_counter/(true_positive_counter+false_negative_counter)))
else:
    print("Recall: denominator (TP+FN) is 0")    
print("F1 score: "+str((2*true_positive_counter)/(2*true_positive_counter+false_positive_counter+false_negative_counter)))