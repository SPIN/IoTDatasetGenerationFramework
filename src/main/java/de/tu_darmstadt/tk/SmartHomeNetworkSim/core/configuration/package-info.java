/**
 * Package for configuration and setting classes of the model
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;