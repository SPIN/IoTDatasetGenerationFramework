
/**
 * Contains implementations of protocols, which can be used to generate packets. 
 *
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols;