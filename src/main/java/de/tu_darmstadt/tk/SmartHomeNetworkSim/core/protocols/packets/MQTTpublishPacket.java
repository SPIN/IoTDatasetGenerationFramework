package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols.packets;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;

/**
 * MQTT Publish Packet
 * 
 * 
 * @author Andreas T. Meyer-Berg
 */
public class MQTTpublishPacket extends MQTT_packet {
	/**
	 * Topic of the Publish Packet
	 */
	private String topic = "";
	/**
	 * Value of the packet
	 */
	private float value = 0;
	/**
	 * True if boolean
	 */
	private boolean isBoolean = false;
	
	/**
	 * MQTT Publish Packet
	 * @param timestamp time it is send
	 * @param source source port
	 * @param destination destination port
	 * @param topic topic of the message
	 * @param value value of the message
	 */
	public MQTTpublishPacket(long timestamp, Port source, Port destination, String topic, float value) {
		super(MQTT_packet.PUBLISH, timestamp, source, destination);
		this.setTopic(topic);
		this.setValue(value);
		setBoolean(false);
		this.message = topic + ":"+ value;
	}
	
	/**
	 * MQTT Publish Packet
	 * @param timestamp time it is send
	 * @param source source port
	 * @param destination destination port
	 * @param topic topic of the message
	 * @param value value of the message
	 */
	public MQTTpublishPacket(long timestamp, Port source, Port destination, String topic, boolean value) {
		super(MQTT_packet.PUBLISH, timestamp, source, destination);
		this.setTopic(topic);
		this.setValue(value ? 1 : 0);
		setBoolean(true);
		this.message = "Topic:"+topic + ":"+ value;
	}

	public boolean isBoolean() {
		return isBoolean;
	}

	public void setBoolean(boolean isBoolean) {
		this.isBoolean = isBoolean;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
