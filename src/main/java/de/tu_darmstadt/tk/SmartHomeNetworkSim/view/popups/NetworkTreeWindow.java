package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Container;
import java.awt.Rectangle;

import javax.swing.JFrame;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;


/**
 * Frame which contains the Network Tree
 *
 * @author Andreas T. Meyer-Berg
 */
public class NetworkTreeWindow extends JFrame {

	/**
	 * Serial Version number
	 */
	private static final long serialVersionUID = -4052775416023427170L;

	/**
	 * Frame which contains the Network Tree
	 * @param controller controller which should be used to get and edit the Model
	 * @param parent Parent component, which opens this window
	 */
	public NetworkTreeWindow(Controller controller, Container parent) {
		setBounds(new Rectangle(0, 0, 800, 600));
		setResizable(true);
		setTitle("Network Tree View");
		setIconImage(Utility.loadFile("images/smartHome_icon.png"));
		this.setContentPane(new NetworkTreePanel(controller,this));
		this.setLocationRelativeTo(parent);
	}
}
