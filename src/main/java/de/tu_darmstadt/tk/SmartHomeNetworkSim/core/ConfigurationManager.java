package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.ImportConfiguration;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.LinkColorManager;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.NetworkTreeSettings;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.SelectionModel;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.VisualizationConfiguration;


/**
 * Class which stores and returns the different configuration classes
 *
 * @author Andreas T. Meyer-Berg
 */
public class ConfigurationManager {

	/**
	 * Visual Configuration
	 */
	private VisualizationConfiguration visual;
	
	/**
	 * Import Configuration
	 */
	private ImportConfiguration importConf;
	
	/**
	 * Selection model
	 */
	private SelectionModel selection;
	/**
	 * Settings of the networkTree
	 */
	private NetworkTreeSettings networkTree;
	/**
	 * Link Color manager
	 */
	private LinkColorManager linkColors;
	/**
	 * Initialize the Configuration Manager
	 */
	public ConfigurationManager() {
		visual = new VisualizationConfiguration();
		importConf = new ImportConfiguration();
		selection = new SelectionModel();
		networkTree = new NetworkTreeSettings();
		linkColors = new LinkColorManager();
	}

	/**
	 * Returns the visualization configuration
	 * 
	 * @return the visualization configuration
	 */
	public VisualizationConfiguration getVisualizationConfiguration() {
		return visual;
	}
	
	/**
	 * Returns the import configuration
	 * 
	 * @return the import configuration
	 */
	public ImportConfiguration getImportConfiguration(){
		return importConf;
	}
	
	/**
	 * Returns the selection model
	 * 
	 * @return selection model
	 */
	public SelectionModel getSelectionModel(){
		return selection;
	}
	
	/**
	 * Returns the NetworkTreeSettings
	 * @return network tree settings
	 */
	public NetworkTreeSettings getNetworkTreeSettings(){
		return networkTree;
	}

	/**
	 * @return the linkColors
	 */
	public LinkColorManager getLinkColors() {
		return linkColors;
	}
}
