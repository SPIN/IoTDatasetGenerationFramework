/**
 * A utility package for small classes, which are used in the UI. Like
 * {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.JavaFileFilter},
 * {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.IntegerInputValidator}
 * and {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.LinkToolTip}.
 * 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util;