package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;

import java.util.LinkedList;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.Pair;

public class SelectionModel {
	
	/**
	 * Devices which are currently selected
	 */
	public LinkedList<SmartDevice> selectedDevices = new LinkedList<SmartDevice>();
	public LinkedList<SmartDevice> selectedDevicesDrag = new LinkedList<SmartDevice>();

	/**
	 * Clicked Connections
	 */
	public LinkedList<Pair<Connection,Pair<Port,Port>>> clickedConnection = new LinkedList<Pair<Connection,Pair<Port,Port>>>();
	
	public SelectionModel() {}
}
