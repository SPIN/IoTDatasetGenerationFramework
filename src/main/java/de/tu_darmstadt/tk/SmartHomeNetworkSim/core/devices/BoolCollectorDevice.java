package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;

/**
 * Collector for boolean, which could act as an actuator
 *
 * @author Andreas T. Meyer-Berg
 */
public class BoolCollectorDevice extends SmartDevice implements BoolCollector {

	/**
	 * Name of the information
	 */
	private String infoName;
	/**
	 * last value
	 */
	private boolean val;
	/**
	 * Create a new Collector with the given name
	 * @param name name of the device
	 */
	public BoolCollectorDevice(String name) {
		super(name);
		init();
	}
	
	/**
	 * Creates a new collector
	 */
	public BoolCollectorDevice() {
		super();
		init();
	}
	
	/**
	 * initializes the fields
	 */
	private void init(){
		infoName = "doorOpen";
		val = false;
	}
	
	@Override
	public void setBCinfoName(String name) {
		this.infoName = name;
	}

	@Override
	public String getBCinfoName() {
		return infoName;
	}

	@Override
	public boolean getBCval() {
		return val;
	}

	@Override
	public void setBCval(boolean val) {
		this.val = val;
	}
}
