/**
 * Package for different SmartDevices, such as sensors or actuators
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;