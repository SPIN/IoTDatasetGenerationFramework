/**
 * Control classes, APIs and Interfaces for interaction with the {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model} and {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SimulationManager}.
 * 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.control;