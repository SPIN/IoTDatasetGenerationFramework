package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

/**
 * Analyzes the SWat physical data from a CSV file that are Open Office Calc conversions of the XLSX files.</br>
 * Conversion: Open Office Calc -> File -> Save as -> Text CSV -> Save -> OK 
 * -> Remove first line or two first lines by hand so the column description is in row 1 
 * -> Remove last (empty) line by hand</br>
 * The variable pathToCSVDirectory has to be set correctly.</br>
 * Throws exceptions if unrelated CSV files are in the same directory.
 * 
 * @author Fabian Kaiser
 */
public class SWaTPhysicalDataAnalysis {
	
	/**
	 * The path of the directory that contains the CSV file.
	 * The files for the results are written in this directory, too.
	 */
	public static String pathToCSVDirectory = "C:/Users/user/master";
	
	/**
	 * Main method for the analysis.
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException, ParseException {
		mainAnalysis();
		analyzeFITs();
		partialAnalysisP302("SWaT_Dataset_Normal_v1.csv","P-302",25,3086,500000);
		partialAnalysisMV301Transitions("SWaT_Dataset_Normal_v1.csv");
	}
	
	/**
	 * Analyzes unique values, backwash behavior, and motorized valves. Takes ~10 seconds.
	 */	
	@SuppressWarnings("unused")
	private static void mainAnalysis() throws IOException, ParseException {
		//Sets for the unique entries in the csv files.
		LinkedHashSet<String> device_mv101;
		LinkedHashSet<String> device_p101;	
		LinkedHashSet<String> device_p102;
		LinkedHashSet<String> device_mv201;
		LinkedHashSet<String> device_p201;
		LinkedHashSet<String> device_p202;
		LinkedHashSet<String> device_p203;
		LinkedHashSet<String> device_p204;
		LinkedHashSet<String> device_p205;
		LinkedHashSet<String> device_mv301;
		LinkedHashSet<String> device_mv302;
		LinkedHashSet<String> device_mv303;
		LinkedHashSet<String> device_mv304;
		LinkedHashSet<String> device_p301;
		LinkedHashSet<String> device_p302;
		LinkedHashSet<String> device_p401;
		LinkedHashSet<String> device_p402;
		LinkedHashSet<String> device_p403;
		LinkedHashSet<String> device_p404;
		LinkedHashSet<String> device_uv401;
		LinkedHashSet<String> device_p501;
		LinkedHashSet<String> device_p502;
		LinkedHashSet<String> device_p601;
		LinkedHashSet<String> device_p602;
		LinkedHashSet<String> device_p603;
		
		
		// 0 series for motorized valves
		LinkedList<String> mv101_ZeroLength_1to2;
		LinkedList<String> mv101_ZeroLength_2to1;	
		LinkedList<String> mv201_ZeroLength_1to2;
		LinkedList<String> mv201_ZeroLength_2to1;	
		LinkedList<String> mv301_ZeroLength_1to2;
		LinkedList<String> mv301_ZeroLength_2to1;	
		LinkedList<String> mv302_ZeroLength_1to2;
		LinkedList<String> mv302_ZeroLength_2to1;	
		LinkedList<String> mv303_ZeroLength_1to2;
		LinkedList<String> mv303_ZeroLength_2to1;	
		LinkedList<String> mv304_ZeroLength_1to2;
		LinkedList<String> mv304_ZeroLength_2to1;	
		
		
		LinkedHashSet<String> backWashPumpActivity;
	
		
		List<String> csvFiles = new ArrayList<String>();		
		//Read all csv files from folder
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}
		
		//Process csv files
		String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
		for(String filename:csvFiles) {
			device_mv101 = new LinkedHashSet<String>();
			device_p101 = new LinkedHashSet<String>();	
			device_p102 = new LinkedHashSet<String>();
			device_mv201 = new LinkedHashSet<String>();
			device_p201 = new LinkedHashSet<String>();
			device_p202 = new LinkedHashSet<String>();
			device_p203 = new LinkedHashSet<String>();
			device_p204 = new LinkedHashSet<String>();
			device_p205 = new LinkedHashSet<String>();
			device_mv301 = new LinkedHashSet<String>();
			device_mv302 = new LinkedHashSet<String>();
			device_mv303 = new LinkedHashSet<String>();
			device_mv304 = new LinkedHashSet<String>();
			device_p301 = new LinkedHashSet<String>();
			device_p302 = new LinkedHashSet<String>();
			device_p401 = new LinkedHashSet<String>();
			device_p402 = new LinkedHashSet<String>();
			device_p403 = new LinkedHashSet<String>();
			device_p404 = new LinkedHashSet<String>();
			device_uv401 = new LinkedHashSet<String>();
			device_p501 = new LinkedHashSet<String>();
			device_p502 = new LinkedHashSet<String>();
			device_p601 = new LinkedHashSet<String>();
			device_p602 = new LinkedHashSet<String>();
			device_p603 = new LinkedHashSet<String>();
			mv101_ZeroLength_1to2 = new LinkedList<String>();
			mv101_ZeroLength_1to2.add("Length of 0 series for MV101 at transitions from 1 to 2.\n");
			mv101_ZeroLength_2to1 = new LinkedList<String>();
			mv101_ZeroLength_2to1.add("Length of 0 series for MV101 at transitions from 2 to 1.\n");	
			mv201_ZeroLength_1to2 = new LinkedList<String>();
			mv201_ZeroLength_1to2.add("Length of 0 series for MV201 at transitions from 1 to 2.\n");
			mv201_ZeroLength_2to1 = new LinkedList<String>();
			mv201_ZeroLength_2to1.add("Length of 0 series for MV201 at transitions from 2 to 1.\n");	
			mv301_ZeroLength_1to2 = new LinkedList<String>();
			mv301_ZeroLength_1to2.add("Length of 0 series for MV301 at transitions from 1 to 2.\n");
			mv301_ZeroLength_2to1 = new LinkedList<String>();
			mv301_ZeroLength_2to1.add("Length of 0 series for MV301 at transitions from 2 to 1.\n");	
			mv302_ZeroLength_1to2 = new LinkedList<String>();
			mv302_ZeroLength_1to2.add("Length of 0 series for MV302 at transitions from 1 to 2.\n");
			mv302_ZeroLength_2to1 = new LinkedList<String>();
			mv302_ZeroLength_2to1.add("Length of 0 series for MV302 at transitions from 2 to 1.\n");	
			mv303_ZeroLength_1to2 = new LinkedList<String>();
			mv303_ZeroLength_1to2.add("Length of 0 series for MV303 at transitions from 1 to 2.\n");
			mv303_ZeroLength_2to1 = new LinkedList<String>();
			mv303_ZeroLength_2to1.add("Length of 0 series for MV303 at transitions from 2 to 1.\n");	
			mv304_ZeroLength_1to2 = new LinkedList<String>();
			mv304_ZeroLength_1to2.add("Length of 0 series for MV304 at transitions from 1 to 2.\n");
			mv304_ZeroLength_2to1 = new LinkedList<String>();
			mv304_ZeroLength_2to1.add("Length of 0 series for MV304 at transitions from 2 to 1.\n");
			backWashPumpActivity = new  LinkedHashSet<String>();
			backWashPumpActivity.add("Start and stop times of the backwash pump P-602.\nStart timestamp\t\t\tEnd timestamp\t\t\tDuration\tSeconds since last\t\tMinutes since last\n");
			
			
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			int lineCounter = 0;
			int mv101_zeroCounter = 0;
			int mv101_lastValueBefore0 = -1;
			int mv101_lastValue = -1;
			boolean mv101_lastLineZero = false;
			int mv201_zeroCounter = 0;
			int mv201_lastValueBefore0 = -1;
			int mv201_lastValue = -1;
			boolean mv201_lastLineZero = false;
			int mv301_zeroCounter = 0;
			int mv301_lastValueBefore0 = -1;
			int mv301_lastValue = -1;
			boolean mv301_lastLineZero = false;
			int mv302_zeroCounter = 0;
			int mv302_lastValueBefore0 = -1;
			int mv302_lastValue = -1;
			boolean mv302_lastLineZero = false;
			int mv303_zeroCounter = 0;
			int mv303_lastValueBefore0 = -1;
			int mv303_lastValue = -1;
			boolean mv303_lastLineZero = false;
			int mv304_zeroCounter = 0;
			int mv304_lastValueBefore0 = -1;
			int mv304_lastValue = -1;
			boolean mv304_lastLineZero = false;
			int p602_lastValue =-1;
			int p602_durationCounter = 1;
			String p602_startTime = "";
			Date p602_lastTime = new Date();
			


			
			while (csvFileReader.hasNextLine() == true) {
				lineCounter++;
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
		
				String timestamp = entries[0].trim();
				String fit101 = entries[1].trim();
				String lit101 = entries[2].trim();
				String mv101 = entries[3].trim();
				String p101 = entries[4].trim();
				String p102 = entries[5].trim();
				String ait201 = entries[6].trim();
				String ait202 = entries[7].trim();
				String ait203 = entries[8].trim();
				String fit201 = entries[9].trim();
				String mv201 = entries[10].trim();
				String p201 = entries[11].trim();
				String p202 = entries[12].trim();
				String p203 = entries[13].trim();
				String p204 = entries[14].trim();
				String p205 = entries[15].trim();
				String p206 = entries[16].trim();
				String dpit301 = entries[17].trim();
				String fit301 = entries[18].trim();
				String lit301 = entries[19].trim();
				String mv301 = entries[20].trim();
				String mv302 = entries[21].trim();
				String mv303 = entries[22].trim();
				String mv304 = entries[23].trim();
				String p301 = entries[24].trim();
				String p302 = entries[25].trim();
				String ait401 = entries[26].trim();
				String ait402 = entries[27].trim();
				String fit401 = entries[28].trim();
				String lit401 = entries[29].trim();
				String p401 = entries[30].trim();
				String p402 = entries[31].trim();
				String p403 = entries[32].trim();
				String p404 = entries[33].trim();
				String uv401 = entries[34].trim();
				String ait501 = entries[35].trim();
				String ait502 = entries[36].trim();
				String ait503 = entries[37].trim();
				String ait504 = entries[38].trim();
				String fit501 = entries[39].trim();
				String fit502 = entries[40].trim();
				String fit503 = entries[41].trim();
				String fit504 = entries[42].trim();
				String p501 = entries[43].trim();
				String p502 = entries[44].trim();
				String pit501 = entries[45].trim();
				String pit502 = entries[46].trim();
				String pit503 = entries[47].trim();
				String fit601 = entries[48].trim();
				String p601 = entries[49].trim();
				String p602 = entries[50].trim();
				String p603 = entries[51].trim();
				String behavior = entries[52].trim();
				

				
				if(lineCounter == 2) { //if first data line
					p602_lastTime = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a").parse(timestamp);
				}

				
				if(firstLine == false) { // if not first line

					
					if(p602.equals("2")) {
						if(p602_startTime.equals("") && p602_durationCounter == 1) {
							p602_startTime = timestamp;							
						}
						p602_durationCounter++;
						
					}
					else {
						if(p602_durationCounter > 1) {
							Date p602_thisTime = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a").parse(p602_startTime);
							long distance = (p602_thisTime.getTime()-p602_lastTime.getTime())/1000;
							double distanceMinutes = distance;
							distanceMinutes = distanceMinutes/60;
							backWashPumpActivity.add(p602_startTime+"\t"+timestamp+"\t"+p602_durationCounter+"\t\t\t"+distance+"\t\t\t\t\t"+distanceMinutes);
							p602_lastTime = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a").parse(p602_startTime);
							
						}
						p602_lastValue = 1;
						p602_durationCounter = 1;
						p602_startTime = "";
					}
					
					if(mv101.equals("0")) {
						mv101_zeroCounter++;
						mv101_lastLineZero = true;
						if(mv101_lastValueBefore0 == -1)
							mv101_lastValueBefore0 = mv101_lastValue;
					}
					else {
						if(mv101_zeroCounter != 0 && mv101_lastLineZero) {
							if(mv101_lastValueBefore0 == 1)
								mv101_ZeroLength_1to2.add(String.valueOf(mv101_zeroCounter));
							else if(mv101_lastValueBefore0 == 2)
								mv101_ZeroLength_2to1.add(String.valueOf(mv101_zeroCounter));
						}
						mv101_zeroCounter = 0;
						mv101_lastValueBefore0 = -1;
						mv101_lastLineZero = false;
					}	
					
					if(mv201.equals("0")) {
						mv201_zeroCounter++;
						mv201_lastLineZero = true;
						if(mv201_lastValueBefore0 == -1)
							mv201_lastValueBefore0 = mv201_lastValue;
					}
					else {
						if(mv201_zeroCounter != 0 && mv201_lastLineZero) {
							if(mv201_lastValueBefore0 == 1)
								mv201_ZeroLength_1to2.add(String.valueOf(mv201_zeroCounter));
							else if(mv201_lastValueBefore0 == 2)
								mv201_ZeroLength_2to1.add(String.valueOf(mv201_zeroCounter));
						}
						mv201_zeroCounter = 0;
						mv201_lastValueBefore0 = -1;
						mv201_lastLineZero = false;
					}	
					
					if(mv301.equals("0")) {
						mv301_zeroCounter++;
						mv301_lastLineZero = true;
						if(mv301_lastValueBefore0 == -1)
							mv301_lastValueBefore0 = mv301_lastValue;
					}
					else {
						if(mv301_zeroCounter != 0 && mv301_lastLineZero) {
							if(mv301_lastValueBefore0 == 1)
								mv301_ZeroLength_1to2.add(String.valueOf(mv301_zeroCounter));
							else if(mv301_lastValueBefore0 == 2)
								mv301_ZeroLength_2to1.add(String.valueOf(mv301_zeroCounter));
						}
						mv301_zeroCounter = 0;
						mv301_lastValueBefore0 = -1;
						mv301_lastLineZero = false;
					}	
					
					if(mv302.equals("0")) {
						mv302_zeroCounter++;
						mv302_lastLineZero = true;
						if(mv302_lastValueBefore0 == -1)
							mv302_lastValueBefore0 = mv302_lastValue;
					}
					else {
						if(mv302_zeroCounter != 0 && mv302_lastLineZero) {
							if(mv302_lastValueBefore0 == 1)
								mv302_ZeroLength_1to2.add(String.valueOf(mv302_zeroCounter));
							else if(mv302_lastValueBefore0 == 2)
								mv302_ZeroLength_2to1.add(String.valueOf(mv302_zeroCounter));
						}
						mv302_zeroCounter = 0;
						mv302_lastValueBefore0 = -1;
						mv302_lastLineZero = false;
					}	
					
					if(mv303.equals("0")) {
						mv303_zeroCounter++;
						mv303_lastLineZero = true;
						if(mv303_lastValueBefore0 == -1)
							mv303_lastValueBefore0 = mv303_lastValue;
					}
					else {
						if(mv303_zeroCounter != 0 && mv303_lastLineZero) {
							if(mv303_lastValueBefore0 == 1)
								mv303_ZeroLength_1to2.add(String.valueOf(mv303_zeroCounter));
							else if(mv303_lastValueBefore0 == 2)
								mv303_ZeroLength_2to1.add(String.valueOf(mv303_zeroCounter));
						}
						mv303_zeroCounter = 0;
						mv303_lastValueBefore0 = -1;
						mv303_lastLineZero = false;
					}	
					
					if(mv304.equals("0")) {
						mv304_zeroCounter++;
						mv304_lastLineZero = true;
						if(mv304_lastValueBefore0 == -1)
							mv304_lastValueBefore0 = mv304_lastValue;
					}
					else {
						if(mv304_zeroCounter != 0 && mv304_lastLineZero) {
							if(mv304_lastValueBefore0 == 1)
								mv304_ZeroLength_1to2.add(String.valueOf(mv304_zeroCounter));
							else if(mv304_lastValueBefore0 == 2)
								mv304_ZeroLength_2to1.add(String.valueOf(mv304_zeroCounter));
						}
						mv304_zeroCounter = 0;
						mv304_lastValueBefore0 = -1;
						mv304_lastLineZero = false;
					}	
					
					mv101_lastValue = Integer.parseInt(mv101);
					mv201_lastValue = Integer.parseInt(mv201);
					mv301_lastValue = Integer.parseInt(mv301);
					mv302_lastValue = Integer.parseInt(mv302);
					mv303_lastValue = Integer.parseInt(mv303);
					mv304_lastValue = Integer.parseInt(mv304);
				}
				else { // if first line
					firstLine = false;
				}
				
				device_mv101.add(mv101.trim());
				device_p101.add(p101.trim());	
				device_p102.add(p102.trim());
				device_mv201.add(mv201.trim());
				device_p201.add(p201.trim());
				device_p202.add(p202.trim());
				device_p203.add(p203.trim());
				device_p204.add(p204.trim());
				device_p205.add(p205.trim());
				device_mv301.add(mv301.trim());
				device_mv302.add(mv302.trim());
				device_mv303.add(mv303.trim());
				device_mv304.add(mv304.trim());
				device_p301.add(p301.trim());
				device_p302.add(p302.trim());
				device_p401.add(p401.trim());
				device_p402.add(p402.trim());
				device_p403.add(p403.trim());
				device_p404.add(p404.trim());
				device_uv401.add(uv401.trim());
				device_p501.add(p501.trim());
				device_p502.add(p502.trim());
				device_p601.add(p601.trim());
				device_p602.add(p602.trim());
				device_p603.add(p603.trim());
				

			}
			fileNameStart +=filename;
			FileWriter rFWriter;
			
		
			//Write Output
			
			String resultsFileName = fileNameStart+"_Unique Values of Actuators.txt";
		    rFWriter = new FileWriter(pathToCSVDirectory+"/"+resultsFileName);
		    rFWriter.write(device_mv101.toString()+"\n");
		    rFWriter.write(device_p101.toString()+"\n");	
		    rFWriter.write(device_p102.toString()+"\n");
		    rFWriter.write(device_mv201.toString()+"\n");
		    rFWriter.write(device_p201.toString()+"\n");
		    rFWriter.write(device_p202.toString()+"\n");
		    rFWriter.write(device_p203.toString()+"\n");
		    rFWriter.write(device_p204.toString()+"\n");
		    rFWriter.write(device_p205.toString()+"\n");
		    rFWriter.write(device_mv301.toString()+"\n");
		    rFWriter.write(device_mv302.toString()+"\n");
		    rFWriter.write(device_mv303.toString()+"\n");
		    rFWriter.write(device_mv304.toString()+"\n");
		    rFWriter.write(device_p301.toString()+"\n");
		    rFWriter.write(device_p302.toString()+"\n");
		    rFWriter.write(device_p401.toString()+"\n");
		    rFWriter.write(device_p402.toString()+"\n");
		    rFWriter.write(device_p403.toString()+"\n");
		    rFWriter.write(device_p404.toString()+"\n");
		    rFWriter.write(device_uv401.toString()+"\n");
		    rFWriter.write(device_p501.toString()+"\n");
		    rFWriter.write(device_p502.toString()+"\n");
		    rFWriter.write(device_p601.toString()+"\n");
		    rFWriter.write(device_p602.toString()+"\n");
		    rFWriter.write(device_p603.toString()+"\n");
		    rFWriter.close();

		    HashMap<String, LinkedList<String>> outputList = new HashMap<String, LinkedList<String>>();
		    outputList.put(fileNameStart+"_0 Series MV101 1 to 2.txt",mv101_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV101 2 to 1.txt",mv101_ZeroLength_2to1);
		    outputList.put(fileNameStart+"_0 Series MV201 1 to 2.txt",mv201_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV201 2 to 1.txt",mv201_ZeroLength_2to1);
		    outputList.put(fileNameStart+"_0 Series MV301 1 to 2.txt",mv301_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV301 2 to 1.txt",mv301_ZeroLength_2to1);
		    outputList.put(fileNameStart+"_0 Series MV302 1 to 2.txt",mv302_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV302 2 to 1.txt",mv302_ZeroLength_2to1);
		    outputList.put(fileNameStart+"_0 Series MV303 1 to 2.txt",mv303_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV303 2 to 1.txt",mv303_ZeroLength_2to1);
		    outputList.put(fileNameStart+"_0 Series MV304 1 to 2.txt",mv304_ZeroLength_1to2);
		    outputList.put(fileNameStart+"_0 Series MV304 2 to 1.txt",mv304_ZeroLength_2to1);
		    
		    for(String fileNameComplete:outputList.keySet()) {
			    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameComplete);
			    boolean firstLineOut1 = true;
			    HashMap<String, Integer> zeroDistribution = new HashMap<String, Integer>();
			    LinkedList<String> zeroLengthList = outputList.get(fileNameComplete);
			    for(String str:zeroLengthList) {		    	
			    	if(firstLineOut1 == false) {
			    		if(zeroDistribution.containsKey(str)) {
			    			zeroDistribution.put(str, zeroDistribution.get(str)+1);
			    		}
			    		else
			    			zeroDistribution.put(str, 1);
			    	}
			    	else {
			    		firstLineOut1 = false;
			    		rFWriter.write(str+"\n");
			    	}
			    	
			    	
			    }
			    int totalZeroes = zeroLengthList.size()-1;
			    for(String key:zeroDistribution.keySet()) {
			    	int currentValue = zeroDistribution.get(key);
			    	double d = currentValue;
			    	d = d/totalZeroes;
			    	rFWriter.write(key+"\t"+currentValue+"/"+totalZeroes+"\t"+d+"\n");
			    }
			    	
			    
			    rFWriter.close();	    	
		    } 
		    
		    
			HashMap<String, LinkedHashSet<String>> outputList2 = new HashMap<String, LinkedHashSet<String>>();
			outputList2.put(fileNameStart+"_Backwash Pump Activity.txt", backWashPumpActivity);


		    for(String fileNameComplete:outputList2.keySet()) {
			    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameComplete);
			    for(String str:outputList2.get(fileNameComplete)) {
			    	rFWriter.write(str+"\n");	
			    }
			    rFWriter.close();	    	
		    } 		
		
		
		    csvFileReader.close();
			}
	
		
		
	}
	/**
	 * Analyzes the extremes of FIT.
	 */
	private static void partialAnalysisFITExtrema(String fileName, String name, int csvColumnNumber, int startPosition, int stopPosition, double lowerBoundForMinimum, double upperBoundForMaximum) throws IOException {
		LinkedList<Double> fitWithoutPlateau;
		
		List<String> csvFiles = new ArrayList<String>();		
		csvFiles.add(fileName);
		
		//Process csv files
		String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
		for(String filename:csvFiles) {
			fitWithoutPlateau = new LinkedList<Double>();
			
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			int lineCounter = 0;
			
			while (csvFileReader.hasNextLine() == true) {
				lineCounter++;
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");		
				String fit101 = entries[csvColumnNumber].trim();
			
				if(lineCounter >= startPosition && lineCounter <= stopPosition) {
					double fitValue = Double.parseDouble(fit101);
					if(fitWithoutPlateau.size() == 0 || fitWithoutPlateau.get(fitWithoutPlateau.size()-1) != fitValue)
						fitWithoutPlateau.add(fitValue);
				}

			}
			csvFileReader.close();
			fileNameStart +=filename;
			FileWriter rFWriter;
			
			LinkedHashMap<String, LinkedList<Double>> fitWithoutPlateauMap = new LinkedHashMap<String, LinkedList<Double>>();
			fitWithoutPlateauMap.put(name,  fitWithoutPlateau);

			for(String keyStr:fitWithoutPlateauMap.keySet()) {
				LinkedList<Double> plateauList = fitWithoutPlateauMap.get(keyStr);
				LinkedList<Double> maximaList = new LinkedList<Double>();
				LinkedList<Double> minimaList = new LinkedList<Double>();
				double db;
				double lastValue = 0.0;
				double secondLastValue = 0.0;
				for(int i = 0; i < plateauList.size(); i++) {
					db = plateauList.get(i);
					if(i > 1) {				
							if(secondLastValue < lastValue && lastValue > db && lastValue <= upperBoundForMaximum) {
								maximaList.add(lastValue);
							}
								
							else if(secondLastValue > lastValue && lastValue < db && lastValue >= lowerBoundForMinimum)
								minimaList.add(lastValue);
							secondLastValue = lastValue;
							lastValue = db;
					}
					else if(i == 0) secondLastValue = db;
					else if(i == 1) lastValue = db;
				}
				LinkedHashMap<Double, Integer> minimaCounter = new LinkedHashMap<Double, Integer>();
				for(double dbmin:minimaList) {
					if(minimaCounter.containsKey(dbmin))
						minimaCounter.put(dbmin, minimaCounter.get(dbmin)+1);
					else
						minimaCounter.put(dbmin,1);
				}
				LinkedHashMap<Double, Integer> maximaCounter = new LinkedHashMap<Double, Integer>();
				for(double dbmin:maximaList) {
					if(maximaCounter.containsKey(dbmin))
						maximaCounter.put(dbmin, maximaCounter.get(dbmin)+1);
					else
						maximaCounter.put(dbmin,1);
				}
				DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));
				 boolean firstWriteLine = true;
				 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+keyStr+" Minima Distribution.txt");
				 for(double key: minimaCounter.keySet()) {
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					 rFWriter.write(minimaCounter.get(key)+"\t"+decimalFormat.format(key));
				 }
				 rFWriter.close();
				 firstWriteLine = true;
				 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+keyStr+" Maxima Distribution.txt");
				 for(double key: maximaCounter.keySet()) {
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					 rFWriter.write(maximaCounter.get(key)+"\t"+decimalFormat.format(key));
				 }
				 rFWriter.close();			 
			}			 
			///// FIT -- END (3/3)
		}
	}
	
	
	
	/**
	 * Analyzes the flanks of FITs.
	 * @throws IOException 
	 */	
	private static void partialAnalysisFITSharpFlanksDistribution(String fileName, String name, int csvColumnNumber) throws IOException {
		LinkedList<Double> fit101Values;
		
		List<String> csvFiles = new ArrayList<String>();		
		csvFiles.add(fileName);
		
		//Process csv files
		String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
		for(String filename:csvFiles) {
			
			fit101Values = new LinkedList<Double>();
	
			
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
		
				String fitEntry = entries[csvColumnNumber].trim();

				
				if(firstLine == false) {
					fit101Values.add(Double.parseDouble(fitEntry));	
				}
				else {
					firstLine = false;
				}
			}
			csvFileReader.close();
			fileNameStart +=filename;
			FileWriter rFWriter;
			
			LinkedHashMap<String, LinkedList<Double>> fitValues = new LinkedHashMap<String, LinkedList<Double>>();
			fitValues.put(name,  fit101Values);
			
			LinkedHashMap<String, LinkedHashMap<Double, Integer>> fitIncreaseDistribution = new LinkedHashMap<String, LinkedHashMap<Double, Integer>>();
			fitIncreaseDistribution.put(name, new LinkedHashMap<Double, Integer>());
	
			LinkedHashMap<String, LinkedHashMap<Double, Integer>> fitDecreaseDistribution = new LinkedHashMap<String, LinkedHashMap<Double, Integer>>();
			fitDecreaseDistribution.put(name, new LinkedHashMap<Double, Integer>());
			
			LinkedList<Double> list;
			for(String fit:fitValues.keySet()) {
				list = fitValues.get(fit);
				int listSize = list.size();
				double value;
				double lastValue = 0.0;
				boolean newFlank = false;
				LinkedList<Double> valuesSinceZero = new LinkedList<Double>();
				for(int i = 0; i < listSize; i++) {
					value = list.get(i);
					if(i != 0) {
						if(value != 0 && lastValue == 0) 
							newFlank = true;
						if(newFlank) {
							if(lastValue <= value) {
								valuesSinceZero.add(value);
							}
							else {
								double lastDouble = 0.0;
								LinkedHashMap<Double, Integer> valuesTemp = fitIncreaseDistribution.get(fit);
								for(double db:valuesSinceZero) {
									double duration = BigDecimal.valueOf(db).subtract(BigDecimal.valueOf(lastDouble)).doubleValue();
									if(duration > 2) {
										System.out.println(db+" - "+lastDouble+" = "+duration);
										System.out.println(valuesSinceZero.toString());
									}
									if(valuesTemp.containsKey(duration))
										valuesTemp.put(duration, valuesTemp.get(duration)+1);
									else
										valuesTemp.put(duration, 1);
									fitIncreaseDistribution.put(fit, valuesTemp);
									lastDouble = db;
								}
								valuesSinceZero = new LinkedList<Double>();
								newFlank = false;
							}
						}
					}
					lastValue = value;
				}
				newFlank = false;
				valuesSinceZero = new LinkedList<Double>();
				for(int i = listSize-1; i > 0; i--) {
					value = list.get(i);
					if(i != listSize-1) {
						if(value != 0 && lastValue == 0) 
							newFlank = true;
						if(newFlank) {
							if(lastValue <= value) {
								valuesSinceZero.add(value);
							}
							else {
								double lastDouble = 0.0;
								LinkedHashMap<Double, Integer> valuesTemp = fitDecreaseDistribution.get(fit);
								for(double db:valuesSinceZero) {
									double duration = BigDecimal.valueOf(db).subtract(BigDecimal.valueOf(lastDouble)).doubleValue();
									if(valuesTemp.containsKey(duration))
										valuesTemp.put(duration, valuesTemp.get(duration)+1);
									else
										valuesTemp.put(duration, 1);
									fitDecreaseDistribution.put(fit, valuesTemp);
									lastDouble = db;
								}
								valuesSinceZero = new LinkedList<Double>();
								newFlank = false;
							}
						}
						lastValue = value;					
					}
				}
			}
			DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));
			boolean firstWriteLine = true;
			for(String keyStr:fitIncreaseDistribution.keySet()) {	
				firstWriteLine = true;
				rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+keyStr+" Flank Distribution Sharply Increasing.txt");
				LinkedHashMap<Double, Integer> current = fitIncreaseDistribution.get(keyStr);
				for(double dbNow:current.keySet()){
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					rFWriter.write(current.get(dbNow)+"\t"+decimalFormat.format(dbNow));
				}
				rFWriter.close();
			
				firstWriteLine = true;
				rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+keyStr+" Flank Distribution Sharply Decreasing.txt");
				current = fitDecreaseDistribution.get(keyStr);
				for(double dbNow:current.keySet()){
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					rFWriter.write(current.get(dbNow)+"\t"+decimalFormat.format(dbNow));
				}
				rFWriter.close();				
				
			}
			
			
		}
	
	}
	
	/**
	 * Analyzes the flank increase and decrease distribution of FITs.
	 * @throws IOException
	 */
	private static void partialAnalysisFITFlankDistributions(String fileName, String name, int csvColumnNumber, double lowerBound, double upperBound, double minimumValue, double maximumValue) throws IOException {
		List<String> csvFiles = new ArrayList<String>();		
		csvFiles.add(fileName);
		
		LinkedList<Double> increaseList;
		LinkedList<Double> decreaseList;
		LinkedList<Integer> plateauList;
		//Process csv files
		String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
		for(String filename:csvFiles) {
			increaseList = new LinkedList<Double>();
			decreaseList = new LinkedList<Double>();
			plateauList = new LinkedList<Integer>();
			
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			
			double lastValue = 0.0;
			int counter = 0;
			while (csvFileReader.hasNextLine() == true) {
				counter++;
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
		
				String fitEntry = entries[csvColumnNumber].trim();				
				
				if(firstLine == false) {
					double value = Double.parseDouble(fitEntry);
					if(value >= lowerBound && value <= upperBound) {
						if(lastValue != value) {
							double difference = 0.0;
							if(lastValue < value) { // increasing
								difference = BigDecimal.valueOf(value).subtract(BigDecimal.valueOf(lastValue)).doubleValue();
								increaseList.add(difference);
							}
							else if(lastValue > value) { //decreasing
								difference = BigDecimal.valueOf(lastValue).subtract(BigDecimal.valueOf(value)).doubleValue();
								decreaseList.add(difference);
							}
						}
						else {
							plateauList.add(counter);
						}
					}
					
					lastValue = value;
				}
				else {
					firstLine = false;
				}
			}
			csvFileReader.close();
			fileNameStart +=filename;
			FileWriter rFWriter;
			
			LinkedHashMap<Double, Integer> uniqueValues = new LinkedHashMap<Double, Integer>();
			for(double db:increaseList) {
				if(uniqueValues.containsKey(db))
					uniqueValues.put(db, uniqueValues.get(db)+1);
				else
					uniqueValues.put(db, 1);
			}			
			DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));			 
			 boolean firstWriteLine = true;
			 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Flank Distribution Increasing.txt");
			 for(double key: uniqueValues.keySet()) {
				 if(key >= minimumValue && key <= maximumValue) {
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					 rFWriter.write(uniqueValues.get(key)+"\t"+decimalFormat.format(key));
				 }
			 }
			  rFWriter.close();
			 uniqueValues = new LinkedHashMap<Double, Integer>();
			 for(double db:decreaseList) {
				if(uniqueValues.containsKey(db))
					uniqueValues.put(db, uniqueValues.get(db)+1);
				else
					uniqueValues.put(db, 1);
			 }
			 firstWriteLine = true;
			 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Flank Distribution Decreasing.txt");
			 for(double key: uniqueValues.keySet()) {
				 if(key >= minimumValue && key <= maximumValue) {
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					 rFWriter.write(uniqueValues.get(key)+"\t"+decimalFormat.format(key));
				 }
			 }
			 rFWriter.close();	
			 
			 LinkedHashMap<Integer, Integer> plateauDistributions = new LinkedHashMap<Integer, Integer>();
			 int currentPlateauLength = 0;
			 for(int i = 1; i < plateauList.size(); i++) {
				 if(plateauList.get(i) == plateauList.get(i-1)+1) {
					 currentPlateauLength++;
				 }
				 else {
					if(plateauDistributions.containsKey(currentPlateauLength))
						plateauDistributions.put(currentPlateauLength, plateauDistributions.get(currentPlateauLength)+1);
					else
						plateauDistributions.put(currentPlateauLength, 1);
					currentPlateauLength = 0;
				 }
			 }
			 firstWriteLine = true;
			 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Flank Plateaus Distribution.txt");
			 for(int key: plateauDistributions.keySet()) {
				 if(firstWriteLine == false)
					 rFWriter.write("\n");
				 else
					 firstWriteLine = false;
				 rFWriter.write(plateauDistributions.get(key)+"\t"+decimalFormat.format(key));
			 }
			 rFWriter.close();	
		}
	}

	/**
	 * Analyzes the fluctuation of LIT-101.
	 * Results are useless.
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	private static void partialAnalysisLIT101() throws IOException {
		List<String> csvFiles = new ArrayList<String>();		
		//Read all csv files from folder
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}
			
			//Process csv files
			String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
			for(String filename:csvFiles) {
				//Open file
				File csvFile = new File(pathToCSVDirectory+"/"+filename);
				Scanner csvFileReader = new Scanner(csvFile);
				boolean firstLine = true;
				boolean lastValueOK = false;
				double lastValue = 0.0;
				boolean lastValueOKfit101 = false;
				double lastValuefit101 = 0.0;
				boolean lastValueOK3 = false;
				double lastValue3 = 0.0;
				boolean lastValueOK4 = false;
				double lastValue4 = 0.0;	
				LinkedHashMap<Double, Integer> differences = new LinkedHashMap<Double, Integer>();
				LinkedHashMap<Double, Integer> differences2 = new LinkedHashMap<Double, Integer>();
				LinkedHashMap<Double, Integer> differences3 = new LinkedHashMap<Double, Integer>();
				LinkedHashMap<Double, Integer> differences4 = new LinkedHashMap<Double, Integer>();
				while (csvFileReader.hasNextLine() == true) {
					String csvLine = csvFileReader.nextLine();
					String[] entries = csvLine.split(",");
			
					String fit101 = entries[1].trim();
					String lit101 = entries[2].trim();
					String mv101 = entries[3].trim();
					String p101 = entries[4].trim();
					String p102 = entries[5].trim();
					
					if(firstLine == false) {
						double fit101db = Double.parseDouble(fit101);
						double lit101db = Double.parseDouble(lit101);
						if(fit101db == 0 && mv101.equals("1") && p101.equals("1") && p102.equals("1")) {
							if(lastValueOK) {
								double difference = BigDecimal.valueOf(lit101db).subtract(BigDecimal.valueOf(lastValue)).doubleValue();
								if(differences.containsKey(difference))
									differences.put(difference, differences.get(difference)+1);
								else
									differences.put(difference, 1);
							}
							else
								lastValueOK = true;
						}
						else
							lastValueOK = false;
						lastValue = lit101db;
						
						
						if(fit101db > 0 && mv101.equals("2") && p101.equals("1") && p102.equals("1")) {
							if(lastValueOKfit101) {
								double difference = BigDecimal.valueOf(lit101db).subtract(BigDecimal.valueOf(lastValuefit101)).doubleValue();
								if(differences2.containsKey(difference))
									differences2.put(difference, differences2.get(difference)+1);
								else
									differences2.put(difference, 1);	
							}
							else
								lastValueOKfit101 = true;
						}
						else
							lastValueOKfit101 = false;
						lastValuefit101 = lit101db;
						
						
						if(fit101db > 0 && mv101.equals("2") && (p101.equals("2") || p102.equals("2"))) {
							if(lastValueOK3) {
								double difference = BigDecimal.valueOf(lit101db).subtract(BigDecimal.valueOf(lastValue3)).doubleValue();
								if(differences3.containsKey(difference))
									differences3.put(difference, differences3.get(difference)+1);
								else
									differences3.put(difference, 1);	
							}
							else
								lastValueOK3 = true;
						}
						else
							lastValueOK3 = false;
						lastValue3 = lit101db;
						
						
						if(fit101db == 0 && mv101.equals("1") && (p101.equals("2") || p102.equals("2"))) {
							if(lastValueOK4) {
								double difference = BigDecimal.valueOf(lit101db).subtract(BigDecimal.valueOf(lastValue4)).doubleValue();
								if(differences4.containsKey(difference))
									differences4.put(difference, differences4.get(difference)+1);
								else
									differences4.put(difference, 1);	
							}
							else
								lastValueOK4 = true;
						}
						else
							lastValueOK4 = false;
						lastValue4 = lit101db;
						
					}
					else {
						firstLine = false;
					}
				}
				csvFileReader.close();
				
				csvFileReader.close();
				fileNameStart +=filename;
				FileWriter rFWriter;
				DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));			 
				
				HashMap<String, LinkedHashMap<Double, Integer>> outputList2 = new HashMap<String, LinkedHashMap<Double, Integer>>();
				outputList2.put(fileNameStart+"_LIT-101 Ventil Closed Pumps Off Distribution.txt", differences);
				outputList2.put(fileNameStart+"_LIT-101 Ventil Open Pumps Off Distribution.txt", differences2);
				outputList2.put(fileNameStart+"_LIT-101 Ventil Open Pumps On Distribution.txt", differences3);
				outputList2.put(fileNameStart+"_LIT-101 Ventil Closed Pumps On Distribution.txt", differences4);
				
				boolean firstWriteLine = true;
			    for(String fileNameComplete:outputList2.keySet()) {
			    	firstWriteLine = true;
				    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameComplete);
				    LinkedHashMap<Double, Integer> temper = outputList2.get(fileNameComplete);
					 for(double key: temper.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(temper.get(key)+"\t"+decimalFormat.format(key));
					 }
				    rFWriter.close();	    	
			    } 	
			}
	}	
				
			/**
			 * Analyzes the extremes of LITs.
			 */
			private static void partialAnalysisLITExtrema(String fileName, String name, int csvColumnNumber, int startPosition, int stopPosition) throws IOException {
				LinkedList<Double> litWithoutPlateau;				
				
				List<String> csvFiles = new ArrayList<String>();		
				csvFiles.add(fileName);
				
				//Process csv files
				String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
				for(String filename:csvFiles) {
					litWithoutPlateau = new LinkedList<Double>();
					
					//Open file
					File csvFile = new File(pathToCSVDirectory+"/"+filename);
					Scanner csvFileReader = new Scanner(csvFile);
					int lineCounter = 0;
					
					while (csvFileReader.hasNextLine() == true) {
						lineCounter++;
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");
				
						String lit = entries[csvColumnNumber].trim();
					
						if(lineCounter >= startPosition && lineCounter <= stopPosition) {
							double litdb = Double.parseDouble(lit);
							if(litWithoutPlateau.size() == 0 || litWithoutPlateau.get(litWithoutPlateau.size()-1) != litdb)
								litWithoutPlateau.add(litdb);
						}
	
					}
					csvFileReader.close();
					fileNameStart +=filename;
					FileWriter rFWriter;

					
					LinkedList<Double> plateauList = litWithoutPlateau;
					LinkedList<Double> maximaList = new LinkedList<Double>();
					LinkedList<Double> minimaList = new LinkedList<Double>();
					LinkedList<Double> minMaxList = new LinkedList<Double>();
					double currentValue;
					double lastValue = 0.0;
					double secondLastValue = 0.0;
					double increaseDecreaseStart = 0.0;
					boolean increase = false;
					boolean decrease = false;
					for(int i = 0; i < plateauList.size(); i++) {
						currentValue = plateauList.get(i);
						if(i > 1) {	
							if(increase == false && decrease == false) {
								if(secondLastValue < lastValue) {
									increase = true;
									increaseDecreaseStart = secondLastValue;
								}
								else if(secondLastValue > lastValue) {
									decrease = true;
									increaseDecreaseStart = secondLastValue;
								}
							}
							double extreme;
							if(increase && lastValue > currentValue) {
								extreme = BigDecimal.valueOf(lastValue).subtract(BigDecimal.valueOf(increaseDecreaseStart)).doubleValue();
								maximaList.add(extreme);
								minMaxList.add(extreme);
								decrease = true;
								increase = false;
								increaseDecreaseStart = currentValue;
							}
							else if (decrease && lastValue < currentValue) {
								extreme = BigDecimal.valueOf(increaseDecreaseStart).subtract(BigDecimal.valueOf(lastValue)).doubleValue();
								minimaList.add(extreme);
								minMaxList.add(-extreme);
								decrease = false;
								increase = true;
								increaseDecreaseStart = currentValue;
							}
							secondLastValue = lastValue;
							lastValue = currentValue;
						}
						else if(i == 0) secondLastValue = currentValue;
						else if(i == 1) lastValue = currentValue;
					}
					LinkedHashMap<Double, Integer> minimaCounter = new LinkedHashMap<Double, Integer>();
					for(double dbmin:minimaList) {
						if(minimaCounter.containsKey(dbmin))
							minimaCounter.put(dbmin, minimaCounter.get(dbmin)+1);
						else
							minimaCounter.put(dbmin,1);
					}
					LinkedHashMap<Double, Integer> maximaCounter = new LinkedHashMap<Double, Integer>();
					for(double dbmin:maximaList) {
						if(maximaCounter.containsKey(dbmin))
							maximaCounter.put(dbmin, maximaCounter.get(dbmin)+1);
						else
							maximaCounter.put(dbmin,1);
					}
					DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));
					 boolean firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Minima Distribution.txt");
					 for(double key: minimaCounter.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(minimaCounter.get(key)+"\t"+decimalFormat.format(key));
					 }
					 rFWriter.close();
					 firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Maxima Distribution.txt");
					 for(double key: maximaCounter.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(maximaCounter.get(key)+"\t"+decimalFormat.format(key));
					 }
					 rFWriter.close();					 
					 firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Extreme Value List.txt");
					 System.out.println(1);
					 for(double key: minMaxList) {
						 if(firstWriteLine == false)							 
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 System.out.println("valueList.add("+decimalFormat.format(key)+");");
						 rFWriter.write(decimalFormat.format(key)); 
					 }
					 rFWriter.close();	System.out.println(1);
				}
			}
		

			
			/**
			 * Analyzes the flank increase and decrease distribution of LITs.
			 * @throws IOException
			 */
			private static void partialAnalysisLITFlanks(String fileName, String name, int csvColumnNumber, int startPosition, int stopPosition) throws IOException {
				List<String> csvFiles = new ArrayList<String>();		
				csvFiles.add(fileName);
				
				LinkedList<Double> increaseList;
				LinkedList<Double> decreaseList;
				//Process csv files
				String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
				for(String filename:csvFiles) {
					increaseList = new LinkedList<Double>();
					decreaseList = new LinkedList<Double>();
					
					//Open file
					File csvFile = new File(pathToCSVDirectory+"/"+filename);
					Scanner csvFileReader = new Scanner(csvFile);

					double lastValue = 0.0;
					int lineCounter = 0;
					while (csvFileReader.hasNextLine() == true) {
						lineCounter++;
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");				
						
						String lit = entries[csvColumnNumber].trim();
					
						if(lineCounter >= startPosition && lineCounter <= stopPosition) {
							double value = Double.parseDouble(lit);
								if(lastValue != value) {
									double difference = 0.0;
									if(lastValue < value) { // increasing
										difference = BigDecimal.valueOf(value).subtract(BigDecimal.valueOf(lastValue)).doubleValue();
										increaseList.add(difference);
									}
									else if(lastValue > value) { //decreasing
										difference = BigDecimal.valueOf(lastValue).subtract(BigDecimal.valueOf(value)).doubleValue();
										decreaseList.add(difference);
									}
							}
							
							lastValue = value;
						}
					}
					csvFileReader.close();
					fileNameStart +=filename;
					FileWriter rFWriter;
					
					LinkedHashMap<Double, Integer> uniqueValues = new LinkedHashMap<Double, Integer>();
					for(double db:increaseList) {
						if(uniqueValues.containsKey(db))
							uniqueValues.put(db, uniqueValues.get(db)+1);
						else
							uniqueValues.put(db, 1);
					}			
					DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));			 
					 boolean firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" "+"Flank Distribution Increasing.txt");
					 for(double key: uniqueValues.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(uniqueValues.get(key)+"\t"+decimalFormat.format(key));
					 }
					  rFWriter.close();
					 uniqueValues = new LinkedHashMap<Double, Integer>();
					 for(double db:decreaseList) {
						if(uniqueValues.containsKey(db))
							uniqueValues.put(db, uniqueValues.get(db)+1);
						else
							uniqueValues.put(db, 1);
					 }
					 firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" "+"Flank Distribution Decreasing.txt");
					 for(double key: uniqueValues.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(uniqueValues.get(key)+"\t"+decimalFormat.format(key));
					 }
					 rFWriter.close();	
				}
			}
			
			
			/**
			 * Analyzes the flank increase and decrease distribution of LITs.
			 * @throws IOException
			 */
			private static void partialAnalysisLITZeros(String fileName, String name, int csvColumnNumber, int startPosition, int stopPosition) throws IOException {
				List<String> csvFiles = new ArrayList<String>();		
				csvFiles.add(fileName);
				
				LinkedList<Double> valuesList;
				//Process csv files
				String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
				for(String filename:csvFiles) {
					valuesList = new LinkedList<Double>();
					
					//Open file
					File csvFile = new File(pathToCSVDirectory+"/"+filename);
					Scanner csvFileReader = new Scanner(csvFile);

					int lineCounter = 1;
					while (csvFileReader.hasNextLine() == true) {
						lineCounter++;
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");				
						
						String lit = entries[csvColumnNumber].trim();
					
						if(lineCounter >= startPosition && lineCounter <= stopPosition) {
							double value = Double.parseDouble(lit);
								valuesList.add(value);
						}
						
					}
					csvFileReader.close();
					fileNameStart +=filename;
					FileWriter rFWriter;
					
					double temp = BigDecimal.valueOf(valuesList.getLast()).subtract(BigDecimal.valueOf(valuesList.getFirst())).doubleValue();
					double mean = temp/valuesList.size();
					
					LinkedList<Double> differences = new LinkedList<Double>();
					for(int i = 1; i < valuesList.size(); i++) {
						//valuesList.get(i-1)-valuesList.get(i)-mean						
						double difference = BigDecimal.valueOf(valuesList.get(i)).subtract(BigDecimal.valueOf(valuesList.get(i-1))).subtract(BigDecimal.valueOf(mean)).doubleValue();
						differences.add(difference);
						
					}
					

					LinkedHashMap<Integer, Integer> zeroOccurences = new LinkedHashMap<Integer, Integer>();
					int lastZero = 0;
					boolean lastValuePositive = false;
					if(differences.getFirst() > 0) lastValuePositive = true;
					for(int i = 0; i < differences.size(); i++) {		
						if(differences.get(i) < 0) {
							if(lastValuePositive) { 
								int newValue = 1;
								if(zeroOccurences.containsKey(lastZero))
									newValue = zeroOccurences.get(lastZero)+1;
								zeroOccurences.put(lastZero,newValue);
								lastValuePositive = false;								
								lastZero = 1;
								 
							}
							else {
								lastZero++;
							}
						}
						else if(differences.get(i) > 0) {
							if(lastValuePositive == false) {
								int newValue = 1;
								if(zeroOccurences.containsKey(lastZero))
									newValue = zeroOccurences.get(lastZero)+1;
								zeroOccurences.put(lastZero,newValue);
								lastValuePositive = true;
								lastZero = 1;
								
							}
							else {
								lastZero++;
							}
						}						
					}
					
					LinkedHashMap<Double, Integer> occurrences = new LinkedHashMap<Double, Integer>();
					for(double value:differences) {
						if(occurrences.containsKey(value))
							occurrences.put(value, occurrences.get(value)+1);
						else
							occurrences.put(value,1);
					}
					
	
					DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));			 
					 boolean firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" "+"Additional Increase or Decrease.txt");

					 for(double key: differences) {
						 if(firstWriteLine == false) {
							 rFWriter.write("\n");
						 }else
							 firstWriteLine = false;
						 rFWriter.write(decimalFormat.format(key));
					 }
					 rFWriter.close();
					 firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" "+"Additional Increase or Decrease Distribution.txt");
					 for(double key: occurrences.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(occurrences.get(key)+"\t"+decimalFormat.format(key));
					 }
					 rFWriter.close();	
					 firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" "+"Seconds Between Zeros Distribution.txt");
					 for(int key: zeroOccurences.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(zeroOccurences.get(key)+"\t"+decimalFormat.format(key));
					 }
					 rFWriter.close();	
				}
			}
			
			/**
			 * Analyzes the Distribution of Durations of Unchanged Values and Value Change Slopes Distribution of AIT20X.
			 * @throws IOException
			 */
		private static void partialAnalysisAIT20X(String fileName, String name,int csvColumnNumber) throws IOException{
			
			List<String> csvFiles = new ArrayList<String>();		
			csvFiles.add(fileName);
			
			LinkedHashMap<Integer, Integer> valueDurations = new LinkedHashMap<Integer,Integer>();
			LinkedHashMap<Double, Integer> valueChanges= new LinkedHashMap<Double,Integer>();
			
			//Process csv files
			String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
			for(String filename:csvFiles) {				
				//Open file
				File csvFile = new File(pathToCSVDirectory+"/"+filename);
				Scanner csvFileReader = new Scanner(csvFile);
				int lineCounter = 0;
				double lastAITValue = 0.0;
				int valueCounter = 0;
				while (csvFileReader.hasNextLine() == true) {
					lineCounter++;
					String csvLine = csvFileReader.nextLine();
					String[] entries = csvLine.split(",");
					if(lineCounter != 1) {
						double aitValue = Double.parseDouble(entries[csvColumnNumber]);
						if(lineCounter == 2) {
							lastAITValue = aitValue;
							valueCounter = 0;
						}
						else {
							if(aitValue != lastAITValue) {
								if(valueDurations.containsKey(valueCounter))
									valueDurations.put(valueCounter, valueDurations.get(valueCounter)+1);
								else
									valueDurations.put(valueCounter, 1);

								double valueChange = BigDecimal.valueOf(lastAITValue).subtract(BigDecimal.valueOf(aitValue)).doubleValue();
								if(valueChanges.containsKey(valueChange))
									valueChanges.put(valueChange, valueChanges.get(valueChange)+1);
								else
									valueChanges.put(valueChange, 1);	
								
								lastAITValue = aitValue;
								valueCounter = 0;
							}
						}
						valueCounter++;
						
					}
				}
				csvFileReader.close();
				fileNameStart +=filename;
				FileWriter rFWriter;
				
				DecimalFormat decimalFormat = new DecimalFormat("0.##########################", new DecimalFormatSymbols(Locale.ROOT));			 
				 boolean firstWriteLine = true;
				 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Distribution of Durations of Unchanged Values.txt");
				 for(int key: valueDurations.keySet()) {
					 if(firstWriteLine == false)
						 rFWriter.write("\n");
					 else
						 firstWriteLine = false;
					 rFWriter.write(valueDurations.get(key)+"\t"+key);
				 }
				  rFWriter.close();
				  firstWriteLine = true;
					 rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Value Change Slopes Distribution.txt");
					 for(double key: valueChanges.keySet()) {
						 if(firstWriteLine == false)
							 rFWriter.write("\n");
						 else
							 firstWriteLine = false;
						 rFWriter.write(valueChanges.get(key)+"\t"+decimalFormat.format(key));
					 }
					  rFWriter.close();
			}
		}
			
		
		/**
		 * Analyzes the intervals of Pumps.
		 * Results are useless.
		 * @throws IOException 
		 */
		private static void partialAnalysisP302(String fileName, String name, int csvColumnNumber, int startPosition, int stopPosition) throws IOException {
			List<String> csvFiles = new ArrayList<String>();		
			    	csvFiles.add(fileName);
				
				//Process csv files
				String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
				for(String filename:csvFiles) {
					LinkedHashMap<Integer, Integer> onIntervals = new LinkedHashMap<Integer, Integer>();
					LinkedHashMap<Integer, Integer> offIntervals = new LinkedHashMap<Integer, Integer>();
					
					//Open file
					File csvFile = new File(pathToCSVDirectory+"/"+filename);
					Scanner csvFileReader = new Scanner(csvFile);
					int lineCounter = 0;
					int onCounter = 0;
					int offCounter = 0;
					while (csvFileReader.hasNextLine() == true) {
						lineCounter++;
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");
				
						String pump = entries[csvColumnNumber].trim();

						if(lineCounter >= startPosition && lineCounter <= stopPosition) {
							int pumpNumber = Integer.parseInt(pump);
							if(pumpNumber == 1) { // Off
								offCounter++;
								if(onCounter > 0) {
									if(onIntervals.containsKey(onCounter))
										onIntervals.put(onCounter, onIntervals.get(onCounter)+1);
									else
										onIntervals.put(onCounter, 1);
									onCounter = 0;
								}
							}
							else { // On
								onCounter++;
								if(offCounter > 0) {
									if(offIntervals.containsKey(offCounter))
										offIntervals.put(offCounter, offIntervals.get(offCounter)+1);
									else
										offIntervals.put(offCounter, 1);
									offCounter = 0;
								}
							}
							
						}
					}			
					csvFileReader.close();
					fileNameStart +=filename;
					FileWriter rFWriter;

					HashMap<String, LinkedHashMap<Integer, Integer>> outputList2 = new HashMap<String, LinkedHashMap<Integer, Integer>>();
					outputList2.put(fileNameStart+"_"+name+" On Intervals.txt", onIntervals);
					outputList2.put(fileNameStart+"_"+name+" Off Intervals.txt", offIntervals);

					
					boolean firstWriteLine = true;
				    for(String fileNameComplete:outputList2.keySet()) {
				    	firstWriteLine = true;
					    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameComplete);
					    LinkedHashMap<Integer, Integer> temper = outputList2.get(fileNameComplete);
						 for(int key: temper.keySet()) {
							 if(firstWriteLine == false)
								 rFWriter.write("\n");
							 else
								 firstWriteLine = false;
							 rFWriter.write(temper.get(key)+"\t"+key);
						 }
					    rFWriter.close();	    	
				    } 	
				}
		}	
		
		/**
		 * Analyzes the transitions of MV-301..
		 * Results are useless.
		 * @throws IOException 
		 */
		private static void partialAnalysisMV301Transitions(String fileName) throws IOException {
			String name = "MV-301"; 
			int csvColumnNumber = 20;
			List<String> csvFiles = new ArrayList<String>();		
			    	csvFiles.add(fileName);
				
				//Process csv files
				String fileNameStart = "physicalDataAnalysis_"+System.currentTimeMillis()+"_";
				for(String filename:csvFiles) {
					LinkedList<Integer> mv301TransitionList = new LinkedList<Integer>();
					
					//Open file
					File csvFile = new File(pathToCSVDirectory+"/"+filename);
					Scanner csvFileReader = new Scanner(csvFile);
					boolean firstLine = true;
					int lineCounter = 0;
					int lastMV301Value = -1;
					while (csvFileReader.hasNextLine() == true) {
						lineCounter++;
						String csvLine = csvFileReader.nextLine();
						String[] entries = csvLine.split(",");
				
						if(firstLine == false) {
							int mv = Integer.parseInt(entries[csvColumnNumber].trim());
							if(lastMV301Value == 1 && mv == 0) {
								mv301TransitionList.add(lineCounter);
							}
							lastMV301Value = mv;
							
						}
						else {
							firstLine = false;
						}
							


					}			
					csvFileReader.close();
					fileNameStart +=filename;
					FileWriter rFWriter;
//					System.out.println(mv301TransitionList.size());
					boolean firstWriteLine = true;
				    	firstWriteLine = true;
					    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_"+name+" Transitions from 1 to 0.txt");
						 for(int key:mv301TransitionList) {
//							 System.out.println(key);
							 if(firstWriteLine == false)
								 rFWriter.write("\n");
							 else
								 firstWriteLine = false;
							 rFWriter.write(key+"");
						 }
					    rFWriter.close();	    	
				}
		}
		
		/**
		 * LIT analysis.
		 * @throws IOException
		 */
		@SuppressWarnings("unused")
		private static void analyzeLITs() throws IOException {
			partialAnalysisLITExtrema("SWaT_Dataset_Normal_v1.csv","LIT-101",2,19545,21543);
			partialAnalysisLITFlanks("SWaT_Dataset_Normal_v1.csv","LIT-101",2,19545,21543);
			partialAnalysisLITExtrema("SWaT_Dataset_Normal_v1.csv","LIT-101",2,900,10740);
			partialAnalysisLITFlanks("SWaT_Dataset_Normal_v1.csv","LIT-101",2,900,10740);
			partialAnalysisLITZeros("SWaT_Dataset_Normal_v1.csv","LIT-101",2,900,10740);
		}
		
		/**
		 * FIT analysis.
		 * @throws IOException
		 */
		private static void analyzeFITs() throws IOException {
			partialAnalysisFITExtrema("SWaT_Dataset_Normal_v1.csv", "FIT-101", 1, 620, 13776, 2.42, 2.67);
			partialAnalysisFITSharpFlanksDistribution("SWaT_Dataset_Normal_v1.csv", "FIT-101", 1);
			partialAnalysisFITFlankDistributions("SWaT_Dataset_Normal_v1.csv", "FIT-101", 1, 2.43, 2.66, 0.0, 9.9);

			partialAnalysisFITExtrema("SWaT_Dataset_Normal_v1.csv", "FIT-201", 9, 800, 491473, 2.4163, 2.488);
			partialAnalysisFITSharpFlanksDistribution("SWaT_Dataset_Normal_v1.csv", "FIT-201", 9);
			partialAnalysisFITFlankDistributions("SWaT_Dataset_Normal_v1.csv", "FIT-201", 9, 2.43, 2.66, 0.0, 0.005);
			
			partialAnalysisFITSharpFlanksDistribution("SWaT_Dataset_Normal_v1.csv", "FIT-301", 18);
			
		}
		
		/**
		 * AIT analysis
		 * @throws FileNotFoundException 
		 */
		@SuppressWarnings("unused")
		private static void analyzeAITs() throws IOException {
			partialAnalysisAIT20X("SWaT_Dataset_Normal_v1.csv", "AIT-201", 6);
			partialAnalysisAIT20X("SWaT_Dataset_Normal_v1.csv", "AIT-202", 7);
			partialAnalysisAIT20X("SWaT_Dataset_Normal_v1.csv", "AIT-203", 8);
		}
}
