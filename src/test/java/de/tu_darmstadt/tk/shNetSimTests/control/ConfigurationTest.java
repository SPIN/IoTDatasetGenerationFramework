package de.tu_darmstadt.tk.shNetSimTests.control;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.SettingsController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model;

/**
 * Tests Configuration Controller
 * 
 *
 * @author Andreas T. Meyer-Berg
 */
public class ConfigurationTest {
	private Controller control;
	private Model model;
	private SettingsController config;

	@Before
	public void initialize() {
		model = new Model();
		control = new Controller(model);
		config = control.getSettingsController();
	}

	@Test
	public void testConfigurationManager() {
		Assert.assertEquals(model.getConfigurator(), control
				.getSettingsController().getConfigurationManager());
		Assert.assertNotNull(config);
	}

	@Test
	public void testShowConnectionsDefault() {
		Assert.assertTrue(config.isShowConnections());
	}

	@Test
	public void testShowConnections() {
		config.setShowConnections(false);
		Assert.assertFalse(config.isShowConnections());
		config.setShowConnections(true);
		Assert.assertTrue(config.isShowConnections());
	}

	@Test
	public void testShowLinksDefault() {
		Assert.assertTrue(config.isShowLinks());
	}

	@Test
	public void testShowLinks() {
		config.setShowLinks(false);
		Assert.assertFalse(config.isShowLinks());
		config.setShowLinks(true);
		Assert.assertTrue(config.isShowLinks());
	}

	@Test
	public void testShowSmartDevicesDefault() {
		Assert.assertTrue(config.isShowSmartDevices());
	}

	@Test
	public void testShowSmartDevices() {
		config.setShowSmartDevices(false);
		Assert.assertFalse(config.isShowSmartDevices());
		config.setShowSmartDevices(true);
		Assert.assertTrue(config.isShowSmartDevices());
	}

	@Test
	public void testShowSmartDeviceNamesDefault() {
		Assert.assertTrue(config.isShowSmartDeviceNames());
	}

	@Test
	public void testShowSmartDeviceNames() {
		config.setShowSmartDeviceNames(false);
		Assert.assertFalse(config.isShowSmartDeviceNames());
		config.setShowSmartDeviceNames(true);
		Assert.assertTrue(config.isShowSmartDeviceNames());
	}

	@Test
	public void testShowTerminatedConnectionsDefault() {
		Assert.assertFalse(config.isShowTerminatedConnections());
	}

	@Test
	public void testShowTerminatedConnections() {
		config.setShowTerminatedConnections(true);
		Assert.assertTrue(config.isShowTerminatedConnections());
		config.setShowTerminatedConnections(false);
		Assert.assertFalse(config.isShowTerminatedConnections());
	}

	@Test
	public void testGetDeviceVisualizationRadiusDefault() {
		Assert.assertEquals(25, config.getDeviceVisualizationRadius());
	}

	@Test
	public void testGetDeviceVisualizationRadius() {
		config.setDeviceVisualizationRadius(12);
		Assert.assertEquals(12, config.getDeviceVisualizationRadius());
		config.setDeviceVisualizationRadius(56);
		Assert.assertEquals(56, config.getDeviceVisualizationRadius());
	}
}
