package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Interface for processing of captured packets. If registered in the program,
 * the {@link #processPackets(HashMap)} method will be called after each
 * simulation step, and the captured packets during the last step, will be the argument.
 * 
 * @author Andreas T. Meyer-Berg
 */
public interface PacketSniffer {

	/**
	 * Process the packages. This method will be called after every simulation
	 * step, if and object implementing this Interface, is registered in the
	 * framework.
	 * 
	 * @param packets
	 *            the packets captured during the last simulation step. They are
	 *            grouped by each link.
	 */
	public void processPackets(HashMap<Link, LinkedList<Packet>> packets);
	
	/**
	 * Set the mode of the algorithm, whether it should be training or testing 
	 * with the given packets
	 * @param testing <code>false</code> if it should be training, <code>true</code> if
	 * it should be testing
	 */
	public void setMode(boolean testing);
	
	/**
	 * Returns the mode of the algorithm, whether it should be training or testing 
	 * with the given packets.<br>
	 * <code>false</code>: if it should be training<br> 
	 * <code>true</code>:  if it should be testing
	 */
	public boolean getMode();

}
