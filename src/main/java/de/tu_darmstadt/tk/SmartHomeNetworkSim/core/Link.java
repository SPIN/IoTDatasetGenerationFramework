package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import java.util.Collection;

/**
 * Physical Link medium for SmartDevices, which connects two or more devices and
 * allows communication between them
 *
 * @author Andreas T. Meyer-Berg
 */
public interface Link {
	/**
	 * @return the name
	 */
	public String getName();

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name);

	/**
	 * @return the devices
	 */
	public Collection<SmartDevice> getDevices();

	/**
	 * @param device
	 *            the devices to add
	 */
	public void addDevice(SmartDevice device);

	/**
	 * @param device
	 *            the devices to remove
	 */
	public void removeDevice(SmartDevice device);
	
	/**
	 * @return the connections
	 */
	public Collection<Connection> getConnections();

	/**
	 * @param connection
	 *            the connection to add
	 */
	public void addConnection(Connection connection);
	
	/**
	 * @param connection
	 *            the connection to remove
	 */
	public void removeConnection(Connection connection);

	/**
	 * Simulates an interval starting at startTime for a given duration
	 * 
	 * @param startTime
	 *            Time the simulation interval starts in
	 *            System.currentTimeMillis() time
	 * @param duration
	 *            Duration of the simulation interval in milliseconds
	 */
	@Deprecated
	public void simulateTimeInterval(long startTime, long duration);
	
	/**
	 * Initializes the simulation interval, by clearing the previously generated 
	 * packets and adds Packets of previous iterations, which are are part of this interval.
	 * @param startTime startTime of the simulation interval
	 * @param duration duration of the simulation interval
	 */
	public void initSimulationInterval(long startTime, long duration);
	
	/**
	 * Encapsulates the given Packets
	 * @param packets Packets which should be encapsulated
	 * @return Encapsulated Packets
	 */
	public Collection<Packet> encapsulatePackages(Collection<Packet> packets);

	/**
	 * Time at the end of an simulation interval, to remove Packets, which are not in bound, sort the array
	 * or manipulate the packets slightly.
	 * 
	 * @param startTime startTime of the simulation interval
	 * @param duration duration of the interval
	 */
	public void finalizeSimulationInterval(long startTime, long duration);
	
	/**
	 * Returns all packets which where sent during the last Simulation time step
	 * 
	 * @return packets, which were sent during the last time step
	 */
	public Collection<Packet> getPackets();
	
	/**
	 * Returns true if one or more connections have changed, and the Panel might have to be repainted
	 * @return true if changed
	 */
	public boolean getStatusChanged();
	
	/**
	 * Returns the Transmission delay between the two given devices
	 * @param from SmartDevice which starts transmission
	 * @param to SmartDevices which receives transmission
	 * @return delay of the transmission, infinite if to is unreachable or the Packets is lost.
	 */
	public long getTransmissionDelayFrom(SmartDevice from, SmartDevice to);

	/**
	 * Adds Packets to the internal data structure
	 * @param packets Packets, which should be added
	 */
	public void addPackets(Collection<Packet> packets);
}
