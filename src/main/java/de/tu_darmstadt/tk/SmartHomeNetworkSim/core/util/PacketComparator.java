package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.util.Comparator;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;

/**
 * Comparator of Packet Timestamps for sorting packages
 *
 * @author Andreas T. Meyer-Berg
 */
public class PacketComparator implements Comparator<Packet> {

	@Override
	public int compare(Packet o1, Packet o2) {
		return Long.compare(o1.getTimestamp(),o2.getTimestamp());
	}
}
