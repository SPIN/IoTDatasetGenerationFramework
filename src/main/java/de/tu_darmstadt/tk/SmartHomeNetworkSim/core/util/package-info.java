/**
 * Package for useful classes, which don't fit into other packages.
 * 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;