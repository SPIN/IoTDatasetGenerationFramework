package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * File Filter to filter Java Source Files. (e.g. test.java)
 *
 * @author Andreas T. Meyer-Berg
 */
public class JavaFileFilter extends FileFilter {
	
	@Override
	public String getDescription() {
		return "Java Source Files(.java)";
	}

	@Override
	public boolean accept(File f) {
		if (f == null)
			return false;
		else if (f.isDirectory())
			return true;
		String[] parts = f.getName().split("[.]");
		if (parts.length <= 1)
			return false;
		String ending = parts[parts.length - 1].toLowerCase();
		if (ending.equals("java"))
			return true;
		else
			return false;
	}
}