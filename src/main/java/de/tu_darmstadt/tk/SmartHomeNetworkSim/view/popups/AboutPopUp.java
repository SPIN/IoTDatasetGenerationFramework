package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.UIManager;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * PopUp which shows a short description of the program and developers for the SmartHome scenario.
 *
 * @author Andreas T. Meyer-Berg
 */
public class AboutPopUp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7426102751026092131L;

	/**
	 * Create PopUp
	 */
	public AboutPopUp() {
		//Created using Eclipse Window Builder
		this.setMinimumSize(new Dimension(500, 400));
		this.setPreferredSize(new Dimension(500, 400));
		setIconImage(Utility.loadFile("/images/smartHome_icon.png"));
		setTitle("SmartHomeNetworkSim: About");
		setFont(new Font("Roboto", Font.PLAIN, 42));
		getContentPane().setLayout(null);

		JLabel lblSmartHomeNetwork = new JLabel("Smart Home Network Sim");
		lblSmartHomeNetwork.setFont(new Font("Roboto", Font.PLAIN, 26));
		lblSmartHomeNetwork.setBounds(12, 13, 458, 51);
		getContentPane().add(lblSmartHomeNetwork);

		JTextArea txtrSmarthomenetworksim = new JTextArea();
		txtrSmarthomenetworksim.setWrapStyleWord(true);
		txtrSmarthomenetworksim.setBackground(UIManager
				.getColor("Button.background"));
		txtrSmarthomenetworksim.setEditable(false);
		txtrSmarthomenetworksim.setLineWrap(true);
		txtrSmarthomenetworksim.setFont(new Font("Roboto", Font.PLAIN, 16));
		txtrSmarthomenetworksim
				.setText("The SmartHomeNetworkSim is a framework for visualization and generation of IoT/SmartHome "
						+ "network traffic. It allows the user to configure an IoT/SmartHome network and simulate it.");
		txtrSmarthomenetworksim.setBounds(12, 81, 458, 103);
		getContentPane().add(txtrSmarthomenetworksim);

		JLabel lblDevelopers = new JLabel(
				"Developer: Andreas T. Meyer-Berg (Bachelor Thesis)");
		lblDevelopers.setFont(new Font("Roboto", Font.PLAIN, 16));
		lblDevelopers.setBounds(12, 260, 458, 51);
		getContentPane().add(lblDevelopers);
		// TODO Auto-generated constructor stub
	}
}
