package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration;

import java.util.HashMap;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;

/**
 * Settings of the Network Tree and contains the settings for each node
 *
 * @author Andreas T. Meyer-Berg
 */
public class NetworkTreeSettings {
	
	/**
	 * HashMap to access nodes
	 */
	private HashMap<Object, NetworkTreeNodeStatus> map; 
	
	/**
	 * Initialize the NetworkTreeSettings
	 */
	public NetworkTreeSettings() {
		map = new HashMap<Object, NetworkTreeNodeStatus>();
	}
	
	/**
	 * Returns the Status for the given Object
	 * @param o Object, whose status should be returned
	 * @return Status of the object
	 */
	public NetworkTreeNodeStatus getStatusOfObject(Object o){
		NetworkTreeNodeStatus ret = map.get(o);
		if(o instanceof Link || o instanceof Connection || o instanceof SmartDevice || o instanceof String){
			if(ret == null){
				/**
				 * if no status stored -> create and return new one
				 */
				ret = new NetworkTreeNodeStatus(o);
				map.put(o, ret);
			}
			return ret;
		}
		/**
		 * Error for simple debug - should be
		 */
		throw new Error("Invalid Object in Tree: "+o);
	}
	
	/**
	 * Adds an status for the given Object
	 * @param o Object to be added
	 * @param status Status of the object
	 */
	public void addStatusOfObject(Object o, NetworkTreeNodeStatus status){
		map.put(o, status);
	}
	
	/**
	 * Removes the status of the object o
	 * @param o Object, which status should be removed
	 */
	public void removeStatusOfObject(Object o){
		map.remove(o);
	}
}
