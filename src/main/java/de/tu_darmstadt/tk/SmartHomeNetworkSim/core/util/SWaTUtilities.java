package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.swing.table.DefaultTableModel;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTActuator;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTSensor;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler.SWaTDatasetDistributionHandler;

/**
 * Utility functions for the SWaT scenario.
 * @author Fabian Kaiser
 */
public class SWaTUtilities {

	/**
	 * Creates and returns a list of the names of the currently displayed devices alphabetically sorted.
	 * @param networkController A NetworkController.
	 * @return A list of the names of the currently displayed devices alphabetically sorted.
	 */
	public static List<String> getSortedDeviceNames(NetworkController networkController){
		List<SmartDevice> deviceList = SWaTUtilities.getSortedDeviceList(networkController);
		List<String> deviceNames = new LinkedList<String>();
		
		Iterator<SmartDevice> deviceListIterator = deviceList.iterator();
		SmartDevice device;
		while (deviceListIterator.hasNext()) {
			device = deviceListIterator.next();
			deviceNames.add(device.getName());
		}
		
		return deviceNames;
	}
	
	/**
	 * Returns the smart device with the given name.
	 * @param deviceName The given name.
	 * @param networkController A NetworkController.
	 * @return The smart device with the given name or null if not existent.
	 */
	public static SmartDevice getDeviceByName(String deviceName, NetworkController networkController) {
		List<SmartDevice> deviceList = SWaTUtilities.getSortedDeviceList(networkController);		
		Iterator<SmartDevice> deviceListIterator = deviceList.iterator();
		SmartDevice device;
		while (deviceListIterator.hasNext()) {
			device = deviceListIterator.next();
			if(device.getName().equals(deviceName)) return device;
		}		
		return null;
	}
	
	/**
	 * Creates and returns a list of the currently displayed devices alphabetically sorted according to their names.
	 * @param networkController A NetworkController.
	 * @return A list of the currently displayed devices alphabetically sorted according to their names.
	 */
	public static List<SmartDevice> getSortedDeviceList(NetworkController networkController) {
		List<SmartDevice> deviceList = (List<SmartDevice>) networkController.getSmartDevices();
		deviceList.sort(new Comparator<SmartDevice>(){ // Sort device list
                    public int compare(SmartDevice dev1, SmartDevice dev2){
                        return dev1.getName().compareTo(dev2.getName());
                    }        
        });
		return deviceList;
	}
	
	/**
	 * Returns an overview of the elements the device is connected to.
	 * @param dev A SmartDevice.
	 * @param networkController A NetworkController.
	 * @return An overview of the elements the device is connected to.
	 */
	public static List<String> getElementConnections(SmartDevice dev, NetworkController networkController) {
		List<String> elementConnections = new LinkedList<String>();
		String currentDeviceName = dev.getName();
		
		List<String> allElementsConnections = SWaTUtilities.createAllPossibleElementConnections(networkController);
		for(String elementConnection:allElementsConnections) {
			if(elementConnection.startsWith(currentDeviceName)) elementConnections.add(elementConnection);
		}
		
		return elementConnections;
	}
	
	/**
	 * Creates a list of all possible connections in the current model.
	 * @param networkController A NetworkController.
	 * @return A list of all possible connections in the current model.
	 */
	public static List<String> createAllPossibleElementConnections(NetworkController networkController) {	
		List<String> elementConnections = new LinkedList<String>();
		
		List<String> devicesConnectedToSwitch1 = new LinkedList<String>();
		List<String> devicesConnectedToSwitch2 = new LinkedList<String>();
		boolean switch1Exists = false;
		boolean switch2Exists = false;
		
		Connection connection = ((List<Connection>) networkController.getConnections()).get(0);
		Collection<Pair<Port,Port>> protocolTopology = connection.getProtocol().getTopology();
		
		Iterator<Pair<Port,Port>> topologyIterator = protocolTopology.iterator();
		Pair<Port,Port> connectedDevices; // Pairs of connected devices
		while (topologyIterator.hasNext()) {
			connectedDevices = topologyIterator.next();
			String leftDeviceName = connectedDevices.getLeft().getOwner().getName();
			String rightDeviceName = connectedDevices.getRight().getOwner().getName();
			if(rightDeviceName.equals("Switch 1") && leftDeviceName.equals("Switch 2") == false) {
				switch1Exists = true;
				devicesConnectedToSwitch1.add(leftDeviceName); // Devices connected to Switch 1
			}				
			else if(rightDeviceName.equals("Switch 2") && leftDeviceName.equals("Switch 1") == false) {
				switch2Exists = true;
				devicesConnectedToSwitch2.add(leftDeviceName); // Devices connected to Switch 2		
			}
		}		
		
		// Combine all elements connected to Switch 1
		for(String device1:devicesConnectedToSwitch1) {			
			for(String device2:devicesConnectedToSwitch1) {
				if(device1.equals(device2) == false)
					elementConnections.add(device1+" via Switch 1 to "+device2);
			}
			elementConnections.add("Switch 1 forwards packets from Switch 2 to "+device1);
			elementConnections.add("Switch 1 forwards packets from "+device1+" to Switch 2");
		}		
		// Combine all elements connected to Switch 2
		for(String device1:devicesConnectedToSwitch2) {			
			for(String device2:devicesConnectedToSwitch2) {
				if(device1.equals(device2) == false)
					elementConnections.add(device1+" via Switch 2 to "+device2);
			}
			elementConnections.add("Switch 2 forwards packets from Switch 1 to "+device1);
			elementConnections.add("Switch 2 forwards packets from "+device1+" to Switch 1");			
		}		
		// Combine all devices over both switches
		if(switch1Exists && switch2Exists) {
			// device1 -> Switch 1 -> Switch 2 -> device 2
			for(String device1:devicesConnectedToSwitch1) {			
				for(String device2:devicesConnectedToSwitch2) {
					elementConnections.add(device1+" via Switch 1 and Switch 2 to "+device2);
				}
			}	
			// device1 -> Switch 2 -> Switch 1 -> device 2
			for(String device1:devicesConnectedToSwitch2) {			
				for(String device2:devicesConnectedToSwitch1) {
					elementConnections.add(device1+" via Switch 2 and Switch 1 to "+device2);
				}
			}	
		}
		
		return elementConnections;
	}
	
	/**
	 * Finds the name of the destination device from a connection string.
	 * </br>
	 * Not for actuators and sensors.
	 * @param connection A connection string.
	 * @return The name of the destination device from a connection string.
	 */
	public static String getDestinationDeviceName(String connection) {
		String destinationDeviceName = "";
		if(connection.endsWith("PLC 1")) destinationDeviceName = "PLC 1";
		else if(connection.endsWith("PLC 2")) destinationDeviceName = "PLC 2";
		else if(connection.endsWith("PLC 3")) destinationDeviceName = "PLC 3";
		else if(connection.endsWith("PLC 4")) destinationDeviceName = "PLC 4";
		else if(connection.endsWith("PLC 5")) destinationDeviceName = "PLC 5";
		else if(connection.endsWith("PLC 6")) destinationDeviceName = "PLC 6";
		else if(connection.endsWith("SCADA")) destinationDeviceName = "SCADA";
		else if(connection.endsWith("Historian")) destinationDeviceName = "Historian";
		else if(connection.endsWith("Engineering Workstation")) destinationDeviceName = "Engineering Workstation";
		return destinationDeviceName;
	}
	
	/**
	 * Finds the name of the source device from a connection string.
	 * </br>
	 * Not for actuators and sensors.
	 * @param connection A connection string.
	 * @return The name of the source device from a connection string.
	 */
	public static String getSourceDeviceName(String connection) {
		String destinationDeviceName = "";
		if(connection.startsWith("PLC 1")) destinationDeviceName = "PLC 1";
		else if(connection.startsWith("PLC 2")) destinationDeviceName = "PLC 2";
		else if(connection.startsWith("PLC 3")) destinationDeviceName = "PLC 3";
		else if(connection.startsWith("PLC 4")) destinationDeviceName = "PLC 4";
		else if(connection.startsWith("PLC 5")) destinationDeviceName = "PLC 5";
		else if(connection.startsWith("PLC 6")) destinationDeviceName = "PLC 6";
		else if(connection.startsWith("SCADA")) destinationDeviceName = "SCADA";
		else if(connection.startsWith("Historian")) destinationDeviceName = "Historian";
		else if(connection.startsWith("Engineering Workstation")) destinationDeviceName = "Engineering Workstation";
		return destinationDeviceName;
	}	
	
	/**
	 * Finds information of the connection between the two devices
	 * @param sourceDeviceName Device 1.
	 * @param destinationDeviceName Device 2.
	 * @param networkController A NetworkController.
	 * @return A list with sourceIPAddress and inboundPort of Device 1. 
	 */
	public static List<String> getConnectionInformation(String sourceDeviceName, String destinationDeviceName, NetworkController networkController) {
		LinkedList<String> information = new LinkedList<String>();
		SWaTDevice sourceDevice = (SWaTDevice) SWaTUtilities.getDeviceByName(sourceDeviceName, networkController);
		List<String> elementConnections = SWaTUtilities.getElementConnections(sourceDevice, networkController);
		for(String connection:elementConnections) {
			if(connection.endsWith(destinationDeviceName)) {
				information.add(sourceDevice.getDeviceData().get(connection+"_sourceIPAddress"));
				information.add(sourceDevice.getDeviceData().get(connection+"_inboundPort"));
				information.add(sourceDevice.getDeviceData().get(connection+"_responseCurrent"));
			}
		}
		return information;
	}
	
	/**
	 * Deletes the content of the user data file;
	 */
	public static void deleteUserData() {
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		if(userDataFile.isFile())
			try {
				FileWriter userDataWriter = new FileWriter(userDataFile);
				userDataWriter.write("");	    	
			    userDataWriter.close();
			} catch (IOException e) {
				System.out.println("Cannot write to user data file.");
			}
	}
	
	/**
	 * Loads user data into the user data file;
	 * @param fileToLoadPath The path of the file to load.
	 */
	public static void loadUserData(String fileToLoadPath) {
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		File loadFile = new File(fileToLoadPath);
		try {
			Files.copy(loadFile.toPath(), userDataFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("Cannot import file.");
		}
	}
	
	/**
	 * Saves user data into a file;
	 * @param savePath The path of the file to save to.
	 */
	public static void saveUserData(String savePath) {
		if(savePath.endsWith(".SWaT") == false)
			savePath += ".SWaT";
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		File saveFile = new File(savePath);
		try {
			Files.copy(userDataFile.toPath(), saveFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("Cannot export file.");
		}
	}
	
	/**
	 * Saves the visualization information to the user data file.
	 * @param customization Array with the selected stages. [SCADA, EngineeringWorkstation, Historian, PLC 1, PLC 2, PLC 3, PLC 4, PLC 5, PLC 6]
	 */
	public static void saveVisualizationToUserDataFile(boolean customization[]){	
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		if(customization.length != 9) return;
		if(userDataFile.isFile()) {
			try {				
				String customizationEncoding = "";
				boolean firstLine = true;
				for(int i = 0; i <= 8; i++) {
					if(firstLine == false)
						customizationEncoding += ";";
					firstLine = false;
					customizationEncoding += customization[i];
				}
				
				LinkedHashMap<String, String> userData = new LinkedHashMap<String, String>();
								
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] entries = csvLine.split(",");
					userData.put(entries[0], entries[1]);						
				}
				userDataFileReader.close();	
				userData.put("visualizationInformation",customizationEncoding);
				
			    FileWriter userDataWriter;
				try {
					userDataWriter = new FileWriter(userDataFile);
					firstLine = true;
				    for(String key:userData.keySet()) {
						if(firstLine == false)
							userDataWriter.write("\n");
						firstLine = false;
						userDataWriter.write(key+","+userData.get(key));	    	
				    } 
				    userDataWriter.close();
				} catch (IOException e) {
					System.out.println("Cannot write to user data file.");
				}
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
	}
	
	/**
	 * Loads the visualization information from the file.
	 * @return Array with the selected stages. [SCADA, EngineeringWorkstation, Historian, PLC 1, PLC 2, PLC 3, PLC 4, PLC 5, PLC 6]
	 */
	public static boolean[] loadVisualizationFromUserDataFile(){	
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		boolean[] customization = new boolean[9];
		if(userDataFile.isFile()) {
			try {			
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] deviceEntries = csvLine.split(",");
					if(deviceEntries[0].equals("visualizationInformation")){							
						String[] userDataListEntries = deviceEntries[1].split(";");
						for(int i = 0; i <= 8; i++) {
							customization[i] = Boolean.parseBoolean(userDataListEntries[i]);
						}
					}						
				}
				userDataFileReader.close();				
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
		return customization;
	}
	
	/**
	 * Saves the attack configuration to the user data file.
	 * @param dataVector The  TableModel in which the attacks are stored.
	 */
	public static void saveAttacksToUserDataFile(DefaultTableModel tableModel){	
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");
		if(userDataFile.isFile()) {
			try {				
				String attacksEncoding = "";
				boolean firstLine = true;
				for(int i = 0; i < tableModel.getRowCount(); i++) {
					if(firstLine == false)
						attacksEncoding += "#";					
					firstLine = false;
					attacksEncoding += tableModel.getValueAt(i, 0) + ";";
					attacksEncoding += tableModel.getValueAt(i, 1) + ";";
					attacksEncoding += tableModel.getValueAt(i, 2) + ";";
					attacksEncoding += tableModel.getValueAt(i, 3) + ";";
					attacksEncoding += tableModel.getValueAt(i, 4);
				}
				
				LinkedHashMap<String, String> userData = new LinkedHashMap<String, String>();
								
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] entries = csvLine.split(",");
					userData.put(entries[0], entries[1]);						
				}
				userDataFileReader.close();	
				userData.put("attacks",attacksEncoding);
				
			    FileWriter userDataWriter;
				try {
					userDataWriter = new FileWriter(userDataFile);
					firstLine = true;
				    for(String key:userData.keySet()) {
						if(firstLine == false)
							userDataWriter.write("\n");
						firstLine = false;
						userDataWriter.write(key+","+userData.get(key));	    	
				    } 
				    userDataWriter.close();
				} catch (IOException e) {
					System.out.println("Cannot write to user data file.");
				}
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
	}
	
	/**
	 * Loads the attacks on the currently displayed devices from the file.
	 * @param networkController A NetworkController.
	 * @return A data vector for the the TableModel in which the attacks are stored.
	 */
	public static Object[][] loadAttacksFromUserDataFile(NetworkController networkController){	
		List<String> deviceList = getSortedDeviceNames(networkController);
		
		File userDataFile = new File("src/main/resources/SWaT/userData.SWaT");		
		LinkedList<Object[]> attackTableDataList = new LinkedList<Object[]>();
		if(userDataFile.isFile()) {
			try {			
				Scanner userDataFileReader = new Scanner(userDataFile);
				while (userDataFileReader.hasNextLine() == true) {
					String csvLine = userDataFileReader.nextLine();
					String[] deviceEntries = csvLine.split(",");
					if(deviceEntries[0].equals("attacks")){							
						String[] userDataListEntries = deviceEntries[1].split("#");
						for(int i = 0; i < userDataListEntries.length; i++) {
							String[] attackCells = userDataListEntries[i].split(";");
							Object[] currentAttack = new Object[5];
							currentAttack[0] = attackCells[0];
							currentAttack[1] = attackCells[1];
							currentAttack[2] = attackCells[2];
							currentAttack[3] = attackCells[3];
							currentAttack[4] = attackCells[4];
							if(deviceList.contains((String) currentAttack[2]))
								attackTableDataList.add(currentAttack);
						}
					}						
				}
				userDataFileReader.close();				
			} catch (FileNotFoundException e) {
				System.out.println("User data file not found.");
			}
		}
		int attackRows = attackTableDataList.size();
		Object[][] attackTableData = new Object[attackRows][5];
		for(int i = 0; i < attackRows; i++) {
			attackTableData[i] = attackTableDataList.get(i);
		}
		return attackTableData;
	}
	
	/**
	 * Reads a dataset distribution file and creates the corresponding distribution handler.
	 * @param fileName Name of the dataset distribution file.
	 * @return The corresponding distribution handler.
	 */
	public static SWaTDatasetDistributionHandler readDatasetDistributionData(String fileName){
		File distributionFile = new File("src/main/resources/SWaT/"+fileName);
		SWaTDatasetDistributionHandler distributionHandler = null;

		if(distributionFile.isFile()) {
			try {
				LinkedList<Double> dataPoints = new LinkedList<Double>();
				LinkedList<Double> CUMSUMweights = new LinkedList<Double>();
				double currentCUMSUM = 0.0;
			
				Scanner distributionFileReader = new Scanner(distributionFile);
				while (distributionFileReader.hasNextLine() == true) {
					String[] entries = distributionFileReader.nextLine().split("\t");
					double value = Double.parseDouble(entries[1]);
					double occurenceNumber = Double.parseDouble(entries[0]);
					
					dataPoints.add(value);
					currentCUMSUM += occurenceNumber;
					CUMSUMweights.add(currentCUMSUM);
				}
				distributionFileReader.close();	

				distributionHandler = new SWaTDatasetDistributionHandler(dataPoints, CUMSUMweights, currentCUMSUM);
			} catch (FileNotFoundException e) {
				System.out.println("Distribution file not found.");
			}
		}
		return distributionHandler;
	}
	
	/**
	 * Converts a string representing a double into an IEEE-754 little endian hex string.</br>
	 * For instance: "1035.48779297" becomes "0x9c 0x6f 0x81 0x44".
	 * @param doubleString A string representing a double.
	 * @return The conversion result.
	 */
	public static String convertDoubleStringToSWaTNetworkHexString(String doubleString) {
		float valueFloat = Float.parseFloat(doubleString);
		int bitsBigEndian = Float.floatToIntBits(valueFloat);
		int bitsLittleEndian = Integer.reverseBytes(bitsBigEndian);
		String hex = Integer.toHexString(bitsLittleEndian);
		// Add leading zeros if necessary
			int hexLength = 8 - hex.length();
			if(hexLength != 0) { 
				StringBuilder hexSB = new StringBuilder();
				for(int i = 0; i < hexLength; i++) {
					hexSB.append("0");
				}
				hexSB.append(hex);
				hex = hexSB.toString();
			}
		String hexFormatted = "0x"+hex.substring(0, 2)+" 0x"+hex.substring(2, 4)+" 0x"+hex.substring(4, 6)+" 0x"+hex.substring(6, 8);
		return hexFormatted;
	}
	
	/**
	 * Corrects the exponential number format that is sometimes used in the SWaT XLSX files.</br>
	 * Examples:</br>
	 * 6.41E-05 becomes 0.0000641</br>
	 * 635423534.41E-05 becomes 6354.2353441</br>
	 * 0E-00 becomes 0.0
	 * @param value The number to correct the exponential format of.
	 * @return The correct number String or the input if it did not contain an exponent.
	 */
	public static String correctExponentialFormat(String value) {
		int exponentialPosition = value.indexOf("E-");
		if(exponentialPosition != -1) {
			String resolvedExponentString = "";
			String exponentString = value.substring(exponentialPosition+2);
			int exponent = Integer.parseInt(exponentString);
			int pointPosition = value.indexOf(".");
			value = value.substring(0, exponentialPosition);
			value = value.replace(".", "");
			
			int newPointPosition = pointPosition-exponent;
			if(newPointPosition < 0) {
				StringBuilder resolvedExponentValue = new StringBuilder();
				resolvedExponentValue.append("0.");
				for(int i = 0; i < newPointPosition*-1; i++)
					resolvedExponentValue.append("0");
				resolvedExponentValue.append(value);
				resolvedExponentString = resolvedExponentValue.toString();				
			}
			else {
				resolvedExponentString = value.substring(0, newPointPosition);
				resolvedExponentString += ".";
				resolvedExponentString += value.substring(newPointPosition);
			}
			return resolvedExponentString;
		}		
		else
			return value;
	}
	
	/**
	 * Creates a string like the one if a whole row of the XLSX dataset files is copied.
	 * @param networkController networkController A NetworkController.
	 * @param startTime The time that is to be used as timestamp in the row
	 * @param attackLabel "Attack" or "Normal".
	 * @return A string like the one if a whole row of the XLSX dataset files is copied.
	 */
	public static String createXLSXRowString(NetworkController networkController, Calendar startTime, String attackLabel) {
		LinkedHashMap<Integer, String> deviceNamePositionMap = new  LinkedHashMap<Integer, String>();
		deviceNamePositionMap.put(1,"FIT-101");
		deviceNamePositionMap.put(2,"LIT-101");
		deviceNamePositionMap.put(3,"MV-101");
		deviceNamePositionMap.put(4,"P-101");
		deviceNamePositionMap.put(5,"P-102");
		deviceNamePositionMap.put(6,"AIT-201");
		deviceNamePositionMap.put(7,"AIT-202");
		deviceNamePositionMap.put(8,"AIT-203");
		deviceNamePositionMap.put(9,"FIT-201");
		deviceNamePositionMap.put(10,"MV-201");
		deviceNamePositionMap.put(11,"P-201");
		deviceNamePositionMap.put(12,"P-202");
		deviceNamePositionMap.put(13,"P-203");
		deviceNamePositionMap.put(14,"P-204");
		deviceNamePositionMap.put(15,"P-205");
		deviceNamePositionMap.put(16,"P-206");
		deviceNamePositionMap.put(17,"DPIT-301");
		deviceNamePositionMap.put(18,"FIT-301");
		deviceNamePositionMap.put(19,"LIT-301");
		deviceNamePositionMap.put(20,"MV-301");
		deviceNamePositionMap.put(21,"MV-302");
		deviceNamePositionMap.put(22,"MV-303");
		deviceNamePositionMap.put(23,"MV-304");
		deviceNamePositionMap.put(24,"P-301");
		deviceNamePositionMap.put(25,"P-302");
		deviceNamePositionMap.put(26,"AIT-401");
		deviceNamePositionMap.put(27,"AIT-402");
		deviceNamePositionMap.put(28,"FIT-401");
		deviceNamePositionMap.put(29,"LIT-401");
		deviceNamePositionMap.put(30,"P-401");
		deviceNamePositionMap.put(31,"P-402");
		deviceNamePositionMap.put(32,"P-403");
		deviceNamePositionMap.put(33,"P-404");
		deviceNamePositionMap.put(34,"UV-401");
		deviceNamePositionMap.put(35,"AIT-501");
		deviceNamePositionMap.put(36,"AIT-502");
		deviceNamePositionMap.put(37,"AIT-503");
		deviceNamePositionMap.put(38,"AIT-504");
		deviceNamePositionMap.put(39,"FIT-501");
		deviceNamePositionMap.put(40,"FIT-502");
		deviceNamePositionMap.put(41,"FIT-503");
		deviceNamePositionMap.put(42,"FIT-504");
		deviceNamePositionMap.put(43,"P-501");
		deviceNamePositionMap.put(44,"P-502");
		deviceNamePositionMap.put(45,"PIT-501");
		deviceNamePositionMap.put(46,"PIT-502");
		deviceNamePositionMap.put(47,"PIT-503");
		deviceNamePositionMap.put(48,"FIT-601");
		deviceNamePositionMap.put(49,"P-601");
		deviceNamePositionMap.put(50,"P-602");
		deviceNamePositionMap.put(51,"P-603");
		
		LinkedList<Integer> floatingPointPositions = new LinkedList<Integer>();
		floatingPointPositions.add(1);
		floatingPointPositions.add(2);
		floatingPointPositions.add(6);
		floatingPointPositions.add(7);
		floatingPointPositions.add(8);
		floatingPointPositions.add(9);
		floatingPointPositions.add(17);
		floatingPointPositions.add(18);
		floatingPointPositions.add(19);
		floatingPointPositions.add(26);
		floatingPointPositions.add(27);
		floatingPointPositions.add(28);
		floatingPointPositions.add(29);
		floatingPointPositions.add(35);
		floatingPointPositions.add(36);
		floatingPointPositions.add(37);
		floatingPointPositions.add(38);
		floatingPointPositions.add(39);
		floatingPointPositions.add(40);
		floatingPointPositions.add(41);
		floatingPointPositions.add(42);	
		floatingPointPositions.add(45);
		floatingPointPositions.add(46);
		floatingPointPositions.add(47);
		floatingPointPositions.add(48);
		
		List<String> existingDevices = getSortedDeviceNames(networkController);
		
		String rowString = "";
		rowString += " "+new SimpleDateFormat("dd/MM/yyyy h:mm:ss a").format(startTime.getTime());
		for(int i = 1; i < 52; i++) {
			String currentDeviceName = deviceNamePositionMap.get(i);
			LinkedHashMap<String, String> currentDeviceData;
			if(existingDevices.contains(currentDeviceName))
				currentDeviceData = ((SWaTDevice) getDeviceByName(currentDeviceName, networkController)).getDeviceData();		
			else {
				// If the device is currently not visualized, recreate it to get its data.
				if(floatingPointPositions.contains(i))
					currentDeviceData = new SWaTSensor(0, currentDeviceName).getDeviceData();
				else
					currentDeviceData = new SWaTActuator(0, currentDeviceName).getDeviceData();
			}		
				
			if(floatingPointPositions.contains(i))
				rowString += "\t"+(currentDeviceData.get("value"));
			else
				rowString += "\t"+(currentDeviceData.get("status"));
			
		}		
		rowString += "\t"+attackLabel;
		return rowString;
	}
	
	/**
	 * Converts milliseconds to a time string.</br>
	 * Format: Hours:Minutes:Seconds.Milliseconds
	 * @param milliseconds Milliseconds.
	 * @return The milliseconds converted to the time string.
	 */	
	public static String millisecondsToTimeString(long milliseconds) {
		String msString = "";
		// Hours
		long hourMultiplicator = 60*60*1000;
		long hours = milliseconds/hourMultiplicator;
		milliseconds -= hours*hourMultiplicator;
		if(hours > 0)
			msString += hours+":";

		// Minutes
		long minuteMultiplicator = 60*1000;
		long minutes = milliseconds/minuteMultiplicator;
		milliseconds -= minutes*minuteMultiplicator;
		if(minutes > 9)
			msString += minutes+":";
		else if(minutes > 0) {
			if(hours > 0)
				msString += "0"+minutes+":";
			else
				msString += minutes+":";
		}
		else if(hours > 0)
			msString += "00:";
			
		
		// Seconds
		long secondsMultiplicator = 1000;
		long seconds = milliseconds/secondsMultiplicator;
		milliseconds -= seconds*secondsMultiplicator;
		if(seconds > 9)
			msString += seconds+".";
		else if(seconds > 0) {
			if(hours > 0 || minutes > 0)
				msString += "0"+seconds+".";
			else
				msString += seconds+".";
		}
		else if(hours > 0 || minutes > 0)
			msString += "00.";
		else
			msString += "0.";
		
		// Milliseconds
		String millisecondsToString = String.valueOf(milliseconds);
		while(millisecondsToString.length() < 3) // Add trailing zeros
			millisecondsToString = "0" + millisecondsToString;
		msString += millisecondsToString;
		
		return msString;
	}
}
