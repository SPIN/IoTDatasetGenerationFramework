package classifier;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;
import weka.clusterers.HierarchicalClusterer;
import weka.core.EuclideanDistance;
import weka.core.Instance;
import weka.core.Instances;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.simpleImplementation.BasicPacketClassifier;


/**
 * Hierarchical Clustering Approach
 * @author Andreas T. Meyer-Berg
 */
public class HierarchicalClustering extends BasicPacketClassifier {

	/**
	 * Hierarchical cluster which is used
	 */
	private HierarchicalClusterer clusterer;
	
	/**
	 * Initialize the clusterer
	 */
	public HierarchicalClustering() {
		clusterer = new HierarchicalClusterer();
		clusterer.setDistanceFunction(new EuclideanDistance());
		clusterer.setNumClusters(16);
	}
	
	@Override
	public void trainModel(Instances instances) {
		try {
			clusterer.buildClusterer(instances);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public double classifyInstance(Instance instance, Packet origin) throws Exception {
		/**
		 * Id of the closes cluster centroid
		 */
		int x = clusterer.clusterInstance(instance);
		/**
		 * centroid instance
		 */
		/*
		System.out.print(origin.getTextualRepresentation()+": ");
		double[] posteriori = clusterer.distributionForInstance(instance);
		for(int i = 0; i<posteriori.length; i++) {
			System.out.print(posteriori[i]);
			if(i<posteriori.length-1)
				System.out.print(", ");
		}*/
		return 1;
	}

	@Override
	public long getClassificationStart() {
		return 3600000;
	}

	@Override
	public String getAlgoName() {
		return "HC";
	}

}
