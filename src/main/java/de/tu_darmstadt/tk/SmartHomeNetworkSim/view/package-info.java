/**
 * GUI for visualization and interaction with the {@link de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model}.
 * 
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.view;