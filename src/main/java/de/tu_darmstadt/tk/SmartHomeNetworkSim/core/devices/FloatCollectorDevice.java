package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;

/**
 * Collector for floats, which could act as an actuator
 *
 * @author Andreas T. Meyer-Berg
 */
public class FloatCollectorDevice extends SmartDevice implements FloatCollector {

	/**
	 * Name of the information
	 */
	private String infoName;
	/**
	 * last value
	 */
	private float val;
	/**
	 * Create a new Collector with the given name
	 * @param name name of the device
	 */
	public FloatCollectorDevice(String name) {
		super(name);
		init();
	}
	
	/**
	 * Creates a new collector
	 */
	public FloatCollectorDevice() {
		super();
		init();
	}
	
	/**
	 * initializes the fields
	 */
	private void init(){
		infoName = "temperature";
		val = 22;
	}
	
	@Override
	public void setFCinfoName(String name) {
		this.infoName = name;
	}

	@Override
	public String getFCinfoName() {
		return infoName;
	}

	@Override
	public float getFCval() {
		return val;
	}

	@Override
	public void setFCval(float val) {
		this.val = val;
	}
}
