package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.SWaTSimulationController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Window for the simulation of the SWaT scenario.
 * 
 * @author Fabian Kaiser
 */
public class SWaTSimulationWindow extends JDialog implements PropertyChangeListener{
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 1452366235523521L;
	
	/**
	 * The controller of the simulation.
	 */
	private SWaTSimulationController simController;
	
	/**
	 * Textfield for the simulation duration in seconds.
	 */
	private JTextField durationSeconds = new JTextField();
	
	/**
	 * Textfield for the output directory.
	 */
	private JTextField outputDirectory = new JTextField("not set");
	
	/**
	 * Path of the output directory.
	 */
	private String outputDirectoryPath = "";	
	
	/**
	 * Textfield for the time offset in seconds.
	 */
	private JTextField timeOffsetTextfield = new JTextField();
	
	/**
	 * Start button of the simulation.
	 */
	private JButton startButton = new JButton("Start");
	
	/**
	 * Set button for the output directory.
	 */
	private JButton outputDirectoryButton;
	
	/**
	 * Whether the simulation is currently running.
	 */
	private boolean simulationIsRunning = false;
	
	/**
	 * Label for the status information of the simulation.
	 */
	private JLabel statusInformation = new JLabel(" ");

	/**
	 * Label for the start time information of the simulation.
	 */
	private JLabel timeInformationStarted = new JLabel(" ");
	
	/**
	 * Label for the stop time information of the simulation.
	 */
	private JLabel timeInformationStopped = new JLabel(" ");
	
	/**
	 * Label for the run time information of the simulation.
	 */
	private JLabel timeInformationRuntime = new JLabel(" ");
	
	/**
	 *  Label description for the status information of the simulation.
	 */
	private JLabel statusLabel = new JLabel(" ");
	
	/**
	 *  Label description for the start time information of the simulation.
	 */
	private JLabel timeLabelStarted = new JLabel(" ");
	
	/**
	 *  Label description for the stop time information of the simulation.
	 */
	private JLabel timeLabelStopped = new JLabel(" ");
	
	/**
	 *  Label description for the run time information of the simulation.
	 */
	private JLabel timeLabelRuntime = new JLabel(" ");
	
	/**
	 * Date object for the start time.
	 */
	private Date timeStartedDate;
	
	/**
	 * Date object for the stop time.
	 */
	private Date timeStoppedDate;
	
	/**
	 * Menu item for the output selection.
	 */
	private JMenuItem outputDataAll;
	
	/**
	 * The point for the status information of the network simulation.
	 */
	private int statusPercentageNetworkSimulation = 0;

	/**
	 * Constructor.
	 * @param controller The controller of the model.
	 */
	public SWaTSimulationWindow(Controller controller) {
		simController = new SWaTSimulationController(controller, this);
		createWindow();
		this.setModal(true);
	}
	
	/**
	 * Creates the window.
	 */
	private void createWindow() {
		getContentPane().setMinimumSize(new Dimension(640, 320));
		setBounds(new Rectangle(0, 0, 640, 320));
		setResizable(false);
		setTitle("Simulation");
		setIconImage(Utility.loadFile("images/IoTDGF_square.png"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		JLabel labelDuration = new JLabel("Duration in seconds: ");
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(5,5,12,5);
		getContentPane().add(labelDuration, gridBagConstraints); 
		
		durationSeconds = new JTextField("");
		durationSeconds.setColumns(10);	
		durationSeconds.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(durationSeconds.getText() == null)
					durationSeconds.setBackground(Color.RED);
				else {
					String text = durationSeconds.getText();
					if(Pattern.matches("[0-9]+", text))
						durationSeconds.setBackground(Color.WHITE);
					else
						durationSeconds.setBackground(Color.RED);
				}
				checkSimulationConfiguration();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.insets = new Insets(5,0,12,0);
		getContentPane().add(durationSeconds, gridBagConstraints); 

		JLabel labelOutputDiectory = new JLabel("Output directory: ");
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,12,5);
		getContentPane().add(labelOutputDiectory, gridBagConstraints); 
		
		outputDirectory.setEditable(false);
		outputDirectory.setColumns(10);	
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.insets = new Insets(0,0,12,0);
		getContentPane().add(outputDirectory, gridBagConstraints); 
		
		outputDirectoryButton = new JButton("Set");
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.insets = new Insets(0,5,12,5);
		getContentPane().add(outputDirectoryButton, gridBagConstraints); 
		outputDirectoryButton.addActionListener(a->{
			JFileChooser outputDirectoryChooser = new JFileChooser();
			outputDirectoryChooser.setDialogTitle("Output directory");
			outputDirectoryChooser.setCurrentDirectory(new java.io.File("."));
			outputDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);	
			outputDirectoryChooser.setAcceptAllFileFilterUsed(false);
			if(outputDirectoryChooser.showSaveDialog(getContentPane()) == JFileChooser.APPROVE_OPTION) {
				outputDirectoryPath = outputDirectoryChooser.getSelectedFile().getPath();
				outputDirectory.setText(outputDirectoryPath);
				checkSimulationConfiguration();
			}
		});
		
		JLabel labelOutputData = new JLabel("Output data: ");
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,12,5);
		getContentPane().add(labelOutputData, gridBagConstraints); 
		
		JLabel labelTimeOffset = new JLabel("Time offset in seconds: ");
		ButtonGroup outputDataGroup = new ButtonGroup();
		outputDataAll = new JRadioButtonMenuItem ("physical & network", true);
		outputDataAll.setBorderPainted(false);
		outputDataGroup.add(outputDataAll);
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,12,0);
		getContentPane().add(outputDataAll, gridBagConstraints); 
		JMenuItem outputDataPhysical = new JRadioButtonMenuItem ("physical only");
		outputDataPhysical.addChangeListener(a -> {
			if(outputDataPhysical.isSelected()) {
				timeOffsetTextfield.setEnabled(false);
				labelTimeOffset.setEnabled(false);
			}
			else {
				timeOffsetTextfield.setEnabled(true);
				labelTimeOffset.setEnabled(true);	
			}
			checkSimulationConfiguration();
		});
		outputDataPhysical.setBorderPainted(false);
		outputDataGroup.add(outputDataAll);
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,12,0);
		getContentPane().add(outputDataPhysical, gridBagConstraints); 
		outputDataGroup.add(outputDataPhysical);		

		
		ToolTipManager.sharedInstance().setDismissDelay(120000); // Show tool tips for 120 seconds		
		labelTimeOffset.setToolTipText(
				"<html>"+
				"The sensor reading used in a network traffic packet is the one<br>"+
				"from X seconds ago in the physical data, where X is the offset.<br>"+
				"Hence, the simulation of the network traffic can only begin <br>"+
				"after the offset is over.<br>"+
				"If the duration is smaller than the offset, no network traffic <br>"+
				"is simulated.<br>"+
				"The original offset is 365 seconds."+
				"</html>");
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,12,5);
		getContentPane().add(labelTimeOffset, gridBagConstraints); 
		
		timeOffsetTextfield = new JTextField("365");
		timeOffsetTextfield.setColumns(10);	
		timeOffsetTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(timeOffsetTextfield.getText() == null)
					timeOffsetTextfield.setBackground(Color.RED);
				else {
					String text = timeOffsetTextfield.getText();
					if(Pattern.matches("[0-9]+", text))
						timeOffsetTextfield.setBackground(Color.WHITE);
					else
						timeOffsetTextfield.setBackground(Color.RED);
				}
				checkSimulationConfiguration();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.insets = new Insets(0,0,12,0);
		getContentPane().add(timeOffsetTextfield, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,2,10);
		getContentPane().add(statusLabel, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,2,0);
		getContentPane().add(statusInformation, gridBagConstraints); 

		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,5,10);
		getContentPane().add(timeLabelStarted, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,5,0);
		getContentPane().add(timeInformationStarted, gridBagConstraints); 		
		
		timeLabelStopped = new JLabel();
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,5,10);
		getContentPane().add(timeLabelStopped, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,5,0);
		getContentPane().add(timeInformationStopped, gridBagConstraints); 		
		
		timeLabelRuntime = new JLabel();
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,5,10);
		getContentPane().add(timeLabelRuntime, gridBagConstraints); 
		
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,5,0);
		getContentPane().add(timeInformationRuntime, gridBagConstraints); 
		
		startButton.setEnabled(false);
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		getContentPane().add(startButton, gridBagConstraints); 
		startButton.addActionListener(a->{
			startButton.setFocusPainted(false);
			statusLabel.setText("Status:");
			timeLabelStarted.setText("Time started:");
			if(simulationIsRunning == false) { // Start simulation
				String timeOffset = "0";
				if(outputDataAll.isSelected()) timeOffset = timeOffsetTextfield.getText();
				simController.startSimulation(durationSeconds.getText(), outputDirectoryPath, outputDataAll.isSelected(), timeOffset);
				simulationIsRunning = true;
				startButton.setText("Cancel");
				startButton.setBackground(Color.RED);
				outputDirectoryButton.setEnabled(false);
				durationSeconds.setEditable(false);
				statusInformation.setText("Intializing simulation");
				timeStartedDate = new Date();			
				timeInformationStarted.setText(new SimpleDateFormat("HH:mm:ss.SSS yyyy-MM-dd").format(timeStartedDate));
				this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				timeLabelStopped.setText(" ");
				timeInformationStopped.setText(" ");
				timeLabelRuntime.setText(" ");
				timeInformationRuntime.setText(" ");
				statusPercentageNetworkSimulation = 0;
			}
			else { // Cancel simulation
				simController.cancelSimulation();
				simulationIsRunning = false;
				startButton.setText("Start");
				startButton.setBackground(null);
				outputDirectoryButton.setEnabled(true);
				durationSeconds.setEditable(true);	
				statusInformation.setText("Simulation cancelled");
				timeLabelStopped.setText("Time cancelled:");
				timeStoppedDate = new Date();
				timeInformationStopped.setText(new SimpleDateFormat("HH:mm:ss.SSS yyyy-MM-dd").format(timeStoppedDate));
				timeLabelRuntime.setText("Run time:");
				timeInformationRuntime.setText(SWaTUtilities.millisecondsToTimeString(timeStoppedDate.getTime()-timeStartedDate.getTime()));
				this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				repaint();
			}
		});
		
		
		JLabel bottomBuffer = new JLabel(" "); // Just a space at the bottom to better position the elements
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 5;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,0,0);
		getContentPane().add(bottomBuffer, gridBagConstraints); 
	}
	
	/**
	 * Checks the simulation configuration for correctness.
	 */
	private void checkSimulationConfiguration() { 
		boolean correct = true;

		// Duration
		if(durationSeconds.getText() == null)
			correct = false;
		else {
			String text = durationSeconds.getText();
			if(Pattern.matches("[0-9]+", text)){
				
			}
			else
				correct = false;
		}
		
		// Output directory
		if(outputDirectory.getText().equals("not set"))
			correct = false;

		// Time offset
		if(outputDataAll.isSelected()) {
			if(timeOffsetTextfield.getText() == null)
				correct = false;
			else {
				String text = timeOffsetTextfield.getText();
				if(Pattern.matches("[0-9]+", text)){
					
				}
				else
					correct = false;
			}
		}

		startButton.setEnabled(correct);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		switch(evt.getPropertyName()) {
			case "statusPercentageNetworkOutput":
				statusInformation.setText("Network traffic \u2013 Writing output "+(int)evt.getNewValue()+"%");
				break;		
			case "statusPercentageNetworkSimulation":
				statusPercentageNetworkSimulation += 1;
				if(statusPercentageNetworkSimulation == 6) statusPercentageNetworkSimulation = 1;
				StringBuilder informationText = new StringBuilder(40);
				informationText.append("Network traffic \u2013 Simulating ");
				for(int i = 1; i <= statusPercentageNetworkSimulation; i++)
					informationText.append(".");
				statusInformation.setText(informationText.toString());
				break;
			case "statusPercentagePhysicalOutput":
				statusInformation.setText("Physical \u2013 Writing output "+(int)evt.getNewValue()+"%");
				break;	
			case "statusPercentagePhysicalSimulation":
				statusInformation.setText("Physical \u2013 Simulating "+(int)evt.getNewValue()+"%");
				break;	
			case "simulationFinished":
				if(simulationIsRunning == false) break;
				simulationIsRunning = false;
				startButton.setText("Start");
				startButton.setBackground(null);
				outputDirectoryButton.setEnabled(true);
				durationSeconds.setEditable(true);	
				statusInformation.setText("Simulation finished");
				timeLabelStopped.setText("Time finished:");
				timeStoppedDate = new Date();
				timeInformationStopped.setText(new SimpleDateFormat("HH:mm:ss.SSS yyyy-MM-dd").format(timeStoppedDate));
				timeLabelRuntime.setText("Run time:");
				timeInformationRuntime.setText(SWaTUtilities.millisecondsToTimeString(timeStoppedDate.getTime()-timeStartedDate.getTime()));
				this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				break;			
		}		
	}

}
