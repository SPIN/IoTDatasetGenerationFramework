package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

/**
 * Constants for the types of actuators in SWaT.
 * 
 * @author Fabian Kaiser
 */
public final class SWaTActuatorTypes {
	/**
	 * Motorized Valve (MV).
	 */
	public static final int MV = 0;
	
	/**
	 * Pump (P).
	 */
	public static final int P = 1;
	
	/**
	 * UV dechlorinator (UV).
	 */
	public static final int UV = 2;
	
	/**
	 * Level switch (LS). Not used in the datasets.
	 */
	public static final int UnusedLS = 3;	

	/**
	 * Pressure switch for low values (PSL). Not used in the datasets.
	 */
	public static final int UnusedPSL = 4;	
	
	/**
	 * Pressure switch for high values (PSH). Not used in the datasets.
	 */
	public static final int UnusedPSH = 5;	

	/**
	 * Differential pressure switch for high values (DPSH). Not used in the datasets.
	 */
	public static final int UnusedDPSH = 6;	
}
