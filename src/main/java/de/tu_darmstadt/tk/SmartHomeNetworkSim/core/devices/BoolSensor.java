package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

/**
 * Interface for bool sensors
 *
 * @author Andreas T. Meyer-Berg
 */
public interface BoolSensor {

	// Information Name
	/**
	 * Name of the sensor reading
	 * @param name name of the sensed information
	 */
	public void setBSinfoName(String name);
	
	/**
	 * Return the sensor reading name
	 * @return name of the sensed information
	 */
	public String getBSinfoName();

	/**
	 * Return the current value of the sensor
	 * @return value
	 */
	public boolean getBSval();
	
	/**
	 * Set the current value of the sensor
	 * @param val new value
	 */
	public void setBSval(boolean val);

}
