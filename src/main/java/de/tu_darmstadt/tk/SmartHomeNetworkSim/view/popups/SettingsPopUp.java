package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.SettingsController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * PopUp to display and edit the settings
 *
 * @author Andreas T. Meyer-Berg
 */
public class SettingsPopUp extends JFrame implements Observer {

	/**
	 * Main Controller
	 */
	private Controller controller;
	/**
	 * Settings controller
	 */
	private SettingsController config;

	/**
	 * Reference to this
	 */
	private SettingsPopUp that;

	/**
	 * Creates a SettingsPopUp
	 * 
	 * @param controller
	 *            controller which should manipulate the model
	 */
	public SettingsPopUp(Controller controller) {
		setBounds(new Rectangle(0, 0, 470, 360));
		this.controller = controller;
		this.config = this.controller.getSettingsController();
		this.that = this;

		setTitle("Settings");
		setIconImage(Utility.loadFile("/images/settings.png"));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);

		/****************************************************************************
		 * Visualization Radius Settings
		 ****************************************************************************/
		JPanel pVisualisation = new JPanel();
		tabbedPane.addTab("Visualization", null, pVisualisation, null);
		pVisualisation.setLayout(null);

		JLabel lblVisualisationSize = new JLabel("Visualization Radius:");
		lblVisualisationSize.setBounds(10, 10, 150, 20);
		lblVisualisationSize.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVisualisationSize
				.setToolTipText("<html>Radius of the SmartDevice Visualization, which is half of the Device width.</html>");
		pVisualisation.add(lblVisualisationSize);

		JLabel lblradiusInPixels = new JLabel("(Radius in pixels:)");
		lblradiusInPixels.setHorizontalAlignment(SwingConstants.RIGHT);
		lblradiusInPixels.setBounds(10, 30, 150, 20);
		pVisualisation.add(lblradiusInPixels);

		sliderRadius = new JSlider();
		sliderRadius.setPaintLabels(true);
		sliderRadius.setPaintTicks(true);
		sliderRadius.setMajorTickSpacing(18);
		sliderRadius.setMaximum(110);
		sliderRadius.setMinimum(2);
		sliderRadius.setBounds(160, 10, 280, 50);
		pVisualisation.add(sliderRadius);
		sliderRadius.addChangeListener(a -> {
			config.setDeviceVisualizationRadius(sliderRadius.getValue());
			controller.notifyObservers();
		});
		
		JLabel lblLinkSize = new JLabel("Link dimension:");
		lblLinkSize.setBounds(10, 60, 150, 20);
		lblLinkSize
				.setToolTipText("<html>Dimenions of the Link visualization around SmartDevices.</html>");
		lblLinkSize.setHorizontalAlignment(SwingConstants.RIGHT);
		pVisualisation.add(lblLinkSize);

		JLabel lblDimInPixels = new JLabel("(Dimension in pixels:)");
		lblDimInPixels.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDimInPixels.setBounds(10, 80, 150, 20);
		pVisualisation.add(lblDimInPixels);
		
		sliderLinkRadius = new JSlider();
		sliderLinkRadius.setPaintLabels(true);
		sliderLinkRadius.setPaintTicks(true);
		sliderLinkRadius.setMajorTickSpacing(18);
		sliderLinkRadius.setMaximum(110);
		sliderLinkRadius.setMinimum(2);
		sliderLinkRadius.setBounds(160, 70, 280, 50);
		pVisualisation.add(sliderLinkRadius);
		sliderLinkRadius.addChangeListener(a -> {
			config.setLinkRadius(sliderLinkRadius.getValue());
			controller.notifyObservers();
		});

		chckbxTerminatedConnections = new JCheckBox(
				"Show terminated connections");
		chckbxTerminatedConnections.setBounds(0, 150, 240, 25);
		chckbxTerminatedConnections.addActionListener(a -> config
				.setShowTerminatedConnections(chckbxTerminatedConnections
						.isSelected()));
		chckbxTerminatedConnections
				.setToolTipText("<html>True if connections to devices, which were removed from the connection,<br> should be shown until the terminating packages of this device were sent.</html>");
		pVisualisation.add(chckbxTerminatedConnections);

		chckbxConnections = new JCheckBox("Show connections");
		chckbxConnections.setBounds(240, 150, 200, 25);
		chckbxConnections.addActionListener(a -> config
				.setShowConnections(chckbxConnections.isSelected()));
		chckbxConnections
				.setToolTipText("True if connections (Services/Connections) should be shown on the Visualization Panel.");
		pVisualisation.add(chckbxConnections);

		chckbxLinks = new JCheckBox("Show links");
		chckbxLinks.setBounds(240, 180, 200, 25);
		chckbxLinks.addActionListener(a -> config.setShowLinks(chckbxLinks
				.isSelected()));
		chckbxLinks
				.setToolTipText("<html>True if Links (Mediums of physical connection for devices like Wifi, Ethernet, Zigbee),<br> should be visualized (As part of the Circle around the Devices)</html>");
		pVisualisation.add(chckbxLinks);

		chckbxLinkToolTips = new JCheckBox("Show link ToolTips");
		chckbxLinkToolTips.setBounds(240, 210, 200, 25);
		chckbxLinkToolTips.addActionListener(a -> config
				.setShowLinkToolTips(chckbxLinkToolTips.isSelected()));
		chckbxLinkToolTips
				.setToolTipText("True if the link name should be shown on, when the mouse hovers over the link");
		pVisualisation.add(chckbxLinkToolTips);
		
		chckbxLinkNames = new JCheckBox("Show Link names as a list");
		chckbxLinkNames.setBounds(240, 240, 200, 25);
		chckbxLinkNames.addActionListener(a -> config
				.setShowLinkNameList(chckbxLinkNames.isSelected()));
		chckbxLinkNames
				.setToolTipText("True if the link names should be shown in a list in the visualization panel");
		pVisualisation.add(chckbxLinkNames);

		chckbxSmartdevices = new JCheckBox("Show SmartDevices");
		chckbxSmartdevices.setBounds(0, 180, 240, 25);
		chckbxSmartdevices.addActionListener(a -> config
				.setShowSmartDevices(chckbxSmartdevices.isSelected()));
		chckbxSmartdevices
				.setToolTipText("True if SmartDevices should be shown (Circles on the VisualizationPanel)");
		pVisualisation.add(chckbxSmartdevices);

		chckbxDeviceNames = new JCheckBox("Show SmartDevice names");
		chckbxDeviceNames.setBounds(0, 210, 240, 25);
		chckbxDeviceNames.addActionListener(a -> config
				.setShowSmartDeviceNames(chckbxDeviceNames.isSelected()));
		chckbxDeviceNames
				.setToolTipText("True if SmartDevice names should be shown");
		pVisualisation.add(chckbxDeviceNames);

		
		
		chckbxDebugMode = new JCheckBox("Show debug information");
		chckbxDebugMode.setBounds(0, 240, 240, 25);
		chckbxDebugMode.addActionListener(a -> config
				.setDebugModus(chckbxDebugMode.isSelected()));
		chckbxDebugMode
				.setToolTipText("True if debug information should be shown");
		pVisualisation.add(chckbxDebugMode);
		/****************************************************************************
		 * Show Further tabs for: Imports, Simulation, Graphics e.g.
		 ****************************************************************************/
		/**
		 * Unused Tabs for further settings
		 **
		 * JPanel pImports = new JPanel(); tabbedPane.addTab("Imports", null,
		 * pImports, null);
		 * 
		 * JPanel pSimulation = new JPanel(); tabbedPane.addTab("Simulation",
		 * null, pSimulation, null);
		 */
		update(null, null);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				controller.removeObserver(that);
			}
		});
		controller.addObserver(this);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7638231800254589350L;
	private JCheckBox chckbxDeviceNames;
	private JSlider sliderRadius;
	private JSlider sliderLinkRadius;
	private JCheckBox chckbxTerminatedConnections;
	private JCheckBox chckbxConnections;
	private JCheckBox chckbxLinks;
	private JCheckBox chckbxLinkToolTips;
	private JCheckBox chckbxLinkNames;
	private JCheckBox chckbxSmartdevices;
	private JCheckBox chckbxDebugMode;

	@Override
	public void update(Observable o, Object arg) {
		/**
		 * Update all sliders and checkboxes to the current values in the
		 * SettingsController
		 */
		sliderRadius.setValue(config.getDeviceVisualizationRadius());
		sliderLinkRadius.setValue(config.getLinkRadius());
		chckbxTerminatedConnections.setSelected(config
				.isShowTerminatedConnections());
		chckbxConnections.setSelected(config.isShowConnections());
		chckbxLinks.setSelected(config.isShowLinks());
		chckbxLinkToolTips.setSelected(config.isShowLinkToolTips());
		chckbxSmartdevices.setSelected(config.isShowSmartDevices());
		chckbxDeviceNames.setSelected(config.isShowSmartDeviceNames());
		chckbxLinkNames.setSelected(config.isShowLinkNameList());
		chckbxDebugMode.setSelected(config.isDebugModus());
	}
}
