package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.random.RandomGenerator;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.ProbabilityDistributionHandler;

/**
 * Example class representing a Normal Distribution
 *
 * @author Andreas T. Meyer-Berg
 */
public class PoissonDistributionHandler implements
		ProbabilityDistributionHandler {

	/**
	 * Poisson Mean value
	 */
	private double p;
	
	/**
	 * Standard Deviation
	 */
	private double epsilon;
	
	/**
	 * Normal Distribution
	 */
	private PoissonDistribution dist;
	
	/**
	 * 
	 */
	private RandomGenerator gen = null;
	
	/**
	 * Creates a new distribution which returns the given value
	 * @param value value to be returned
	 */
	public PoissonDistributionHandler(double p, double epsilon) {
		this.p = p;
		this.epsilon = epsilon;
		dist = new PoissonDistribution(p, epsilon);
	}

	/**
	 * Creates a default distribution, which returns 50.
	 */
	public PoissonDistributionHandler() {
		this.p = 50.0;
		this.epsilon = 12.0;
		dist = new PoissonDistribution(p, epsilon);
	}
	
	@Override
	public void setRandomGenerator(RandomGenerator rng) {
		gen = rng;
		dist = new PoissonDistribution(rng, p, epsilon, 100);
	}

	@Override
	public long sampleNextValue() {
		return (long)Math.round(dist.sample());
	}

	@Override
	public JPanel getConfigurationPanel() {
		/**
		 * JPanel which allows configuration of this Distribution
		 */
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(90, 80));
		panel.setLayout(null);
		
		/**
		 * Label 
		 */
		JLabel label = new JLabel("p(Mean): ");
		panel.add(label);
		label.setLocation(20, 20);
		label.setSize(100,20);
		label.setMinimumSize(new Dimension(50, 20));

		/**
		 * Textfield for changing the value
		 */
		JTextField valueConfig = new JTextField(""+p);
		panel.add(valueConfig);
		valueConfig.setLocation(130, 20);
		valueConfig.setMinimumSize(new Dimension(70, 20));
		valueConfig.setSize(70, 20);
		valueConfig.addActionListener(a->{
			try {
				/**
				 * Update the value
				 */
				p = Double.parseDouble(valueConfig.getText());
				initializeDistribution();
				valueConfig.setBackground(Color.WHITE);
			} catch (Exception e) {
				valueConfig.setBackground(Color.RED);
			}
		});
		
		
		
		/**
		 * Label 
		 */
		JLabel lbSD = new JLabel("epsilon: ");
		panel.add(lbSD);
		lbSD.setLocation(20, 50);
		lbSD.setSize(100,20);
		lbSD.setMinimumSize(new Dimension(50, 20));

		/**
		 * Textfield for changing the value
		 */
		JTextField tfSDvalue = new JTextField(""+epsilon);
		panel.add(tfSDvalue);
		tfSDvalue.setLocation(130, 50);
		tfSDvalue.setMinimumSize(new Dimension(70, 20));
		tfSDvalue.setSize(70, 20);
		tfSDvalue.addActionListener(a->{
			try {
				/**
				 * Update the value
				 */
				epsilon = Double.parseDouble(tfSDvalue.getText());
				tfSDvalue.setBackground(Color.WHITE);
			} catch (Exception e) {
				tfSDvalue.setBackground(Color.RED);
			}
		});
		return panel;
	}

	private void initializeDistribution() {
		if(gen==null)
			dist = new PoissonDistribution(p, epsilon);
		else
			dist = new PoissonDistribution(gen, p, epsilon, 100);
			
	}

	public static void main(String[] args) {
		PoissonDistributionHandler c = new PoissonDistributionHandler(12.0, 5.0);
		JFrame test = new JFrame("test");
		test.setSize(400, 400);
		test.add(c.getConfigurationPanel());
		test.setEnabled(true);
		test.setVisible(true);
	}

	@Override
	public String getSimpleDescription() {
		return "Normal Distribution";
	}
}
