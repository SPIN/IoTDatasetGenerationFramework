package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

/**
 * Analyzes the SWat network traffic data from the CSV files.</br>
 * Approximate execution time: several hours.</br>
 * The variable pathToCSVDirectory has to be set correctly.</br>
 * Throws exceptions if unrelated CSV files are in the same directory.
 * @author Fabian Kaiser
 */
public class SWaTNetworkTrafficAnalysis {
	
	/**
	 * The path of the directory that contains the CSV files of the SWaT A1 dataset.
	 * The files for the results are written in this directory, too.
	 */
	public static String pathToCSVDirectory = "C:/Users/user/master/Evaluation";

	/**
	 * Main method for the analysis.
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		mainAnalysis();
//		additionalAnalysis5();
	}
	
	/**
	 * The main analysis.
	 * @throws IOException 
	 */
	private static void mainAnalysis() throws IOException {
		//Sets for the unique entries in the csv files.
		LinkedHashSet<String> csv_0_num = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_1_date = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_2_time = new LinkedHashSet<String>();			
		LinkedHashSet<String> csv_3_orig = new LinkedHashSet<String>();
		LinkedHashSet<String> csv_4_type = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_5_ifname = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_6_ifdir = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_7_src = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_8_dst = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_9_proto = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_10_appiname = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_11_proxysrcip = new LinkedHashSet<String>();
		LinkedHashSet<String> csv_12_modbusfunctioncode = new LinkedHashSet<String>();	
		LinkedHashSet<String> csv_13_modbusfunctiondescription = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_14_modbustransactionid = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_15_scadatag = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_16_modbusvalue = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_17_service = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_18_sport = new LinkedHashSet<String>();		
		LinkedHashSet<String> csv_19_tag = new LinkedHashSet<String>();		

		//List that collects data of when a value appeared for the first time
		List<String> firstOccurrence = new ArrayList<String>();
		firstOccurrence.add("First occurrences of values\n"+"Filename \t Number \t Column name \t Value\n");

		//List that collects the unique connections between IP addresses
		LinkedHashSet<String> uniqueConnections = new LinkedHashSet<String>();
		uniqueConnections.add("Unique connections between IP addresses\n"+"Source -> Destination\n");
		
		//List that collects the unique connections between IP addresses including their ports
		LinkedHashSet<String> uniqueConnectionsWithPorts = new LinkedHashSet<String>();
		uniqueConnectionsWithPorts.add("Unique connections between IP addresses with ports\n"+"Source:Port -> Destination:Port\n");

		//List that collects source and proxy source IP addresses that differ from each other
		LinkedHashSet<String> srcProxySrcDifference = new LinkedHashSet<String>();
		srcProxySrcDifference.add("Cases for which source and proxy source IP addresses differ\n"+"Source IP Address \t Proxy Source IP Address\n");
		
		//List that collects information about the protocol usage
		LinkedHashSet<String> protocolInfo = new LinkedHashSet<String>();
		protocolInfo.add("Information about the protocol usage\n"+"Protocol \t Direction \t Source IP Address \t Destination IP Address\n");
		
		// Investigates connection between UDP and Modbus
		LinkedHashSet<String> udpInfo = new LinkedHashSet<String>();
		udpInfo.add("Information about Modbus values when UDP is used (empty if none)\n"+"Protocol \t Modbus_function_description\n");
	
		// Lists the source and destination IP addresses of unique Modbus functions
		LinkedHashSet<String> modbusFunctionSrcDst = new LinkedHashSet<String>();
		modbusFunctionSrcDst.add("Information about the source and destination IP addresses of unique Modbus function\n"+"Source IP Address \t Destination IP address \t Modbus_function_description\n");

		//List that collects information about the unique combinations of destination IP addresses and SCADA tags
		LinkedHashSet<String> destScadaInfo = new LinkedHashSet<String>();
		destScadaInfo.add("Information about the unique combinations of destination IP addresses and SCADA tags\n"+"Destination IP Address \t SCADA Tag\n");
				
		//List that collects information about the start, stop, and reset Modbus command
		LinkedHashSet<String> ssrInformation = new LinkedHashSet<String>();
		ssrInformation.add("Information about the start, stop, and reset Modbus commands\n"+"Date \t Time \t Source IP Address \t Destination IP Address \t Modbus_function_description\n");
				
		//List that collects information about the unique Modbus values
		LinkedHashSet<String> uniqueModbusValues = new LinkedHashSet<String>();
		uniqueModbusValues.add("Information about the unique Modbus values\n"+"SCADA Tag \t Modbus Value \t Modbus Function Description\n");
		
		//List that collects CSV lines of response packets that do not have a corresponding request packet
		LinkedHashSet<String> csvAnomalies = new LinkedHashSet<String>();
		csvAnomalies.add("Complete CSV lines of response packets that do not have corresponding request packets.\n");
		
		// SCADA system information is processed separately 
		LinkedList<String> SCADASystemIPAddresses = new LinkedList<String>();
		SCADASystemIPAddresses.add("192.168.1.100");
		SCADASystemIPAddresses.add("192.168.1.200");
		SCADASystemIPAddresses.add("192.168.1.201");
		LinkedList<String> SCADASystemInformation = new LinkedList<String>();
		
		// Requests from or responses to the SCADA system
		LinkedHashSet<String> csvSCADASystem1 = new LinkedHashSet<String>();
		csvSCADASystem1.add("num,date,time,orig,type,i/f_name,i/f_dir,src,dst,proto,appi_name,proxy_src_ip,Modbus_Function_Code,Modbus_Function_Description,Modbus_Transaction_ID,SCADA_Tag,Modbus_Value,service,s_port,Tag\n");

		// Requests from or responses to the SCADA system regarding PLCs
		LinkedHashSet<String> csvSCADASystem2 = new LinkedHashSet<String>();
		csvSCADASystem2.add("num,date,time,orig,type,i/f_name,i/f_dir,src,dst,proto,appi_name,proxy_src_ip,Modbus_Function_Code,Modbus_Function_Description,Modbus_Transaction_ID,SCADA_Tag,Modbus_Value,service,s_port,Tag\n");
		
		LinkedList<String> elements = new LinkedList<String>();
		elements.add("192.168.1.100");
		elements.add("192.168.1.200");
		elements.add("192.168.1.201");
		elements.add("192.168.1.10");
		elements.add("192.168.1.20");
		elements.add("192.168.1.30");
		elements.add("192.168.1.40");
		elements.add("192.168.1.50");
		elements.add("192.168.1.60");
		
		// Information about the CSV columns
		LinkedHashSet<String> csvColumns = new LinkedHashSet<String>();
		csvColumns.add("Information about the CSV columns.\n"+"CSV columns\t\t\t\t\t\t\tFile name\n");
		
		// Collects the unique tags as value in the last column
		LinkedHashSet<String> uniqueTags = new LinkedHashSet<String>();
		uniqueTags.add("Information about the unique values of the Tag column.\n");		
		
		// Information about modbus values of scada tags
		LinkedHashSet<String> scadaTagHMI_LIT101 = new LinkedHashSet<String>();
//		scadaTagHMI_LIT101.add("Modbus_Value,SCADA_Tag,num,date,time,filename");
		LinkedHashSet<String> scadaTagHMI_FIT201 = new LinkedHashSet<String>();
//		scadaTagHMI_FIT201.add("Modbus_Value,SCADA_Tag,num,date,time,filename");
		LinkedHashSet<String> scadaTagHMI_AIT202 = new LinkedHashSet<String>();
//		scadaTagHMI_AIT202.add("Modbus_Value,SCADA_Tag,num,date,time,filename");
		LinkedHashSet<String> scadaTagHMI_LIT301 = new LinkedHashSet<String>();
//		scadaTagHMI_LIT301.add("Modbus_Value,SCADA_Tag,num,date,time,filename");
		LinkedHashSet<String> scadaTagHMI_LIT401 = new LinkedHashSet<String>();
//		scadaTagHMI_LIT401.add("Modbus_Value,SCADA_Tag,num,date,time,filename");
		
		// The values of the modbus payloads with respect to the SCADA tags
		LinkedHashMap<String, String> valuesHMI_LIT101 = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> valuesHMI_FIT201 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_AIT202 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_LIT301 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_LIT401 = new LinkedHashMap<String, String>();	
		
		
		
		List<String> csvFiles = new ArrayList<String>();		
		//Read all csv files from folder
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}
		
		//Process csv files
		for(String filename:csvFiles) {
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
				
				if(firstLine == true)
					csvColumns.add(csvLine+"\t"+filename);	
				//Check for unique entries. Comment out lines to make the resulting files smaller.
				if(firstLine == false){
	//				if(csv_0_num.contains(entries[0]) == false) firstOccurence.add(filename+"\t"+entries[0]+"\t num\t"+entries[0]);
	//				if(csv_1_date.contains(entries[1]) == false) firstOccurence.add(filename+"\t"+entries[0]+"\t date\t"+entries[1]);
	//				if(csv_2_time.contains(entries[2]) == false) firstOccurence.add(filename+"\t"+entries[0]+"\t time\t"+entries[2]);
					if(csv_3_orig.contains(entries[3]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t orig\t"+entries[3]);
					if(csv_4_type.contains(entries[4]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t type\t"+entries[4]);
					if(csv_5_ifname.contains(entries[5]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t i/f_name\t"+entries[5]);
					if(csv_6_ifdir.contains(entries[6]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t i/f_dir\t"+entries[6]);
					if(csv_7_src.contains(entries[7]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t src\t"+entries[7]);
					if(csv_8_dst.contains(entries[8]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t dst\t"+entries[8]);
					if(csv_9_proto.contains(entries[9]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t proto\t"+entries[9]);
					if(csv_10_appiname.contains(entries[10]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t appi_name\t"+entries[10]);
					if(csv_11_proxysrcip.contains(entries[11]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t proxy_src_ip\t"+entries[11]);
					if(csv_12_modbusfunctioncode.contains(entries[12]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t Modbus_Function_Code\t"+entries[12]);
					if(csv_13_modbusfunctiondescription.contains(entries[13]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t Modbus_Function_Description\t"+entries[13]);
	//				if(csv_14_modbustransactionid.contains(entries[14]) == false) firstOccurence.add(filename+"\t"+entries[0]+"\t Modbus_Transaction_ID\t"+entries[14]);
					if(csv_15_scadatag.contains(entries[15]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t SCADA_Tag\t"+entries[15]);
	//				if(csv_16_modbusvalue.contains(entries[16]) == false) firstOccurence.add(filename+"\t"+entries[0]+"\t Modbus_Value\t"+entries[16]);
					if(csv_17_service.contains(entries[17]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t service\t"+entries[17]);
					if(csv_18_sport.contains(entries[18]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t s_port\t"+entries[18]);
					if(csv_19_tag.contains(entries[19]) == false) firstOccurrence.add(filename+"\t"+entries[0]+"\t Tag\t"+entries[19]);
					
					//Add connection information
					uniqueConnections.add(entries[7]+" -> "+entries[8]);
					uniqueConnectionsWithPorts.add(entries[7]+":"+entries[18]+" -> "+entries[8]+":"+entries[17]);
					if(entries[7].equals(entries[11]) == false) srcProxySrcDifference.add(entries[7]+"\t"+entries[11]);
					protocolInfo.add(entries[9]+"\t\t"+entries[6]+"\t"+entries[7]+"\t"+entries[8]);
					if(entries[9].equals("UDP") == true) udpInfo.add(entries[9]+"\t"+entries[13]);
					if(entries[13].equals("") == false) modbusFunctionSrcDst.add(entries[7]+"\t"+entries[8]+"\t"+entries[13]);
					if(entries[15].trim().equals("") == false) destScadaInfo.add(entries[8]+"\t"+entries[15]);
					if(entries[13].startsWith("Start") || entries[13].startsWith("Stop") || entries[13].startsWith("Reset")) ssrInformation.add(entries[1]+"\t"+entries[2]+"\t"+entries[7]+"\t"+entries[8]+"\t"+entries[13]);
					uniqueModbusValues.add(entries[15]+"\t"+entries[16]+"\t"+entries[13]);
					
					if(SCADASystemIPAddresses.contains(entries[7]) || SCADASystemIPAddresses.contains(entries[8])) {
						SCADASystemInformation.add(entries[0]+" "+entries[1]+" "+entries[2]+" "+entries[7]+" "+entries[8]+" "+entries[13]+" ");
					}
					
					if(
							(entries[7].equals("192.168.1.40") && entries[8].equals("192.168.1.50"))
							|| (entries[7].equals("192.168.1.20") && entries[8].equals("192.168.1.50"))
							|| (entries[7].equals("192.168.1.10") && entries[8].equals("192.168.1.50"))
							|| (entries[7].equals("192.168.1.30") && entries[8].equals("192.168.1.60"))
							|| (entries[7].equals("192.168.1.10") && entries[8].equals("192.168.1.40"))
							|| (entries[7].equals("192.168.1.20") && entries[8].equals("192.168.1.40"))
							|| (entries[7].equals("192.168.1.30") && entries[8].equals("192.168.1.20"))
							|| (entries[7].equals("192.168.1.10") && entries[8].equals("192.168.1.30"))
						)
						csvAnomalies.add(csvLine);
					
					if(SCADASystemIPAddresses.contains(entries[7]) || SCADASystemIPAddresses.contains(entries[8]))
						csvSCADASystem1.add(csvLine);							
					if(elements.contains(entries[7]) && SCADASystemIPAddresses.contains(entries[8])
						|| SCADASystemIPAddresses.contains(entries[7]) && elements.contains(entries[8]))
						csvSCADASystem2.add(csvLine);	
					
					uniqueTags.add(entries[entries.length-1]);
					
					if(entries[13].equals("Read Tag Service - Response")) {
						String scadaString = entries[16];//+","+entries[15]+","+entries[0]+","+entries[1]+","+entries[2]+","+filename;
						switch(entries[15]) {
							case "HMI_LIT101":
								scadaTagHMI_LIT101.add(scadaString);
								break;
							case "HMI_FIT201":
								scadaTagHMI_FIT201.add(scadaString);
								break;
							case "HMI_AIT202":
								scadaTagHMI_AIT202.add(scadaString);
								break;
							case "HMI_LIT301":
								scadaTagHMI_LIT301.add(scadaString);
								break;
							case "HMI_LIT401":
								scadaTagHMI_LIT401.add(scadaString);
								break;
						}
					}
					
					if(entries[16].startsWith("Number of Elements") == false 
							&& entries[1].equals("21Dec2015") == false 
							) {
						// Filter rows before 22Dec2015 16:30:00 when the physical data starts
						String[] time = entries[2].split(":");
						int hour = Integer.parseInt(time[0]);
						int min = Integer.parseInt(time[1]);						
						if(entries[1].equals("22Dec2015") == false || (entries[1].equals("22Dec2015") && (hour < 16 || (hour == 16 && min < 30)) == false)) {
							if(entries[15].equals("HMI_LIT101") 
									|| entries[15].equals("HMI_FIT201") 
									|| entries[15].equals("HMI_AIT202") 
									|| entries[15].equals("HMI_LIT301") 
									|| entries[15].equals("HMI_LIT401")) {
								String str1 = entries[16].split(";")[0].replace("0x", " ");
								String[] entries3 = str1.trim().split("  ");
								if(entries3.length == 4) {
									String hex = entries3[3]+entries3[2]+entries3[1]+entries3[0];
								    int intBits = Integer.parseInt(hex, 16);
								    float hexFloat = Float.intBitsToFloat(intBits);
								    
									switch(entries[15]) {
									case "HMI_LIT101":
										if(valuesHMI_LIT101.size() == 495000) break;
										valuesHMI_LIT101.put(entries[1]+entries[2],String.valueOf(hexFloat));
										break;
									case "HMI_FIT201":
										if(valuesHMI_FIT201.size() == 495000) break;
										valuesHMI_FIT201.put(entries[1]+entries[2],String.valueOf(hexFloat));
										break;
									case "HMI_AIT202":
										if(valuesHMI_AIT202.size() == 495000) break;
										valuesHMI_AIT202.put(entries[1]+entries[2],String.valueOf(hexFloat));
										break;
									case "HMI_LIT301":
										if(valuesHMI_LIT301.size() == 495000) break;
										valuesHMI_LIT301.put(entries[1]+entries[2],String.valueOf(hexFloat));
										break;
									case "HMI_LIT401":
										if(valuesHMI_LIT401.size() == 495000) break;
										valuesHMI_LIT401.put(entries[1]+entries[2],String.valueOf(hexFloat));
										
										break;
									}	
								}
							}	
						}

					}
				}
				firstLine = false;
				
				//Add entries to hashsets
				csv_0_num.add(entries[0]);		
				csv_1_date.add(entries[1]);		
				csv_2_time.add(entries[2]);			
				csv_3_orig.add(entries[3]);
				csv_4_type.add(entries[4]);		
				csv_5_ifname.add(entries[5]);		
				csv_6_ifdir.add(entries[6]);		
				csv_7_src.add(entries[7]);		
				csv_8_dst.add(entries[8]);		
				csv_9_proto.add(entries[9]);		
				csv_10_appiname.add(entries[10]);		
				csv_11_proxysrcip.add(entries[11]);
				csv_12_modbusfunctioncode.add(entries[12]);
				csv_13_modbusfunctiondescription.add(entries[13]);		
				csv_14_modbustransactionid.add(entries[14]);		
				csv_15_scadatag.add(entries[15]);		
				csv_16_modbusvalue.add(entries[16]);		
				csv_17_service.add(entries[17]);		
				csv_18_sport.add(entries[18]);		
				csv_19_tag.add(entries[19]);
				
				
				}
			csvFileReader.close();
		}

		//Write results to a files.  Comment out lines to make the resulting files smaller.
		String fileNameStart = "csvAnalysis_"+System.currentTimeMillis();
		String resultsFileName = fileNameStart+"_Unique Values.txt";
	    FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/"+resultsFileName);
	    rFWriter.write("Overview of the unique values for the categories.\nThe first entry is always the name of the category.\n\n");
//	    rFWriter.write(csv_0_num.toString()+"\n");		
//	    rFWriter.write(csv_1_date.toString()+"\n");		
//	    rFWriter.write(csv_2_time.toString()+"\n");			
	    rFWriter.write(csv_3_orig.toString()+"\n");
	    rFWriter.write(csv_4_type.toString()+"\n");		
	    rFWriter.write(csv_5_ifname.toString()+"\n");		
	    rFWriter.write(csv_6_ifdir.toString()+"\n");		
	    rFWriter.write(csv_7_src.toString()+"\n");		
	    rFWriter.write(csv_8_dst.toString()+"\n");		
	    rFWriter.write(csv_9_proto.toString()+"\n");		
	    rFWriter.write(csv_10_appiname.toString()+"\n");		
	    rFWriter.write(csv_11_proxysrcip.toString()+"\n");
	    rFWriter.write(csv_12_modbusfunctioncode.toString()+"\n");
	    rFWriter.write(csv_13_modbusfunctiondescription.toString()+"\n");		
//	    rFWriter.write(csv_14_modbustransactionid.toString()+"\n");		
	    rFWriter.write(csv_15_scadatag.toString()+"\n");		
//	    rFWriter.write(csv_16_modbusvalue.toString()+"\n");		
	    rFWriter.write(csv_17_service.toString()+"\n");		
	    rFWriter.write(csv_18_sport.toString()+"\n");		
	    rFWriter.write(csv_19_tag.toString()+"\n");
	    rFWriter.close();
		
	    
		resultsFileName = fileNameStart+"_First Occurences.txt";
	    rFWriter = new FileWriter(pathToCSVDirectory+"/"+resultsFileName);
	    for(String str:firstOccurrence) {
	    	rFWriter.write(str+"\n");	
	    }
	    rFWriter.close();
	   
		rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameStart+"_SCADA System Info");
		rFWriter.write("Packets related to the SCADA system.\nNumber Date Time SourceIPAddress DestinationIPAddress ModbusFunctionDescription\n");
		for(String info:SCADASystemInformation) {
			rFWriter.write(info+"\n");
		}
		rFWriter.close();
	    
	    HashMap<String, LinkedHashSet<String>> outputList = new HashMap<String, LinkedHashSet<String>>();
	    outputList.put(fileNameStart+"_Unique Connections.txt", uniqueConnections);
	    outputList.put(fileNameStart+"_Unique Connections with Ports.txt", uniqueConnectionsWithPorts);
	    outputList.put(fileNameStart+"_Source-Source Proxy Differences.txt", srcProxySrcDifference);
	    outputList.put(fileNameStart+"_Protocol Information.txt", protocolInfo);
	    outputList.put(fileNameStart+"_Unique Destination-SCADA values.txt", destScadaInfo);
	    outputList.put(fileNameStart+"_UDP-Modbus Information.txt", udpInfo);
	    outputList.put(fileNameStart+"_Modbus Source-Destination Information.txt", modbusFunctionSrcDst);
	    outputList.put(fileNameStart+"_Modbus Start-Stop-Reset Information.txt", ssrInformation);
	    outputList.put(fileNameStart+"_Modbus Unique Values.txt", uniqueModbusValues);
	    outputList.put(fileNameStart+"_Responses Without Requests.txt",csvAnomalies);
	    outputList.put(fileNameStart+"_Requests from or responses to SCADA system.csv", csvSCADASystem1);
	    outputList.put(fileNameStart+"_Requests from or responses to SCADA system regarding PLCs.csv", csvSCADASystem2);
	    outputList.put(fileNameStart+"_Column Information.txt", csvColumns);
	    outputList.put(fileNameStart+"_Unique Tag Values.txt", uniqueTags);
	    outputList.put(fileNameStart+"_Unique Modus Value of HMI_LIT101.csv", scadaTagHMI_LIT101);
	    outputList.put(fileNameStart+"_Unique Modus Value of HMI_FIT201.csv", scadaTagHMI_FIT201);
	    outputList.put(fileNameStart+"_Unique Modus Value of HMI_AIT202.csv", scadaTagHMI_AIT202);
	    outputList.put(fileNameStart+"_Unique Modus Value of HMI_LIT301.csv", scadaTagHMI_LIT301);
	    outputList.put(fileNameStart+"_Unique Modus Value of HMI_LIT401.csv", scadaTagHMI_LIT401);

	    for(String fileNameComplete:outputList.keySet()) {
		    rFWriter = new FileWriter(pathToCSVDirectory+"/"+fileNameComplete);
		    for(String str:outputList.get(fileNameComplete)) {
		    	rFWriter.write(str+"\n");	
		    }
		    rFWriter.close();	    	
	    }  	
	    
	    
		HashMap<String, LinkedHashMap<String, String>> outputList2 = new HashMap<String, LinkedHashMap<String, String>>();		
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_LIT101.txt", valuesHMI_LIT101);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_FIT201.txt", valuesHMI_FIT201);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_AIT202.txt", valuesHMI_AIT202);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_LIT301.txt", valuesHMI_LIT301);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_LIT401.txt", valuesHMI_LIT401);
	    
	    for(String outerKey:outputList2.keySet()) {
	    	rFWriter = new FileWriter(pathToCSVDirectory+"/"+outerKey);
	    	LinkedHashMap<String, String> values = outputList2.get(outerKey);
	    	for(String key:values.keySet()) {
		    	rFWriter.write(values.get(key)+"\n");	
	    	}
	    	 rFWriter.close();
	    }
	   	
	}
	
	/**
	 * Test method for new analysis techniques.
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	private static void additionalAnalysis4() throws IOException {
		
		// The values of the modbus payloads with respect to the SCADA tags
		LinkedHashMap<String, String> valuesHMI_LIT101 = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> valuesHMI_FIT201 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_AIT202 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_LIT301 = new LinkedHashMap<String, String>();	
		LinkedHashMap<String, String> valuesHMI_LIT401 = new LinkedHashMap<String, String>();	
		
		List<String> csvFiles = new ArrayList<String>();		
		//Read all csv files from folder
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}
		
		//Process csv files
		for(String filename:csvFiles) {
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;
			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
				
			
				if(firstLine == false){
					if(entries[16].startsWith("Number of Elements") == false 
							&& entries[1].equals("21Dec2015") == false 
							) {
						// Filter rows before 22Dec2015 16:30:00 when the physical data starts
						String[] time = entries[2].split(":");
						int hour = Integer.parseInt(time[0]);
						int min = Integer.parseInt(time[1]);						
						if(entries[1].equals("22Dec2015") == false || (entries[1].equals("22Dec2015") && (hour < 16 || (hour == 16 && min < 30)) == false)) {
							if(entries[15].equals("HMI_LIT101") 
									|| entries[15].equals("HMI_FIT201") 
									|| entries[15].equals("HMI_AIT202") 
									|| entries[15].equals("HMI_LIT301") 
									|| entries[15].equals("HMI_LIT401")) {
								String str1 = entries[16].split(";")[0].replace("0x", " ");
								String[] entries3 = str1.trim().split("  ");
								String hex = entries3[3]+entries3[2]+entries3[1]+entries3[0];
							    int intBits = Integer.parseInt(hex, 16);
							    float hexFloat = Float.intBitsToFloat(intBits);
							    
								switch(entries[15]) {
								case "HMI_LIT101":
									if(valuesHMI_LIT101.size() == 495000) break;
									valuesHMI_LIT101.put(entries[1]+entries[2],String.valueOf(hexFloat));
									break;
								case "HMI_FIT201":
									if(valuesHMI_FIT201.size() == 495000) break;
									valuesHMI_FIT201.put(entries[1]+entries[2],String.valueOf(hexFloat));
									break;
								case "HMI_AIT202":
									if(valuesHMI_AIT202.size() == 495000) break;
									valuesHMI_AIT202.put(entries[1]+entries[2],String.valueOf(hexFloat));
									break;
								case "HMI_LIT301":
									if(valuesHMI_LIT301.size() == 495000) break;
									valuesHMI_LIT301.put(entries[1]+entries[2],String.valueOf(hexFloat));
									break;
								case "HMI_LIT401":
									if(valuesHMI_LIT401.size() == 495000) break;
									valuesHMI_LIT401.put(entries[1]+entries[2],String.valueOf(hexFloat));
									
									break;
								}								
							}	
						}

					}
						

				}
				firstLine = false;
				}		
		
			csvFileReader.close();
		}
		String fileNameStart = "output";
		HashMap<String, LinkedHashMap<String, String>> outputList2 = new HashMap<String, LinkedHashMap<String, String>>();
		
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_LIT101.txt", valuesHMI_LIT101);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_FIT201.txt", valuesHMI_FIT201);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_AIT202.txt", valuesHMI_AIT202);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of HMI_LIT301.txt", valuesHMI_LIT301);
	    outputList2.put(fileNameStart+"_Modbus Payloads At Physical Data Time of valuesHMI_LIT401.txt", valuesHMI_LIT401);
	    for(String outerKey:outputList2.keySet()) {
	    	FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/"+outerKey);
	    	LinkedHashMap<String, String> values = outputList2.get(outerKey);
	    	for(String key:values.keySet()) {
		    	rFWriter.write(values.get(key)+"\n");	
	    	}
	    	 rFWriter.close();
	    }	    	
	      
	}	
	
	/**
	 * Test method for new analysis techniques.
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	private static void additionalAnalysis5() throws IOException {
		
		// The values of the modbus payloads with respect to the SCADA tags
		LinkedList<Integer> frequencyPLC1 = new LinkedList<Integer>();
		LinkedList<Integer> frequencyPLC2 = new LinkedList<Integer>();
		LinkedList<Integer> frequencyPLC3 = new LinkedList<Integer>();
		LinkedList<Integer> frequencyPLC4 = new LinkedList<Integer>();
		LinkedList<Integer> frequencyPLC5 = new LinkedList<Integer>();
		LinkedList<Integer> frequencyPLC6 = new LinkedList<Integer>();
		LinkedList<Integer> totalFrequencyPerSeconds = new LinkedList<Integer>();
		
		
		List<String> csvFiles = new ArrayList<String>();		
		//Read all csv files from folder
		File csvFolder = new File(pathToCSVDirectory);
		for(File file:csvFolder.listFiles()) {
		    if (file.isFile() && file.getName().endsWith(".csv")) {
		    	csvFiles.add(file.getName());
		    }
		}
		
		//Process csv files
		String currentTime = "";
		int plc1Counter = 0;
		int plc2Counter = 0;
		int plc3Counter = 0;
		int plc4Counter = 0;
		int plc5Counter = 0;
		int plc6Counter = 0;
		int totalCounter = 0;
		for(String filename:csvFiles) {
			//Open file
			File csvFile = new File(pathToCSVDirectory+"/"+filename);
			Scanner csvFileReader = new Scanner(csvFile);
			boolean firstLine = true;

			while (csvFileReader.hasNextLine() == true) {
				String csvLine = csvFileReader.nextLine();
				String[] entries = csvLine.split(",");
				
				String time = entries[2];
				String src = entries[7];
			
				if(firstLine == false){
					if(currentTime.equals("") == false && currentTime.equals(time) == false) {
						frequencyPLC1.add(plc1Counter);
						frequencyPLC2.add(plc2Counter);
						frequencyPLC3.add(plc3Counter);
						frequencyPLC4.add(plc4Counter);
						frequencyPLC5.add(plc5Counter);
						frequencyPLC6.add(plc6Counter);
						totalFrequencyPerSeconds.add(totalCounter);
						currentTime = time;
						plc1Counter = 0;
						plc2Counter = 0;
						plc3Counter = 0;
						plc4Counter = 0;
						plc5Counter = 0;
						plc6Counter = 0;
						totalCounter = 0;
					}
					else {
						currentTime = time;
						totalCounter++;
						switch(src) {
							case "192.168.1.10":
								plc1Counter++;
								break;
							case "192.168.1.20":
								plc2Counter++;
								break;
							case "192.168.1.30":
								plc3Counter++;
								break;
							case "192.168.1.40":
								plc4Counter++;
								break;
							case "192.168.1.50":
								plc5Counter++;
								break;
							case "192.168.1.60":
								plc6Counter++;
								break;
						}
								
					}
						

				}
				firstLine = false;
				}		
		
			csvFileReader.close();
		}
		
		Collections.sort(totalFrequencyPerSeconds);
		Collections.sort(frequencyPLC1);
		Collections.sort(frequencyPLC2);
		Collections.sort(frequencyPLC3);
		Collections.sort(frequencyPLC4);
		Collections.sort(frequencyPLC6);
		
	    	FileWriter rFWriter = new FileWriter(pathToCSVDirectory+"/FrequencyCounter.txt");
		    	rFWriter.write("totalFrequencyPerSeconds: "+totalFrequencyPerSeconds+"\n");
		    	rFWriter.write("frequencyPLC1: "+frequencyPLC1+"\n");	
		    	rFWriter.write("frequencyPLC2: "+frequencyPLC2+"\n");	
		    	rFWriter.write("frequencyPLC3: "+frequencyPLC3+"\n");
		    	rFWriter.write("frequencyPLC4: "+frequencyPLC4+"\n");
		    	rFWriter.write("frequencyPLC5: "+frequencyPLC5+"\n");
		    	rFWriter.write("frequencyPLC6: "+frequencyPLC6);	
	    	 rFWriter.close();
	    	    	
	      
	}	
}
