package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Window for the configuration of the attacks.
 * @author Fabian Kaiser
 */
public class SWaTAttacksWindow extends JDialog {
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -7201026417948697362L;
	
	/**
	 * The network controller.
	 */
	private NetworkController networkController;

	/**
	 * String for pattern matching to find floating point numbers.
	 */
	private String regExStringFloatingPoint = "[0-9]+|([0-9]+\\.?[0-9]+)";
	
	/**
	 * Whether elements were just deleted.
	 */
	private boolean elementsJustDeleted = false;
	
	/**
	 * Constructor.
	 * @param controller The controller of the model.
	 */
	public SWaTAttacksWindow(Controller controller) {
		networkController = controller.getNetworkController();
		createWindow();
		this.setModal(true);
	}
	
	/**
	 * Creates the window.
	 */
	private void createWindow() {
		getContentPane().setMinimumSize(new Dimension(640, 320));
		setBounds(new Rectangle(0, 0, 640, 320));
		setResizable(false);
		setTitle("Attacks");
		setIconImage(Utility.loadFile("images/IoTDGF_square.png"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		JLabel description = new JLabel("<html>Attacks take place from start time to end time and affect the IoT device as attack point by changing its value or status according to the attack type and attack columns. Increases and decreases are per second.</html>");
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0.08;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		getContentPane().add(description, gridBagConstraints); 
		
		String columnDescription[] = {"Start time", "End time", "Attack point", "Attack type", "Attack"};  	
		TableModel tableModel = new DefaultTableModel(SWaTUtilities.loadAttacksFromUserDataFile(networkController), columnDescription);
		TableWithIndividualCellRenderers attackTable = new TableWithIndividualCellRenderers();
		attackTable.setModel(tableModel);
		
		attackTable.setPreferredScrollableViewportSize(new Dimension(getWidth()-20, (int) getHeight()/3));
		attackTable.setFillsViewportHeight(true);
		
		JComboBox<String> attackPoints = new JComboBox<String>();
		List<String> sortedDeviceList = SWaTUtilities.getSortedDeviceNames(networkController);
		attackPoints.addItem("Select");
		for(String deviceName:sortedDeviceList) {			
			if((deviceName.startsWith("Switch") || deviceName.startsWith("PLC") || deviceName.startsWith("Engineering Workstation") || deviceName.startsWith("Historian") || deviceName.startsWith("SCADA")) == false)
				attackPoints.addItem(deviceName);
		}
		JTextField attackTypes = new JTextField();
		
		attackTable.getColumnModel().getColumn(0).setCellRenderer(new CellRendererTime());
		attackTable.getColumnModel().getColumn(1).setCellRenderer(new CellRendererTime());
		attackTable.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(attackPoints));
		attackTable.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(attackTypes));
		attackTable.getColumnModel().getColumn(4).setCellRenderer(new CellRendererAttackFloatingPoint());

		JScrollPane attackTablePane = new JScrollPane(attackTable); 
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.9;
		gridBagConstraints.weighty = 0.84;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		getContentPane().add(attackTablePane, gridBagConstraints); 
		
		JButton deleteButton = new JButton("Delete selection");
		deleteButton.addActionListener(a->{
			elementsJustDeleted = true;
			int[] selectedTableRowNumbers = attackTable.getSelectedRows();
			Arrays.sort(selectedTableRowNumbers);
			DefaultTableModel dataModelToRemoveFrom = (DefaultTableModel) attackTable.getModel();
			for(int i = selectedTableRowNumbers.length-1; i >= 0; i--)
				dataModelToRemoveFrom.removeRow(selectedTableRowNumbers[i]);	
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weightx = 0;
		gridBagConstraints.weighty = 0.08;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		getContentPane().add(deleteButton, gridBagConstraints);
		
		JButton addButton = new JButton("Add new row");
		addButton.addActionListener(a->{
			DefaultTableModel dataModelToAddTo = (DefaultTableModel) attackTable.getModel();
			dataModelToAddTo.addRow(new Object[] {"0", "0", "Select", "", ""});
		});		
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0.08;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(0,20,0,0);
		getContentPane().add(addButton, gridBagConstraints);
		
		JButton applyButton = new JButton("Apply");
		applyButton.setEnabled(false);
		applyButton.addActionListener(a->{
			SWaTUtilities.saveAttacksToUserDataFile((DefaultTableModel) attackTable.getModel());
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0.08;
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_END;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		getContentPane().add(applyButton, gridBagConstraints);	
		
		tableModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent arg0) {
				boolean tableCorrect = true;
				int rowCount = attackTable.getRowCount();
				if(rowCount == 0) {
					if(elementsJustDeleted == false)
						tableCorrect = false;
				}
				else {
					for(int i = 0; i < rowCount; i++) {
						// Check time cells					
				    	try {
				    		int startTime = Integer.parseInt((String) tableModel.getValueAt(i, 0));
				    		int endTime = Integer.parseInt((String) tableModel.getValueAt(i, 1));
			    			if(startTime > endTime && startTime > 0) {
			    				tableCorrect = false;
			    				break;
			    			}
				    	}
				    	catch (Exception e) {
				    		tableCorrect = false;
				    		break;
				    	}
				    	
				    	// Check attack point cell
				    	if(((String) tableModel.getValueAt(i, 2)).equals("Select")) {
				    		tableCorrect = false;
				    		break;
				    	}
				    	
				    	// Check attack type cell
				    	if(((String) tableModel.getValueAt(i, 3)).equals("Select")) {
				    		tableCorrect = false;
				    		break;
				    	}
				    	
				    	// Check attack cell
				    	String column3Value = (String) tableModel.getValueAt(i, 4);
				    	if(column3Value.equals("On") == false && column3Value.equals("Off") == false && column3Value.equals("Open") == false && column3Value.equals("Closed") == false && column3Value.equals("Transition") == false && Pattern.matches(regExStringFloatingPoint, column3Value) == false) {
				    		tableCorrect = false;
				    		break;
				    	}
					}
				}
				if(tableCorrect == false)
					applyButton.setEnabled(false);
				else
					applyButton.setEnabled(true);
				repaint();
			}			
		});
	}
}





/**
 * Internal class to change the attack column cells as needed.
 */
class TableWithIndividualCellRenderers extends JTable{
	/**
	*  Serial version UID.
	*/
	private static final long serialVersionUID = 6065789600166065902L;

	/**
	 * Change the attack column cells as needed.
	 */
	public TableCellEditor getCellEditor(int row, int column) {
		TableModel model = getModel();
		if(column == 2) {
			model.setValueAt("", row, 3);
			model.setValueAt("", row, 4);
			return super.getCellEditor(row, column);			
		}
		else if(column == 3) {
			String attackPointValue = (String) model.getValueAt(row, 2);
			if(attackPointValue.equals("Select")) {
				JTextField nonEditable = new JTextField("");
				model.setValueAt("", row, column);
				nonEditable.setEnabled(false);
				return new DefaultCellEditor(nonEditable);
			}
			else if(attackPointValue.startsWith("P-") || attackPointValue.startsWith("UV-") || attackPointValue.startsWith("MV-")) {
				JTextField nonEditable = new JTextField("");
				model.setValueAt("Set to", row, column);
				nonEditable.setEnabled(false);
				return new DefaultCellEditor(nonEditable);
				
			}
			else {
				JComboBox<String> rowComboBox = new JComboBox<String>();
				rowComboBox.addItem("Set to");
				rowComboBox.addItem("Increase by");
				rowComboBox.addItem("Decrease by");
				return new DefaultCellEditor(rowComboBox);
			}			
		}
		else if(column == 4) {
			String attackPointValue = (String) model.getValueAt(row, 2);
			if(attackPointValue.equals("Select")) {
				JTextField nonEditable = new JTextField("");
				model.setValueAt("", row, column);
				nonEditable.setEnabled(false);
				return new DefaultCellEditor(nonEditable);
			}
			else if(attackPointValue.startsWith("P-") || attackPointValue.startsWith("UV-")) {
				JComboBox<String> rowComboBox = new JComboBox<String>();
				rowComboBox.addItem("On");
				rowComboBox.addItem("Off");
				return new DefaultCellEditor(rowComboBox);
			}
			else if(attackPointValue.startsWith("MV-")) {
				JComboBox<String> rowComboBox = new JComboBox<String>();
				rowComboBox.addItem("Open");
				rowComboBox.addItem("Closed");
				rowComboBox.addItem("Transition");
				return new DefaultCellEditor(rowComboBox);
			}
		}
		return super.getCellEditor(row, column);
	}	
}

/**
 * Internal class to check the format and values of start time and end time.
 */
class CellRendererTime extends DefaultTableCellRenderer {		 
	/**
	*  Serial version UID.
	*/
	private static final long serialVersionUID = 8655976343827337991L;
	 
    /**
     * Checks whether start time and end time have the correct format and whether star time <= end time applies.
     */	    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String cellValue = (String) value;
    	try {	    		
    		int currentTime = Integer.parseInt(cellValue);
    		if(column == 0) {
    			int stopTime = Integer.parseInt((String) table.getModel().getValueAt(row, 1));
    			if(currentTime <= stopTime && currentTime > 0)
    				component.setForeground(null);
    			else
    				component.setForeground(Color.RED);
    		}
    		else if (column == 1) {
    			int startTime = Integer.parseInt((String) table.getModel().getValueAt(row, 0));
    			if(startTime <= currentTime  && startTime > 0)
    				component.setForeground(null);
    			else
    				component.setForeground(Color.RED);
    		}
    	}
    	catch (Exception e) {
    		component.setForeground(Color.RED);
    	}
    	table.repaint();
		return component;
    }
}

/**
 * Internal class to check the format of the sensor attacks.
 */
class CellRendererAttackFloatingPoint extends DefaultTableCellRenderer {		 
	/**
	 *  Serial version UID.
	 */
	private static final long serialVersionUID = -2022259064460377675L;

	/**
	 * String for pattern matching to find floating point numbers.
	 */
	private String regExStringFloatingPoint = "[0-9]+|([0-9]+\\.?[0-9]+)";
	
	/**
     * Checks the format of the sensor attacks.
     */	    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String cellValue = (String) value;
    	
    	if((cellValue.equals("On") || cellValue.equals("Off") || cellValue.equals("Open") || cellValue.equals("Closed") || cellValue.equals("Transition")) == false)
	    	if(Pattern.matches(regExStringFloatingPoint, cellValue))
	    		component.setForeground(null);
	    	else    		
	    		component.setForeground(Color.RED);
    	else
    		component.setForeground(null);
    	table.repaint();
		return component;
    }
}

