package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Protocol;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.Pair;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTDeviceTypes;

/**
 * A simplified version of the Modbus protocol for the SWaT scenario
 * @author Fabian Kaiser
 */
public class SWaTSimplifiedModbusProtocol implements Protocol {
	
	/**
	 * A normal device that can send and receive packets.
	 */	
	public final int roleSenderAndReceiver = 0;
	
	/**
	 * A switch that does only forward packets.
	 */
	public final int roleSwitch = 1;
	
	/**
	 * A list containing all ports of devices which can send and receive packets.
	 */
	private List<Port> sendersAndReceivers;
	
	/**
	 * List of the ports of deleted connections.
	 */
	private List<Pair<Port,Port>> deletedTopology;
	
	/**
	 * A list containing all ports of switches.
	 */
	private List<Port> switches;
	
	/**
	 * Constructor.
	 */
	public SWaTSimplifiedModbusProtocol() {
		super();
		sendersAndReceivers = new LinkedList<Port>();
		switches = new LinkedList<Port>();
		deletedTopology = new LinkedList<Pair<Port,Port>>();
	}
	
	@Override
	public Collection<Packet> generateNextPackets(Port port, long timestep, boolean packetLost) {
		return null;
	}

	@Override
	public String[] getRoles() {
		return new String[]{"Sender & Receiver", "Switch"};
	}

	@Override
	public Collection<Port> getDevicesWithRole(int role) {
		switch(role) {
			case roleSenderAndReceiver:
				return sendersAndReceivers;
			case roleSwitch:
				return switches;
			default:
				return new LinkedList<Port>();
		}
	}

	@Override
	public boolean addDeviceOfRole(Port device, int role) {
		switch(role) {
		case roleSenderAndReceiver:
			sendersAndReceivers.add(device);
			return true;
		case roleSwitch:
			switches.add(device);
			return true;
		default:
			return false;
		}
	}

	@Override
	public void removeDevice(Port device) {
		int deviceIndex = sendersAndReceivers.indexOf(device);
		if(deviceIndex != -1) sendersAndReceivers.remove(deviceIndex);
		deviceIndex = switches.indexOf(device);
		if(deviceIndex != -1) switches.remove(deviceIndex);		
	}

	@Override
	public String getName() {
		return "Simplified Modbus";
	}

	@Override
	public Collection<Pair<Port, Port>> getDeletedTopology() {
		return deletedTopology;
	}
	
	@Override
	public int getNumberOfRoles() {
		return 2;
	}
	
	@Override
	public byte getTopologyType() {
		return CUSTOM;
	}
	
	@Override
	public Collection<Pair<Port, Port>> getTopology() {
		// Hashset, so no duplicates
		LinkedHashSet<Pair<Port, Port>> topology = new LinkedHashSet<Pair<Port, Port>>();
		
		// Chain switches together
		HashMap<String, Port> switchHashMap = new HashMap<String, Port>();
		Collection<Port> switchList = getDevicesWithRole(roleSwitch);
		int numberOfSwitches = switchList.size();
		if(numberOfSwitches >= 2) {
			Iterator<Port> switchListIterator = switchList.iterator();
			Port lastSwitch = switchListIterator.next();
			Port currentSwitch;
			switchHashMap.put(lastSwitch.getOwner().getName(), lastSwitch);
			while (switchListIterator.hasNext()) {
				currentSwitch = switchListIterator.next();
				switchHashMap.put(currentSwitch.getOwner().getName(), currentSwitch);
				topology.add(new Pair<Port,Port>(lastSwitch, currentSwitch));
				lastSwitch = currentSwitch;	
			}			
		}
		else if(numberOfSwitches == 1) {
			Iterator<Port> switchListIterator = switchList.iterator();
			Port lastSwitch = switchListIterator.next();
			switchHashMap.put(lastSwitch.getOwner().getName(), lastSwitch);			
		}
		
		// Connect devices according to SWaT structure
		Collection<Port> senderAndReceiverList = getDevicesWithRole(roleSenderAndReceiver);
		Iterator<Port> senderAndReceiverListIterator = senderAndReceiverList.iterator();
		Port currentSenderAndReceiver;
		// Count device types
		int counterPLCs = 0;
		int counterRest = 0;
		while (senderAndReceiverListIterator.hasNext()) {
			currentSenderAndReceiver = senderAndReceiverListIterator.next();
			SmartDevice device = currentSenderAndReceiver.getOwner();
			if(device.getClass().getSimpleName().equals("SWaTDevice")) {
				SWaTDevice swatDevice = (SWaTDevice) device;
				switch(swatDevice.getDeviceType()) {
					case SWaTDeviceTypes.Switch:
						break;
					case SWaTDeviceTypes.PLC:
						counterPLCs++;
						break;
					default: // SCADA, EngineeringWorkstation, Historian
						counterRest++;
				}
			}
		}
		// Add connections
		senderAndReceiverList = getDevicesWithRole(roleSenderAndReceiver);
		senderAndReceiverListIterator = senderAndReceiverList.iterator();
		while (senderAndReceiverListIterator.hasNext()) {
			currentSenderAndReceiver = senderAndReceiverListIterator.next();
			SmartDevice device = currentSenderAndReceiver.getOwner();
			if(device.getClass().getSimpleName().equals("SWaTDevice")) { 
				SWaTDevice swatDevice = (SWaTDevice) device;
				switch(swatDevice.getDeviceType()) {
					case SWaTDeviceTypes.Switch:
						break;
					case SWaTDeviceTypes.PLC:
						if(counterPLCs > 1 || counterRest >= 1) 
							topology.add(new Pair<Port,Port>(currentSenderAndReceiver, switchHashMap.get("Switch 1")));
						break;
					default: // SCADA, EngineeringWorkstation, Historian
						if(counterPLCs >= 1 || counterRest > 1)
							topology.add(new Pair<Port,Port>(currentSenderAndReceiver, switchHashMap.get("Switch 2")));
				}
			}
		}						
				
		return topology;
	}	

}
