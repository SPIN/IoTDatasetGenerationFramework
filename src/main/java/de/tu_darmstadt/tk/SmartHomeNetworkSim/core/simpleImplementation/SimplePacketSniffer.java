package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.simpleImplementation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.PacketSniffer;

/**
 * SimpleImplementation of the PacketSniffer interface, which counts the packets sent per link and print them to the console.
 * 
 *
 * @author Andreas T. Meyer-Berg
 */
public class SimplePacketSniffer implements PacketSniffer {

	/**
	 * True if in testing mode;
	 */
	private boolean testing = true;
	
	@Override
	public void processPackets(HashMap<Link, LinkedList<Packet>> packets) {
		System.out.println("PacketSniffer: ");
		if(packets==null)return;
		/**
		 * Print all links and their number of packets sent
		 */
		for(Entry<Link, LinkedList<Packet>> e:packets.entrySet()){
			if(e == null || e.getKey() == null || e.getValue() == null)continue;
			System.out.println(e.getKey().getName()+": "+e.getValue().size()+" Packets");
		}
		System.out.println("");

	}

	@Override
	public void setMode(boolean testing) {
		this.testing = testing;
	}

	@Override
	public boolean getMode() {
		return this.testing;
	}
	
	

}
