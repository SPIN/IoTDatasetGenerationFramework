package de.tu_darmstadt.tk.SmartHomeNetworkSim.control;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Calendar;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SWaTSimulationManager;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;

/**
 * Controller for the simulation of the SWaT scenario.
 * @author Fabian Kaiser
 */
public class SWaTSimulationController implements PropertyChangeListener {	
	/**
	 * The network controller.
	 */
	private NetworkController networkController;
	
	/**
	 * The SWaT simulation manager.
	 */
	private SWaTSimulationManager simulationManager;

	/**
	 * The thread in which the simulation runs.
	 */
	private Thread simulationThread;
	
	/**
	 * Pass-by-reference value to stop the simulation.
	 */
	private boolean[] passByReferenceValue_KeepSimulationRunning = {true};

	/**
	 * Variable needed as Observable.
	 */
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);		
	
	/**
	 * The constructor.
	 * @param controller The controller of the scenario.
	 * @param propertyChangeListener The PropertyChangeListener to observe this class.
	 */
	public SWaTSimulationController(Controller controller, PropertyChangeListener propertyChangeListener) {
		networkController = controller.getNetworkController();
		simulationManager = new SWaTSimulationManager(controller);
		propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
	}
	
	/**
	 * Starts the simulation.
	 * @param duration The duration of the simulation in seconds.
	 * @param outputDirectoryPath The path of the directory for the output files.
	 * @param simulateAll True if physical data and network traffic is to be simulated, false if only physical data is to be simulated.
	 * @param timeOffset The time offset between the occurrence of a sensor reading in the physical data and the time it is used in network packets.
	 */
	public void startSimulation(String duration, String outputDirectoryPath, boolean simulateAll, String timeOffset) {
		int timeOffsetInt = Integer.parseInt(timeOffset);
		passByReferenceValue_KeepSimulationRunning[0] = true;
		String attackLabel = "Normal";
		Calendar startTime = Calendar.getInstance();
		String physicalStartStatus = SWaTUtilities.createXLSXRowString(networkController, startTime, attackLabel);
		simulationThread = new Thread(() -> {
			if(simulateAll && Integer.parseInt(duration) >= timeOffsetInt)
				simulationManager.startSimulations(duration, startTime, outputDirectoryPath, passByReferenceValue_KeepSimulationRunning, this, physicalStartStatus, timeOffsetInt);
			else
				simulationManager.startPhysicalSimulation(duration, startTime, outputDirectoryPath, passByReferenceValue_KeepSimulationRunning, this, physicalStartStatus);
		});
		simulationThread.start();		
	}
	
	/**
	 * Cancels a running simulation.
	 */
	public void cancelSimulation() {
		passByReferenceValue_KeepSimulationRunning[0] = false;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		propertyChangeSupport.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());		
	}
}

