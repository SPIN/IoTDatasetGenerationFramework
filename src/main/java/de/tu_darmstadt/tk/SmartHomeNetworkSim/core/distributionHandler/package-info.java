/**
 * Packet for the different Distribution Handler, which allow configuration and storage of different Distributions Functions for Ports. 
 *
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler;