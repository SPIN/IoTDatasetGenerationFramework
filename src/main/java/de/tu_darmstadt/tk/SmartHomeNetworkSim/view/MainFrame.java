package de.tu_darmstadt.tk.SmartHomeNetworkSim.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Main class of the GUI, which combines the UserInterface and visualization of the simulation.
 *
 * @author Andreas T. Meyer-Berg, Fabian Kaiser (adjustments for SWaT scenario)
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	/**
	 * Controller for manipulation of the smart home model.
	 */
	private Controller controllerSmartHome;
	/**
	 * MenuBar of the GUI of the smart home scenario.
	 */
	private MenuBar menuSmartHome;
	/**
	 * Panel which visualizes the devices, connections and links of the smart home scenario.
	 */
	public final VisualizationPanel panelSmartHome;
	/**
	 * Controller for manipulation of the SWaT model
	 */
	private Controller controllerSWaT;
	/**
	 * MenuBar of the GUI of the SWaT scenario.
	 */
	private SWaTMenuBar menuSWaT;
	/**
	 * The name of the current scenario. Either "SmartHome" or "SWaT".
	 */
	private String currentScenario = "SmartHome";
	/**
	 * Panel which visualizes the devices, connections and links of the SWaT scenario.
	 */
	public final VisualizationPanel panelSWaT;	
	
	/**
	 * Creates two frames for the scenarios of the program, which are the most outer frames of the application.
	 * @param cSmartHome Controller which handles the user interaction of the smart home scenario.
	 * @param cSWaT Controller which handles the user interaction of the SWaT scenario.
	 */
	public MainFrame(Controller cSmartHome, Controller cSWaT) {
		//Set and control
		this.controllerSmartHome = cSmartHome;
		this.controllerSWaT = cSWaT;
		
		setTitle("IoT Dataset Generation Framework \u2013 Smart Home");	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setIconImage(Utility.loadFile("images/smartHome_icon.png"));
		setLayout(new FlowLayout());
		
		//Set initial size of the frame
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = screenSize.width; 
		int height = screenSize.height;
		this.setBounds(width/8, height/8, 6*width/8, 6*height/8);
		this.setMinimumSize(new Dimension(640, 480));
		
		/*
		 * Add Visualization Panels for both scenarios.
		 */
		//Could be a more complex panel later on (TabbedPanel, ScrollPanel, SplitPanel
		panelSmartHome = new VisualizationPanel(controllerSmartHome);
		this.setContentPane(panelSmartHome);
		cSmartHome.addObserver(panelSmartHome);
		
		panelSWaT = new VisualizationPanel(controllerSWaT);
		cSWaT.addObserver(panelSWaT);		
		
		/*
		 * Add Menu Bar
		 */
		menuSWaT = new SWaTMenuBar(controllerSWaT);
		menuSmartHome = new MenuBar(controllerSmartHome);
		super.setJMenuBar(menuSmartHome);
		
		//Show Frame
		this.setVisible(true);
		setupPanel(panelSmartHome);
		setupPanel(panelSWaT);
		
		// Panel should get Focus, so Clicks on panel would be recognized
		panelSmartHome.requestFocusInWindow();		
		
	}
	
	/**
	 * Setup operations for the panel.
	 * @panel The panel to set up.
	 */
	private void setupPanel(VisualizationPanel panel) {
		//Set Dimension of the model to the JPanel size
		panel.delayedInit();
		
		//Update components if window was maximized
		this.addWindowStateListener(new WindowStateListener() {
			@Override
			public void windowStateChanged(WindowEvent e) {
				if ((e.getNewState() & MainFrame.MAXIMIZED_BOTH) == MainFrame.MAXIMIZED_BOTH){
					revalidate();
					panel.revalidate();
					panel.repaint();
				}
			}
		});
	}
	
	/**
	 * Switches the user interfaces of the scenarios.
	 * @param scenarioName "SWaT" to switch to the SWaT scenario, otherwise it switches to the smart home scenario.
	 */
	public void switchScenarioGUI(String scenarioName){
		if(scenarioName == "SWaT") {
			setTitle("IoT Dataset Generation Framework \u2013 Secure Water Treatment (SWaT)");
			this.setContentPane(panelSWaT);
			setIconImage(Utility.loadFile("images/IoTDGF_square.png"));
			super.setJMenuBar(menuSWaT);
			panelSWaT.requestFocusInWindow();
			currentScenario = "SWaT";
		}
		else {
			setTitle("IoT Dataset Generation Framework \u2013 Smart Home");
			this.setContentPane(panelSmartHome);
			setIconImage(Utility.loadFile("images/smartHome_icon.png"));
			super.setJMenuBar(menuSmartHome);
			panelSmartHome.requestFocusInWindow();
			currentScenario = "SmartHome";
		}
		revalidate();
		repaint();
	}
	
	/**
	 * Gets the current scenario's name. 
	 * @return Either "SmartHome" or "SWaT".
	 */
	public String getCurrentScenario() {
		return currentScenario;
	}
}
