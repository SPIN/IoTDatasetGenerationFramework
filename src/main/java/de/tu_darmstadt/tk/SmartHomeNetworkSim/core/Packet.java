package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

/**
 * Virtual network packet, that can be sent, collected, dumped and edited
 * 
 * @author Andreas T. Meyer-Berg
 */
public abstract class Packet {

	/**
	 * Time when the packet was sent
	 */
	protected long timestamp;
	
	/**
	 * SourcePort of the packet
	 */
	protected Port source;
	
	/**
	 * DestinationPort of the packet
	 */
	protected Port destination;
	
	/**
	 * Number representing the label of the package
	 */
	protected short label = 0;
	
	/**
	 * Creates a new packet with the given timestamp
	 * @param timestamp time the packet was sent
	 * @param source Source of the packet
	 * @param destination destination of the packet
	 */
	protected Packet(long timestamp, Port source, Port destination){
		this.timestamp = timestamp;
		this.source = source;
		this.destination = destination;
	}
	/**
	 * 
	 * @return Byte representation of the current packet
	 */
	public abstract byte[] dumpBytes();

	/**
	 * Returns a textual representation of the package
	 * 
	 * @return Textual Representation of the Package
	 */
	public abstract String getTextualRepresentation();

	/**
	 * Returns the Payload as String
	 * 
	 * @return Textual Representation of the Payload
	 */
	public abstract String getPayload();

	/**
	 * Returns the Timestamp, at which the packet was sent
	 * 
	 * @return timestamp, the packet was sent
	 */
	public long getTimestamp(){
		return timestamp;
	}
	
	/**
	 * Sets the timestamp to the given value
	 * 
	 * @param timestamp timstamp to set
	 */
	public void setTimestamp(long timestamp){
		this.timestamp = timestamp;
	}
	
	/**
	 * Returns the source port of the packet
	 * 
	 * @return source port
	 */
	public Port getSource(){
		return source;
	}
	
	/**
	 * Returns the destination port of the packet
	 * 
	 * @return destination port
	 */
	public Port getDestination(){
		return destination;
	}
	
	/**
	 * Return name of the protocol, the packets is part of
	 * 
	 * @return Protocol name
	 */
	public abstract String getProtocolName();
	
	/**
	 * Returns the label represented as a short
	 * @return label value
	 */
	public short getLabel() {
		return label;
	}
	
	/**
	 * Set the label value
	 * @param label new label value
	 */
	public void setLabel(short label) {
		this.label = label;
	}
}
