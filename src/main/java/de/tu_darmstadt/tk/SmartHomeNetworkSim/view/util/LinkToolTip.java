package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.VisualizationPanel;

/**
 * Class to store information of ToolTips on the {@link VisualizationPanel}
 *
 * @author Andreas T. Meyer-Berg
 */
public class LinkToolTip {
	
	/**
	 * Text of the toolTip
	 */
	private String text;
	/**
	 * x Position
	 */
	private int x;
	/**
	 * y Position
	 */
	private int y;
	/**
	 * Visible & enabled ?
	 */
	private boolean enabled;
	/**
	 * Link which is described
	 */
	private Link link;
	
		/**
		 * Creates a new ToolTip for the given Link
		 * @param link Link which is represented
		 * @param x x location
		 * @param y y location
		 */
		public LinkToolTip(Link link, int x, int y) {
			this.link = link;
			this.x = x;
			this.y = y;
			this.enabled = true;
			this.text = "Link specific text";
		}
		
		/**
		 * Creates a new ToolTip
		 */
		public LinkToolTip() {
			this.link = null;
			this.x = 0;
			this.y = 0;
			this.enabled = false;
			this.text = "Link specific text";
		}

		/**
		 * @return the text
		 */
		public String getText() {
			return text;
		}

		/**
		 * @param text the text to set
		 */
		public void setText(String text) {
			this.text = text;
		}

		/**
		 * @return the x
		 */
		public int getX() {
			return x;
		}

		/**
		 * @param x the x to set
		 */
		public void setX(int x) {
			this.x = x;
		}

		/**
		 * @return the y
		 */
		public int getY() {
			return y;
		}

		/**
		 * @param y the y to set
		 */
		public void setY(int y) {
			this.y = y;
		}

		/**
		 * @return the enabled
		 */
		public boolean isEnabled() {
			return enabled;
		}

		/**
		 * @param enabled the enabled to set
		 */
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		/**
		 * @return the link
		 */
		public Link getLink() {
			return link;
		}

		/**
		 * @param link the link to set
		 */
		public void setLink(Link link) {
			this.link = link;
		}
	
}
