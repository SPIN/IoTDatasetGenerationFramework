package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import java.util.Random;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SimulationManager;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.scheduler.Schedulable;


/**
 * A simple float sensor, which will generate float reading within the given bounds.
 *
 * @author Andreas T. Meyer-Berg
 */
public class FloatSensorDevice extends SmartDevice implements FloatSensor, Schedulable {

	/**
	 * Minimum value
	 */
	private float min;
	/**
	 * Maximum value
	 */
	private float max;
	/**
	 * Current value
	 */
	private float val;
	/**
	 * Previous value
	 */
	private float oldVal;
	
	/**
	 * Timestep it will be udpated again
	 */
	long nextSimulationTime = 0;
	
	/**
	 * Name of the sensor reading
	 */
	private String infoName;
	
	/**
	 * Initialize the Sensor
	 */
	public FloatSensorDevice() {
		super();
		init();
	}
	/**
	 * Create a new Sensor with the given name
	 * @param name
	 */
	public FloatSensorDevice(String name) {
		super(name);
		init();
	}
	
	public void init(){
		min = 10;
		max = 35;
		val = 22;
		oldVal = 22;
		infoName = "temperature";
		SimulationManager.scheduleEvent(this);
	}
	
	@Override
	public void setFSinfoName(String name) {
		this.infoName = name;
	}

	@Override
	public String getFSinfoName() {
		return infoName;
	}

	@Override
	public float getFSmax() {
		return max;
	}

	@Override
	public void setFSmax(float max) {
		this.max = max;
	}

	@Override
	public float getFSmin() {
		return min;
	}

	@Override
	public void setFSmin(float min) {
		this.min = min;
	}

	@Override
	public float getFSval() {
		return val;
	}

	@Override
	public void setFSval(float val) {
		this.oldVal = this.val;
		this.val = val;
	}
	
	@Override
	public void simulateTimeStep(long startTime, long duration) {
		
	}
	
	@Override
	public long getEventTime() {
		return nextSimulationTime;
	}
	@Override
	public void simulateEvent(long time) {
		updateData();
		//Update again in 10 seconds
		nextSimulationTime=time *10000;
		SimulationManager.scheduleEvent(this);
	}
	
	private void updateData() {
		float newVal = val + (val-oldVal)+new Random().nextFloat()/2.0f-0.25f;
		newVal = Math.max(Math.min(newVal, max),min);
		this.oldVal = val;
		val = newVal;
	}

}
