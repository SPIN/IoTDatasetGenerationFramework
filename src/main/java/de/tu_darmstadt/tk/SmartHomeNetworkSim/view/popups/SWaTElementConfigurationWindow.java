package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.NetworkController;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices.SWaTSensor;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTUtilities;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.view.util.Utility;

/**
 * Window for the configuration of devices and connections in the SWaT scenario.
 * 
 * @author Fabian Kaiser
 */
public class SWaTElementConfigurationWindow extends JDialog{
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 1456364354363453453L;

	/**
	 * The SmartDevice if the current element is one.
	 */
	private SmartDevice device;
	
	/**
	 * The NetworkController.
	 */
	private NetworkController networkController;
	
	/**
	 * The descriptions of the elements.
	 */
	private HashMap<String, String> elementDescriptions;
	
	/**
	 * Panel for the connections tab.
	 */
	private JPanel connectionsTab;
	
	/**
	 * Label for the connections of a device in the connections tab.
	 */
	private JLabel labelConnections = new JLabel();
	
	/**
	 * Label for the description of a device in the general tab.
	 */
	private JLabel labelDescription = new JLabel();
	
	/**
	 * The tabs of the window. 
	 */
	private JTabbedPane tabs = new JTabbedPane(JTabbedPane.TOP);
	
	/**
	 * The combo box for connections.
	 */
	private JComboBox<String> connectionsComboBox = new JComboBox<String>();
	
	/**
	 * The panel for the configuration of connections.
	 */
	private JPanel connectionConfiguration = new JPanel(); 
	
	/**
	 * Textfield for the source IP address of the currently selected element for configuration.
	 */
	private JTextField sourceIPAddressTextfield = new JTextField();

	/**
	 * Textfield for the outbound port of the currently selected element for configuration.
	 */
	private	JTextField outboundPortTextfield = new JTextField();
	
	/**
	 * Textfield for the inbound port of the currently selected element for configuration.
	 */	
	private JTextField inboundPortTextfield = new JTextField();
	
	/**
	 * Save button for the connection configuration.
	 */
	private JButton saveButton = new JButton("Apply");
	
	/**
	 * Button to open the sending frequency configuration.
	 */
	private JButton sendingFrequencyButton = new JButton();
	
	/**
	 * Button to open the response frequency configuration.
	 */
	private JButton responseTimeButton = new JButton();
	
	/**
	 * Panel for the distribution configuration of connections.
	 */
	private JPanel connectionDistributionConfiguration = new JPanel();
	
	/**
	 * The ComboBox for the distributions.
	 */
	private JComboBox<String> distributionComboBox = new JComboBox<String>();
	
	/**
	 * Description of the selected distribution;
	 */
	private JLabel distributionDescription = new JLabel();

	/**
	 * The apply button for the distribution configuration.
	 */
	private JButton applyButtonDistributionConfiguration = new JButton("Apply");
	
	/**
	 * Text field for the constant value distribution.
	 */
	private JTextField constantValueTextfield = new JTextField();
	
	/**
	 * Text field for the mean value of the normal distribution.
	 */	
	private JTextField normalDistributionTextfieldMean = new JTextField();
	
	/**
	 * Text field for the SD value of the normal distribution.
	 */		
	private JTextField normalDistributionTextfieldSD = new JTextField();
	
	/**
	 * Label for the name of the sending/response distribution configuration.
	 */
	private JLabel distributionLabel = new JLabel();
	
	/**
	 * The type of distribution that is to be configured. "sending" or "response".
	 */
	private String distributionType = "sending";
	
	/**
	 * The name of the selected connection.
	 */
	private String selectedConnection = "Overview";
	
	/**
	 * The combo box for actuator statuses.
	 */
	private JComboBox<Integer> actuatorStatusComboBox = new JComboBox<Integer>();
	
	/**
	 * String for pattern matching to find floating point numbers.
	 */
	private String regExStringFloatingPoint = "[0-9]+|([0-9]+\\.?[0-9]+)";

	/**
	 * String for pattern matching to find IP addresses.
	 */
	private String regExStringIPAddresses = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}";
	
	/**
	 * String for pattern matching to find port numbers.
	 */
	private String regExPortNumbers = "[0-9]{1,5}";
	
	
	/**
	 * Constructor.
	 * @param controller The controller of the model.
	 * @param device The clicked smart device or null.
	 * @param connection The clicked connection or null.
	 */
	public SWaTElementConfigurationWindow(Controller controller, SmartDevice device, Connection connection) {	
		if(device != null) {
			this.device = device;
		}
		
		this.networkController = controller.getNetworkController();
		
		connectionsTab = createConnectionsTab();
		
		createWindow();
		this.setModal(true);
	}
	
	/**
	 * Creates the window.
	 */
	private void createWindow() {
		getContentPane().setMinimumSize(new Dimension(640, 320));
		setBounds(new Rectangle(0, 0, 640, 320));
		setResizable(false);
		setTitle("Configuration");
		setIconImage(Utility.loadFile("images/IoTDGF_square.png"));
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		// Label on top
		JLabel labelComponents = new JLabel("Selected component: ");
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		gridBagConstraints.insets = new Insets(9,5,0,0);
		this.add(labelComponents, gridBagConstraints); 	
		
		createElementComboBox();
		
		// Tabs for the selected element
		tabs.addTab("Description", createGeneralTab());
		tabs.addTab("Connections", connectionsTab);				
		if(device.getClass().getSimpleName().equals("SWaTSensor")) tabs.addTab("Value", createValueTab());
		else if(device.getClass().getSimpleName().equals("SWaTActuator"))tabs.addTab("Status", createStatusTab());
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		this.add(tabs, gridBagConstraints); 	
		
		
	}

	/**
	 * Creates the ComboBox for the elements.
	 */
	private void createElementComboBox() {
		// ComboBox for elements
		JComboBox<String> elementComboBox = new JComboBox<String>();
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		gridBagConstraints.insets = new Insets(5,5,0,0);
		this.add(elementComboBox, gridBagConstraints); 	

		// Fill ComboBox
		List<SmartDevice> deviceList = SWaTUtilities.getSortedDeviceList(networkController);
		Iterator<SmartDevice> deviceListIterator = deviceList.iterator();
		SmartDevice dev;
		while (deviceListIterator.hasNext()) {
			dev = deviceListIterator.next();
			elementComboBox.addItem(dev.getName());			
		}
		elementComboBox.setSelectedItem(device.getName());
		
		// Actionlistener for combo box
		elementComboBox.addActionListener(a -> {			
			String selectedDeviceName = elementComboBox.getSelectedItem().toString();
			// Associate devices with their names
			List<SmartDevice> deviceListComboBox = (List<SmartDevice>) networkController.getSmartDevices();
			Iterator<SmartDevice> deviceListIteratorComboBox = deviceListComboBox.iterator();
			HashMap<String, SmartDevice> currentDevices = new HashMap<String, SmartDevice>();
			SmartDevice deviceComboBox;
			while(deviceListIteratorComboBox.hasNext()) {
				deviceComboBox = deviceListIteratorComboBox.next();
				currentDevices.put(deviceComboBox.getName(), deviceComboBox);
			}
			
			device = SWaTUtilities.getDeviceByName(selectedDeviceName, networkController);
			// Change description in general tab
			labelDescription.setText(getElementDescription(device));
			
			// Change connections in connection tab
			labelConnections.setText(createConnectionsString());

			List<String> elementConnections = SWaTUtilities.getElementConnections(device, networkController);
			int connectionCounter = elementConnections.size();
			int comboBoxComponents = connectionsComboBox.getItemCount();
			for(int i = 1; i < comboBoxComponents; i++) {
				connectionsComboBox.removeItemAt(1);		
			}
			for(int i = 0; i < connectionCounter; i++) {
				connectionsComboBox.addItem(elementConnections.get(i));
			}
			connectionsComboBox.setSelectedItem("Overview");

			if(device.getClass().getSimpleName().equals("SWaTDevice") && device.getName().startsWith("Switch") == false) {
				connectionsComboBox.setVisible(true);
			}
			else {
				connectionsComboBox.setVisible(false);
			}
			connectionConfiguration.setVisible(false);
				
			int selectedTabBefore = tabs.getSelectedIndex();
			// Add or remove status/value tab
			if(tabs.getComponentCount() == 3)
				tabs.remove(2);
			if(device.getClass().getSimpleName().equals("SWaTSensor")) tabs.addTab("Value", createValueTab());
			else if(device.getClass().getSimpleName().equals("SWaTActuator"))tabs.addTab("Status", createStatusTab());
			
			switchConnectionsTab("normal");
			
			// If attack tab was selected, it is to be selected again
			if(tabs.getComponentCount() == 3 && selectedTabBefore == 2)
				tabs.setSelectedIndex(2);
			
			adjustLabelConnectionsBounds();
		});
	}
	
	/**
	 * Creates the general tab.
	 * @return The panel for the general tab.
	 */
	private JPanel createGeneralTab() {
		JPanel statusPanel = new JPanel();	
		statusPanel.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		// Description for current element
		labelDescription.setText(getElementDescription(device));
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
		gridBagConstraints.insets = new Insets(5,5,0,0);
		statusPanel.add(labelDescription, gridBagConstraints); 	

//		JLabel bottomBufferGeneralTab = new JLabel(" "); // buffer space between elements
//		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
//		gridBagConstraints.gridx = 0;
//		gridBagConstraints.gridy = 3;
//		gridBagConstraints.weighty = 1;
//		gridBagConstraints.weightx = 1;
//		gridBagConstraints.gridwidth = 4;
//		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
//		gridBagConstraints.insets = new Insets(0,0,0,0);
//		statusPanel.add(bottomBufferGeneralTab, gridBagConstraints); 
		
		return statusPanel;
	}
	
	/**
	 * Creates the connections tab.
	 * @return The panel for the connections tab.
	 */
	private JPanel createConnectionsTab() {
		JPanel connectionsPanel = new JPanel();
		connectionsPanel.setLayout(null);
		
		createConnectionsComboBox();
		connectionsComboBox.setAlignmentY(TOP_ALIGNMENT);
		connectionsComboBox.setBounds(5, 3, 600, 20);
		connectionsPanel.add(connectionsComboBox);	
		
		labelConnections.setText(createConnectionsString());
		labelConnections.setVerticalAlignment(SwingConstants.TOP);
		labelConnections.setBounds(5, 33, 610, 250);
		adjustLabelConnectionsBounds();
		connectionsPanel.add(labelConnections);

		createConnectionConfiguration();
		connectionsPanel.add(connectionConfiguration);
		connectionConfiguration.setBounds(5, 35, 600, 200);
		return connectionsPanel;
	}

	/**
	 * Creates the panel for the configuration of connections.
	 * @return The panel for the configuration of connections.
	 */
	private	void createConnectionConfiguration() {
		connectionConfiguration = new JPanel();
		connectionConfiguration.setLayout(null);
		
		// Design parameters
		int initialDistanceHeight = 22;
		int distanceBetweenColumns = 25;
		int rowHeight = 20;		
		int column1X = 0;
		int column2X = 160+30;
		int row1Width = 140+30;
		int row2Width = 170+30;
		
		ToolTipManager.sharedInstance().setDismissDelay(120000); // Show tool tips for 120 seconds
		
		// Source IP Address
		String sourceIPAddressToolTip = "The IP address of the currently selected component.";		
		JLabel sourceIPAddressLabel = new JLabel("Source IP Address:");
		sourceIPAddressLabel.setToolTipText(sourceIPAddressToolTip);
		sourceIPAddressLabel.setBounds(column1X, initialDistanceHeight, row1Width, rowHeight);	
		connectionConfiguration.add(sourceIPAddressLabel);
		
		sourceIPAddressTextfield = new JTextField(getFieldText("sourceIPAddress"));
		sourceIPAddressTextfield.setColumns(10);
		sourceIPAddressTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(sourceIPAddressTextfield.getText() == null)
					sourceIPAddressTextfield.setBackground(Color.RED);
				else {
					String text = sourceIPAddressTextfield.getText();
					if(Pattern.matches(regExStringIPAddresses, text))
						sourceIPAddressTextfield.setBackground(Color.WHITE);
					else
						sourceIPAddressTextfield.setBackground(Color.RED);
				}
				checkConnectionConfiguration();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		sourceIPAddressTextfield.setBounds(column2X, initialDistanceHeight, row2Width, rowHeight);	
		connectionConfiguration.add(sourceIPAddressTextfield);	  		
		
		// Outbound Port
		String outboundPortToolTip = "The port from which the currently selected component sends packets to the connection destination.";		
		JLabel outboundPortLabel = new JLabel("Outbound Port:");
		outboundPortLabel.setToolTipText(outboundPortToolTip);
		outboundPortLabel.setBounds(column1X, initialDistanceHeight+1*distanceBetweenColumns, row1Width, rowHeight);		
		connectionConfiguration.add(outboundPortLabel);
		
		outboundPortTextfield = new JTextField(getFieldText("outboundPort"));
		outboundPortTextfield.setColumns(10);	
		outboundPortTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(outboundPortTextfield.getText() == null)
					outboundPortTextfield.setBackground(Color.RED);
				else {
					String text = outboundPortTextfield.getText();
					if(Pattern.matches(regExPortNumbers, text)) {
						int portNumber = Integer.parseInt(text);
						if(portNumber >=0 && portNumber <= 65535)
							outboundPortTextfield.setBackground(Color.WHITE);
						else
							outboundPortTextfield.setBackground(Color.RED);
					}
					else
						outboundPortTextfield.setBackground(Color.RED);
				}
				checkConnectionConfiguration();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		outboundPortTextfield.setBounds(column2X, initialDistanceHeight+1*distanceBetweenColumns, row2Width, rowHeight);		
		connectionConfiguration.add(outboundPortTextfield);	  		
		
		// Inbound Port
		String inboundPortToolTip = "The port at which the currently selected component listens for answers from the connection destination.";		
		JLabel inboundPortLabel = new JLabel("Inbound Port:");
		inboundPortLabel.setToolTipText(inboundPortToolTip);
		inboundPortLabel.setBounds(column1X, initialDistanceHeight+2*distanceBetweenColumns, row1Width, rowHeight);	
		connectionConfiguration.add(inboundPortLabel);

		inboundPortTextfield = new JTextField(getFieldText("inboundPort"));
		inboundPortTextfield.setColumns(10);
		inboundPortTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(inboundPortTextfield.getText() == null)
					inboundPortTextfield.setBackground(Color.RED);
				else {
					String text = inboundPortTextfield.getText();
					if(Pattern.matches(regExPortNumbers, text)) {
						int portNumber = Integer.parseInt(text);
						if(portNumber >=0 && portNumber <= 65535)
							inboundPortTextfield.setBackground(Color.WHITE);
						else
							inboundPortTextfield.setBackground(Color.RED);
					}
					else
						inboundPortTextfield.setBackground(Color.RED);
				}
				checkConnectionConfiguration();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		inboundPortTextfield.setBounds(column2X, initialDistanceHeight+2*distanceBetweenColumns, row2Width, rowHeight);	
		connectionConfiguration.add(inboundPortTextfield);		

		// Sending Frequency
		String sendingFrequencyToolTip = "The frequency according to which the currently selected component sends packets to the connection destination.";		
		JLabel sendingFrequencyLabel = new JLabel("Sending Frequency:");
		sendingFrequencyLabel.setToolTipText(sendingFrequencyToolTip);
		sendingFrequencyLabel.setBounds(column1X, initialDistanceHeight+3*distanceBetweenColumns, row1Width, rowHeight);		
		connectionConfiguration.add(sendingFrequencyLabel);
		
		sendingFrequencyButton = new JButton(getFieldText("sendingFrequencyButton"));
		sendingFrequencyButton.setBorder(new LineBorder(Color.WHITE, 1));
		sendingFrequencyButton.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	sendingFrequencyButton.setBorder(new LineBorder(Color.GRAY, 1));
		    }
		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	sendingFrequencyButton.setBorder(new LineBorder(Color.WHITE, 1));
		    }
		    public void mouseClicked(java.awt.event.MouseEvent evt) {		    	
		    	distributionType = "sending";
		    	distributionComboBox.setSelectedItem(((SWaTDevice) device).getDeviceData().get(selectedConnection+"_"+distributionType+"Current"));
		    	switchConnectionsTab("sendingDistribution");
		    }
		});		
		sendingFrequencyButton.setBounds(column2X, initialDistanceHeight+3*distanceBetweenColumns, row2Width, rowHeight);			
		connectionConfiguration.add(sendingFrequencyButton);		

		// Response Time
		String responseTimeToolTip = "The frecquency according to which the currently selected component responds to packets from the connection destination.";		
		JLabel responseTimeLabel = new JLabel("Response Frequency:");
		responseTimeLabel.setToolTipText(responseTimeToolTip);
		responseTimeLabel.setBounds(column1X, initialDistanceHeight+4*distanceBetweenColumns, row1Width, rowHeight);		
		connectionConfiguration.add(responseTimeLabel);
		
		responseTimeButton = new JButton(getFieldText("responseTimeButton"));
		responseTimeButton.setBorder(new LineBorder(Color.WHITE, 1));
		responseTimeButton.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	responseTimeButton.setBorder(new LineBorder(Color.GRAY, 1));
		    }
		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	responseTimeButton.setBorder(new LineBorder(Color.WHITE, 1));
		    }
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		    	distributionType = "response";
		    	distributionComboBox.setSelectedItem(((SWaTDevice) device).getDeviceData().get(selectedConnection+"_"+distributionType+"Current"));
		    	switchConnectionsTab("responseDistribution");
		    }
		});
		responseTimeButton.setBounds(column2X, initialDistanceHeight+4*distanceBetweenColumns, row2Width, rowHeight);		
		connectionConfiguration.add(responseTimeButton);	
		
		// Buttons
		JButton resetButton = new JButton("Reset to Standard");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
				LinkedHashMap<String, String> deviceDataStandard = ((SWaTDevice) device).getDeviceDataStandard();
				// Reset parameters
				LinkedList<String> parameters = new LinkedList<String>();
				parameters.add(selectedConnection+"_sourceIPAddress");
				parameters.add(selectedConnection+"_outboundPort");
				parameters.add(selectedConnection+"_inboundPort");
				parameters.add(selectedConnection+"_"+distributionType+"NormalDistributionMean");
				parameters.add(selectedConnection+"_"+distributionType+"NormalDistributionSD");
				parameters.add(selectedConnection+"_"+distributionType+"ConstantValue");
				parameters.add(selectedConnection+"_sendingCurrent");
				parameters.add(selectedConnection+"_responseCurrent");
				for(String parameter:parameters) {
					deviceData.put(parameter, deviceDataStandard.get(parameter));
				}
				((SWaTDevice) device).setDeviceData(deviceData);
				// Reset button and textField texts
				sendingFrequencyButton.setText(getFieldText("sendingFrequencyButton"));
				responseTimeButton.setText(getFieldText("responseTimeButton"));
				sourceIPAddressTextfield.setText(getFieldText("sourceIPAddress"));
				outboundPortTextfield.setText(getFieldText("outboundPort"));
				inboundPortTextfield.setText(getFieldText("inboundPort"));			
			}		
		});
		resetButton.setBounds(0, 155, 150+30, 20);
		connectionConfiguration.add(resetButton);	
		
		
		saveButton.setBounds(510, 155, 90, 20);
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
				deviceData.put(selectedConnection+"_sourceIPAddress", sourceIPAddressTextfield.getText());
				deviceData.put(selectedConnection+"_outboundPort", outboundPortTextfield.getText());
				deviceData.put(selectedConnection+"_inboundPort", inboundPortTextfield.getText());
				((SWaTDevice) device).setDeviceData(deviceData);
			}		
		});
		connectionConfiguration.add(saveButton);		

		connectionConfiguration.setVisible(false);
	}
	
	/**
	 * Creates the panel for the distribution configuration of connections.
	 * @return The panel for the distribution configuration of connections.
	 * @param type "Sending" or "Response"
	 */
	private	void createConnectionDistributionConfiguration(String type) {
		connectionDistributionConfiguration = new JPanel();
		connectionDistributionConfiguration.setBounds(5, 0, 600, 290);
		connectionDistributionConfiguration.setLayout(null);
		
		distributionLabel = new JLabel(type+" Frequency Distribution:");
		distributionLabel.setBounds(0, 35, 610, 20);
		connectionDistributionConfiguration.add(distributionLabel);		
		
		// Configuration options
		JLabel normalDistributionLabelMean = new JLabel("Mean:");
		normalDistributionLabelMean.setVisible(false);
		normalDistributionLabelMean.setBounds(0, 110, 120+30, 20);
		connectionDistributionConfiguration.add(normalDistributionLabelMean);			
		
		normalDistributionTextfieldMean = new JTextField(getFieldText("normalDistributionTextfieldMean"));
		normalDistributionTextfieldMean.setVisible(false);
		normalDistributionTextfieldMean.setColumns(10);	
		normalDistributionTextfieldMean.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(normalDistributionTextfieldMean.getText() == null) {
					normalDistributionTextfieldMean.setBackground(Color.RED);
				}
				else {
					String text = normalDistributionTextfieldMean.getText();
					if(Pattern.matches(regExStringFloatingPoint, text)) {
						normalDistributionTextfieldMean.setBackground(Color.WHITE);
					}
					else {
						normalDistributionTextfieldMean.setBackground(Color.RED);
					}
				}
				checkDistributionConfigurationNormalDistribution();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		normalDistributionTextfieldMean.setBounds(140+30, 110, 400, 20);
		connectionDistributionConfiguration.add(normalDistributionTextfieldMean);	
		
		JLabel normalDistributionLabelSD = new JLabel("Standard Deviation:");
		normalDistributionLabelSD.setVisible(false);
		normalDistributionLabelSD.setBounds(0, 135, 120+30, 20);
		connectionDistributionConfiguration.add(normalDistributionLabelSD);
		
		normalDistributionTextfieldSD = new JTextField(getFieldText("normalDistributionTextfieldSD"));
		normalDistributionTextfieldSD.setVisible(false);
		normalDistributionTextfieldSD.setColumns(10);	
		normalDistributionTextfieldSD.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(normalDistributionTextfieldSD.getText() == null) {
					normalDistributionTextfieldSD.setBackground(Color.RED);
				}
				else {
					String text = normalDistributionTextfieldSD.getText();
					if(Pattern.matches(regExStringFloatingPoint, text)) {
						normalDistributionTextfieldSD.setBackground(Color.WHITE);
					}
					else {
						normalDistributionTextfieldSD.setBackground(Color.RED);
					}
				}
				checkDistributionConfigurationNormalDistribution();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		normalDistributionTextfieldSD.setBounds(140+30, 135, 400, 20);
		connectionDistributionConfiguration.add(normalDistributionTextfieldSD);
				
		JLabel constantValueLabel = new JLabel("Value:");
		constantValueLabel.setVisible(false);
		constantValueLabel.setBounds(0, 110, 120, 20);
		connectionDistributionConfiguration.add(constantValueLabel);			

		constantValueTextfield = new JTextField(getFieldText("constantValueTextfield"));
		constantValueTextfield.setVisible(false);
		constantValueTextfield.setColumns(10);	
		constantValueTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(constantValueTextfield.getText() == null) {
					constantValueTextfield.setBackground(Color.RED);
				}
				else {
					String text = constantValueTextfield.getText();
					if(Pattern.matches(regExStringFloatingPoint, text)) {
						constantValueTextfield.setBackground(Color.WHITE);
					}
					else {
						constantValueTextfield.setBackground(Color.RED);
					}
				}
				checkDistributionConfigurationNormalDistribution();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		constantValueTextfield.setBounds(140+30, 110, 400, 20);
		connectionDistributionConfiguration.add(constantValueTextfield);			
		
		
		// ComboBox for the distributions
		distributionComboBox = new JComboBox<String>();
		distributionComboBox.addActionListener(a -> {
			String distribution = distributionComboBox.getSelectedItem().toString();
			setDistributionDescription(distribution);
			
			// Set all options to invisible and then only make the needed visible again
			normalDistributionLabelMean.setVisible(false);
			normalDistributionTextfieldMean.setVisible(false);
			normalDistributionLabelSD.setVisible(false);
			normalDistributionTextfieldSD.setVisible(false);
			constantValueLabel.setVisible(false);
			constantValueTextfield.setVisible(false);
			switch(distribution) {
				case "Normal Distribution":
					normalDistributionLabelMean.setVisible(true);
					normalDistributionTextfieldMean.setVisible(true);
					normalDistributionLabelSD.setVisible(true);
					normalDistributionTextfieldSD.setVisible(true);
					break;
				case "Constant Value":
					constantValueLabel.setVisible(true);
					constantValueTextfield.setVisible(true);
					break;
				default:
			}
			checkDistributionConfigurationNormalDistribution();
		});
		distributionComboBox.addItem("Dataset A1 Distribution");
		distributionComboBox.addItem("Normal Distribution");
		distributionComboBox.addItem("Constant Value");
		distributionComboBox.setSelectedItem(((SWaTDevice) device).getDeviceData().get(selectedConnection+"_"+distributionType+"Current"));
		distributionComboBox.setAlignmentY(TOP_ALIGNMENT);
		distributionComboBox.setBounds(0, 60, 600, 20);	
		connectionDistributionConfiguration.add(distributionComboBox);
				
		setDistributionDescription("Dataset A1 Distribution");
		distributionDescription.setBounds(0, 85, 600, 20);	
		connectionDistributionConfiguration.add(distributionDescription);		

		// Buttons
		JButton cancelButton = new JButton("Back");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				 switchConnectionsTab("normal");
			}			
		});
		cancelButton.setBounds(0, 190, 90, 20);
		connectionDistributionConfiguration.add(cancelButton);	
				
		applyButtonDistributionConfiguration.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
				
				String currentDistribution = distributionComboBox.getSelectedItem().toString();		
				deviceData.put(selectedConnection+"_"+distributionType+"Current", currentDistribution);
				
				switch(currentDistribution) {
					case "Normal Distribution":
						deviceData.put(selectedConnection+"_"+distributionType+"NormalDistributionMean", normalDistributionTextfieldMean.getText());
						deviceData.put(selectedConnection+"_"+distributionType+"NormalDistributionSD", normalDistributionTextfieldSD.getText());
						break;
					case "Constant Value":
						deviceData.put(selectedConnection+"_"+distributionType+"ConstantValue",constantValueTextfield.getText());
						break;
					default:
				}
				((SWaTDevice) device).setDeviceData(deviceData);
				// Update button texts
				sendingFrequencyButton.setText(getFieldText("sendingFrequencyButton"));
				responseTimeButton.setText(getFieldText("responseTimeButton"));
			}			
		});
		applyButtonDistributionConfiguration.setBounds(510, 190, 90, 20);
		connectionDistributionConfiguration.add(applyButtonDistributionConfiguration);
	}
	
	/**
	 * Sets the description of the distribution.
	 * @param distributionName The description of the distribution.
	 */
	private void setDistributionDescription(String distributionName) {
		switch(distributionName) {
			case "Dataset A1 Distribution": // Texts have to start with a T (or at least not an N) otherwise the ComboBox moves for unknown reasons!
				distributionDescription.setText("The distribution extracted from dataset A1 of SWaT.");
				break;
			case "Normal Distribution":
				distributionDescription.setText("The normal distribution with respect to the configured values in ms.");
				break;
			case "Constant Value":
				distributionDescription.setText("The frequency is the configured constant value in ms.");
				break;				
			default:
				distributionDescription.setText("");
		}
	}
	
	/**
	 * Checks the configuration of connections for correctness.
	 */
	private void checkConnectionConfiguration() { 
		boolean correct = true;

		// Check source IP address
		if(sourceIPAddressTextfield.getText() == null)
			correct = false;
		else {
			String text = sourceIPAddressTextfield.getText();
			if(Pattern.matches(regExStringIPAddresses, text)){
			}
			else
				correct = false;
		}
		
		// Check outbound port
		if(outboundPortTextfield.getText() == null)
			correct = false;
		else {
			String text = outboundPortTextfield.getText();
			if(Pattern.matches(regExPortNumbers, text)) {
				int portNumber = Integer.parseInt(text);
				if(portNumber >= 0 && portNumber <= 65535) {					
				}
				else
					correct = false;
			}
			else
				correct = false;
		}
		
		// Check inbound port
		if(inboundPortTextfield.getText() == null)
			correct = false;
		else {
			String text = inboundPortTextfield.getText();
			if(Pattern.matches(regExPortNumbers, text)) {
				int portNumber = Integer.parseInt(text);
				if(portNumber >= 0 && portNumber <= 65535) {					
				}
				else
					correct = false;
			}
			else
				correct = false;
		}
		saveButton.setEnabled(correct);
	}
	
	/**
	 * Checks the normal distribution configuration of connections for correctness.
	 */
	private void checkDistributionConfigurationNormalDistribution() { 
		boolean correct = true;		
		String distributionType = distributionComboBox.getSelectedItem().toString();	
		
		switch(distributionType) {
			case "Normal Distribution":
				// Mean
				if(normalDistributionTextfieldMean.getText() == null) {
					correct = false;
				}
				else {
					String text = normalDistributionTextfieldMean.getText();
					if(Pattern.matches(regExStringFloatingPoint, text) == false)
						correct = false;
				}
				
				// Check standard deviation
				if(normalDistributionTextfieldSD.getText() == null) {
					correct = false;
				}
				else {
					String text = normalDistributionTextfieldSD.getText();
					if(Pattern.matches(regExStringFloatingPoint, text) == false)
						correct = false;
				}
				break;
			case "Constant Value":
				if(constantValueTextfield.getText() == null) {
					correct = false;
				}
				else {
					String text = constantValueTextfield.getText();
					if(Pattern.matches(regExStringFloatingPoint, text) == false)
						correct = false;
				}
				break;
			default:
				correct = true;
				
		}

		applyButtonDistributionConfiguration.setEnabled(correct);
	}
	
	/**
	 * Creates the connection string.
	 * @return The connection string.
	 */
	private String createConnectionsString() {
		List<String> connectionStrings = SWaTUtilities.getElementConnections(device, networkController);
		String connectionsString = "<html>";
		
		switch(device.getClass().getSimpleName()) {
			case "SWaTDevice":
				for(String connection:connectionStrings) {
				connectionsString += connection;
				connectionsString += "<br/>";
				}
				if(connectionStrings.size() > 0 && device.getName().startsWith("Switch") == false) 
					connectionsString += "<br/>All connections are bidirectional.";
				break;
			default:
				connectionsString += "Forms a ring network with all other sensors and actuators of the same stage. ";
				connectionsString += "No further information is available since the SWaT datasets do not contain the traffic of these networks. ";
				connectionsString += "Therefore, the communication between sensors and actuators is not part of this simulation.";
		}
		connectionsString += "</html>";		
		return connectionsString;
	}
	
	/**
	 * Creates the ComboBox for connections.
	 * @return The ComboBox.
	 */
	private JComboBox<String> createConnectionsComboBox() {
		connectionsComboBox = new JComboBox<String>();
		
		connectionsComboBox.addItem("Overview");
		
		List<String> elementConnections = SWaTUtilities.getElementConnections(device, networkController);
		// Fill ComboBox
		Iterator<String> elementConnectionsIterator = elementConnections.iterator();
		String connectionString;
		while (elementConnectionsIterator.hasNext()) {
			connectionString = elementConnectionsIterator.next();
			connectionsComboBox.addItem(connectionString);			
		}
		connectionsComboBox.setSelectedItem("Overview");
		
		// Actionlistener for combo box
		connectionsComboBox.addActionListener(a -> {
			String selectedConnectionName = connectionsComboBox.getSelectedItem().toString();
			selectedConnection = selectedConnectionName;
			// Change connections in connection tab
			if(selectedConnectionName.equals("Overview")) {
				labelConnections.setText(createConnectionsString());
				labelConnections.setBounds(5, 33, 610, 250);
				connectionConfiguration.setVisible(false);
			}
			else {
				labelConnections.setText("Configuration Parameters");
				labelConnections.setBounds(5, 33, 610, 250);
				connectionConfiguration.setVisible(true);
			}
			// Set correct values for buttons and textFields
			sendingFrequencyButton.setText(getFieldText("sendingFrequencyButton"));
			responseTimeButton.setText(getFieldText("responseTimeButton"));
			sourceIPAddressTextfield.setText(getFieldText("sourceIPAddress"));
			outboundPortTextfield.setText(getFieldText("outboundPort"));
			inboundPortTextfield.setText(getFieldText("inboundPort"));
			switchConnectionsTab("normal");
		});
		
		if(device.getClass().getSimpleName().equals("SWaTDevice") && device.getName().startsWith("Switch") == false)
			connectionsComboBox.setVisible(true);
		else
			connectionsComboBox.setVisible(false);
			
		return connectionsComboBox;
	}
	
	/**
	 * Creates the descriptions for the elements.
	 */
	private void createElementDescriptions() {
		HashMap<String, String> descriptions = new HashMap<String, String>(); // device name, device description		
		descriptions.put("PLC 1", "<html>Programmable Logic Controller of stage one.</html>");
		descriptions.put("PLC 2", "<html>Programmable Logic Controller of stage two.</html>");
		descriptions.put("PLC 3", "<html>Programmable Logic Controller of stage three.</html>");
		descriptions.put("PLC 4", "<html>Programmable Logic Controller of stage four.</html>");
		descriptions.put("PLC 5", "<html>Programmable Logic Controller of stage five.</html>");
		descriptions.put("PLC 6", "<html>Programmable Logic Controller of stage six.</html>");
		descriptions.put("Switch 1", "<html>A switch which forms the center of the star network of PLCs and forwards packets to the second switch. Connects the PLCs with SCADA, Engineering Workstation, and Historian.</html>");
		descriptions.put("Switch 2", "<html>A switch which forwards packets from SCADA, Engineering Workstation, and Historian to the first switch. Connects them with the PLCs.</html>");
		descriptions.put("SCADA", "<html>The main server of the Supervisory Control and Data Acquisition system.</html>");
		descriptions.put("Engineering Workstation", "<html>Allows access to the SWaT testbed.</html>");
		descriptions.put("Historian", "<html>Archives the network traffic and physical data.</html>");
		descriptions.put("P-", "<html>Pump which is either on or off.</html>");
		descriptions.put("MV-", "<html>Motorized Valve which is either open or closed.</html>");
		descriptions.put("UV-", "<html>UV Dechlorinator which is either on or off.</html>");
		descriptions.put("0", "<html>Flow Indicator Transmitter which measures the water flow in m"+"\u00B3"+"/hr.</html>");
		descriptions.put("1", "<html>Level Indicator Transmitter which measures the water level in mm.</html>");
		descriptions.put("2", "<html>Analyser Indicator Transmitter which measures conductivity in "+"\u03BC"+"S/cm.</html>");
		descriptions.put("3", "<html>Analyser Indicator Transmitter which measures the pH value.</html>");
		descriptions.put("4", "<html>Analyser Indicator Transmitter which measures the oxidation reduction potential in mV.</html>");
		descriptions.put("5", "<html>Analyser Indicator Transmitter which measures the water hardness in mg/l of calcium carbonat.</html>");
		descriptions.put("6", "<html>Pressure Indicator Transmitter which measures the pressure in kPa.</html>");
		descriptions.put("7", "<html>Differential Pressure Indicator Transmitter which measures the differential pressure in kPa.</html>");
		elementDescriptions = descriptions;
	}
	
	/**
	 * Creates the status tab for actuators.
	 * @return The status tab for actuators.
	 */
	private JPanel createStatusTab() {
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		JLabel description = new JLabel("Status:");
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(5,5,0,30);
		statusPanel.add(description, gridBagConstraints); 
		
		JLabel info = new JLabel(" ");
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weighty = 0.1;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,0,0);
		statusPanel.add(info, gridBagConstraints); 
		
		if(device.getName().startsWith("MV-")) {
			actuatorStatusComboBox.removeAllItems();
			info.setText("<html>A motorized valve can be open (2), closed (1), or in transition between the two former statuses (0).</html>");			
			actuatorStatusComboBox.addItem(2);
			actuatorStatusComboBox.addItem(1);
			actuatorStatusComboBox.addItem(0);
		}
		else {
			if(device.getName().startsWith("P-")) {
				info.setText("<html>A pump can be either turned on (2) or turned off (1).</html>");
			}
			else
				info.setText("<html>The UV dechlorinator can be either turned on (2) or turned off (1).</html>");
			actuatorStatusComboBox.removeAllItems();
			actuatorStatusComboBox.addItem(2);
			actuatorStatusComboBox.addItem(1);
		}
		actuatorStatusComboBox.setSelectedItem(Integer.parseInt(((SWaTDevice) device).getDeviceData().get("status")));
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(5,5,0,5);
		statusPanel.add(actuatorStatusComboBox, gridBagConstraints); 
		
		JButton resetButton = new JButton("Reset to Standard");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String standardStatus = ((SWaTDevice) device).getDeviceDataStandard().get("status");
				LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
				deviceData.put("status", standardStatus);
				((SWaTDevice) device).setDeviceData(deviceData);
				actuatorStatusComboBox.setSelectedItem(Integer.parseInt(standardStatus));
			}
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		statusPanel.add(resetButton, gridBagConstraints); 
		
		JButton applyButtonStatus = new JButton("Apply");
		applyButtonStatus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(actuatorStatusComboBox.getItemCount() > 0) {
					LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
					deviceData.put("status", actuatorStatusComboBox.getSelectedItem().toString());
					((SWaTDevice) device).setDeviceData(deviceData);
				}
			}
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_END;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		statusPanel.add(applyButtonStatus, gridBagConstraints); 
		
		JLabel bottomBufferStatusTab = new JLabel(" "); // buffer space between elements
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.weighty = 0.9;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,0,0);
		statusPanel.add(bottomBufferStatusTab, gridBagConstraints); 
		
		return statusPanel;
	}
	
	/**
	 * Creates the status tab for sensors.
	 * @return The status tab for sensors.
	 */
	private JPanel createValueTab() {
		JPanel valuePanel = new JPanel();
		valuePanel.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		JLabel description = new JLabel("Value:");
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(5,5,0,30);
		valuePanel.add(description, gridBagConstraints); 
		
		JButton applyButtonStatus = new JButton("Apply");
		JTextField valueTextfield = new JTextField(((SWaTDevice) device).getDeviceData().get("value"));
		valueTextfield.setColumns(20);	
		valueTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void checkFormat() {
				if(valueTextfield.getText() == null) {
					valueTextfield.setBackground(Color.RED);
				}
				else {
					String text = valueTextfield.getText();
					if(Pattern.matches(regExStringFloatingPoint, text)) {
						valueTextfield.setBackground(Color.WHITE);
						applyButtonStatus.setEnabled(true);
					}
					else {
						valueTextfield.setBackground(Color.RED);
						applyButtonStatus.setEnabled(false);
					}
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {				
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkFormat();				
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkFormat();				
			}			
		});
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(5,5,0,5);
		valuePanel.add(valueTextfield, gridBagConstraints); 
		
		JLabel centerBufferValueTab = new JLabel(" "); // buffer space between elements
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weighty = 0.1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,0,0);
		valuePanel.add(centerBufferValueTab, gridBagConstraints); 
		
		JButton resetButton = new JButton("Reset to Standard");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String standardValue = ((SWaTDevice) device).getDeviceDataStandard().get("value");
				LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
				deviceData.put("value", standardValue);
				((SWaTDevice) device).setDeviceData(deviceData);
				valueTextfield.setText(standardValue);
				valueTextfield.setBackground(Color.WHITE);
			}
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		valuePanel.add(resetButton, gridBagConstraints); 		
	
		applyButtonStatus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					LinkedHashMap<String, String> deviceData = ((SWaTDevice) device).getDeviceData();
					deviceData.put("value", valueTextfield.getText());
					((SWaTDevice) device).setDeviceData(deviceData);
			}
		});
		gridBagConstraints.fill = GridBagConstraints.NONE;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.anchor = GridBagConstraints.LINE_END;
		gridBagConstraints.insets = new Insets(0,5,0,5);
		valuePanel.add(applyButtonStatus, gridBagConstraints); 
		
		JLabel bottomBufferValueTab = new JLabel(" "); // buffer space between elements
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.weighty = 0.9;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.LINE_START;
		gridBagConstraints.insets = new Insets(0,0,0,0);
		valuePanel.add(bottomBufferValueTab, gridBagConstraints); 
		return valuePanel;
	}	
	
	/**
	 * Returns the element description for the given device.
	 * @param dev The given SWaT device.
	 * @return The device's description
	 */
	private String getElementDescription(SmartDevice dev) {
		createElementDescriptions();
		String description = "";
		String deviceClass = dev.getClass().getSimpleName();
		switch(deviceClass) {
			case "SWaTDevice":
				description = elementDescriptions.get(dev.getName());				
				break;
			default:
				String deviceName = dev.getName();
				if(deviceName.startsWith("P-")) description = elementDescriptions.get("P-");
				else if(deviceName.startsWith("MV-")) description = elementDescriptions.get("MV-");
				else if(deviceName.startsWith("UV-")) description = elementDescriptions.get("UV-");
				else if(device.getClass().getSimpleName().equals("SWaTSensor"))
					description = elementDescriptions.get(String.valueOf(((SWaTSensor) device).getType()));
				else description = "Connections";
		}		
		return description;
	}
	
	/**
	 * Switches the content of the connections tab to destination.
	 * @param destination The content to switch to. "normal", "sendingDistribution", or "responseDistribution".
	 */
	private void switchConnectionsTab(String destination) {
		connectionsTab.remove(connectionsTab.getComponentCount()-1);

		switch(destination) {
			case "normal":
				connectionsTab.add(connectionConfiguration);
				labelConnections.setVisible(true);
				break;
			case "sendingDistribution":
				createConnectionDistributionConfiguration("Sending");
				connectionsTab.add(connectionDistributionConfiguration);
				labelConnections.setVisible(false);
				break;
			case "responseDistribution":				
				createConnectionDistributionConfiguration("Response");
				connectionsTab.add(connectionDistributionConfiguration);
				labelConnections.setVisible(false);
				break;				
			default:
		} 
		repaint();
	}
	
	/**
	 * Gets the text for the field according to the current device.
	 * @param field A Textfield or button.
	 * @return The text for the field according to the current device.
	 */
	private String getFieldText(String field) {
		if(field.equals("sourceIPAddress") || field.equals("outboundPort") || field.equals("inboundPort")) {
			return ((SWaTDevice) device).getDeviceData().get(selectedConnection+"_"+field);
		}
		String fieldName = field;
		if(field.contains("Button")) {
			fieldName = field.replaceAll("FrequencyButton", "Current");
			fieldName = fieldName.replaceAll("TimeButton", "Current");
		}
		else {
			fieldName = field.replaceAll("Textfield", "");
			fieldName = fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
			fieldName = distributionType+fieldName;
		}
		return ((SWaTDevice) device).getDeviceData().get(selectedConnection+"_"+fieldName);
	}
	
	/**
	 * Adjusts the bounds of the labelConnections label to place it at the top of the pane when no ComboBox is needed.
	 */
	private void adjustLabelConnectionsBounds() {
		String deviceName = device.getName();
		String deviceClass = device.getClass().getSimpleName();
		if(deviceClass.equals("SWaTDevice") && deviceName.startsWith("Switch") == false)
			labelConnections.setBounds(5, 33, 610, 250);
		else
			labelConnections.setBounds(5, 3, 610, 250);
	}
}