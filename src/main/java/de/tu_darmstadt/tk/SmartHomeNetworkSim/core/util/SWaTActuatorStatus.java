package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

/**
 * Constants for the status of actuators in SWaT.
 * 
 * @author Fabian Kaiser
 */
public final class SWaTActuatorStatus {

	/**
	 * Actuator is turned off.
	 */
	public static final int off = 1;
	
	/**
	 * Actuator is turned on.
	 */
	public static final int on = 2;	
}
