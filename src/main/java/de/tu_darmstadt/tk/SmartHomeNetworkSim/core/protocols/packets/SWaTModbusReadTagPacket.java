package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols.packets;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Packet;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Port;

/**
 * Modbus Read Tag packets for the SWaT scenario.
 * @author Fabian Kaiser
 */
public class SWaTModbusReadTagPacket extends Packet{	
	/**
	 * The description of the type of read tag service. Either "Read Tag Service" or "Read Tag Service - Response".
	 */
	private int modbusFunctionDescription;
	
	/**
	 * The source IP address.
	 */
	private String sourceIPAddress;
	
	/**
	 * The port number of the source.
	 */
	private String sourcePortNumber;
	
	/**
	 * The destination IP address.
	 */
	private String destinationIPAddress;
	
	/**
	 * The port number of the destination.
	 */
	private String destinationPortNumber;
	
	/**
	 * The date the packet was sent. Format example: 21Dec2015.
	 */
	private String date;
	
	/**
	 * The time the packet was sent. Format example: 22:17:56. 
	 */
	private String time;
	
	/**
	 * The transaction ID of this packet. Temporarily unique for request-response pair.
	 */
	private int modbusTransactionID;
	
	/**
	 * The payload of the packet.
	 */
	private String modbusValue = "";
	
	/**
	 * The SCADA Tag. Format example: HMI_LIT101. Is always HMI_X.
	 */
	private String SCADATag;
	
	/**
	 * The IP address of the origin server.
	 */
	private String originServerIPAddress = "192.168.1.48";
	
	/**
	 * The message type. 
	 */
	private String messageType = "log";
	
	/**
	 * The name of the network interface.
	 */
	private String interfaceName = "eth1";
	
	/**
	 * The interface direction. "outbound" or "inbound", usually the former.
	 */
	private String interfaceDirection = "outbound";
	
	/**
	 * The lower layer Ethernet/IP and Modbus are built on.
	 */
	private String lowerLayerProtocol = "tcp";
	
	/**
	 * The name of the network application.
	 */
	private String applicationName = "CIP_read_tag_service";
	
	/**
	 * Proxy IP address of the source. Usually the same as the source IP address.
	 */
	private String proxySourceIPAddress;
	
	/**
	 * The Modbus function code. 0x4c or 76 for the read tag service
	 */
	private int modbusFunctionCode = 76;
	
	/**
	 * The tag of the packet. Usually 0.
	 */
	private String tag = "0";
	
	/**
	 * Constructor.
	 * @param date The date the packet was sent. Format example: 21Dec2015.
	 * @param time The time the packet was sent. Format example: 22:17:56. 
	 * @param sourceIPAddress The source IP address in the usual format.
	 * @param destinationIPAddress The destination IP address in the usual format.	 
	 * @param modbusFunctionDescription The description of the type of read tag service. Either "Read Tag Service" 
	 * 			or "Read Tag Service - Response".
	 * @param modbusTransactionID The transaction ID of this packet. Temporarily unique for request-response pair.
	 * @param SCADATag The SCADA Tag. Format example: HMI_LIT101. Is always HMI_X.
	 * @param modbusValue The payload of the packet.
	 * @param destinationPortNumber The port number of the destination. 0 <= destinationPortNumber <= 65355.
	 * @param sourcePortNumber The port number of the source. 0 <= sourcePortNumber <= 65355.	 
	 * 
	 * @param timestamp The timestamp of the packet.
	 * @param source The source port object.
	 * @param destination The destination port object.
	 */
	public SWaTModbusReadTagPacket(
			String date, String time, String sourceIPAddress, String destinationIPAddress, int modbusFunctionDescription,
			int modbusTransactionID, String SCADATag, String modbusValue, String destinationPortNumber, String sourcePortNumber,
			long timestamp, Port source, Port destination) {
		super(timestamp, source, destination);
		
		this.date = date;
		this.time = time;
		this.sourceIPAddress = sourceIPAddress;
		this.destinationIPAddress = destinationIPAddress;		
		this.proxySourceIPAddress = sourceIPAddress;
		this.modbusFunctionDescription = modbusFunctionDescription;		
		this.modbusTransactionID = modbusTransactionID;
		this.SCADATag = SCADATag;
		this.modbusValue = modbusValue;
		this.destinationPortNumber = destinationPortNumber;		
		this.sourcePortNumber = sourcePortNumber;		
	}	

	@Override
	public byte[] dumpBytes() {
		return getSVCRepresentation().getBytes();
	}

	@Override
	public String getTextualRepresentation() {
		return getSVCRepresentation();
	}

	/**
	 * Returns the SVC representation of this packet in the style of dataset A1.
	 * @return The SVC representation of this packet in the style of dataset A1.
	 */
	public String getSVCRepresentation() {
		String svcTextRepresentation = date+","+time+","+originServerIPAddress+","+messageType+","+interfaceName+","
				+interfaceDirection+","+sourceIPAddress+","+destinationIPAddress+","+lowerLayerProtocol+","+applicationName+","
				+proxySourceIPAddress+","+modbusFunctionCode+","+modbusFunctionDescription+","+modbusTransactionID+","+SCADATag+","
				+modbusValue+","+destinationPortNumber+","+sourcePortNumber+","+tag;
		return svcTextRepresentation;
	}
	
	@Override
	public String getPayload() {
		return modbusValue;
	}

	@Override
	public String getProtocolName() {
		return "Simplified Modbus";
	}
}
