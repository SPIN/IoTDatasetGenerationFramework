package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.scheduler;

import java.util.Comparator;

/**
 * Comparator used for scheduling different events in the global event queue
 *
 * @author Andreas T. Meyer-Berg
 */
public class ScheduleComparator implements Comparator<Schedulable> {

	
	@Override
	public int compare(Schedulable o1, Schedulable o2) {
		int res = Long.compare(o1.getEventTime(), o2.getEventTime());
		if(res==0)
			//Advanced handling ? TODO: Maybe microtime ? Some random alternation or event priority
			return Integer.compare(o1.hashCode(), o2.hashCode());

		return res;
	}

}
