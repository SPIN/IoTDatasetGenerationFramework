package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

/**
 * Interface for float sensors
 *
 * @author Andreas T. Meyer-Berg
 */
public interface FloatSensor {

	// Information Name
	/**
	 * Name of the sensor reading
	 * @param name name of the sensed information
	 */
	public void setFSinfoName(String name);
	
	/**
	 * Return the sensor reading name
	 * @return name of the sensed information
	 */
	public String getFSinfoName();

	/**
	 * Get max sensor value
	 * @return max value
	 */
	public float getFSmax();
	
	/**
	 * Set max sensor value
	 * @param max new maximum value
	 */
	public void setFSmax(float max);

	/**
	 * Get min sensor value
	 * @return minimum value
	 */
	public float getFSmin();
	
	/**
	 * Set min sensor value
	 * @param min new minimum value
	 */
	public void setFSmin(float min);
	
	/**
	 * Return the current value of the sensor
	 * @return value
	 */
	public float getFSval();
	
	/**
	 * Set the current value of the sensor
	 * @param val new value
	 */
	public void setFSval(float val);

}
