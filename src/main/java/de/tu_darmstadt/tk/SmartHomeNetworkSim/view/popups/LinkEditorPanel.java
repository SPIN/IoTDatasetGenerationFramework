package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Dimension;
import java.awt.Window;

import javax.swing.JFrame;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.simpleImplementation.SimpleLink;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class LinkEditorPanel extends JScrollPane{
	
	JPanel content;
	
	private Window frame;
	
	private SmartDevice device;
	
	private Controller controller;
	
	private String deviceNames;
	public LinkEditorPanel(SmartDevice deviceWithLinks, Controller control) {
		this.device = deviceWithLinks;
		this.controller = control;
		this.setPreferredSize(new Dimension(630, 400));
		
		content = new JPanel();
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		content.setPreferredSize(new Dimension(630, 80+20*device.getLinks().size()));
		this.setViewportView(content);
		content.setLayout(null);
		
		JLabel lblDescription = new JLabel("Links of Device: "+device.getName());
		lblDescription.setToolTipText(device.getName());
		lblDescription.setBounds(10, 10, 268, 16);
		content.add(lblDescription);

		JLabel lblAddLink = new JLabel("Add Link:");
		lblAddLink.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAddLink.setBounds(12, 42, 138, 16);
		content.add(lblAddLink);
		
		JButton btnAddLink= new JButton("Add Link");
		btnAddLink.setBounds(162, 38, 116, 25);
		content.add(btnAddLink);
		btnAddLink.addActionListener(a->System.out.println("WARNING: Not implemented yet"));
		
		//int i = 0;
		int currentHeight = 70;
		for(Link l:device.getLinks()){
			
			JLabel lblLinkA = new JLabel(l.getName()+":");
			lblLinkA.setToolTipText(l.getName());
			lblLinkA.setHorizontalAlignment(SwingConstants.RIGHT);
			lblLinkA.setBounds(10, currentHeight, 150, 18);
			content.add(lblLinkA);
			
			JButton btnEditLink = new JButton("Edit");
			btnEditLink.setBounds(160, currentHeight, 80, 18);
			content.add(btnEditLink);
			btnEditLink.addActionListener(a -> new LinkCreationDialog(l, controller, this));
			
			deviceNames = "Devices: ";
			if(l.getDevices().isEmpty())
				deviceNames += "empty  ";
			else
				l.getDevices().forEach(s->deviceNames+=s.getName()+", ");
			JLabel lblLinkADevices = new JLabel(deviceNames.substring(0, deviceNames.length()-2));
			lblLinkADevices.setToolTipText(deviceNames);
			lblLinkADevices.setBounds(240, currentHeight, 350, 18);
			content.add(lblLinkADevices);
			
			//i++;
			currentHeight+=20;
		}
	}

	
	 /**
     * Test the Panel
     */
    private static void testGUI() {
        JFrame frame = new JFrame("Links Panel");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        LinkEditorPanel panel;
        
        SmartDevice devicesOfLink = new SmartDevice("Test");
        for(int i = 0; i<30; i++)
        	devicesOfLink.addLink(new SimpleLink("Link"+i));
        panel = new LinkEditorPanel(devicesOfLink, new Controller(new Model()));

        panel.setFrame(frame);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
    }
    

 
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                testGUI();
            }
        });
    }

	/**
	 * @return the frame
	 */
	public Window getFrame() {
		return frame;
	}

	/**
	 * @param frame the frame to set
	 */
	public void setFrame(Window frame) {
		this.frame = frame;
	}
}