package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import javax.swing.JPanel;
import org.apache.commons.math3.random.RandomGenerator;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.ProbabilityDistributionHandler;

/**
 * Implementation of kernel density estimation with a Gaussian basis function as kernel 
 * that uses the values from the datasets.
 * @author Fabian Kaiser
 */
public class SWaTDatasetDistributionHandler implements ProbabilityDistributionHandler {

	/**
	 * The linked list containing the values used for the Kernel density estimation. 
	 */
	private LinkedList<Double> data;
	
	/**
	 * The linked list containing the data points of the dataset. 
	 */
	private LinkedList<Double> dataPoints;
	
	/**
	 * The linked list with the cumulative sum (CUMSUM, not CUSUM!) of weights of the data points at the same indexes.
	 * E.g., CUMSUM([1, 2, 3, 4]) = [1, 3, 6, 10].
	 */
	private LinkedList<Double> CUMSUMweights;	
	
	/**
	 * The last and largest CUMSUM value. E.g., 10 from the example above.
	 */
	private double lastCUMSUMweight;
	
	/**
	 * The bandwidth of the Kernel density estimation. 
	 */
	private double bandwidth = 0.00004;
	
	/**
	 * Constructor, if weights are used.
	 * @param dataPoints A linked list containing the data points of the dataset.
	 * @param weights A linked list with the cumulative sum (CUMSUM, not CUSUM!) of weights of the data points at the same indexes. 
	 * E.g., CUMSUM([1, 2, 3, 4]) = [1, 3, 6, 10].
	 * @param lastCUMSUMweight The last and largest CUMSUM value. E.g., 10 from the example above.
	 */
	public SWaTDatasetDistributionHandler(LinkedList<Double> dataPoints, LinkedList<Double> CUMSUMweights, double lastCUMSUMweight) {
		this.dataPoints = dataPoints;
		this.CUMSUMweights = CUMSUMweights;
		this.lastCUMSUMweight = lastCUMSUMweight;
	}
	
	/**
	 * Constructor if no weights are used.
	 * @param A linked list containing the data points of the dataset.
	 */
	@Deprecated
	public SWaTDatasetDistributionHandler(LinkedList<Double> data) {
		this.data = data;
	}	
	
	/**
	 * Method has no effect. The standard RNG is always used.
	 */
	@Override
	public void setRandomGenerator(RandomGenerator rng) {
	}

	/**
	 * Kernel density estimation with a Gaussian basis function as kernel.
	 * </br>
	 * The implementation is a simplification for this use case based on:
	 * </br>
	 * https://github.com/scikit-learn/scikit-learn/blob/95119c13a/sklearn/neighbors/_kde.py#L268
	 * </br>
	 * https://scikit-learn.org/stable/modules/density.html
	 * @param noNegatives Whether the return values should only be >= 0;
	 * @return One sample from the kernel density estimation.
	 */	
	private double getKernelDensityEstimationSampleWithWeights(boolean noNegatives) { // Replaced the array with a list to improve performance
		// Random number generator
		Random rng = new Random();
		// Uniformly distributed random value in [0,1)
		double u = rng.nextDouble();		
		// Index for the data array to the random value
		int i = Math.abs(Collections.binarySearch(CUMSUMweights, u*lastCUMSUMweight))-1;
		// Output is the array element with respect to the index plus the standard deviation of a normal (Gaussian) distribution
		// Equivalent to normal distribution with mean=data[i] and standardDeviation=bandwidth
		double output = 0.0;
		if(noNegatives) {
			while(true) {
				output = rng.nextGaussian()*bandwidth+dataPoints.get(i);
				if(output >= 0.0)
					break;
			}
		}
		else
			output = rng.nextGaussian()*bandwidth+dataPoints.get(i);
		return output;		
	}
	
	/**
	 * Kernel density estimation with a Gaussian basis function as kernel.
	 * </br>
	 * Deprecated because very bad performance on large datasets.
	 * </br>
	 * The implementation is a simplification for this use case based on:
	 * </br>
	 * https://github.com/scikit-learn/scikit-learn/blob/95119c13a/sklearn/neighbors/_kde.py#L263
	 * </br>
	 * https://scikit-learn.org/stable/modules/density.html
	 * 
	 * @return One sample from the kernel density estimation.
	 */
	@SuppressWarnings("unused")
	@Deprecated	
	private double getKernelDensityEstimationSampleWithoutWeights() { // Replaced the array with a list to improve performance
		// Random number generator
		Random rng = new Random();
		// Uniformly distributed random value in [0,1)
		double u = rng.nextDouble();
		// Index for the data array according to the random value
		int i = (int) Math.floor(u*data.size()); 
		// Output is the array element with respect to the index plus the standard deviation of a normal (Gaussian) distribution
		// Equivalent to normal distribution with mean=data[i] and standardDeviation=bandwidth
		double output = rng.nextGaussian()*bandwidth+data.get(i);		
		
		return output;
	}
	
	@Override
	public long sampleNextValue() {		
		return Math.round(getKernelDensityEstimationSampleWithWeights(false));
	}
	
	/**
	 * Return the exact sample value of the distribution.
	 * @return The exact sample value of the distribution.
	 */
	public double sampleNextValueExactly() {
		return getKernelDensityEstimationSampleWithWeights(true);
	}

	/**
	 * Return the exact sample value of the distribution, even if it is negative.
	 * @return The exact sample value of the distribution.
	 */
	public double sampleNextValueExactlyAlsoNegative() {
		return getKernelDensityEstimationSampleWithWeights(false);
	}
	
	/**
	 * There is no configuration panel needed.
	 * @return null, since there are no configuration options.
	 */
	@Override
	public JPanel getConfigurationPanel() {
		return null;
	}

	@Override
	public String getSimpleDescription() {
		return "Distribution according to the selected SWaT dataset";
	}

	/**
	 * Sets the bandwidth. Standard is 0.4.
	 * @param bandwidth The bandwidth of the Kernel density estimation.
	 */
	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}
}