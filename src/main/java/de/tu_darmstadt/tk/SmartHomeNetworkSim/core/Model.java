package de.tu_darmstadt.tk.SmartHomeNetworkSim.core;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * Model of the smart home, which contains all the important parts of the simulation, like {@link SmartDevice} and their {@link Link}.
 *
 * @author Andreas T. Meyer-Berg
 */
public class Model extends Observable{

	private List<SmartDevice> devices;
	private List<Link> connectionNetworks;
	private List<Connection> connections;
	
	/**
	 * Width of the smart home model, 0 <= device.x < width
	 */
	private int width;
	
	/**
	 * Height of the smart home model, 0 <= device.y < height
	 */
	private int height;
	
	/**
	 * Depth of the smart home model, 0 <= device.y < depth
	 */
	private int depth;
	
	/**
	 * Simulation Manager which allows simulation of this model
	 */
	private SimulationManager sim;
	
	/**
	 * Configurations of the Program
	 */
	private ConfigurationManager config;
	
	/**
	 * Initializes the Model, with 3 default devices for testing purposes
	 */
	public Model() {
		setSim(new SimulationManager(this));
		setWidth(640);
		setHeight(480);
		setDepth(480);
		
		config = new ConfigurationManager();
		
		devices = new LinkedList<SmartDevice>();
		connectionNetworks = new LinkedList<Link>();
		connections = new LinkedList<Connection>();
	}

	/**
	 * @return the connectionNetworks
	 */
	public List<Link> getConnectionNetworks() {
		return connectionNetworks;
	}

	/**
	 * Adds network connection
	 * @param connectionNetwork the connectionNetwork to add
	 */
	public void addConnectionNetwork(Link connectionNetwork) {
		this.connectionNetworks.add(connectionNetwork);
	}

	/**
	 * @return the devices
	 */
	public List<SmartDevice> getDevices() {
		return devices;
	}

	/**
	 * @param device the smartDevice to add to the smart home
	 */
	public void addDevices(SmartDevice device) {
		this.devices.add(device);
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the depth
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * @param depth the depth to set
	 */
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	/**
	 * @return the connections
	 */
	public List<Connection> getConnections() {
		return connections;
	}

	/**
	 * @param connection the connection to add
	 */
	public void addConnection(Connection connection) {
		this.connections.add(connection);
	}	
	
	public void setChanged(){
		super.setChanged();
	}

	/**
	 * @return the sim
	 */
	public SimulationManager getSim() {
		return sim;
	}

	/**
	 * @param sim the sim to set
	 */
	public void setSim(SimulationManager sim) {
		this.sim = sim;
	}

	/**
	 * @return the configurations
	 */
	public ConfigurationManager getConfigurator() {
		return config;
	}
}
