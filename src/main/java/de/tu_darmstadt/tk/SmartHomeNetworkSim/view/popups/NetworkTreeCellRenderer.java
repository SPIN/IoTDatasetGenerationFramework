package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Connection;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.NetworkTreeNodeStatus;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.Pair;

/**
 * TreeCellRenderer for the Network tree, which visualizes the different network parts
 *
 * @author Andreas T. Meyer-Berg
 */
public class NetworkTreeCellRenderer implements TreeCellRenderer {
	/**
	 * Label which describes the node
	 */
	private JCheckBox label;
	/**
	 * Controller to manipulate / get infos from the model
	 */
	private Controller controller;
	/**
	 * Creates a new NetworkTreeCellRenderer
	 * @param controller Controller for manipulation
	 */
	public NetworkTreeCellRenderer(Controller controller) {
		this.controller = controller;
		label = new JCheckBox();
		label.setBackground(Color.WHITE);
	}
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		/**
		 * Object which should be rendered
		 */
		Object o = ((DefaultMutableTreeNode) value).getUserObject();
		/**
		 * Status of the object (hidden, expanded etc.)
		 */
		NetworkTreeNodeStatus status = controller.getSettingsController().getNetworkTreeSettingsController().getStatusOfObject(o);
		label.setSelected(status.isVisible());
		
		if (o instanceof SmartDevice) {
			/**
			 * Device to be rendered
			 */
			SmartDevice device = (SmartDevice) o;
			boolean hidden = !status.isVisible();
			//label.setIcon(new ImageIcon(Utility.loadFile("/images/smartHome_icon.png").getScaledInstance(12, 12, 0)));

			label.setText("<html><font color=\""+(hidden?"red":"black")+"\">"+device.getName()+"</font></html>");
		} else if (o instanceof Connection){
			/**
			 * Connection to be rendered
			 */
			Connection connection = (Connection) o;
			//label.setIcon(new ImageIcon(Utility.loadFile("/images/smartHome_icon.png").getScaledInstance(12, 12, 0)));
			
			label.setText(connection.getName());				
		}else if(o instanceof Link){
			/**
			 * Link to be rendered
			 */
			Link link = (Link) o;
			//label.setIcon(new ImageIcon(Utility.loadFile("/images/smartHome_icon.png").getScaledInstance(12, 12, 0)));
			/**
			 * Index & Color of the link
			 */
			Pair<Integer,Color> pair = controller.getSettingsController().getLinkColors().getColorOfLink(link);
			int r = pair.getRight().getRed();
			int g = pair.getRight().getGreen();
			int b = pair.getRight().getBlue();
			label.setText("<html><font color=\"rgb("+r+","+g+","+b+")\">"+link.getName()+"</font></html>");
			
		}
		else {
			/**
			 * Default use toString
			 */
			//label.setIcon(null);
			label.setText("" + value);
		}
		return label;
	}
}