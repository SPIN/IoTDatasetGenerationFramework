package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.math3.random.RandomGenerator;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.ProbabilityDistributionHandler;

/**
 * Example class representing a Distribution, with only one constant value
 *
 * @author Andreas T. Meyer-Berg
 */
public class ConstantValueDistributionHandler implements
		ProbabilityDistributionHandler {

	/**
	 * Fixed value this distribution returns
	 */
	private long fixedValue;
	
	/**
	 * Creates a new distribution which returns the given value
	 * @param value value to be returned
	 */
	public ConstantValueDistributionHandler(long value) {
		fixedValue = value;
	}

	/**
	 * Creates a default distribution, which returns 50.
	 */
	public ConstantValueDistributionHandler() {
		fixedValue = 50;
	}
	
	@Override
	public void setRandomGenerator(RandomGenerator rng) {
		// Fixed Value requires no randomness
	}

	@Override
	public long sampleNextValue() {
		return fixedValue;
	}

	/**
	 * Sets the sample value to the given one
	 * @param value new fixed value
	 */
	public void setValue(long value){
		this.fixedValue = value;
	}
	@Override
	public JPanel getConfigurationPanel() {
		/**
		 * JPanel which allows configuration of this Distribution
		 */
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(90, 40));
		panel.setLayout(null);
		
		/**
		 * Label 
		 */
		JLabel label = new JLabel("Value: ");
		panel.add(label);
		label.setLocation(20, 20);
		label.setSize(50,20);
		label.setMinimumSize(new Dimension(50, 20));

		/**
		 * Textfield for changing the value
		 */
		JTextField valueConfig = new JTextField(""+fixedValue);
		panel.add(valueConfig);
		valueConfig.setLocation(80, 20);
		valueConfig.setMinimumSize(new Dimension(32, 20));
		valueConfig.setSize(32, 20);
		valueConfig.addActionListener(a->{
			try {
				/**
				 * Update the value
				 */
				fixedValue = Long.parseLong(valueConfig.getText());
				valueConfig.setBackground(Color.WHITE);
			} catch (Exception e) {
				valueConfig.setBackground(Color.RED);
			}
		});
		return panel;
	}

	public static void main(String[] args) {
		ConstantValueDistributionHandler c = new ConstantValueDistributionHandler(12);
		JFrame test = new JFrame("test");
		test.setSize(400, 400);
		test.add(c.getConfigurationPanel());
		test.setEnabled(true);
		test.setVisible(true);
	}

	@Override
	public String getSimpleDescription() {
		return "Constant Value";
	}
}
