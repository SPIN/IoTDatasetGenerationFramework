package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.scheduler;

/**
 * Interface for events or objects, which can be scheduled during simulation of the network or world model.
 *
 * @author Andreas T. Meyer-Berg
 */
public interface Schedulable {
	
	/**
	 * Returns the timestep, in which the event begins
	 * @return timestep, in which the event begins
	 */
	public long getEventTime();
	
	/**
	 * Simulates the given Event, might update states of model parts and schedule further events.
	 * @param time minimum timestamp
	 */
	public void simulateEvent(long time);
}
