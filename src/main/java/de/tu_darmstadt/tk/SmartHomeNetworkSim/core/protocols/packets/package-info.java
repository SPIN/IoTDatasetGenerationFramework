/**
 * Package contains Packet implementations which are sent by protocols of the parent package protocols. 
 *
 * @author Andreas T. Meyer-Berg
 */
package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.protocols.packets;