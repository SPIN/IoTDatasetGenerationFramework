package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTDeviceTypes;

/**
 * Models the actuator devices of SWaT.
 * 
 * @author Fabian Kaiser
 */
public class SWaTActuator extends SWaTDevice{
	/**
	 * The type of the actuator.
	 */
	private int type;
	
	/**
	 * The status of the actuator.
	 */
	private int status;
	
	/**
	 * Constructor for SWaTActuator.
	 * @param deviceType The type of device.
	 * @param type The type of the actuator.
	 * @param name The name of the actuator.
	 */
	public SWaTActuator(int type, String name) {
		super(name, SWaTDeviceTypes.Actuator, 1);		
		this.type = type;
		if(this.getDeviceData().containsKey("status") == false)
			setStandardStatus();
	}
	
	/**
	 * Sets the type of the actuator.
	 * @param type The type of the actuator.
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	 * Gets the type of the actuator.
	 */
	public int getType() {
		return this.type;
	}
	
	/**
	 * Sets the status of the actuator.
	 * @param status The status of the actuator.
	 */
	public void setStatus(int statusToSet) {
		status = statusToSet;
		this.appendToDeviceData("status", String.valueOf(statusToSet));
	}
	
	/**
	 * Gets the status of the actuator.
	 */
	public int getStatus() {
		return this.status;
	}
	
	/**
	 * Sets the standard status according to the device name.
	 */
	private void setStandardStatus() {
		status = 1;
		this.appendToDeviceData("status", "1");
		this.appendToDeviceDataStandard("status", "1");
	}
}
