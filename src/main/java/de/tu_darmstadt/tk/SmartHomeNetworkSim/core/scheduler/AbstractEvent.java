package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.scheduler;

/**
 * Abstract Event can be used to schedule changes to the model. For example, updates of devices.
 *
 * @author Andreas T. Meyer-Berg
 */
public abstract class AbstractEvent implements Schedulable {

	/**
	 * Timestep the event takes place
	 */
	private long timestep;
	
	/**
	 * Creates an Event at the given timestep, which can be scheduled.
	 * @param timestep timestep, the event should be scheduled
	 */
	public AbstractEvent(long timestep){
		this.timestep = timestep;
	}
	
	@Override
	public long getEventTime(){
		return timestep;
	}

	@Override
	public abstract void simulateEvent(long time);

}
