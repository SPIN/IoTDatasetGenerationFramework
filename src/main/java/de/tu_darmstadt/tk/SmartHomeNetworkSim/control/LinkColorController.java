package de.tu_darmstadt.tk.SmartHomeNetworkSim.control;

import java.awt.Color;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Model;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.configuration.LinkColorManager;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.Pair;

/**
 * Controller which enables access to the link colors
 *
 * @author Andreas T. Meyer-Berg
 */
public class LinkColorController {
	
	/**
	 * Link colors
	 */
	private LinkColorManager linkColorManager;
	/**
	 * Model of the program
	 */
	private Model model;
	/**
	 * Main Controller
	 */
	private Controller controller;
	
	/**
	 * Creates new LinkColorController which enables access to Colors of Links
	 * 
	 * @param model Model which should be manipulated
	 * @param controller controller which can be used
	 */
	public LinkColorController(Model model, Controller controller) {
		this.model = model;
		this.controller = controller;
		linkColorManager = this.model.getConfigurator().getLinkColors();
	}
	
	/**
	 * Returns the color and index of the given Link
	 * @param link link which should be looked up
	 * @return Pair of Index and Color of the Link
	 */
	public Pair<Integer,Color> getColorOfLink(Link link){
		Pair<Integer,Color> pair =  linkColorManager.getColorOfLink(link);
		if(pair == null){
			pair = new Pair<Integer, Color>(-1,linkColorManager.getNextLinkColor());
			linkColorManager.addLinkColor(link, pair);
		}
		return pair;
	}
	
	/**
	 * Changes Color of link to the new Color
	 * @param link link which should be updated
	 * @param color new color
	 */
	public void setColorOfLink(Link link, Color color){
		Pair<Integer, Color> elem = linkColorManager.getColorOfLink(link);
		if(elem!=null)
			elem.setRight(color);
		else
			linkColorManager.addLinkColor(link, new Pair<Integer,Color>(-1,color));
		controller.notifyObservers();
	}
	
	/**
	 * Removes the given Link and its color
	 * @param link link to be removed
	 */
	public void removeLink(Link link){
		linkColorManager.removeLink(link);;
	}
}
