package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

/**
 * Interface for collection of booleans, for example for actuators
 *
 * @author Andreas T. Meyer-Berg
 */
public interface BoolCollector {

	// Information Name
	/**
	 * Name of the information that should be collected
	 * @param name name of the collected information
	 */
	public void setBCinfoName(String name);
	
	/**
	 * Return the name of the collected information
	 * @return name of the collected information
	 */
	public String getBCinfoName();
	
	/**
	 * Return the current value of the collector
	 * @return value
	 */
	public boolean getBCval();
	
	/**
	 * Set the current value of the collector
	 * @param val new value
	 */
	public void setBCval(boolean val);

}
