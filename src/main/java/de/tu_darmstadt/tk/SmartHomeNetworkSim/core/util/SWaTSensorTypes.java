package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util;

/**
 * Constants for the types of sensors in SWaT.
 * 
 * @author Fabian Kaiser
 */
public final class SWaTSensorTypes {

	/**
	 * Flow indicator transmitter (FIT). Measures the water flow in m^3/hr.
	 */
	public static final int FIT = 0;
	
	/**
	 * Level indicator transmitter (LIT). Measures the water level in mm.
	 */
	public static final int LIT = 1;
	
	/**
	 * Analyser indicator transmitter (AIT) which measures conductivity in micro-S/cm.
	 */
	public static final int AITConductivity = 2;
	
	/**
	 * Analyser indicator transmitter (AIT) which measures the pH value.
	 */
	public static final int AITPH = 3;	
	
	/**
	 * Analyser indicator transmitter (AIT) which measures the oxidation reduction potential in mV.
	 */
	public static final int AITORP = 4;	
	
	/**
	 * Analyser indicator transmitter (AIT) which measures the water hardness in mg/l of calcium carbonate (CACO3).
	 */
	public static final int AITWaterHardness = 5;		
	
	/**
	 * Pressure indicator transmitter (PIT). Measures the pressure in kPa.
	 */
	public static final int PIT = 6;
	
	/**
	 * Differential pressure indicator transmitter (DPIT). Measures the differential pressure in kPa.
	 */
	public static final int DPIT = 7;	
}
