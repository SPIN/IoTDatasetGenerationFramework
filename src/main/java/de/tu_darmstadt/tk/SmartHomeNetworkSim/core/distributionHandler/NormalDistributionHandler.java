package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.distributionHandler;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.RandomGenerator;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.ProbabilityDistributionHandler;

/**
 * Example class representing a Normal Distribution
 *
 * @author Andreas T. Meyer-Berg
 */
public class NormalDistributionHandler implements
		ProbabilityDistributionHandler {

	/**
	 * Mean value
	 */
	private double mean;
	
	/**
	 * Standard Deviation
	 */
	private double sd;
	
	/**
	 * Normal Distribution
	 */
	private NormalDistribution dist;
	
	/**
	 * 
	 */
	private RandomGenerator gen = null;
	
	/**
	 * Creates a new distribution which returns the given value
	 * @param value value to be returned
	 */
	public NormalDistributionHandler(double mean, double sd) {
		this.mean = mean;
		this.sd = sd;
		dist = new NormalDistribution(mean, sd);
	}

	/**
	 * Creates a default distribution, which returns 50.
	 */
	public NormalDistributionHandler() {
		this.mean = 50.0;
		this.sd = 12.0;
		dist = new NormalDistribution(mean, sd);
	}
	
	@Override
	public void setRandomGenerator(RandomGenerator rng) {
		gen = rng;
		dist = new NormalDistribution(rng, mean, sd);
	}

	@Override
	public long sampleNextValue() {
		return (long)Math.round(dist.sample());
	}

	@Override
	public JPanel getConfigurationPanel() {
		/**
		 * JPanel which allows configuration of this Distribution
		 */
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(90, 80));
		panel.setLayout(null);
		
		/**
		 * Label 
		 */
		JLabel label = new JLabel("Mean: ");
		panel.add(label);
		label.setLocation(20, 20);
		label.setSize(50,20);
		label.setMinimumSize(new Dimension(50, 20));

		/**
		 * Textfield for changing the value
		 */
		JTextField valueConfig = new JTextField(""+mean);
		panel.add(valueConfig);
		valueConfig.setLocation(80, 20);
		valueConfig.setMinimumSize(new Dimension(70, 20));
		valueConfig.setSize(70, 20);
		valueConfig.addActionListener(a->{
			try {
				/**
				 * Update the value
				 */
				mean = Double.parseDouble(valueConfig.getText());
				initializeDistribution();
				valueConfig.setBackground(Color.WHITE);
			} catch (Exception e) {
				valueConfig.setBackground(Color.RED);
			}
		});
		
		
		
		/**
		 * Label 
		 */
		JLabel lbSD = new JLabel("SD: ");
		panel.add(lbSD);
		lbSD.setLocation(20, 50);
		lbSD.setSize(50,20);
		lbSD.setMinimumSize(new Dimension(50, 20));

		/**
		 * Textfield for changing the value
		 */
		JTextField tfSDvalue = new JTextField(""+sd);
		panel.add(tfSDvalue);
		tfSDvalue.setLocation(80, 50);
		tfSDvalue.setMinimumSize(new Dimension(70, 20));
		tfSDvalue.setSize(70, 20);
		tfSDvalue.addActionListener(a->{
			try {
				/**
				 * Update the value
				 */
				sd = Double.parseDouble(tfSDvalue.getText());
				tfSDvalue.setBackground(Color.WHITE);
			} catch (Exception e) {
				tfSDvalue.setBackground(Color.RED);
			}
		});
		return panel;
	}

	private void initializeDistribution() {
		if(gen==null)
			dist = new NormalDistribution(mean, sd);
		else
			dist = new NormalDistribution(gen, mean, sd);
			
	}

	public static void main(String[] args) {
		NormalDistributionHandler c = new NormalDistributionHandler(12.0, 5.0);
		JFrame test = new JFrame("test");
		test.setSize(400, 400);
		test.add(c.getConfigurationPanel());
		test.setEnabled(true);
		test.setVisible(true);
	}

	@Override
	public String getSimpleDescription() {
		return "Normal Distribution";
	}
}
