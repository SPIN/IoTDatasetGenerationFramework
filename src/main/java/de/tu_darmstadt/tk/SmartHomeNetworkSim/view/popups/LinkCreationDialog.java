package de.tu_darmstadt.tk.SmartHomeNetworkSim.view.popups;

import java.awt.Component;
import java.util.Collection;

import javax.swing.JDialog;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.control.Controller;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.Link;
import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.SmartDevice;

/**
 * Simple Window which contains the {@link LinkCreationPanel}
 *
 * @author Andreas T. Meyer-Berg
 */
@SuppressWarnings("serial")
public class LinkCreationDialog extends JDialog {
	/**
	 * Link Creation Panel which is displayed
	 */
	private LinkCreationPanel content;
	/**
	 * Parent Frame for relative location
	 */
	private Component parent;

	/**
	 * Create a Dialog for link editing
	 * @param link link to be edited
	 * @param controller controller to be used
	 * @param panel which is the parent
	 */
	public LinkCreationDialog(Link link, Controller controller, Component panel) {
		super();
		content = new LinkCreationPanel(link, controller, this);
		parent = panel;
		initialize();
	}
	
	/**
	 * Creates a Dialog for creation of a new connections
	 * @param devices devices to be linked
	 * @param controller controller to manipulate the model
	 * @param panel panel which is the parent
	 */
	public LinkCreationDialog(Collection<SmartDevice> devices, Controller controller, Component panel){
		super();
		content = new LinkCreationPanel(devices, controller, this);
		parent = panel;
		initialize();
	}
	
	/**
	 * Initialize the panel
	 */
	private void initialize() {
		this.setModal(true);
		this.setContentPane(content);
		this.pack();
		setLocationRelativeTo(parent);
		this.setVisible(true);
	}
}
