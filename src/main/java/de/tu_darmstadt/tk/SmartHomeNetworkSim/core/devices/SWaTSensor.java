package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

import de.tu_darmstadt.tk.SmartHomeNetworkSim.core.util.SWaTDeviceTypes;

/**
 * Models the sensor devices of SWaT.
 * 
 * @author Fabian Kaiser
 */
public class SWaTSensor extends SWaTDevice{
	/**
	 * The type of the sensor.
	 */
	private int type;	
	
	/**
	 * The reading of the sensor in the unit according to the sensor's type.
	 */
	public double reading;
	
	/**
	 * Constructor for SWaTSensor.
	 * @param type The type of the sensor.
	 * @param name The name of the sensor.
	 */
	public SWaTSensor(int type, String name) {
		super(name, SWaTDeviceTypes.Sensor, 1);
		this.type = type;
		if(this.getDeviceData().containsKey("value") == false)
			setStandardReading();
	}
	
	/**
	 * Sets the type of the sensor.
	 * @param type The type of the sensor.
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	 * Gets the type of the sensor.
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * Sets the reading of the sensor.
	 * @param reading The reading of the sensor.
	 */
	public void setReading(double reading) {
		this.reading = reading;
		this.appendToDeviceData("value", String.valueOf(reading));
	}
	
	/**
	 * Gets the reading of the sensor.
	 */
	public double getReading() {
		return this.reading;
	}
	
	/**
	 * Sets the standard value for the reading according to the device name.
	 */
	private void setStandardReading() {
		String deviceName = this.getName();
		switch(deviceName) {
		case "FIT-101":
			reading = 0.0;
			break;
		case "LIT-101":
			reading = 0.0;
			break;
		case "AIT-201":
			reading = 244.3284;
			break;
		case "AIT-202":
			reading = 8.19008;
			break;
		case "AIT-203":
			reading = 300.8459;
			break;
		case "FIT-201":
			reading = 0.0;
			break;
		case "LIT-301":
			reading = 0.0;
			break;
		case "DPIT-301":
			reading = 2.560983;
			break;
		case "FIT-301":
			reading = 0.0;
			break;
		case "AIT-401":
			reading = 0.0;
			break;
		case "AIT-402":
			reading = 171.3407;
			break;
		case "FIT-401":
			reading = 0.0;
			break;
		case "LIT-401":
			reading = 0.0;
			break;
		case "AIT-501":
			reading = 7.430659;
			break;
		case "AIT-502":
			reading = 177.1597;
			break;
		case "AIT-503":
			reading = 260.7665;
			break;
		case "AIT-504":
			reading = 123.3914;
			break;
		case "FIT-501":
			reading = 0.001538067;
			break;
		case "FIT-502":
			reading = 0.001408992;
			break;
		case "FIT-503":
			reading = 0.001664373;
			break;
		case "FIT-504":
			reading = 0.0;
			break;
		case "PIT-501":
			reading = 10.2698;
			break;
		case "PIT-502":
			reading = 0.0;
			break;
		case "PIT-503":
			reading = 4.6183;
			break;
		case "FIT-601":
			reading = 0.0002563035;
			break;
			default:
		reading = 999.99;
		}
		this.appendToDeviceData("value", String.valueOf(reading));
		this.appendToDeviceDataStandard("value", String.valueOf(reading));
	}

}
