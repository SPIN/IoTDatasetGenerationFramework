package de.tu_darmstadt.tk.SmartHomeNetworkSim.core.devices;

/**
 * Interface for collection of floats, for example for actuators
 *
 * @author Andreas T. Meyer-Berg
 */
public interface FloatCollector {

	// Information Name
	/**
	 * Name of the information that should be collected
	 * @param name name of the collected information
	 */
	public void setFCinfoName(String name);
	
	/**
	 * Return the name of the collected information
	 * @return name of the collected information
	 */
	public String getFCinfoName();
	
	/**
	 * Return the current value of the collector
	 * @return value
	 */
	public float getFCval();
	
	/**
	 * Set the current value of the collector
	 * @param val new value
	 */
	public void setFCval(float val);

}
