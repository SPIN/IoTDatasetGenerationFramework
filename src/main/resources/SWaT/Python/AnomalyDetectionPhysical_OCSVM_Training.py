# Anomaly detection for the physical data of SWaT by training a one-class support vector machine.
# The algorithm parameters are taken from Inoue et al.: https://ieeexplore.ieee.org/document/8215783
# @author Fabian Kaiser

################ Algorithm Parameters ####################
svm_nu = 0.004584962079820046
svm_kernel = "rbf"
svm_gamma = 0.0008181483058667633
sliding_window_size = 4
##########################################################
################## File Path Parameters ##################
# Conversion from XLSX to CSV: Open Office Calc -> File -> Save as -> Text CSV -> Save -> OK -> Remove first line or two first lines by hand so the column description is in row 1 -> Remove last (empty) line by hand
file_path_SWaT_Dataset_Normal = "C:/Users/user/master/Evaluation/physical/IoTDGF_SWaT Simulation_2021-08-02 14.05.18_Physical_500k.csv"
##########################################################


import pandas as pd
import numpy as np
from sklearn import svm
from joblib import dump
from datetime import datetime



print("Start time:\t"+str(datetime.now()))


# Read the files and create the sets
dataset_A1_normal_v1 = pd.read_csv(file_path_SWaT_Dataset_Normal, skipinitialspace = True)
dataset_length = len(dataset_A1_normal_v1)
training_set = dataset_A1_normal_v1.loc[0:int(dataset_length)].copy()
test_set = dataset_A1_normal_v1[int(dataset_length*0.7):].copy()


# No periodicy which is related to minute of hours, hour of day, and so on.
training_set.pop('Timestamp')
test_set.pop('Timestamp')


# Replace attack labels with numeric values
training_set = training_set.replace(to_replace =["Normal", "Attack"], value =["0","1"])
test_set = test_set.replace(to_replace =["Normal", "Attack"], value =["0","1"])


# Standardization
# Only continous values need to be changed since all discrete values are already numeric and in {0, 1, 2} or {1, 2}
training_set_continous = training_set[["FIT101", "LIT101", "AIT201", "AIT202", "AIT203", "FIT201", "DPIT301", "FIT301", "LIT301", "AIT401", "AIT402", "FIT401", "LIT401", "AIT501", "AIT502", "AIT503", "AIT504", "FIT501", "FIT502", "FIT503", "FIT504", "PIT501", "PIT502", "PIT503", "FIT601"]]

training_set_continous_mean = training_set_continous.mean()
training_set_continous_std = training_set_continous.std()
training_set_continous_std = training_set_continous_std.replace(to_replace=[0.0], value=[1.0]) # Prevent division by zero errors for constant values
training_set_continous = (training_set_continous - training_set_continous_mean) / training_set_continous_std

training_set = pd.DataFrame({
    "FIT101": training_set_continous["FIT101"],
    "LIT101": training_set_continous["LIT101"],
    "MV101": training_set["MV101"],
    "P101": training_set["P101"],
    "P102": training_set["P102"],
    "AIT201": training_set_continous["AIT201"],
    "AIT202": training_set_continous["AIT202"],
    "AIT203": training_set_continous["AIT203"],
    "FIT201": training_set_continous["FIT201"],
    "MV201": training_set["MV201"],
    "P201": training_set["P201"],
    "P202": training_set["P202"],
    "P203": training_set["P203"],
    "P204": training_set["P204"],
    "P205": training_set["P205"],
    "P206": training_set["P206"],
    "DPIT301": training_set_continous["DPIT301"],
    "FIT301": training_set_continous["FIT301"],
    "LIT301": training_set_continous["LIT301"],
    "MV301": training_set["MV301"],
    "MV302": training_set["MV302"],
    "MV303": training_set["MV303"],
    "MV304": training_set["MV304"],
    "P301": training_set["P301"],
    "P302": training_set["P302"],
    "AIT401": training_set_continous["AIT401"],
    "AIT402": training_set_continous["AIT402"],
    "FIT401": training_set_continous["FIT401"],
    "LIT401": training_set_continous["LIT401"],
    "P401": training_set["P401"],
    "P402": training_set["P402"],
    "P403": training_set["P403"],
    "P404": training_set["P404"],
    "UV401": training_set["UV401"],
    "AIT501": training_set_continous["AIT501"],
    "AIT502": training_set_continous["AIT502"],
    "AIT503": training_set_continous["AIT503"],
    "AIT504": training_set_continous["AIT504"],
    "FIT501": training_set_continous["FIT501"],
    "FIT502": training_set_continous["FIT502"],
    "FIT503": training_set_continous["FIT503"],
    "FIT504": training_set_continous["FIT504"],
    "P501": training_set["P501"],
    "P502": training_set["P502"],    
    "PIT501": training_set_continous["PIT501"],
    "PIT502": training_set_continous["PIT502"],
    "PIT503": training_set_continous["PIT503"],
    "FIT601": training_set_continous["FIT601"],
    "P601": training_set["P601"],
    "P602": training_set["P602"],
    "P603": training_set["P603"]
})

# Creates the sliding windows with the specified parameters.
# The default values are an example.
def create_sliding_windows(data = np.array([[1,2],[3,4],[5,6],[7,8],[9,10],[11,12]]), sliding_window_size = 3, elements_per_row = 2):
    number_of_sliding_windows = len(data)-sliding_window_size+1
    dimensions_elements_per_row = elements_per_row
    if elements_per_row == 52:
        dimensions_elements_per_row = 51
    array_sliding_windows = np.zeros(shape=(number_of_sliding_windows, sliding_window_size*dimensions_elements_per_row))
    array_sliding_windows_labels = np.zeros(shape=(number_of_sliding_windows))
    array_sliding_windows_index = 0
    for i in range(sliding_window_size, len(data)+1):
        array_current_sliding_window_index = 0
        array_current_sliding_window = np.zeros(shape=(sliding_window_size*dimensions_elements_per_row))
        current_sliding_window_label = 0
        for j in range(i-sliding_window_size, i):  
            for k in range(0, elements_per_row):
                if k == 51:
                    if data[j][k] == 1:
                        current_sliding_window_label = 1
                else:
                    array_current_sliding_window[array_current_sliding_window_index] = data[j][k]
                    array_current_sliding_window_index+=1

        array_sliding_windows[array_sliding_windows_index] = array_current_sliding_window
        array_sliding_windows_labels[array_sliding_windows_index] = current_sliding_window_label
        array_sliding_windows_index+=1 
    return array_sliding_windows, array_sliding_windows_labels

array_sliding_windows_training, _ = create_sliding_windows(training_set.to_numpy(),sliding_window_size,51)

# Train one-class support vector machine
classifier = svm.OneClassSVM(nu=svm_nu, kernel=svm_kernel, gamma=svm_gamma)
np.savetxt("fklnsdaklfasdnlf.csv", array_sliding_windows_training, delimiter=",")
classifier.fit(array_sliding_windows_training)


# Save model to disk (also see https://scikit-learn.org/stable/modules/model_persistence.html)
output_file_name = "AnomalyDetectionPhysical_TrainedClassifier_OCSVM_"+str(datetime.now())+" (nu="+str(svm_nu)+", kernel="+svm_kernel+", gamma="+str(svm_gamma)+", w="+str(sliding_window_size)+").joblib"
output_file_name = output_file_name.replace(':','.')
dump(classifier, output_file_name)


print("End time:\t"+str(datetime.now()))